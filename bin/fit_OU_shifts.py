#!/usr/bin/env python
import argparse

import dendropy
import pandas as pd

from AS_quant.models.phylo import ShiftsOUmodel
from AS_quant.utils import LogTrack, get_OU_global_params
from AS_quant.bin_utils import (add_counts_arguments, add_tree_argument,
                                add_design_arguments, add_log_bias_arguments,
                                add_stan_arguments, add_output_prefix_argument,
                                add_OU_by_element_arguments,
                                add_OU_horse_shoe_argument,
                                read_counts, read_log_bias)


def main():
    description = 'Assuming known parameters of an Ornstein-Uhlenbeck (OU)'
    description += 'infers the changes in the phenotypic optima along the '
    description += 'branches of the phylogenetic tree using a horse-shoe prior'
    description += 'to minimize the number of shifts'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    add_counts_arguments(input_group)
    add_tree_argument(input_group)
    add_design_arguments(input_group)
    add_log_bias_arguments(parser)
    model_group = add_OU_by_element_arguments(parser)
    add_OU_horse_shoe_argument(model_group)
    add_stan_arguments(parser)
    add_output_prefix_argument(parser)

    # Parse arguments
    parsed_args = parser.parse_args()
    inclusion_fpath = parsed_args.inclusion
    total_fpath = parsed_args.total
    design_fpath = parsed_args.design
    phylogeny_fpath = parsed_args.phylogenetic_tree
    samples_col = parsed_args.samples
    ids_fpath = parsed_args.ids

    # stan options
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)
    recompile = parsed_args.recompile

    # Bias options
    log_bias_fpath = parsed_args.log_bias
    log_bias_def = parsed_args.log_bias_def

    # Model options
    log_tau2_over_2a_mean = parsed_args.log_tau2_over_2a_mean
    log_tau2_over_2a_sd = parsed_args.log_tau2_over_2a_sd
    log_beta_mean = parsed_args.log_beta_mean
    log_beta_sd = parsed_args.log_beta_sd
    phylo_hl = parsed_args.phylo_hl
    prior_fpath = parsed_args.prior
    rho_prior = parsed_args.rho_prior

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()

    # Load sample and species data
    design = pd.read_csv(design_fpath).drop_duplicates(subset=[samples_col])
    design.set_index(samples_col, inplace=True)
    tree = dendropy.Tree.get(path=phylogeny_fpath, schema='newick')

    ids = None
    if ids_fpath is not None:
        ids = [line.strip() for line in open(ids_fpath)]

    # Load data
    inclusion, total = read_counts(inclusion_fpath, total_fpath, design.index,
                                   sel_ids=ids)
    design = design.reindex(total.columns)
    n_species = design['species'].unique().shape[0]
    log_bias = read_log_bias(log_bias_fpath, exon_ids=total.index,
                             sample_ids=total.columns,
                             log_bias_def=log_bias_def, log=log)
    msg = 'Loaded {} elements with {} samples from a total of {} species'
    log.write(msg.format(total.shape[0], total.shape[1], n_species))

    # Load model
    log.write('Loading model parameters specified by the user')
    ou_gp = get_OU_global_params(phylo_hl, log_tau2_over_2a_mean,
                                     log_tau2_over_2a_sd, log_beta_mean,
                                     log_beta_sd, prior_fpath)
    
    msg = 'Fit a model for each element accounting for potential shifts'
    msg += ' in the optimal values across the phylogeny\n'
    msg += '\t phylo_hl = {}\n\t tau2_over_2a = {}\n\t sigma = {}'
    log.write(msg.format(ou_gp['phylo_hl'], ou_gp['tau2_over_2a'],
                         ou_gp['sigma']))

    ou_model = ShiftsOUmodel(tree, design, 'logit_binomial',
                             ou_gp['phylo_hl'],
                             ou_gp['log_tau2_over_2a_mean'],
                             ou_gp['log_tau2_over_2a_sd'],
                             ou_gp['log_beta_mean'],
                             ou_gp['log_beta_sd'],
                             rho_prior=rho_prior,
                             recompile=recompile)
    ou_model.observations.load(inclusion, total, log_bias)

    # Fit model
    ou_model.fit(chains=n_chains, n_iter=n_samples)
    ou_model.write_fit(prefix=output_prefix)
    log.write('Results saved at {}*'.format(output_prefix))
    log.finish()


if __name__ == '__main__':
    main()
