#!/usr/bin/env python

import argparse

from AS_quant.utils import LogTrack
import pandas as pd


def main():
    description = 'Generates a table with inclusion and skipping reads'
    description += 'with design variables for later testing'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='Design file')
    parser.add_argument('-s', '--sample_field', default='sample',
                        help='Sample fieldname (sample)')
    parser.add_argument('-S', '--species_field', default='species',
                        help='Species fieldname (species)')
    parser.add_argument('-o', '--output_prefix', required=True,
                        help='Output file prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    design_fpath = parsed_args.input
    out_prefix = parsed_args.output_prefix
    sample_field = parsed_args.sample_field
    species_field = parsed_args.species_field

    # Init logger
    log = LogTrack()
    design = pd.read_csv(design_fpath)
    bam_suffix = 'Aligned.sortedByCoord.out.bam'
    for species, samples in design.groupby(species_field)[sample_field]:
        bams = ['{}.{}'.format(sample, bam_suffix) for sample in samples]
        
        fpath = '{}.{}.bams1'.format(out_prefix, species)
        with open(fpath, 'w') as fhand:
            fhand.write(bams[0] + '\n')
        
        fpath = '{}.{}.bams2'.format(out_prefix, species)
        with open(fpath, 'w') as fhand:
            fhand.write(','.join(bams[1:]) + '\n')
            
        fpath = '{}.{}.samples'.format(out_prefix, species)
        with open(fpath, 'w') as fhand:
            for sample in samples:
                fhand.write(sample + '\n')
        
        log.write('Species {} data written at {}.{}'.format(species, out_prefix,
                                                           species))

    # Read data
    log.write('Done')


if __name__ == '__main__':
    main()
