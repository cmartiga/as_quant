import argparse

import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.evol_models import EvolResultsSets


def main():
    description = 'Evaluates the characterization of evolution of molecular tra'
    description += 'its along the phylogenetic tree regarding inference of '
    description += 'ancestral nodes and shifts in the optimal values derived'
    description += ' from simulated data'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('params_table',
                             help='CSV table used for simulating data')
    
    input_group.add_argument('-r', '--real_prefix', required=True,
                             help='Prefix of files derived from sim_OU')
    input_group.add_argument('-i', '--inferred_prefix', required=True,
                             help='Prefix of files derived from fit_OU')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    params_fpath = parsed_args.params_table
    inferred_prefix = parsed_args.inferred_prefix
    real_prefix = parsed_args.real_prefix
    out_fpath= parsed_args.output

    # Init log
    log = LogTrack()
    log.write('Start analysis')

    # Load input data
    params_table= pd.read_csv(params_fpath)
    results = EvolResultsSets(log=log)
    results.read_datasets_results(params_table, real_prefix,
                                   inferred_prefix)
    results.evaluate_datasets()
    results.write_evaluation(out_fpath)
    results.plot('{}.png'.format(out_fpath))
    results.plot_by_dataset(out_fpath)
    log.finish()


if __name__ == '__main__':
    main()
