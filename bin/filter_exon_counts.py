#!/usr/bin/env python
from copy import deepcopy
import argparse

import numpy as np
import pandas as pd

from AS_quant.event_counts import EventsCounts


def main():
    description = 'Tool to manipulate event inclusion and skipping counts'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-i', '--input_prefix', required=True,
                             help='Counts matrix prefix')
    
    input_group.add_argument('--inc_suffix', default='inclusion',
                             help='Inclusion counts matrix suffix (inclusion)')
    input_group.add_argument('--skp_suffix', default='skipping',
                             help='Skipping counts matrix suffix (skipping)')

    cov_group = parser.add_argument_group('Coverage filter')
    cov_group.add_argument('-t', '--min_total', default=-1, type=int,
                           help='Minimum total counts in >= 1 sample (None)')
    cov_group.add_argument('-s', '--min_skipping', default=-1, type=int,
                           help='Minimum skipping counts in >= 1 sample (None)')
    cov_group.add_argument('-T', '--min_total_median', default=-1, type=float,
                           help='Minimum median total counts (None)')
    cov_group.add_argument('-S', '--min_skipping_median', default=-1, type=float,
                              help='Minimum median skipping counts (None)')
    
    psi_group = parser.add_argument_group('PSI filter')
    psi_group.add_argument('--min_psi', default=0, type=float,
                           help='Select events with PSI >= {min_psi} > 1 sample (0)')
    psi_group.add_argument('--max_psi', default=1, type=float,
                           help='Select events with PSI <= {max_psi} > 1 sample (1)')
    psi_group.add_argument('--min_psi_median', default=0, type=float,
                           help='Select events with median PSI >= {min_psi_median} (0)')
    psi_group.add_argument('--max_psi_median', default=1, type=float,
                           help='Select events with median PSI <= {max_psi_median} (1)')
    
    filter_group = parser.add_argument_group('IDS options')
    filter_group.add_argument('--event_ids', default=None, 
                              help='File containing event ids to filter')
    filter_group.add_argument('--sample_ids', default=None, 
                              help='File containing sample ids to filter')
    filter_group.add_argument('--expand', default=False, action='store_true', 
                              help='Expand counts to single splice sites exons')
    
    sampling_group = parser.add_argument_group('Sampling options')
    sampling_group.add_argument('-k', '--sample_size', default=0, type=int,
                                help='Sample size (no subsampling done)')
    sampling_group.add_argument('--seed', default=0, type=float,
                                help='Random seed (0)')
    
    grouping_group = parser.add_argument_group('Grouping options')
    grouping_group.add_argument('-e', '--event_data', default=None,
                                help='Table containing exon data for grouping')
    grouping_group.add_argument('-n', '--n_groups', default=5,
                                help='Number of groups to make (5)')
    grouping_group.add_argument('-v', '--variable', default=None,
                                help='Grouping variable')
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', required=True,
                              help='Prefix for output files')

    # Parse arguments
    parsed_args = parser.parse_args()
    input_prefix = parsed_args.input_prefix
    inc_suffix = parsed_args.inc_suffix
    skp_suffix = parsed_args.skp_suffix

    # Filtering options
    min_total = parsed_args.min_total
    min_total_median = parsed_args.min_total_median
    min_skipping = parsed_args.min_skipping
    min_skipping_median = parsed_args.min_skipping_median
    
    min_psi = parsed_args.min_psi
    min_psi_median = parsed_args.min_psi_median
    max_psi = parsed_args.max_psi
    max_psi_median = parsed_args.max_psi_median
    
    event_ids_fpath = parsed_args.event_ids
    expand = parsed_args.expand
    sample_ids_fpath = parsed_args.sample_ids
    sample_size = parsed_args.sample_size
    seed = parsed_args.seed
    
    # Grouping options
    event_data_fpath = parsed_args.event_data
    n_groups = parsed_args.n_groups
    grouping_variable = parsed_args.variable 

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init processing
    counts = EventsCounts(inclusion_suffix=inc_suffix,
                          skipping_suffix=skp_suffix)
    counts.init_log()
    counts.load_counts(prefix=input_prefix)
    counts.fill_values()
     
    # Filtering
    if event_ids_fpath is not None:
        event_ids = [line.strip() for line in open(event_ids_fpath)]
        counts.select_ids(event_ids)
    if sample_ids_fpath is not None:
        sample_ids = [line.strip() for line in open(sample_ids_fpath)]
        counts.select_samples(sample_ids)
    counts.filter_missing_samples()
    counts.filter_counts_samples(min_value=min_total, n_samples=1, label='total')
    counts.filter_counts_funct(min_value=min_total_median, label='total',
                               funct=np.median)
    counts.filter_counts_samples(min_value=min_skipping, n_samples=1, label='skipping')
    counts.filter_counts_funct(min_value=min_skipping_median, label='skipping',
                               funct=np.median)
    if min_psi > 0 or max_psi < 1:
        counts.filter_counts_samples(min_value=min_psi, label='psi',
                                     max_value=max_psi)
    
    if min_psi_median > 0 or max_psi_median < 1:
        counts.filter_counts_funct(min_value=min_psi_median, label='psi',
                                   max_value=max_psi_median, funct=np.nanmedian)
    if expand:
        counts.expand()
    
    if event_data_fpath is not None and grouping_variable is not None:
        event_data = pd.read_csv(event_data_fpath, index_col=0)
        qs = np.linspace(0, 100, n_groups + 1)
        percentiles = np.percentile(event_data[grouping_variable], qs)
        
        for subset, (l, u) in enumerate(zip(percentiles, percentiles[1:])):
            sel_rows = np.logical_and(event_data[grouping_variable] >= l,
                                      event_data[grouping_variable] < u)
            sel_ids = event_data.index[sel_rows]
            subset_counts = deepcopy(counts)
            subset_counts.select_ids(sel_ids)
            if sample_size > 0:
                subset_counts.sample(sample_size=sample_size,
                                            seed=seed)
            out_prefix_subset = '{}.{}.q{}'.format(output_prefix,
                                                   grouping_variable, subset)
            subset_counts.write(out_prefix_subset)
            
    else:
        if sample_size > 0:
            counts.sample(sample_size=sample_size, seed=seed)
        counts.write(output_prefix)
    counts.log.finish()


if __name__ == '__main__':
    main()
