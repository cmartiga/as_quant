import argparse

from AS_quant.utils import LogTrack
from AS_quant.genome import get_annotation_db, Genome, get_sj_index

        
def main():
    description = 'Generates a CSV file containing the splice junctions that '
    description += 'support inclusion or skipping for every exon in the genome'
    description += 'using only the longest transcript'
    
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('DB', help='GxF-DB containing exon annotation')

    options_group = parser.add_argument_group('Options')
    help_msg = 'Tabix indexed file with found SJ for expanding the set of SJ '
    help_msg += 'that support inclusion and skipping taking into account '
    help_msg += 'alternative splice sites within a window of length -l'
    options_group.add_argument('-s', '--sj_index', default=None, help=help_msg)
    options_group.add_argument('-l', '--length', default=50, type=int,
                               help='Window size around each splice site (50)')
    help_msg = 'Use whole gene sequence with alternative splice sites rather '
    help_msg += 'than only the longest transcript'
    options_group.add_argument('--whole_gene', default=False, action='store_true',
                               help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    db_fpath = parsed_args.DB
    sj_index_fpath = parsed_args.sj_index
    window_size = parsed_args.length
    whole_gene = parsed_args.whole_gene
    out_fpath = parsed_args.output
    
    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    # Load annotation data
    log.write('Loading exon data from {}'.format(db_fpath))
    annotation_db = get_annotation_db(db_fpath)
    genome = Genome(annotation_db, whole_gene=whole_gene)
    msg = '\tAnnotation loaded:\n\t{} genes\n\t{} exons'
    log.write(msg.format(genome.n_genes, genome.n_exons))
    sj_index = None
    if sj_index_fpath is not None:
        sj_index = get_sj_index(sj_index_fpath)

    # Find SJ for each annotated exon
    log.write('Writing SJs to {}...'.format(out_fpath))
    with open(out_fpath, 'w') as fhand:
        genome.write_exons_splice_junctions(fhand, sj_index=sj_index,
                                            window_size=window_size)
    log.finish()


if __name__ == '__main__':
    main()
