import argparse

from ete3 import Tree

from AS_quant.orthologs import OrthologousExons
from AS_quant.utils import LogTrack, get_ncbi_taxonomy_tree
from AS_quant.genome import get_annotation_db, Genome

            
def main():
    description = 'This script calculates a number of metrics using sets of '
    description += 'orthologous exons and genes to analyze the performance of '
    description += 'different methods or parameters for optimization in a '
    description += 'particular dataset or benchmarking purposes'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-e', '--exons', required=True,
                             help='File containing orthologous exons')
    help_msg = 'DB file with annotation to compare the number of exons per'
    input_group.add_argument('-db', '--annotation_db', default=None,
                             help=help_msg + ' gene')
    
    tree_options = parser.add_argument_group('Phylogeny options')
    help_msg = 'Compare exon presence phylogeny the provided species tree using'
    help_msg += ' Robinson-Foulds distance. It will download the taxonomy tree'
    help_msg += ' from NCBI using ETE3 otherwise'
    tree_options.add_argument('-s', '--species_tree', default=None,
                              help=help_msg)
    help_msg = 'Method to use for building exon-based phylogeny: {nj (def),'
    help_msg += ' upgma, raxml}. Bear in mind that RAxML is much slower.'
    tree_options.add_argument('-m', '--tree_method', default='nj',
                              help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output prefix for output files')

    # Parse arguments
    parsed_args = parser.parse_args()
    exons_fpath = parsed_args.exons
    species_tree_fpath = parsed_args.species_tree
    tree_method = parsed_args.tree_method
    db_fpath = parsed_args.annotation_db
    out_prefix = parsed_args.output

    # Init log
    log = LogTrack()

    # Load input data
    log.write('Loading orthologs...')
    orthologs = OrthologousExons(log=log)
    orthologs.read_csv(exons_fpath)
    
    msg = '\tLoaded {} exon orthologs across {} species from {}'
    log.write(msg.format(orthologs.n_exons, orthologs.n_species, exons_fpath))
    
    if db_fpath is not None:
        db = get_annotation_db(db_fpath)
        genome = Genome(db)
        orthologs.load_n_exons_ref(genome, id_funct=lambda x: x.split('_')[0])

    # Load reference tree
    if species_tree_fpath is None:
        species_tree = get_ncbi_taxonomy_tree(orthologs.species)
        species_tree_fpath = '{}.reference_tree.nh'.format(out_prefix)
        species_tree.write(outfile=species_tree_fpath)
    species_tree = Tree(species_tree_fpath)

    # Calculate and store metrics
    orthologs.calc_metrics(species_tree=species_tree, entropy_estimator='mm',
                           phylogeny_method=tree_method)
    orthologs.write_metrics(out_prefix)
    orthologs.plot_metrics('{}.png'.format(out_prefix))
    
    log.write('Finished')


if __name__ == '__main__':
    main()
