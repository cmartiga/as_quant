import argparse

import pandas as pd

from AS_quant.orthologs import GenomeSet
from AS_quant.utils import LogTrack


def main():
    description = 'Finds orthologous exons across previously defined homologous'
    description += ' genes. It uses MCL-complete subgraphs of a best reciprocal'
    description += ' hit graph constructed based on editing distance'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'File containing genome and annotation files for the species to'
    help_msg += ' include in the analysis'
    input_group.add_argument('-d', '--design', required=True, help=help_msg)
    input_group.add_argument('-O', '--orthologs', required=True,
                             help='File containing gene orthologs')
    input_group.add_argument('-db', '--db_dir', required=True,
                             help='Directory with annotation files')
    input_group.add_argument('-f', '--fasta_dir', required=True,
                             help='Directory with genome files')
    help_msg = 'File containing gene_ids (If not provided all fasta files in '
    help_msg += 'seqs_dir will be aligned)'
    input_group.add_argument('-g', '--gene_ids', default=None, help=help_msg)

    options_group = parser.add_argument_group('Sequence options')
    options_group.add_argument('--only_cds', default=False, action='store_true',
                               help='Use only CDS to extract orthologs')
    help_msg = 'Use whole gene sequence with alternative splice sites. (Select'
    help_msg += 'longest transcript only'
    options_group.add_argument('--whole_gene', default=False, action='store_true',
                               help=help_msg)

    options_group = parser.add_argument_group('Orthologs options')
    options_group.add_argument('-m', '--min_size', default=3, type=int,
                               help='Minimum orthogroup size (3)')
    options_group.add_argument('-s', '--species', default=None,
                               help='File containing a subset of species')
    help_msg = 'Skip the complete sub-graph step: use only MCL'
    options_group.add_argument('--skip_clique', default=False,
                               action='store_true', help=help_msg)
    options_group.add_argument('--expansion', default=2, type=float,
                               help='Expansion parameter for MCL (2)')
    options_group.add_argument('--inflation', default=2, type=float,
                               help='Inflation parameter for MCL (2)')
    
    score_group = parser.add_argument_group('Scoring options')
    help_msg = 'Use Multiple Sequence Alignment (MSA) to match exon sequences'
    help_msg += ' to account for exon order besides sequence'
    score_group.add_argument('--msa', default=False, action='store_true',
                             help=help_msg)
    help_msg = 'MSA method to use {muscle (default), mafft}'
    score_group.add_argument('--msa_method', default='muscle', help=help_msg)
    score_group.add_argument('--intron_score', default=50, type=float,
                             help='Score for matching intron position (50)')
    score_group.add_argument('--ss_score', default=10, type=float,
                             help='Score for matching splice site position (10)')
    help_msg = 'Scoring function to use for pairwise sequence comparison. '
    help_msg += '{nc, prot, edit}. Default: edit (faster option)'
    score_group.add_argument('-D', '--score_func', default='edit',
                             help=help_msg)
    help_msg = 'Additional bases flanking exons for distance calculations (0)'
    score_group.add_argument('--flanking_seq', default=0, type=int,
                             help=help_msg)
    score_group.add_argument('--phase_w', default=0, type=float,
                             help='Score for exons with matching phase (0)')
    score_group.add_argument('--splice_sites_w', default=0, type=float,
                             help='Weight for splice sites alignment (0)')
    score_group.add_argument('--gap_opening', default=-10, type=int,
                             help='Gap opening penalty (-10)')
    score_group.add_argument('--gap_extension', default=-1, type=int,
                             help='Gap extension penalty (-1)')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file for exon orthologs ')

    # Parse arguments
    parsed_args = parser.parse_args()
    
    design_fpath = parsed_args.design
    orthologs_fpath = parsed_args.orthologs
    annotation_dbs_dir = parsed_args.db_dir
    fastafiles_dir = parsed_args.fasta_dir
    species_fpath = parsed_args.species
    genes_fpath = parsed_args.gene_ids
    
    only_cds = parsed_args.only_cds
    whole_gene = parsed_args.whole_gene
    
    use_msa = parsed_args.msa
    msa_method = parsed_args.msa_method
    intron_score = parsed_args.intron_score
    ss_score = parsed_args.ss_score
    gap_opening = parsed_args.gap_opening
    gap_extension = parsed_args.gap_extension
    
    additional_bases = parsed_args.flanking_seq
    score_func_name = parsed_args.score_func
    phase_w = parsed_args.phase_w
    splice_sites_w = parsed_args.splice_sites_w
    
    min_size = parsed_args.min_size
    complete_subgraph = not parsed_args.skip_clique
    expansion = parsed_args.expansion
    inflation = parsed_args.inflation

    out_fpath = parsed_args.output

    # Init log
    log = LogTrack()

    # Load input data
    orthologous_genes = pd.read_csv(orthologs_fpath, index_col=0, dtype=str)
    genome_data = pd.read_csv(design_fpath, index_col=0)
    
    if genes_fpath is not None:
        gene_set_ids = [line.strip() for line in open(genes_fpath)]
        orthologous_genes = orthologous_genes.reindex(gene_set_ids)
    
    log.write('Load gene orthologous sets from {}'.format(orthologs_fpath))
    species = None
    if species_fpath is not None:
        species = [line.strip() for line in open(species_fpath)]
        genome_data = genome_data.reindex(species)
    genome_data = genome_data.to_dict(orient='index')

    # Build genome set
    genome_set = GenomeSet(orthologous_genes, log=log, only_cds=only_cds,
                           whole_gene=whole_gene)
    genome_set.build(genome_data, fastafiles_dir=fastafiles_dir,
                     annotation_dbs_dir=annotation_dbs_dir)
    gene_sets = genome_set.get_exon_orthologs(msa=use_msa, msa_method=msa_method,
                                              gap_penalty=(gap_opening, gap_extension),
                                              intron_score=intron_score,
                                              ss_score=ss_score,
                                              splice_sites_w=splice_sites_w,
                                              intronic_bases=additional_bases,
                                              phase_w=phase_w,
                                              score_function=score_func_name,
                                              min_size=min_size,
                                              complete_subgraph=complete_subgraph,
                                              expansion=expansion,
                                              inflation=inflation)
    genome_set.write_exon_orthologs(gene_sets, out_fpath)
    log.finish()


if __name__ == '__main__':
    main()
