#!/usr/bin/env python
import argparse

from AS_quant.utils import LogTrack
from AS_quant.bin_utils import (add_counts_arguments, read_log_bias,
                                add_log_bias_arguments, add_stan_arguments,
                                add_output_prefix_argument,  read_counts)
from AS_quant.models.psi import MixturePsiModel


def main():
    description = 'Uses a 1-inflated mixture model to infer PSIs across the '
    description += 'the whole transcriptome as well as the proportion of purely'
    description += 'constitutive exons in the dataset'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    add_counts_arguments(input_group)
    add_log_bias_arguments(parser)
    add_stan_arguments(parser)
    add_output_prefix_argument(parser)

    # Parse arguments
    parsed_args = parser.parse_args()
    inclusion_fpath = parsed_args.inclusion
    total_fpath = parsed_args.total

    # stan options
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)
    recompile = parsed_args.recompile

    # Bias options
    log_bias_fpath = parsed_args.log_bias
    log_bias_def = parsed_args.log_bias_def

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()
    log.write('Start analysis')
    
    # Load data
    inclusion, total = read_counts(inclusion_fpath, total_fpath)
    log_bias = read_log_bias(log_bias_fpath, exon_ids=total.index,
                             sample_ids=total.columns,
                             log_bias_def=log_bias_def, log=log)
    msg = 'Loaded {} elements with {} samples'
    log.write(msg.format(total.shape[0], total.shape[1]))
    
    # Load model
    msg = 'Loading model and performing MCMC sampling'
    log.write(msg)
    log.write('Using a 1-inflated model for PSI inference')
    model = MixturePsiModel(n_samples=total.shape[1], recompile=recompile)
    model.observations.load(inclusion, total, log_bias)

    # Fit model        
    model.fit(chains=n_chains, n_iter=n_samples)
    model.write_fit(prefix=output_prefix)
    log.write('Results saved at {}*'.format(output_prefix))
    log.finish()


if __name__ == '__main__':
    main()
