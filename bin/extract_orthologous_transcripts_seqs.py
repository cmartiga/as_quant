import argparse

import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.orthologs import GenomeSet


def main():
    description = 'Extracts transcript sequences from orthologous genes as'
    description += ' previous step to alignment and stores them into independen'
    description += 't fasta files for Multiple Sequence Alignment (MSA) taking '
    description += ' into account intron positions'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'File containing genome and annotation files for the species to'
    help_msg += ' include in the analysis'
    input_group.add_argument('-d', '--design', required=True,
                             help=help_msg)
    input_group.add_argument('-O', '--orthologs', required=True,
                             help='File containing transcript orthologs')
    input_group.add_argument('-db', '--db_dir', required=True,
                             help='Directory with annotation files')
    input_group.add_argument('-f', '--fasta_dir', required=True,
                             help='Directory with genome files')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-s', '--species', default=None,
                               help='File containing a subset of species')
    options_group.add_argument('--only_cds', default=False, action='store_true',
                               help='Extract only coding sequence')
    help_msg = 'Use whole gene sequence with alternative splice sites. (Select'
    help_msg += 'longest transcript only'
    options_group.add_argument('--whole_gene', default=False, action='store_true',
                               help=help_msg)
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output directory for fasta files')

    # Parse arguments
    parsed_args = parser.parse_args()
    design_fpath = parsed_args.design
    orthologs_fpath = parsed_args.orthologs
    annotation_dbs_dir = parsed_args.db_dir
    fastafiles_dir = parsed_args.fasta_dir
    out_dir = parsed_args.output
    species_fpath = parsed_args.species
    only_cds = parsed_args.only_cds
    whole_gene = parsed_args.whole_gene

    # Init log
    log = LogTrack()

    # Load input data
    orthologous_genes = pd.read_csv(orthologs_fpath, index_col=0, dtype=str)
    genome_data = pd.read_csv(design_fpath, index_col=0)
    
    log.write('Load gene orthologous sets from {}'.format(orthologs_fpath))
    species = None
    if species_fpath is not None:
        species = [line.strip() for line in open(species_fpath)]
        genome_data = genome_data.reindex(species)
    genome_data = genome_data.to_dict(orient='index')

    # Build genome set
    genome_set = GenomeSet(orthologous_genes, only_cds=only_cds, log=log,
                           whole_gene=whole_gene)
    genome_set.build(genome_data, fastafiles_dir=fastafiles_dir,
                     annotation_dbs_dir=annotation_dbs_dir)
    genome_set.write_gene_orthologs_seqs(out_dir, species)
    log.write('Unaligned gene sequences written at {}'.format(out_dir))
    log.write('Finished')


if __name__ == '__main__':
    main()
