#!/usr/bin/env python

import argparse

from AS_quant.models.phylo import BMmodelByElement
from AS_quant.utils import LogTrack
import dendropy

import numpy as np
import pandas as pd


def load_binomial_data(input_fpaths, samples):
    inc_fpath, total_fpath = input_fpaths.split(',')
    inclusion = pd.read_csv(inc_fpath, index_col=0)
    total = pd.read_csv(total_fpath, index_col=0)
    sel_samples = np.intersect1d(samples, total.columns)
    total['exon_id'] = total.index
    total.drop_duplicates('exon_id', inplace=True, keep=False)
    total.drop('exon_id', axis=1, inplace=True)
    inclusion = inclusion.loc[total.index, :]
    return(inclusion[sel_samples].fillna(0).astype(int),
           total[sel_samples].fillna(0).astype(int))


def load_trait(fpath, samples):
    x = pd.read_csv(fpath, index_col=0).fillna(0)
    sel_samples = np.intersect1d(samples, x.columns)
    x['exon_id'] = x.index
    x.drop_duplicates('exon_id', inplace=True, keep=False)
    x.drop('exon_id', axis=1, inplace=True)
    return(x[sel_samples])


def load_BM_model(obs_model, tree, design, log, recompile=False):
    log.write('Use {} as observational model'.format(obs_model))
    log.write('Using a global OU model with a single optimal value')
    ou_model = BMmodelByElement(tree, design, obs_model, recompile=recompile)
    return(ou_model)


def main():
    description = 'Infer the parameters of an Brownian-Motion (BM) model '
    description += 'underlying the evolution of quantitative transcriptomic '
    description += 'characters such as Gene Expression (GE) or Alternative '
    description += 'Splicing (AS) derived from RNA-seq experiments along a'
    description += ' given phylogeny. It assumes that all elements evolve'
    description += ' under a common regime to pool information across all '
    description += 'provided elements and takes into account the discret count'
    description += 'nature of the data using Binomial and Poisson'
    description += ' distributions'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('input',
                             help='Input file (s) with sample observations')

    help_msg = 'Sample design matrix (it should include "sample" and '
    help_msg += '"species" columns)'
    input_group.add_argument('-d', '--design', required=True,
                             help=help_msg)
    input_group.add_argument('-p', '--phylogenetic_tree', required=True,
                             help='Phylogenetic tree in newick format')
    input_group.add_argument('--samples', default='sample',
                             help='Column header for sample (sample)')

    obs_group = parser.add_argument_group('Observational model options')
    help_msg = 'Observational model {normal (def), logit_binomial, log_poisson'
    help_msg += ', logit_betabinomial, log_negativebinomial}'
    obs_group.add_argument('-m', '--obs_model',
                           default='normal', help=help_msg)
    obs_group.add_argument('-F', '--log_norm_factors', default=None,
                           help='File with samples log_norm_factors (None)')
    help_msg = 'Matrix containing log(modifier) to account for sample-specific'
    help_msg += ' fragmentation or GC bias. By default, it uses {log_bias_def}'
    help_msg += ' for every element or sample with missing data'
    obs_group.add_argument('--log_bias', default=None, help=help_msg)
    help_msg = 'Bias value to use by default for every exon. By default it use'
    help_msg += 's log(2) to account for exons having 2 possible inclusion SJ '
    help_msg += 'per skipping SJ. Set to "mean" to replace to use rowmeans'
    obs_group.add_argument('-b', '--log_bias_def', default=np.log(2),
                           help=help_msg)
    
    options_group = parser.add_argument_group('MCMC Options')
    options_group.add_argument('-n', '--nthreads', default=4,
                               help='Number of threads to fit the model (4)')
    options_group.add_argument('-c', '--nchains', default=4,
                               help='Number MCMC chains to fit the model (4)')
    options_group.add_argument('-N', '--n_samples', default=2000,
                               help='Number of MCMC samples (2000)')

    model_group = parser.add_argument_group('Evolutionary model options')
    model_group.add_argument('--recompile', default=False, action='store_true',
                             help='Recompile stan model before fitting')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', required=True,
                              help='Prefix for output files')

    # Parse arguments
    parsed_args = parser.parse_args()
    input_fpaths = parsed_args.input
    design_fpath = parsed_args.design
    phylogeny_fpath = parsed_args.phylogenetic_tree
    samples_col = parsed_args.samples

    # MCMC options
    n_threads = int(parsed_args.nthreads)
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)

    # Bias options
    log_bias_fpath = parsed_args.log_bias
    log_bias_def = parsed_args.log_bias_def
    log_norm_factors_fpath = parsed_args.log_norm_factors

    # Model options
    recompile = parsed_args.recompile
    obs_model = parsed_args.obs_model
    
    # Output options
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()

    # Load sample and species data
    design = pd.read_csv(design_fpath).drop_duplicates(subset=[samples_col])
    design.set_index(samples_col, inplace=True)
    tree = dendropy.Tree.get(path=phylogeny_fpath, schema='newick')

    # Load data
    if obs_model in ['logit_binomial', 'logit_betabinomial']:
        inclusion, total = load_binomial_data(input_fpaths, design.index)
    else:
        total = load_trait(input_fpaths, design.index)
    design = design.reindex(total.columns)
    n_species = design['species'].unique().shape[0]

    if log_bias_fpath is not None:
        log_bias = pd.read_csv(log_bias_fpath, index_col=0)
        log_bias = log_bias.loc[total.index, :][total.columns]
        log_bias = log_bias.replace(np.inf, np.nan).replace(-np.inf, np.nan)
        if log_bias_def == 'mean':
            log_bias.apply(lambda row: row.fillna(row.mean()), axis=1)
        else:
            log_bias.fillna(log_bias_def, inplace=True)
        log.write('Loaded exon log(bias) from {}'.format(log_bias_fpath))
    else:
        log_bias = pd.DataFrame(np.full(total.shape, log_bias_def),
                                index=total.index, columns=total.columns)
        msg = 'Missing log(bias) information. Using {} as default'
        log.write(msg.format(log_bias_def))
    msg = 'Loaded {} elements with {} samples from a total of {} species'
    log.write(msg.format(total.shape[0], total.shape[1], n_species))

    # Load log_norm_factors if provided
    if log_norm_factors_fpath is not None:
        log_norm_factors = dict(line.strip().split(',')
                                for line in open(log_norm_factors_fpath))
        log_norm_factors = np.array([log_norm_factors.get(sample, 0)
                                     for sample in total.columns])
    else:
        log_norm_factors = None

    # Load model
    msg = 'Loading model and performing MCMC sampling'
    log.write(msg)
    ou_model = load_BM_model(obs_model, tree, design, log, recompile=recompile)
    ou_model.observations.load(inclusion, total, log_bias)

    # Fit model
    ou_model.fit(chains=n_chains, n_iter=n_samples)
    ou_model.write_fit(prefix=output_prefix)
    log.write('Results saved at {}*'.format(output_prefix))
    log.finish()


if __name__ == '__main__':
    main()
