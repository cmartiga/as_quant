#!/usr/bin/env python
import argparse

import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.bin_utils import (add_counts_arguments, add_design_arguments,
                                add_log_bias_arguments, add_stan_arguments,
                                add_output_prefix_argument,  read_counts,
                                read_log_bias, add_contrast_argument)
from AS_quant.models.psi import RegressionModel


def main():
    description = 'Uses a logistic regression model to infer PSIs across the '
    description += 'different groups (e.g. tissues) and estimates PSI and its '
    description += 'uncertainty for every condition provided in the regression'
    description += ' model'

        # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    add_counts_arguments(input_group)
    add_design_arguments(input_group, add_pred=True)
    add_contrast_argument(input_group)
    add_log_bias_arguments(parser)
    add_stan_arguments(parser)
    add_output_prefix_argument(parser)

    # Parse arguments
    parsed_args = parser.parse_args()
    inclusion_fpath = parsed_args.inclusion
    total_fpath = parsed_args.total
    design_fpath = parsed_args.design
    design_pred_fpath = parsed_args.design_pred
    samples_col = parsed_args.samples
    ids_fpath = parsed_args.ids
    contrast_matrix_fpath = parsed_args.contrast_matrix

    # stan options
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)
    recompile = parsed_args.recompile

    # Bias options
    log_bias_fpath = parsed_args.log_bias
    log_bias_def = parsed_args.log_bias_def

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()
    log.write('Start analysis')
    
    # Load sample and species data
    design = pd.read_csv(design_fpath).drop_duplicates(subset=[samples_col])
    design.set_index(samples_col, inplace=True)
    design_pred = pd.read_csv(design_pred_fpath, index_col=0)
    
    ids = None
    if ids_fpath is not None:
        ids = [line.strip() for line in open(ids_fpath)]
    
    # Load data
    inclusion, total = read_counts(inclusion_fpath, total_fpath, design.index,
                                   sel_ids=ids)
    design = design.reindex(total.columns)
    log_bias = read_log_bias(log_bias_fpath, exon_ids=total.index,
                             sample_ids=total.columns,
                             log_bias_def=log_bias_def, log=log)
    msg = 'Loaded {} elements with {} samples'
    log.write(msg.format(total.shape[0], total.shape[1]))
    
    contrast_matrix = None
    if contrast_matrix_fpath is not None:
        contrast_matrix = pd.read_csv(contrast_matrix, index_col=0)
    
    # Load model
    msg = 'Loading model and performing MCMC sampling'
    log.write(msg)
    log.write('Using a logistic regression model')
    model = RegressionModel(design, design_pred, recompile=recompile,
                            contrast_matrix=contrast_matrix)
    model.observations.load(inclusion, total, log_bias)

    # Fit model        
    model.fit(chains=n_chains, n_iter=n_samples)
    model.write_fit(prefix=output_prefix)
    log.write('Results saved at {}*'.format(output_prefix))
    log.finish()


if __name__ == '__main__':
    main()
