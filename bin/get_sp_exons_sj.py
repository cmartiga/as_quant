from _collections import defaultdict
import argparse

from AS_quant.utils import LogTrack, parse_gff
import pandas as pd


def parse_coords(exon_coords):
    chrom, coords = exon_coords.split(':')
    start, end = [int(x.rstrip('+-')) for x in coords.split('-', 1)]
    return(chrom, start, end)


class SJindex(object):
    def __init__(self):
        self.index = defaultdict(dict)

    def add_record(self, chrom, coord1, coord2):
        try:
            self.index[chrom][coord1].append(coord2)
        except KeyError:
            self.index[chrom][coord1] = [coord2]

    def add_sj(self, chrom, start, end):
        self.add_record(chrom, start, end)
        self.add_record(chrom, end, start)

    def build(self, sjs):
        for sj in sjs:
            chrom, start, end = parse_coords(sj)
            self.add_sj(chrom, start - 1, end)

    def get(self, chrom, coord):
        return(self.index[chrom].get(coord, []))


def format_sj(chrom, coord1, coord2):
    coords = sorted([coord1, coord2])
    return('{}:{}-{}'.format(chrom, coords[0] + 1, coords[1]))


def fetch_exons(parsed_gff):
    for gene_data in parsed_gff.values():
        chrom = gene_data['chrom']
        strand = gene_data['strand']

        if 'exons' not in gene_data:
            continue
        for start, end in gene_data['exons']:
            yield(chrom, start, end, strand)


def fetch_skp_sjs(chrom, sj1, sj2, sj_index):
    for j1 in sj1:
        ends = sj_index.get(chrom, j1)
        for j2 in sj2:
            if j2 in ends:
                yield(format_sj(chrom, j1, j2))


def format_exon(chrom, start, end, strand, prefix=None):
    str_exon = '{}:{}-{}{}'.format(chrom, start + 1, end, strand)
    if prefix is not None:
        str_exon = '{}-{}'.format(prefix, str_exon)
    return(str_exon)


def get_exon_sjs(chrom, start, end, sj_index):
    sj1 = sj_index.get(chrom, start)
    sj2 = sj_index.get(chrom, end)
    if not sj2 or not sj1:
        return((None, None))

    inc = [format_sj(chrom, start, x) for x in sj1]
    inc.extend([format_sj(chrom, end, x) for x in sj2])
    skp = list(fetch_skp_sjs(chrom, sj1, sj2, sj_index))
    return(inc, skp)


def format_line(exon_id, inc, skp):
    line = '{},{},{}\n'.format(exon_id, ';'.join(inc), ';'.join(skp))
    return(line)


def main():
    description = 'Finds SJ supporting exon inclusion and skipping'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-s', '--sj_counts', required=True,
                             help='File containing SJ counts')
    input_group.add_argument('-g', '--gff', required=True,
                             help='GFF containing exon annotation')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-p', '--prefix', default=None,
                               help='Prefix to add to chrom name')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    gff_fpath = parsed_args.gff
    sj_counts_fpath = parsed_args.sj_counts
    out_fpath = parsed_args.output
    prefix = parsed_args.prefix

    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')
    log.write('Loading SJs from {}...'.format(sj_counts_fpath))
    sj_counts = pd.read_csv(sj_counts_fpath, index_col=0).index
    log.write('\tLoaded {} SJ'.format(sj_counts.shape[0]))

    log.write('Building SJ index...')
    sj_index = SJindex()
    sj_index.build(sj_counts)
    log.write('\tDone')

    # Load annotation data
    log.write('Loading exon data from {}'.format(gff_fpath))
    with open(gff_fpath) as gff_fhand:
        sep = '=' if gff_fpath.split('.')[-1] == 'gff' else ' '
        parsed_gff = parse_gff(gff_fhand, store_transcripts=False, sep=sep)
    exons = list(fetch_exons(parsed_gff))
    log.write('\tAnnotation loaded: {} exons'.format(len(exons)))

    # Find SJ for each annotated exon
    exons_sj = 0
    log.write('Writting SJs to {}...'.format(out_fpath))
    with open(out_fpath, 'w') as fhand:
        fhand.write('exon_id,inc_sj,skp_sj\n')

        for total_exons, (chrom, start, end, strand) in enumerate(exons):
            if total_exons % 10000 == 0:
                msg = '\tFound SJ for {} out of {} exons'
                log.write(msg.format(exons_sj, total_exons))

            inc, skp = get_exon_sjs(chrom, start, end, sj_index)
            if inc is None or skp is None:
                continue
            exon_id = format_exon(chrom, start, end, strand, prefix=prefix)
            fhand.write(format_line(exon_id, inc, skp))
            exons_sj += 1

    log.finish()


if __name__ == '__main__':
    main()
