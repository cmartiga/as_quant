#!/usr/bin/env python

import argparse

from AS_quant.utils import LogTrack, get_model, get_summary_df
import numpy as np
import pandas as pd
from AS_quant.psi_models import MixturePsiModel, ExonPsiModel


def get_output_table(fit, exon_ids):
    traces = fit.extract()
    data = {}
    psi = traces['psi']
    data['E[psi]'] = psi.mean(0)
    data['psi.2.5'] = np.percentile(psi, 2.5, axis=0)
    data['psi.97.5'] = np.percentile(psi, 97.5, axis=0)

    return(pd.DataFrame(data, index=exon_ids))


def run_full_model(inclusion, total, log_map_ratio, recompile,
                   n_samples, n_chains, n_threads, output_prefix):
    data = {'log_map_ratio': log_map_ratio, 'K': log_map_ratio.shape[0]}

    # Load and prepare model
    model = get_model('psi', recompile=recompile)
    pars = ['X_mean', 'X_sd', 'psi']

    # Load model and infer PSIs
    psi = {}
    for sample in total.columns:
        data['inclusion'] = inclusion[sample]
        data['total'] = total[sample]
        fit = model.sampling(data=data, pars=pars, chains=n_chains,
                             iter=n_samples, n_jobs=n_threads)

        # Save data
        summary = get_summary_df(fit)
        fpath = '{}.{}.summary.csv'.format(output_prefix, sample)
        summary.to_csv(fpath)

        output_table = get_output_table(fit, total.index)
        psi[sample] = output_table['E[psi]']
        fpath = '{}.{}.out.csv'.format(output_prefix, sample)
        output_table.to_csv(fpath)
    
    psi = pd.DataFrame(psi, index=total.index)
    fpath = '{}.psi.csv'.format(output_prefix)
    psi.to_csv(fpath)


def run_exon_model(inclusion, total, log_map_ratio, recompile,
                   n_samples, n_chains, n_threads, output_prefix):
    # Load and prepare model
    model = get_model('exon_psi', recompile=recompile)
    pars = ['psi']
    samples = total.columns
    inclusion = inclusion.to_dict(orient='index')
    total = total.to_dict(orient='index')
    log_map_ratio = log_map_ratio.to_dict()

    # Load model and infer PSIs
    with open('{}.psi.csv'.format(output_prefix), 'w') as fhand:
        fieldnames = ['exon_id'] + list(samples)
        fhand.write(','.join(fieldnames) + '\n')
        
        for exon in total.keys():
            record = {'exon_id': exon}
            for sample in total[exon].keys():
                data = {'log_map_ratio': log_map_ratio[exon],
                        'inclusion': inclusion[exon][sample],
                        'total': total[exon][sample]}
                fit = model.sampling(data=data, pars=pars, chains=n_chains,
                                     iter=n_samples, n_jobs=n_threads)
                record[sample] = fit.extract(['psi'])['psi'].mean()
            fhand.write(','.join([str(record[fd]) for fd in fieldnames]) + '\n')


def main():
    description = 'Hierarchical bayesian model to infer exon PSIs. It allows'
    description += ' correcting for fragment length distribution per sample'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-i', '--inclusion', required=True,
                             help='Inclusion counts matrix')
    input_group.add_argument('-t', '--total', default=None,
                             help='Total counts matrix')
    input_group.add_argument('-s', '--skipping', default=None,
                             help='Skipping counts matrix')
    input_group.add_argument('-e', '--exon_data', default=None,
                             help='Matrix containing fragmentation and GC bias')
    input_group.add_argument('-f', '--fieldname', default='log_len_bias',
                             help='Fieldname to use as bias (log_len_bias)')

    options_group = parser.add_argument_group('MCMC Options')
    options_group.add_argument('-n', '--nthreads', default=4,
                               help='Number of threads to fit the model (4)')
    options_group.add_argument('-c', '--nchains', default=4,
                               help='Number MCMC chains to fit the model (4)')
    options_group.add_argument('-N', '--n_samples', default=2000,
                               help='Number of MCMC samples (2000)')

    model_group = parser.add_argument_group('Models options')
    model_group.add_argument('-m', '--min_counts', default=5, type=int,
                             help='Minimum total counts in at least 1 sample')
    model_group.add_argument('-b', '--bias_def', default=np.log(2), type=float,
                             help='Bias value to use if exon_data is missing (log(2))')
    model_group.add_argument('--exon_model', default=False, action='store_true',
                             help='Run an independent model for each exon')
    model_group.add_argument('--recompile', default=False, action='store_true',
                             help='Recompile stan model before fitting')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    inc_fpath = parsed_args.inclusion
    total_fpath = parsed_args.total
    skipping_fpath = parsed_args.skipping
    exon_data_fpath = parsed_args.exon_data
    bias_fieldname = parsed_args.fieldname

    # MCMC options
    n_threads = int(parsed_args.nthreads)
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)
    
    # Other options
    recompile = parsed_args.recompile
    min_counts = parsed_args.min_counts
    exon_model = parsed_args.exon_model
    bias_def = parsed_args.bias_def

    # Output options
    output_fpath = parsed_args.output

    # Init log
    log = LogTrack()
    log.write('Start data analysis...')

    # Load count data
    inclusion = pd.read_csv(inc_fpath, index_col=0).fillna(0).astype(int)
    if total_fpath is None:
        if skipping_fpath is None:
            raise ValueError('One of skipping or total reads must be provided')
        skipping = pd.read_csv(skipping_fpath,
                               index_col=0).fillna(0).astype(int)
        total = inclusion + skipping
    else:
        if skipping_fpath is not None:
            raise ValueError('Only one of skipping or total can be used')
        total = pd.read_csv(total_fpath, index_col=0).fillna(0).astype(int)
    log.write('Loaded {} events with {} samples'.format(total.shape[0],
                                                        total.shape[1]))
#     total.index = [x.split('-', 1)[-1] for x in total.index]
#     inclusion.index = [x.split('-', 1)[-1] for x in inclusion.index]
    
    total['exon_id'] = total.index
    total.drop_duplicates('exon_id', inplace=True, keep=False)
    total.drop('exon_id', axis=1, inplace=True)
    inclusion = inclusion.loc[total.index, :]
    
    sel_rows = total.min(1) > min_counts
    total = total.loc[sel_rows, :]
    inclusion = inclusion.loc[sel_rows, :]
    log.write('Filtered {} events with > {} counts'.format(total.shape[0], 
                                                           min_counts))
    
    # Load exon data
    if exon_data_fpath is not None:
        ed = pd.read_csv(exon_data_fpath)
        id_label = ed.columns[0]
        ed = ed.drop_duplicates(id_label).set_index(id_label).reindex(total.index)
        ed.replace(np.inf, np.nan, inplace=True)
        ed.replace(-np.inf, np.nan, inplace=True)
        ed.dropna(inplace=True)
        n_removed = total.shape[0] - ed.shape[0]
        inclusion = inclusion.loc[ed.index, :]
        total = total.loc[ed.index, :]
        log.write('Loaded exon data from {}'.format(exon_data_fpath))
        log.write('\tRemoved {} exons without GC content data'.format(n_removed))
        log_map_ratio = ed[[bias_fieldname]]
    else:
        log_map_ratio = pd.Series(np.full(total.shape[0],
                                          fill_value=bias_def),
                                  index=total.index)
    
    # Load model and fit data
    if exon_model:
        model = ExonPsiModel(recompile=recompile)
    else:
        model = MixturePsiModel(recompile=recompile)
    model.observations.load(inclusion, total, log_map_ratio)
    model.fit(chains=n_chains, n_iter=n_samples, n_jobs=n_threads)
    model.write_fit(output_fpath)
    log.write('Output PSIs written to {}'.format(output_fpath))
    log.finish()


if __name__ == '__main__':
    main()
