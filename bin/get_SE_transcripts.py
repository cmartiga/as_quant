import argparse

from pysam import FastaFile

from AS_simulation.utils import (LogTrack, parse_gff, simulate_SE_transcripts,
                                 write_gff_record, write_transcripts)


def main():
    description = 'Creates a transcript annotation GFF and FASTA based'
    description += 'only on exon skipping event_counts from the longest annotated'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('genome', help='Genome fasta file')
    input_group.add_argument('GFF', help='GFF file with transcript annotation')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-p', '--gene_prefix', default='',
                               help='Prefix to add to gene and chrom')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_gff', required=True,
                              help='Output annotation')
    output_group.add_argument('-t', '--output_transcripts', required=True,
                              help='Output transcripts sequence (fasta)')

    # Parse arguments
    parsed_args = parser.parse_args()
    gff_fpath = parsed_args.GFF
    genome_fpath = parsed_args.genome
    out_gff_fpath = parsed_args.output_gff
    out_fasta_fpath = parsed_args.output_transcripts
    gene_prefix = parsed_args.gene_prefix
    sep = '=' if gff_fpath.split('.')[-1] in ['gff', 'gff3'] else ' '

    # Run
    log = LogTrack()
    log.write('Loading genome from {}'.format(genome_fpath))
    genome = FastaFile(genome_fpath)
    log.write('Loading annotation from {}'.format(gff_fpath))
    with open(gff_fpath) as fhand:
        parsed_gff = parse_gff(fhand, sep=sep)
    log.write('Loaded data from {} genes'.format(len(parsed_gff)))

    log.write('Generating alternative transcripts from SE event_counts...')
    with open(out_gff_fpath, 'w') as out_gff:
        with open(out_fasta_fpath, 'w') as transcript_fhand:
            for gene_data in simulate_SE_transcripts(parsed_gff,
                                                     gene_prefix=gene_prefix):
                write_gff_record(gene_data, out_gff)
                write_transcripts(gene_data, genome, transcript_fhand)

    log.finish()


if __name__ == '__main__':
    main()
