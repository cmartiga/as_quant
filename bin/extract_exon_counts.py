#!/usr/bin/env python

import argparse

from AS_quant.utils import LogTrack, read_sj
import pandas as pd
import numpy as np
from AS_quant.bam_process import load_bamfile, get_exons_SE_counts
from csv import DictWriter


def main():
    description = 'Extracts the number of counts supporting exon inclusion and'
    description += ' skipping using a pre-defined set of Splice Junctions (SJs)'
    description += ' for each exon'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('bam', help='Indexed BAM file')
    help_msg = 'File containing SJs supporting exon inclusion and skipping'
    input_group.add_argument('-e', '--exons_sj', required=True, help=help_msg)
    
    options_group = parser.add_argument_group('Options')
    help_msg = 'Minimum read overhang at both sides of the SJs (8)'
    options_group.add_argument('-O', '--read_overhang', default=8, type=int,
                               help=help_msg)
    help_msg = 'File containing SJ detected in the BAM file to reduce SJ reads'
    help_msg += ' search'
    options_group.add_argument('-sj', '--detected_SJ', default=None,
                               help=help_msg)
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    bam_fpath = parsed_args.bam
    out_fpath = parsed_args.output
    overhang = parsed_args.read_overhang
    exons_sj_fpath = parsed_args.exons_sj
    detected_sj_fpath = parsed_args.detected_SJ

    # Init logger
    log = LogTrack()
    
    # Load bamfile
    bamfile = load_bamfile(bam_fpath)
    log.write('BAM file loaded from {}'.format(bam_fpath))
    
    # Load exons SJ data
    exons_sj = pd.read_csv(exons_sj_fpath, index_col=0, dtype='str')
    exons_sj.replace(np.nan, '', inplace=True)
    log.write('Exons SJ loaded from {}'.format(exons_sj_fpath))
    
    # Load selected SJ files if provided
    detected_sj = None
    if detected_sj_fpath is not None:
        # SJ files from STAR are always 1-indexed
        detected_sj = read_sj(detected_sj_fpath, to_index_0=True)
    
    # Init writer
    with open(out_fpath, 'w') as fhand:
        fieldnames = ['exon_id',  'inc_reads', 'skp_reads',
                      'inc_reads_no_dups', 'skp_reads_no_dups',]
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()
        
        exon_counts = get_exons_SE_counts(exons_sj, overhang, bamfile,
                                          detected_sj=detected_sj)
        for i, record in enumerate(exon_counts):
            writer.writerow(record)
            if i % 100 == 0:
                log.write('\tExons processed: {}'.format(i))
                fhand.flush()

    # Read data
    log.finish()


if __name__ == '__main__':
    main()
