import argparse

import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.bias import get_fragment_size_distrib, calc_eff_len_table


def main():
    description = 'Estimates log-mappability ratios for exon skipping '
    description += 'events depending on exon length, read length and '
    description += 'Splice Junction overhang and fragment size distribution'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'CSV file with distances to transcript start and end'
    input_group.add_argument('exon_dist', help=help_msg)
    
    options_group = parser.add_argument_group('General Options')
    help_msg = 'Do not take into account exon length for calculating eff length'
    help_msg += ' of inclusion form. Use it when directly using SJ counts from '
    help_msg += 'STAR rather than counting the reads mapping to each exon'
    options_group.add_argument('--no_exon_length', default=False,
                               action='store_true', help=help_msg)
    
    rnaseq_group = parser.add_argument_group('Sequencing Options')
    rnaseq_group.add_argument('-r', '--read_length', default=75, type=int,
                              help='Read length (75)')
    rnaseq_group.add_argument('-O', '--read_overhang', default=3, type=int,
                              help='Min read overhang for mapping (3)')
    rnaseq_group.add_argument('-A', '--adaptor_len', default=8, type=int,
                              help='Library adaptor length (8)')
    
    fragments_group = parser.add_argument_group('Fragmentation Options')
    fragments_group.add_argument('-f', '--fragment_length_mean',
                                 default=364, type=int,
                                 help='Mean fragment length used (364)')
    fragments_group.add_argument('-fs', '--fragment_length_sd',
                                 default=132, type=int,
                                 help='Fragment length standard deviation (132)')
    fragments_group.add_argument('-fm', '--fragment_length_min',
                                 default=None, type=int,
                                 help='Minimum fragment length used (read_length)')
    help_msg = 'Empirical inner size distribution as provided by RSeQC'
    fragments_group.add_argument('-F', '--empirical_fragments',
                                 default=None, help=help_msg)
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    exon_dist_fpath = parsed_args.exon_dist
    
    no_exon_length = parsed_args.no_exon_length
    read_length = parsed_args.read_length
    overhang = parsed_args.read_overhang
    adaptor_len = parsed_args.adaptor_len

    empirical_fragments_fpath = parsed_args.empirical_fragments
    fragment_length_mean = parsed_args.fragment_length_mean
    fragment_length_sd = parsed_args.fragment_length_sd
    fragment_length_min = parsed_args.fragment_length_min
    if fragment_length_min is None:
        fragment_length_min = read_length + adaptor_len * 2 
    
    out_fpath = parsed_args.output

    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    log.write('Loading exon distance data from {}'.format(exon_dist_fpath))
    exon_dist = pd.read_csv(exon_dist_fpath)

    # Get fragment size distribution
    f_size, p_f_size = get_fragment_size_distrib(empirical_fragments_fpath,
                                                 fragment_length_mean,
                                                 fragment_length_sd,
                                                 read_length, adaptor_len,
                                                 fragment_length_min)

    # Estimate factors
    exon_data = calc_eff_len_table(exon_dist, read_length, overhang,
                                   fragment_size=f_size,
                                   fragment_size_p=p_f_size,
                                   no_exon_length=no_exon_length)
    exon_data.to_csv(out_fpath, index=False)
    log.write('Fragmentation bias data written at {}'.format(out_fpath))
    log.finish()


if __name__ == '__main__':
    main()
