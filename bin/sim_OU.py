#!/usr/bin/env python
import argparse

from AS_quant.models.phylo import (BasicOUmodel, ShiftsOUmodel)
from AS_quant.utils import LogTrack, get_OU_global_params
import dendropy

import numpy as np
import pandas as pd


def select_ou_model(obs_model, log, tree, design, ou_params):
    shifts = ou_params['adapt_p'] > 0
    if shifts:
        msg = 'Use an element-level model with known OU parameters to'
        msg += ' detect shifts'
        log.write(msg)
        ou_model = ShiftsOUmodel(tree, design, obs_model,
                                 ou_params['phylo_hl'],
                                 ou_params['log_tau2_over_2a_mean'],
                                 ou_params['log_tau2_over_2a_sd'],
                                 ou_params['log_beta_mean'],
                                 ou_params['log_beta_sd'])
    else:
        log.write('Use a global model with only stabilizing selection')
        ou_model = BasicOUmodel(tree, design, obs_model)
    return(ou_model)


def main():
    description = 'Simulates transcriptomic data derived from RNA-seq from '
    description += 'characters such as Gene Expression (GE) or Alternative '
    description += 'Splicing (AS)  evolving under an Ornstein-Uhlenbeck (OU) '
    description += ' process along a given phylogeny.'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'Sample design matrix (it should include "sample" and '
    help_msg += '"species" columns)'
    input_group.add_argument('-d', '--design', required=True,
                             help=help_msg)
    input_group.add_argument('-p', '--phylogenetic_tree', required=True,
                             help='Phylogenetic tree in newick format')
    input_group.add_argument('-N', '--n_elements', default=100, type=int,
                             help='Number of elements to simulate (100)')
    input_group.add_argument('--samples', default='sample',
                             help='Column header for sample (sample)')

    obs_group = parser.add_argument_group('Observational model options')
    help_msg = 'Observational model {normal (def), logit_binomial}'
    help_msg += ', log_poisson'
    obs_group.add_argument('-m', '--obs_model',
                           default='normal', help=help_msg)
    obs_group.add_argument('-C', '--sequencing_depth', default=10,
                           type=float, help='Mean event sequencing depth (10)')
    help_msg = 'Use exponentially distributed coverage across exons instead'
    help_msg += 'of common depth throughout the dataset'
    obs_group.add_argument('--exponential_depth', default=False,
                           action='store_true', help=help_msg)

    model_group = parser.add_argument_group('Evolutionary model options')
    model_group.add_argument('-m0', '--mu0_mean', default=3, type=float,
                             help='Mean of mu values distribution')
    help_msg = 'Standard deviation of mu values distribution'
    model_group.add_argument('-m0s', '--mu0_sd', default=2, type=float,
                             help=help_msg)
    model_group.add_argument('-t12', '--phylo_hl', default=25, type=float,
                             help='Phylogenetic half-life (25my)')
    model_group.add_argument('-t2a', '--tau2_over_2a', default=5, type=float,
                             help='OU equilibrium variance')
    model_group.add_argument('-s', '--sigma', default=0.6, type=float,
                             help='Within species variance (0.6)')
    help_msg = 'Proportion of branches with shifts in optimal value (0)'
    model_group.add_argument('-a', '--adapt_p', default=0, type=float,
                             help=help_msg)
    help_msg = 'Standard deviation of log(tau2) element-wise variation (0)'
    model_group.add_argument('--tau2_sigma', default=0, type=float,
                             help=help_msg)

    help_msg = 'File containing full posterior distribution of a previous fit'
    help_msg += ' not accounting for shifts to infer the global OU parameters'
    model_group.add_argument('--prior', default=None, help=help_msg)
    help_msg = 'Table with different parameter configurations '
    help_msg = 'sets of simulations'
    model_group.add_argument('--params_table', default=None, help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', required=True,
                              help='Prefix for output files')
    output_group.add_argument('--seed', default=None, type=int,
                              help='Seed for random number generator (None)')

    # Parse arguments
    parsed_args = parser.parse_args()
    design_fpath = parsed_args.design
    phylogeny_fpath = parsed_args.phylogenetic_tree
    n_elements = parsed_args.n_elements
    samples_col = parsed_args.samples

    # Model options
    mu0_mean = parsed_args.mu0_mean
    mu0_sd = parsed_args.mu0_sd
    tau2_over_2a = parsed_args.tau2_over_2a
    phylo_hl = parsed_args.phylo_hl
    sigma = parsed_args.sigma
    adapt_p = parsed_args.adapt_p
    tau2_sigma = parsed_args.tau2_sigma
    prior_fpath = parsed_args.prior
    params_table_fpath = parsed_args.params_table

    # Observations model
    obs_model = parsed_args.obs_model
    sequencing_depth = parsed_args.sequencing_depth
    exponential_depth = parsed_args.exponential_depth

    # Output options
    seed = parsed_args.seed
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()
    log.write('Start data analysis...')

    # Load sample and species data
    design = pd.read_csv(design_fpath).drop_duplicates(subset=[samples_col])
    design.set_index(samples_col, inplace=True)
    tree = dendropy.Tree.get(path=phylogeny_fpath, schema='newick')

    # Run model
    msg = 'Setting up simulation parameters...'
    log.write(msg)
    if params_table_fpath is None:
        t2a = np.log(tau2_over_2a)
        log_beta = np.log(tau2_over_2a * 2 * np.log(2) / phylo_hl / (sigma**2))
        ou_params = get_OU_global_params(phylo_hl,
                                         log_tau2_over_2a_mean=t2a,
                                         log_tau2_over_2a_sd=0,
                                         log_beta_mean=log_beta,
                                         log_beta_sd=0,
                                         prior_fpath=prior_fpath,
                                         mu0_mean=mu0_mean, mu0_sd=mu0_sd)
        ou_params['mean_coverage'] = sequencing_depth
        ou_params['exponential_cov'] = exponential_depth
        ou_params['adapt_p'] = adapt_p
        ou_params['n_elements'] = n_elements
        ou_params['dataset'] = 1
        config = [ou_params]
        log.write('Single model configuration for simulations:')
        for k, v in ou_params.items():
            log.write('\t{} = {}'.format(k, v))

    else:
        config = pd.read_csv(params_table_fpath)
        config['mu_mean'] = config['mu0_mean']
        config['mu_sd'] = config['mu0_sd']
        config = config.to_dict(orient='index').values()
        msg = '{} model configuration for simulations:'.format(len(config))
        log.write(msg)

    if seed is not None:
        np.random.seed(seed)
    for ou_params in config:
        ou_model = select_ou_model(obs_model, log, tree, design,
                                   ou_params)
        ou_model.simulate(**ou_params)
        log.write('Simulation completed with {} elements'.format(
            ou_params['n_elements']))
        prefix = '{}.{}'.format(output_prefix, ou_params['dataset'])
        ou_model.write_simulations(prefix=prefix)
        log.write('Results saved at {}*'.format(prefix))
    log.finish()


if __name__ == '__main__':
    main()
