import argparse

from pysam import FastaFile

from AS_quant.utils import LogTrack
from AS_quant.genome import get_annotation_db, Genome

        
def main():
    description = 'Generates a CSV file containing basic exon data derived from'
    description += ' annotation files, including  length, distance to end and '
    description += 'transcript start, reading frame, 3n and flanking intron '
    description += 'sizes using a GxF files with the annotation'
    
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('DB', help='GxF-DB containing exon annotation')

    options_group = parser.add_argument_group('options')
    options_group.add_argument('--genome', default=None,
                               help='Genome fasta file for sequence extraction')
    options_group.add_argument('--only_cds', default=False, action='store_true',
                               help='Use only CDS features')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    db_fpath = parsed_args.DB
    fasta_fpath = parsed_args.genome
    only_cds = parsed_args.only_cds
    out_fpath = parsed_args.output
    
    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    # Load annotation data
    log.write('Loading exon data from {}'.format(db_fpath))
    annotation_db = get_annotation_db(db_fpath)
    fastafile = None if fasta_fpath is None else FastaFile(fasta_fpath)
    genome = Genome(annotation_db, only_cds=only_cds, fastafile=fastafile)
    msg = '\tAnnotation loaded:\n\t{} genes\n\t{} exons'
    log.write(msg.format(genome.n_genes, genome.n_exons))

    # Find SJ for each annotated exon
    log.write('Writing exon data to {}...'.format(out_fpath))
    with open(out_fpath, 'w') as fhand:
        genome.write_exon_data(fhand)
    log.finish()


if __name__ == '__main__':
    main()
