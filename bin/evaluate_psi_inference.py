import argparse

from AS_quant.plot_utils import init_fig, savefig, arrange_plot
from AS_quant.utils import LogTrack

import numpy as np
import pandas as pd
import seaborn as sns
from scipy.stats.mstats_basic import pearsonr


def merge_inferred_and_real_psi(psi, real_psi, counts, gc_data=None,
                                exon_dist=None):
    psi.drop_duplicates(inplace=True, keep=False)
    real_psi.drop_duplicates(inplace=True, keep=False)
    common_ids = np.intersect1d(psi.index, real_psi.index)
    psi = psi.loc[common_ids, :]
    counts = counts.loc[common_ids, :]
    real_psi = real_psi.loc[common_ids, :]
    psi['real_psi'] = real_psi.loc[psi.index, :].iloc[:, 0]
    psi['counts'] = (counts['inc_reads'] + counts['skp_reads']).fillna(0)
    psi['psi_naive'] = counts['inc_reads'] / psi['counts']
    psi['binomial_inc'] = np.random.binomial(psi['counts'], psi['real_psi'])
    psi['p_hat'] = psi['binomial_inc'] / psi['counts']
    if gc_data is not None:
        psi['log_gc_pcr_bias'] = gc_data.loc[psi.index, 'log_gc_pcr_bias']
        psi['log_gc_ratio'] = gc_data.loc[psi.index, 'log_gc_ratio']
    if exon_dist is not None:
        psi['d_end'] = exon_dist.loc[psi.index, 'd_end']
        psi['exon_length'] = exon_dist.loc[psi.index, 'exon_length']
    return(psi)
    

def plot_error_vs_variable(error, values, axes, xlabel, min_error=None):
    common = np.logical_and(np.isnan(error) == False,
                            np.isnan(values) == False)
    rho = pearsonr(error[common], values[common])[0]
    axes.scatter(values, error, s=8, c='purple', label='Model',
                 edgecolor='white', linewidth=0.2)
    if min_error is not None:
        axes.scatter(values, min_error, s=8, c='darkorange',
                     label='Binomial', edgecolor='white', linewidth=0.2)
    xlims = axes.get_xlim()
    axes.plot(xlims, (0,0), linewidth=0.5, c='grey', alpha=0.5)
    arrange_plot(axes, despine=False, xlabel=xlabel,
                 ylabel=r'$\hat\Psi - \Psi_{real}$', xlims=xlims,
                 showlegend=False)
    if min_error is not None:
        axes.legend(loc=0, fontsize=7, frameon=True, fancybox=True)
    axes.set_title(r'$\rho$ = {:.2f}'.format(rho), fontsize=8)

    
def plot_psi_inference_evaluation(psi, out_fpath):
    inferred, real, p_hat = psi.iloc[:, 0], psi['real_psi'], psi['p_hat']
    log_depth, psi_naive = np.log(psi['counts']), psi['psi_naive']
    min_error = p_hat - real
    rho_max = pearsonr(p_hat, real)[0]
    rho_min = pearsonr(psi_naive, real)[0]
    min_mae = np.mean(np.abs(min_error))
    max_mae = np.mean(np.abs(psi_naive - real))
    error = inferred - real
    mae = np.mean(np.abs(error))
    rho = pearsonr(inferred, real)[0]
    data = pd.DataFrame({'rho': [rho, rho_max, rho_min],
                         'mae': [mae, min_mae, max_mae],
                         'label': ['Model', 'Binomial', r'Inc/Total']})
    xorder = ['Binomial', 'Model', r'Inc/Total']
    
    # Init fig
    fig, subplots = init_fig(4, 2, colsize=2.5, rowsize=2)
    
    # Plot correlation
    axes = subplots[0][0]
    axes.plot((0,1), (0,1), linewidth=0.5, c='grey', alpha=0.5)
    axes.scatter(real, inferred, s=8, c='purple',
                 edgecolor='white', linewidth=0.2)
    arrange_plot(axes, xlims=(0, 1), ylims=(0, 1), despine=False,
                 xlabel=r'$\Psi_{real}$', ylabel=r'$\hat\Psi$')
    axes.text(0.05, 0.9, r'$\rho$ = {:.2f}'.format(rho))
    
    # Plot error
    axes = subplots[0][1]
    plot_error_vs_variable(error, real, axes, xlabel=r'$\Psi_{real}$')
    axes.set_xlim((0, 1))
    axes.set_ylim((-1, 1))
    axes.text(0.05, 0.8, r'MAE = {:.2f}'.format(mae))
    
    # Plot correlation by method
    axes = subplots[1][0]
    sns.barplot(x='label', y='mae', data=data, order=xorder, edgecolor='black',
                n_boot=1, color='purple', linewidth=1, ax=axes)
    arrange_plot(axes, despine=False, xlabel='',
                 ylabel=r'MAE', showlegend=False,
                 rotate_xlabels=True, rotation=45)
    
    # Plot error against coverage
    plot_error_vs_variable(error, log_depth, subplots[1][1],
                           xlabel=r'log(Total counts)', min_error=min_error)
    
    # Plot error against gc_ratio
    axes = subplots[2][0]
    plot_error_vs_variable(error, psi['log_gc_ratio'], axes,
                           xlabel='log(GC-ratio)', min_error=min_error)
    
    # Plot error against pcr bias
    axes = subplots[2][1]
    plot_error_vs_variable(error, psi['log_gc_pcr_bias'], axes,
                           xlabel='log(GC-PCR bias)', min_error=min_error)
    
    # Plot error against exon length
    axes = subplots[3][0]
    plot_error_vs_variable(error, psi['exon_length'], axes,
                           xlabel='Exon length (bp)', min_error=min_error)
    axes.set_xlim(0, 300)
    
    # Plot error distance to end
    axes = subplots[3][1]
    plot_error_vs_variable(error, psi['d_end'], axes,
                           xlabel='Distance to transcript end (bp)',
                           min_error=min_error)
    
    # Save plot
    savefig(fig, out_fpath)
    
    
def main():
    description = 'This script compares the inferred PSI values to the real '
    description += 'ones used for simulating RNA-seq reads'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('inferred_psi', help='File with inferred PSIs')
    input_group.add_argument('-r', '--real_psi', required=True,
                             help='File containing real PSI values')
    input_group.add_argument('-c', '--exon_counts', required=True,
                             help='File containing exon read counts')
    input_group.add_argument('-d', '--exon_dist', required=True,
                             help='File containing exon lengths and distances')
    input_group.add_argument('-g', '--gc_bias', required=True,
                             help='File containing exon GC-bias data')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    inferred_psi_fpath = parsed_args.inferred_psi
    real_psi_fpath = parsed_args.real_psi
    counts_fpath = parsed_args.exon_counts
    dist_fpath = parsed_args.exon_dist
    gc_bias_fpath = parsed_args.gc_bias
    out_fpath= parsed_args.output

    # Init log
    log = LogTrack()

    # Load input data
    log.write('Loading data...')
    psi = pd.read_csv(inferred_psi_fpath, index_col='exon_id')
    log.write('\tInferred PSI data loaded from {}'.format(inferred_psi_fpath))
    real_psi = pd.read_csv(real_psi_fpath, index_col='exon_id')
    log.write('\tReal PSI data loaded from {}'.format(real_psi_fpath))
    counts = pd.read_csv(counts_fpath, index_col='exon_id')
    log.write('\tExon counts data loaded from {}'.format(counts_fpath))
    gc_data = pd.read_csv(gc_bias_fpath, index_col='exon_id')
    log.write('\tGC bias data loaded from {}'.format(gc_bias_fpath))
    exon_dist = pd.read_csv(dist_fpath, index_col='exon_id')
    log.write('\tExon distances data loaded from {}'.format(dist_fpath))
    merged = merge_inferred_and_real_psi(psi, real_psi, counts, gc_data,
                                         exon_dist)
    log.write('Tables merged')
    plot_psi_inference_evaluation(merged, out_fpath)
    log.write('Evaluation figure plotted at {}'.format(out_fpath))

    # Load reference tree
    log.write('Finished')


if __name__ == '__main__':
    main()
