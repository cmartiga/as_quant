import argparse
from AS_quant.utils import LogTrack
from AS_quant.genome import get_annotation_db, Genome

        
def main():
    description = 'Builds DB object from a GTF annotation file. This database '
    description += 'can be used to query genome annotations using gffutils'
    
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('GxF', help='GxF containing exon annotation')

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-f', '--force', default=False,
                               action='store_true',
                               help='Create database even if it exists')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    gxf_fpath = parsed_args.GxF
    force = parsed_args.force
    db_fpath = parsed_args.output
    
    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    # Load annotation data
    log.write('Loading exon data from {}'.format(gxf_fpath))
    annotation_db = get_annotation_db(db_fpath, gxf_fpath, force=force)
    genome = Genome(annotation_db)
    msg = '\tAnnotation index:\n\t{} genes\n\t{} exons'
    log.write(msg.format(genome.n_genes, genome.n_exons))
    log.finish()


if __name__ == '__main__':
    main()
