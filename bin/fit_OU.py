#!/usr/bin/env python
import argparse

import dendropy
import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.models.phylo import BasicOUmodel
from AS_quant.bin_utils import (add_counts_arguments, add_design_arguments,
                                add_log_bias_arguments, add_stan_arguments, 
                                add_output_prefix_argument, add_tree_argument,
                                read_log_bias, read_counts)


def main():
    description = 'Infer the parameters of an Ornstein-Uhlenbeck (OU) process '
    description += 'underlying the evolution of exon PSIs derived from RNA-seq '
    description += 'experiments along a given phylogeny. It assumes that all '
    description += 'elements evolve under a common regime to pool information '
    description += 'across all provided elements and takes into account the '
    description += 'discrete nature of the data using Binomial distribution'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    add_counts_arguments(input_group)
    add_tree_argument(input_group)
    add_design_arguments(input_group)
    add_log_bias_arguments(parser)
    add_stan_arguments(parser)
    add_output_prefix_argument(parser)

    # Parse arguments
    parsed_args = parser.parse_args()
    inclusion_fpath = parsed_args.inclusion
    total_fpath = parsed_args.total
    design_fpath = parsed_args.design
    phylogeny_fpath = parsed_args.phylogenetic_tree
    samples_col = parsed_args.samples
    ids_fpath = parsed_args.ids

    # stan options
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)
    recompile = parsed_args.recompile

    # Bias options
    log_bias_fpath = parsed_args.log_bias
    log_bias_def = parsed_args.log_bias_def

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()

    # Load sample and species data
    design = pd.read_csv(design_fpath).drop_duplicates(subset=[samples_col])
    design.set_index(samples_col, inplace=True)
    tree = dendropy.Tree.get(path=phylogeny_fpath, schema='newick')

    ids = None
    if ids_fpath is not None:
        ids = [line.strip() for line in open(ids_fpath)]

    # Load data
    inclusion, total = read_counts(inclusion_fpath, total_fpath, design.index,
                                   sel_ids=ids)
    design = design.reindex(total.columns)
    n_species = design['species'].unique().shape[0]
    log_bias = read_log_bias(log_bias_fpath, exon_ids=total.index,
                             sample_ids=total.columns,
                             log_bias_def=log_bias_def, log=log)
    msg = 'Loaded {} elements with {} samples from a total of {} species'
    log.write(msg.format(total.shape[0], total.shape[1], n_species))

    # Load model
    msg = 'Loading model and performing MCMC sampling'
    log.write(msg)
    log.write('Using a global OU model with a single optimal value')
    ou_model = BasicOUmodel(tree, design, obs_model='logit_binomial',
                            recompile=recompile)
    ou_model.observations.load(inclusion, total, log_bias)

    # Fit model
    ou_model.fit(chains=n_chains, n_iter=n_samples)
    ou_model.write_fit(prefix=output_prefix)
    log.write('Results saved at {}*'.format(output_prefix))
    log.finish()


if __name__ == '__main__':
    main()
