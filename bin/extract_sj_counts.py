import argparse

from AS_quant.utils import LogTrack
from AS_quant.genome import get_annotation_db, Genome
from AS_quant.bam_process import load_bamfile

        
def main():
    description = 'Generates a CSV file containing the number of reads mapping '
    description += 'across every position Splice Junctions in a BAM file'
    
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('DB', help='GxF-DB containing exon annotation')
    input_group.add_argument('BAM', help='Indexed BAM file')

    options_group = parser.add_argument_group('Options')
    help_msg = 'Tabix indexed file with found SJ for expanding the set of SJ '
    help_msg += 'that support inclusion and skipping taking into account '
    help_msg += 'alternative splice sites within a window of length -l'
    options_group.add_argument('-r', '--read_length', required=True, type=int,
                               help='Sequencing read length')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    db_fpath = parsed_args.DB
    bam_fpath = parsed_args.BAM
    read_length = parsed_args.read_length
    out_fpath = parsed_args.output
    
    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    # Load annotation data
    log.write('Loading exon data from {}'.format(db_fpath))
    annotation_db = get_annotation_db(db_fpath)
    genome = Genome(annotation_db)
    bamfile = load_bamfile(bam_fpath)
    msg = '\tAnnotation loaded:\n\t{} genes\n\t{} exons'
    log.write(msg.format(genome.n_genes, genome.n_exons))

    # Find SJ for each annotated exon
    log.write('Writing SJ counts to {}...'.format(out_fpath))
    sj_profiles = genome.calc_mapping_profile(bamfile, read_length=read_length)
    with open(out_fpath, 'w') as fhand:
        genome.write_mapping_profiles(sj_profiles, fhand, log=log)
    log.finish()


if __name__ == '__main__':
    main()
