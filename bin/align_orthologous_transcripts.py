import argparse

from AS_quant.utils import LogTrack
from AS_quant.orthologs import GeneSetCollection


def main():
    description = 'Aligns orthologous transcripts using MAFFT as a step '
    description += 'in the assignment of exon orthology relationships'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'Directory containing fasta files with unaligned transcript'
    help_msg += ' orthologs'
    input_group.add_argument('-f', '--fasta_dir', required=True,
                             help=help_msg)
    help_msg = 'File containing gene_ids (If not provided all fasta files in '
    help_msg += 'seqs_dir will be aligned)'
    input_group.add_argument('-g', '--gene_ids', default=None, help=help_msg)

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-m', '--method', default='muscle',
                               help='MSA method {muscle (def), mafft}')
    options_group.add_argument('--intron_score', default=10, type=int,
                               help='Score for matching intron position (10)')
    options_group.add_argument('--gap_opening', default=-10, type=int,
                               help='Gap opening penalty (-10)')
    options_group.add_argument('--gap_extension', default=-1, type=int,
                               help='Gap extension penalty (-1)')
    
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output directory for MSA fasta files')

    # Parse arguments
    parsed_args = parser.parse_args()
    fasta_dir = parsed_args.fasta_dir
    genes_fpath = parsed_args.gene_ids
    gene_set_ids = None
    if genes_fpath is not None:
        gene_set_ids = [line.strip() for line in open(genes_fpath)]
    
    out_dir = parsed_args.output
    method = parsed_args.method
    gap_opening = parsed_args.gap_opening
    gap_extension = parsed_args.gap_extension
    gap_penalty = (gap_opening, gap_extension)
    intron_score = parsed_args.intron_score

    # Init log
    log = LogTrack()

    # Perform MSA for each gene set
    log.write('Aligning orthologous transcripts...')
    log.write('Save alignments at {}'.format(out_dir))
    gene_set_collection = GeneSetCollection(fasta_dir=fasta_dir,
                                            gene_set_ids=gene_set_ids,
                                            log=log)
    gene_set_collection.align(method=method, gap_penalty=gap_penalty,
                              intron_score=intron_score)
    gene_set_collection.write_fasta(out_dir)
    log.write('Finished')


if __name__ == '__main__':
    main()
