import argparse
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile

import pandas as pd
from AS_simulation.utils import LogTrack
from AS_simulation.settings import DEF_RUNFILE


def add_fasta_quant(transcriptome_fpath, transcripts_quant):
    for line in open(transcriptome_fpath):
        if line.startswith('>'):
            line = line.strip()
            tid = line.lstrip('>')
            line = '{}${}\n'.format(line, transcripts_quant.get(tid, 0))
        yield(line)


def main():
    description = 'Creates a transcript annotation GFF and FASTA based'
    description += 'only on exon skipping event_counts from the longest'
    description += ' annotated transcript'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('transcriptome', help='Transcriptome fasta file')
    input_group.add_argument('-t', '--transcripts_quant', required=True,
                             help='Transcript quantifications files')

    options_group = parser.add_argument_group('RNA-seq options')
    options_group.add_argument('-p', '--paired_end', default=False,
                               action='store_true',
                               help='Simulate paired end reads')
    options_group.add_argument('-n', '--million_reads', default=20, type=float,
                               help='Million fragments to simulate (20M)')
    options_group.add_argument('-l', '--read_length', default=100,
                               help='Read length (100)', type=int)
    options_group.add_argument('-r', '--runfile', default=DEF_RUNFILE,
                               help='simNGS runfile (s_8_4x.runfile)')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')
    output_group.add_argument('-f', '--fasta_file_quant', default=None,
                              help='Intermediate fasta file with quants')

    # Parse arguments
    parsed_args = parser.parse_args()
    fasta_fpath = parsed_args.transcriptome
    transcripts_fpath = parsed_args.transcripts_quant
    million_reads = parsed_args.million_reads
    paired_end = parsed_args.paired_end
    read_length = parsed_args.read_length
    out_fpath = parsed_args.output
    runfile = parsed_args.runfile
    fasta_file_quant_fpath = parsed_args.fasta_file_quant

    # Run
    log = LogTrack()
    data = pd.read_csv(transcripts_fpath, index_col=0)
    transcripts_quant = data['tpms'].to_dict()
    msg = 'Loaded transcript quantifications from {}'.format(transcripts_fpath)
    log.write(msg)
    log.write('\t{} transcripts loaded'.format(data.shape[0]))
    fasta_quant = add_fasta_quant(fasta_fpath, transcripts_quant)

    if fasta_file_quant_fpath is None:
        fasta_fhand = NamedTemporaryFile()
    else:
        fasta_fhand = open(fasta_file_quant_fpath, 'wb')

    msg = 'Storing transcript quantifications in fasta at {}'
    log.write(msg.format(fasta_fhand.name))
    for line in fasta_quant:
        fasta_fhand.write(line.encode('utf8'))

    # Call rlsim
    cmd = ['rlsim', '-n', str(int(million_reads * 1e6)), fasta_fhand.name]
    log.write('Calling rlsim...')
    log.write('\t {}'.format(' '.join(cmd)))
    
    if fasta_file_quant_fpath is None:
        rlsim = Popen(cmd, stdout=PIPE)
    else:
        cmd_fpath = '{}.sim.sh'.format(out_fpath)
        cmd_fhand = open(cmd_fpath, 'w')
        cmd_fhand.write(' '.join(cmd) + ' | ')

    cmd = ['simNGS', '-o', 'fastq', '-O', out_fpath, '-n',
           str(read_length), runfile]
    if paired_end:
        cmd.extend(['-p', 'paired'])
        
    if fasta_file_quant_fpath is None:
        simngs = Popen(cmd, stdin=rlsim.stdout)
        log.write('Calling simNGS...')
        log.write('\t {}'.format(' '.join(cmd)))
        simngs.communicate()
    else:
        cmd_fhand.write(' '.join(cmd))
    log.finish()


if __name__ == '__main__':
    main()
