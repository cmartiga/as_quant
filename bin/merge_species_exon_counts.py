#!/usr/bin/env python
import argparse

import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.event_counts import EventsCounts


def main():
    description = 'Merges exon inclusion and skipping counts from different '
    description += 'species into a single matrix for each count type using '
    description += 'exon orthology relationships provided by the user'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'File containing samples to merge. '
    help_msg += 'You may include several species by including a column with'
    help_msg += 'the for each sample. A file for each species will be provided'
    input_group.add_argument('input_prefix',
                             help='Prefix for species counts files')
    input_group.add_argument('-e', '--orthologous_exons', required=True,
                             help='CSV File containing orthologous exons')

    options_group = parser.add_argument_group('Options')
    help_msg = 'File containing species names to merge. It uses all species in '
    help_msg += 'the orthologs file otherwise'
    options_group.add_argument('-s', '--species', default=None, help=help_msg)
    help_msg = 'Column header for run in case to sum counts from the same sample'
    options_group.add_argument('--add_sp_to_sample', default=False,
                               action='store_true',
                               help='Add species to sample name')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file/prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    exon_counts_prefix = parsed_args.input_prefix
    orthologs_fpath = parsed_args.orthologous_exons
    species_fpath = parsed_args.species
    output = parsed_args.output
    add_sp_to_sample = parsed_args.add_sp_to_sample

    # Run program
    log = LogTrack()
    log.write('Start')
    exon_orthologs = pd.read_csv(orthologs_fpath, dtype={'exon_set_id': str})
    exon_orthologs.set_index('exon_set_id', inplace=True)
    log.write('Loaded orthologous exons from {}'.format(orthologs_fpath))
    
    species_set = [col for col in exon_orthologs.columns if '_id' not in col]
    if species_fpath is not None:
        species_set = [line.strip() for line in open(species_fpath)]
    log.write('Merging data from {} species'.format(len(species_set)))
    
    species_counts_dict = {}
    for species in species_set:
        event_counts_species = EventsCounts()
        species_prefix = '{}.{}'.format(exon_counts_prefix, species)
        event_counts_species.load_counts(prefix=species_prefix)
        species_counts_dict[species] = event_counts_species
    event_counts = EventsCounts()
    event_counts.log = log
    event_counts.merge_species(species_counts_dict, exon_orthologs,
                               add_sp_to_sample_ids=add_sp_to_sample)
    event_counts.write(output)
    log.finish()


if __name__ == '__main__':
    main()
