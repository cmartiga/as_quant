import argparse
from AS_quant.utils import LogTrack
from AS_quant.genome import get_annotation_db, Genome


        
def main():
    description = 'Generates a CSV file containing length, distance to end and '
    description += 'transcript start of each exon belonging to the longest tran'
    description += 'script using a GxF files with the annotation'
    
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('DB', help='GxF-DB containing exon annotation')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    db_fpath = parsed_args.DB
    out_fpath = parsed_args.output
    
    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    # Load annotation data
    log.write('Loading exon data from {}'.format(db_fpath))
    annotation_db = get_annotation_db(db_fpath)
    genome = Genome(annotation_db)
    msg = '\tAnnotation loaded:\n\t{} genes\n\t{} exons'
    log.write(msg.format(genome.n_genes, genome.n_exons))

    # Find SJ for each annotated exon
    log.write('Writing SJs to {}...'.format(out_fpath))
    with open(out_fpath, 'w') as fhand:
        genome.write_exons_distances(fhand)
    log.finish()


if __name__ == '__main__':
    main()
