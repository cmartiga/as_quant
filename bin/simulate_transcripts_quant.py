import argparse

from pysam import FastaFile

from AS_simulation.expression import derive_transcripts_quant
from AS_simulation.utils import LogTrack, parse_gff
import pandas as pd
import numpy as np
from AS_quant.utils import fetch_internal_exon_ids
from AS_simulation.psi import simulate_exon_psi


def main():
    description = 'Simulates transcript quantifications based on a '
    description += 'certain distribution of exon inclusion rates. It uses'
    description += 'annotation created by get_SE_transcripts.py script'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('genome', help='Genome fasta file')
    input_group.add_argument('GFF', help='GFF file with transcript annotation')

    options_group = parser.add_argument_group('PSI options')
    options_group.add_argument('-m', '--logit_mean', default=5, type=float,
                               help='Mean of exon logit-PSI (5)')
    options_group.add_argument('-s', '--logit_sd', default=3, type=float,
                               help='Standard deviation of exon logit-PSI (3)')
    help_msg = 'Use pre-set exon PSIs from this file. Missing exons are assumed'
    help_msg += ' to be constitutive'
    options_group.add_argument('--psi', default=None, help=help_msg)
    help_msg = 'Max number of exons that can be skipped in a transcript'
    help_msg += ' simultaneously (3)'
    options_group.add_argument('--max_skipped', default=3, type=int,
                               help=help_msg)
    help_msg = 'Proportion of alternative exons (0.1)'
    options_group.add_argument('--p_alternative', default=0.1, type=float,
                               help=help_msg)

    options_group = parser.add_argument_group('TPMs options')
    options_group.add_argument('-mu', '--log_mean', default=3, type=float,
                               help='Mean of exon log-TPMs (3)')
    options_group.add_argument('-sigma', '--log_sd', default=1, type=float,
                               help='Standard deviation of log-TPMs (1)')
    help_msg = 'Use pre-set gene TPMs from this file. Expression from missing'
    help_msg += ' genes is assumed to be 0.'
    options_group.add_argument('--tpms', default=None, help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-q', '--quantification', required=True,
                              help='Output quantification file')
    help_msg = 'Output exon PSI file. Small modifications are made due to the '
    help_msg += 'limited number of exon skipping combinations that are taken '
    help_msg += 'into account. Increase --max_skipped to reduce difference with'
    help_msg += 'the file provided with --psi'
    output_group.add_argument('-P', '--exon_PSI', required=True,
                              help=help_msg)
    output_group.add_argument('-t', '--transcriptome_fasta', required=True,
                              help='Output transcripts fasta file')

    # Parse arguments
    parsed_args = parser.parse_args()
    gff_fpath = parsed_args.GFF
    quant_fpath = parsed_args.quantification
    transcripts_fpath = parsed_args.transcriptome_fasta
    genome_fpath = parsed_args.genome
    p_alternative = parsed_args.p_alternative

    logit_mean = parsed_args.logit_mean
    logit_sd = parsed_args.logit_sd
    log_mean = parsed_args.log_mean
    log_sd = parsed_args.log_sd
    max_skipped = parsed_args.max_skipped
    psi_fpath = parsed_args.psi
    tpms_fpath = parsed_args.tpms
    psi_out_fpath = parsed_args.exon_PSI

    # Load genome sequence and annotation
    log = LogTrack()
    log.write('Loading annotation from {}'.format(gff_fpath))
    sep = '=' if gff_fpath.split('.')[-1] in ['gff', 'gff3'] else ' '
    with open(gff_fpath) as fhand:
        parsed_gff = parse_gff(fhand, sep=sep)
    log.write('Loaded data from {} genes'.format(len(parsed_gff)))
    log.write('Loading genome from {}'.format(genome_fpath))
    genome = FastaFile(genome_fpath)

    # Load or simulate gene expression and exon PSI
    exon_ids = list(fetch_internal_exon_ids(parsed_gff))
    if psi_fpath is None:
        psi = simulate_exon_psi(logit_mean, logit_sd, p_alternative,
                                n_exons=len(exon_ids))
        psi = dict(zip(exon_ids, psi))
    else:
        log.write('Loading PSIs from {}'.format(psi_fpath))
        psi = pd.read_csv(psi_fpath, index_col=0)['psi'].to_dict()

    gene_ids = parsed_gff.keys()
    if tpms_fpath is None:
        tpms = np.exp(np.random.normal(log_mean, log_sd, size=len(gene_ids)))
        tpms = dict(zip(gene_ids, tpms))
    else:
        log.write('Loading TPMs from {}'.format(tpms_fpath))
        tpms = pd.read_csv(tpms_fpath, index_col=0)['tpms'].to_dict()
    
    # Init output files
    log.write('Generating quantifications')
    transcripts_fhand = open(transcripts_fpath, 'w')
    log.write('\tSaving transcript sequences at {}'.format(transcripts_fpath))
    quant_fhand = open(quant_fpath, 'w')
    quant_fhand.write('transcript,tpm,psi\n')
    log.write('\tSaving transcript sequences at {}'.format(quant_fpath))
    psi_out_fhand = open(psi_out_fpath, 'w')
    psi_out_fhand.write('exon_id,psi\n')
    log.write('\tSaving new exons PSIs at {}'.format(psi_out_fpath))

    # Derive transcripts quantification and fasta file from simulation
    res = derive_transcripts_quant(parsed_gff, genome, exons_psi=psi, tpms=tpms,
                                   max_skipped=max_skipped)
    for transcript_records, exons_psi in res:
        
        for record in transcript_records:
            line = '>{}${}\n{}\n'.format(record['transcript_id'], record['tpm'],
                                         record['seq'])
            transcripts_fhand.write(line)
            line = '{},{},{}\n'.format(record['transcript_id'], record['tpm'],
                                       record['psi'])
            quant_fhand.write(line)
        
        for exon_id, exon_psi in exons_psi.items():
            line = '{},{}\n'.format(exon_id, exon_psi)
            psi_out_fhand.write(line)

    log.finish()


if __name__ == '__main__':
    main()
