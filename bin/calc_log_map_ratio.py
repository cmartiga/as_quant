import argparse

from pysam import FastaFile
import pandas as pd

from AS_quant.utils import (LogTrack, parse_gff, get_gtf_longest_transcripts,
                            get_seq, GC_CONTENT)
from bin.get_sp_exons_sj import fetch_exons
import numpy as np
from scipy.stats._discrete_distns import nbinom
import time
from scipy.stats.kde import gaussian_kde


def get_exons_seqs(chrom, exons, genome):
    return([get_seq(genome, chrom, start, end, strand='+')
            for start, end in exons])
    
    
def get_fragment_size_distrib(empirical_fragments_fpath, fragment_length_mean,
                              fragment_length_sd, read_length, adaptor_len,
                              fragment_length_min):
    if empirical_fragments_fpath is None:
        fragment_lengths, weights = get_fragment_lengths(fragment_length_mean,
                                                         fragment_length_sd,
                                                         read_length,
                                                         min_frag_len=fragment_length_min)
    else:
        fragments = pd.read_csv(empirical_fragments_fpath, sep='\t',
                                names=['l', 'r', 'n'], header=None)
        fragments['l'] = fragments['l'] + 2*(read_length + adaptor_len) 
        fragments = fragments[fragments['n'] > 0]
        weights = fragments['n'] / fragments['n'].sum()
        kde = gaussian_kde(fragments['l'], weights=weights)
        fragment_lengths = np.arange(max(fragment_length_min, fragments['l'].min()),
                                     fragments['l'].max())
        weights = kde.pdf(fragment_lengths)
        weights = weights / weights.sum()
    return(fragment_lengths, weights)
    
    
def get_transcript_fragment_seq(exons, length, seq,
                                go_upstream=False):
    seq_len = len(seq)
    if seq_len >= length or len(exons) == 0:
        return(seq)
    else:
        if go_upstream:
            remaining_len = max(length - seq_len, 0)
            seq = exons[-1][-remaining_len:] + seq
            return(get_transcript_fragment_seq(exons[:-1], length, seq, 
                                               go_upstream))
        else:
            remaining_len = min(length - seq_len, len(exons[0]))
            seq = seq + exons[0][:remaining_len]
            return(get_transcript_fragment_seq(exons[1:], length, seq, 
                                               go_upstream))


def calc_pcr_eff(gc, min_eff, max_eff, alpha):
    eff = min_eff + (max_eff - min_eff) * (1 - gc ** alpha) ** alpha
    return(eff)


def get_meta_fragments(upstream, exon, downstream, fragment_len, overhang,
                       def_seq=''):
    extra_len = fragment_len - overhang
    seq1 = get_transcript_fragment_seq(upstream, length=extra_len, seq=def_seq,
                                       go_upstream=True)
    seq2 = get_transcript_fragment_seq(downstream, length=extra_len, seq=def_seq,)
    inc_max_fragment = seq1 + exon + seq2
    start = len(seq1)
    end = start + len(exon)
    skp_max_fragment = seq1 + seq2
    return(inc_max_fragment, skp_max_fragment, start, end)


def get_sj_overlap(meta_fragment_len, sj_pos, fragment_len, read_len, overhang):
    '''Returns a vector of True/False for positions in which a fragment with
       reads overlapping the SJs starts
    '''
    sj_overlap = np.full(meta_fragment_len, fill_value=False)
    
    for pos in sj_pos:
        # Fragment ends maps to SJ
        start = max(0, pos - fragment_len + overhang)
        end = max(0, pos - fragment_len + read_len - overhang + 1)
        sj_overlap[start:end] = True 
        
        # Fragment start maps to SJ
        start = max(0, pos - read_len + overhang)
        end = max(0, pos - overhang + 1)
        sj_overlap[start:end] = True
    
    # In case of short last exon
    max_position = max(0, meta_fragment_len - fragment_len - overhang + 2)
    sj_overlap[max_position:] = False
         
    return(sj_overlap)

def count_gc(fragment):
    return(fragment.count('G') + fragment.count('C'))


def calc_gc(fragment):
    return(count_gc(fragment) / float(len(fragment)))


def calc_fragments_gc_new(meta_fragment, sj_overlap, fragment_len):
    idxs = np.where(sj_overlap)[0]
    if idxs.shape[0] == 0:
        return(np.array([]))
    prev_idx = idxs[0]
    seq0 = meta_fragment[idxs[0]:idxs[0]+fragment_len]
    prev_base = GC_CONTENT.get(seq0[0], 0)
    gcs = [count_gc(seq0)]
    prev_gc = gcs[-1]
    
    for start in idxs[1:]:
        end = start + fragment_len
        base = GC_CONTENT.get(meta_fragment[end], 0)
        if prev_idx == start - 1:
            gc = prev_gc - prev_base + base
        else:
            gc = count_gc(meta_fragment[start:end]) 
        
        gcs.append(gc)
        prev_gc = gc
        prev_base = base
        prev_idx = start
        
    return(np.array(gcs) / float(fragment_len))
    


def calc_fragments_gc(meta_fragment, sj_overlap, fragment_len):
    return(np.array([calc_gc(meta_fragment[start:start + fragment_len])
                     for start in np.where(sj_overlap)[0]]))


def get_fragment_lengths(fragment_length_mean, fragment_length_sd,
                         read_length, min_frag_len=0, q=0.01):
    # Fragment lengths drawn from NB distribution
    p = fragment_length_mean / fragment_length_sd**2
    n = fragment_length_mean**2 / (fragment_length_sd**2 - fragment_length_mean)
    distrib = nbinom(p=p, n=n)
    min_frag_len = max(min_frag_len, read_length)
    max_len = distrib.ppf(1 - q)
    fragment_lengths = np.arange(min_frag_len, max_len).astype(int)
    weights = distrib.pmf(fragment_lengths)
    weights = weights / weights.sum() # Normalize to sum to 1
    return(fragment_lengths, weights)


def get_exons(parsed_gff, genome):
    
    for chrom, strand, _, exons in get_gtf_longest_transcripts(parsed_gff):
        n_exons = len(exons)
        if n_exons < 3:
            continue
        exon_seqs = get_exons_seqs(chrom, exons, genome=genome)
        
        for i in range(1, n_exons - 1):  # Iterate over internal exons
            start, end = exons[i]
            exon_id = '{}:{}-{}{}'.format(chrom, start, end, strand)
            exon_data = {'exon_id': exon_id,
                         'exon': exon_seqs[i],
                         'upstream': exon_seqs[:i],
                         'downstream':exon_seqs[i + 1:]}
            yield(exon_data)


class BiasCalculator(object):
    def __init__(self, fragment_lengths, weights, max_fragment_len,
                 read_length, overhang, pcr_cycles, min_eff, max_eff, alpha):
        self.fragment_lengths = fragment_lengths
        self.weights = weights
        self.max_fragment_len = max_fragment_len
        self.read_length = read_length
        self.overhang= overhang
        self.pcr_cycles = pcr_cycles
        self.min_eff = min_eff
        self.max_eff = max_eff
        self.alpha = alpha
    
    def run_gc_pcr(self, gc, w):
        eff = calc_pcr_eff(gc, self.min_eff, self.max_eff, self.alpha)
        pcr = w * np.power(1 + eff, self.pcr_cycles)
        return(pcr)
    
    def try_log_funct_ratio(self, inc, skp, funct):
        try:
            return(np.log(funct(inc) / funct(skp)))
        except ZeroDivisionError:
            return(np.nan)
    
    def __call__(self, exon_data):
        # Get meta-fragments for inclusion and skipping events
        res = get_meta_fragments(exon_data['upstream'], exon_data['exon'],
                                 exon_data['downstream'],
                                 self.max_fragment_len, self.overhang) 
        inc_max_fragment, skp_max_fragment, exon_start, exon_end = res
        
        inc_gc = []
        skp_gc = []
        
        inc_w = []
        skp_w = []
        
        for frag_len, w in zip(self.fragment_lengths, self.weights):
            
            # Inclusion SJ overlapping positions
            inc_sj_overlap = get_sj_overlap(len(inc_max_fragment),
                                            [exon_start, exon_end],
                                            frag_len,
                                            self.read_length, self.overhang)
            frag_gc = calc_fragments_gc_new(inc_max_fragment, inc_sj_overlap, frag_len)
            inc_gc.append(frag_gc * w)
            inc_w.append([w] * frag_gc.shape[0])
            
            # Skipping SJ overlapping positions
            skp_sj_overlap = get_sj_overlap(len(skp_max_fragment), [exon_start],
                                            frag_len, self.read_length,
                                            self.overhang)
            frag_gc = calc_fragments_gc_new(skp_max_fragment, skp_sj_overlap, frag_len)
            skp_gc.append(frag_gc * w)
            skp_w.append([w] * frag_gc.shape[0])
    
        inc_w = np.concatenate(inc_w)
        skp_w = np.concatenate(skp_w)
        inc_gc = np.concatenate(inc_gc)
        skp_gc = np.concatenate(skp_gc)
        inc_pcr = self.run_gc_pcr(inc_gc, inc_w)
        skp_pcr = self.run_gc_pcr(skp_gc, skp_w)
    
        exon_data = {'exon_id': exon_data['exon_id'],
                     'log_gc_ratio': self.try_log_funct_ratio(inc_gc, skp_gc, funct=np.nanmean),
                     'log_gc_pcr_bias': self.try_log_funct_ratio(inc_pcr, skp_pcr, funct=np.nanmean),
                     'log_fragment_ratio': self.try_log_funct_ratio(inc_w, skp_w, funct=np.nansum)}
        return(exon_data)
    

def get_exon_tech_variables(parsed_gff, genome, read_length,
                            fragment_lengths, weights,
                            pcr_cycles, min_eff, max_eff,
                            alpha, overhang=1):
    max_fragment_len = fragment_lengths.max()
    exons = get_exons(parsed_gff, genome)
    _calc_bias = BiasCalculator(fragment_lengths, weights, max_fragment_len,
                                read_length, overhang, pcr_cycles, min_eff,
                                max_eff, alpha)
    for exon in exons:
        yield(_calc_bias(exon))
            
            
def main():
    description = 'Estimates log-mappability ratios for exon skipping '
    description += 'events depending on exon length, read length and '
    description += 'Splice Junction overhang. It also calculates GC content'
    description += ' of inclusion and skipping fragments of mapped reads '
    description += 'for estimating sample specific GC bias'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-g', '--gff', required=True,
                             help='GFF containing exon annotation')
    help_msg = 'Genome fasta file for calculation of log_GC_ratio if provided'
    input_group.add_argument('-G', '--genome', required=True, help=help_msg)
    input_group.add_argument('--gene_list', default=None,
                             help='Gene ids to filter')
    
    rnaseq_group = parser.add_argument_group('Sequencing Options')
    rnaseq_group.add_argument('-r', '--read_length', default=75, type=int,
                              help='Read length (75)')
    rnaseq_group.add_argument('-O', '--read_overhang', default=3, type=int,
                              help='Min read overhang for mapping (3)')
    rnaseq_group.add_argument('-A', '--adaptor_len', default=8, type=int,
                              help='Library adaptor length (8)')
    
    fragments_group = parser.add_argument_group('Fragmentation Options')
    fragments_group.add_argument('-f', '--fragment_length_mean',
                                 default=364, type=int,
                                 help='Mean fragment length used (364)')
    fragments_group.add_argument('-fs', '--fragment_length_sd',
                                 default=132, type=int,
                                 help='Fragment length standard deviation (132)')
    fragments_group.add_argument('-fm', '--fragment_length_min',
                                 default=None, type=int,
                                 help='Minimum fragment length used (read_length)')
    help_msg = 'Empirical inner size distribution as provided by RSeQC'
    fragments_group.add_argument('-F', '--empirical_fragments',
                                 default=None, help=help_msg)
    
    pcr_group = parser.add_argument_group('PCR Options')
    pcr_group.add_argument('-c', '--pcr_cycles', default=15, type=int,
                           help='Number of PCR amplification cycles (15, TrueSeq.v2)')
    pcr_group.add_argument('-m', '--min_eff', default=0.51, type=float,
                           help='Min amplification efficiency (0.51)')
    pcr_group.add_argument('-M', '--max_eff', default=1, type=float,
                           help='Max amplification efficiency (1)')
    pcr_group.add_argument('-a', '--alpha', default=4.95, type=float,
                           help='Shape parameter for GC bias (4.95)')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    gff_fpath = parsed_args.gff
    genome_fpath = parsed_args.genome
    genes_fpath = parsed_args.gene_list
    genes_subset = None
    if genes_fpath is not None:
        genes_subset = set([line.strip() for line in open(genes_fpath)])
    
    read_length = parsed_args.read_length
    overhang = parsed_args.read_overhang
    adaptor_len = parsed_args.adaptor_len
    empirical_fragments_fpath = parsed_args.empirical_fragments
    
    fragment_length_mean = parsed_args.fragment_length_mean
    fragment_length_sd = parsed_args.fragment_length_sd
    fragment_length_min = parsed_args.fragment_length_min
    if fragment_length_min is None:
        fragment_length_min = read_length + adaptor_len * 2 
    pcr_cycles = parsed_args.pcr_cycles
    min_eff = parsed_args.min_eff
    max_eff = parsed_args.max_eff
    alpha = parsed_args.alpha
    
    out_fpath = parsed_args.output

    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')

    # Load genome if provided
    log.write('Loading genome from {}'.format(genome_fpath))
    genome = FastaFile(genome_fpath)

    # Load annotation data
    log.write('Loading exon data from {}'.format(gff_fpath))
    with open(gff_fpath) as gff_fhand:
        sep = '=' if gff_fpath.split('.')[-1] == 'gff' else ' '
        parsed_gff = parse_gff(gff_fhand, store_transcripts=True, sep=sep,
                               genes_subset=genes_subset)
    exons = list(fetch_exons(parsed_gff))
    log.write('\tAnnotation loaded: {} exons'.format(len(exons)))

    # Get fragment sizes and weights
    fs = get_fragment_size_distrib(empirical_fragments_fpath, fragment_length_mean,
                                   fragment_length_sd, read_length, adaptor_len,
                                   fragment_length_min)
    fragment_lengths, weights = fs

    # Estimate factors
    exons_data = get_exon_tech_variables(parsed_gff, genome=genome,
                                         read_length=read_length,
                                         fragment_lengths=fragment_lengths,
                                         weights=weights, pcr_cycles=pcr_cycles,
                                         min_eff=min_eff, max_eff=max_eff,
                                         alpha=alpha, overhang=overhang)
    
    log.write('Processing started. Results are saved at {}'.format(out_fpath))
    with open(out_fpath, 'w') as fhand:
        fieldnames = ['exon_id', 'log_gc_ratio', 'log_fragment_ratio',
                      'log_gc_pcr_bias']
        fhand.write(','.join(fieldnames) + '\n')
        t0 = time.time()
        for i, exon_data in enumerate(exons_data):
            fhand.write(','.join([str(exon_data[fd]) for fd in fieldnames]) + '\n')
            if  i % 100 == 0 and i > 0:
                msg = '\tExons processed: {}. Estimated remaining time: {} min'
                t = time.time() - t0
                speed = i / t * 60 # per minute
                remaining = len(exons) - i
                log.write(msg.format(i, int(remaining / speed)))
                fhand.flush()
    log.finish()


if __name__ == '__main__':
    main()
