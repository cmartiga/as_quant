#!/usr/bin/env python
import argparse

from AS_quant.utils import LogTrack
from AS_quant.event_counts import EventsCounts
from AS_quant.settings import RMATS_TYPES, ES


def main():
    description = 'Generates a tables with inclusion, skipping and total reads'
    description += ' together with log_bias data from the output of different'  
    description += ' programs. These matrices can then be used for direct'
    description += ' modeling AS conditioning on counts data as observations'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='rMATS output file')
    parser.add_argument('-p', '--program', default='rmats',
                        help='Program used to produce results [rmats, darts] (rmats)')
    parser.add_argument('-s', '--sample_names', default=None,
                        help='File containing ordered sample names')
    parser.add_argument('-e', '--event_type', default='exon_skipping',
                        help='AS event type: {} ()'.format(RMATS_TYPES, ES))
    parser.add_argument('-o', '--output', required=True,
                        help='Output file prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    program = parsed_args.program
    output_prefix = parsed_args.output
    samples_fpath = parsed_args.sample_names
    event_type = parsed_args.event_type

    sample_names = None
    if samples_fpath is not None:
        sample_names = [line.strip() for line in open(samples_fpath)]

    # Init logger
    log = LogTrack()
    log.write('Starting...')
    counts = EventsCounts()
    log.write('Parsing {} results'.format(program))
    counts.parse(in_fpath, program=program, sample_names=sample_names,
                 event_type=event_type)
    log.write('Writting files to {}.*'.format(output_prefix))
    counts.write(output_prefix)

    # Read data
    log.write('Done')


if __name__ == '__main__':
    main()
