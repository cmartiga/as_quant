import argparse

from AS_quant.utils import LogTrack
from AS_quant.junctions import SpliceJunctionCounts
from AS_quant.event_counts import EventsCounts
from AS_quant.genome import ExonsSJReader


def main():
    description = 'Builds matrices for inclusion and skipping counts across'
    description += ' provided exons and their corresponding splice junction '
    description += 'counts. These counts can be obtained with merge_sj_counts'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('-s', '--sj_counts', required=True,
                             help='File containing SJ counts')
    input_group.add_argument('-e', '--sj_exons', required=True,
                             help='File containing inclusion and skipping SJ')

    input_group = parser.add_argument_group('Options')
    help_msg = 'Prefix for files containing sample-specific exon inclusion '
    help_msg += 'log-bias calculations (from calc_frag_size_bias)'
    input_group.add_argument('--log_bias_dir', default=None, help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    exons_sj_fpath = parsed_args.sj_exons
    sj_counts_fpath = parsed_args.sj_counts
    log_bias_dir = parsed_args.log_bias_dir
    output_prefix = parsed_args.output

    # Load SJ data
    log = LogTrack()
    log.write('Start analysis')
    sj_counts = SpliceJunctionCounts()
    sj_counts.read_csv(sj_counts_fpath)
    log.write('Loaded {} SJ for {} samples'.format(sj_counts.n_sj,
                                                   sj_counts.n_samples))
    
    with open(exons_sj_fpath) as fhand:
        events_sj = ExonsSJReader(fhand)
        exon_counts = EventsCounts()
        exon_counts.build_from_sj_counts(sj_counts, events_sj)
        if log_bias_dir is not None:
            exon_counts.read_log_bias(log_bias_dir)
        exon_counts.write(output_prefix)
    
    log.finish()


if __name__ == '__main__':
    main()
