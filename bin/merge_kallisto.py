#!/usr/bin/env python
from os.path import join, exists
import argparse

from AS_quant.utils import LogTrack
import numpy as np
import pandas as pd
from _collections import defaultdict


def summarize_by_gene(df, transcript2gene):
    data = defaultdict(lambda: {'eff_length': [],
                                'est_counts': [],
                                'tpm': []})
    for t_id, record in df.to_dict(orient='index').items():
        gene = transcript2gene.get(t_id.split('.')[0], None)
        if gene is None:
            continue
        for f in ['eff_length', 'est_counts', 'tpm']:
            data[gene][f].append(record[f])
    
    summarized = {}
    for gene, record in data.items():
        tpms = np.nansum(record['tpm'])
        w = np.array(record['tpm']) / tpms
        record['tpm'] = tpms
        record['eff_length'] = np.sum(w * np.array(record['eff_length']))
        record['est_counts'] = np.sum(record['est_counts'])
        summarized[gene] = record
    return(pd.DataFrame.from_dict(summarized, orient='index'))


def merge_data(design, ortho, kallisto_dir, runs_col, species_col,
               transcript2gene, log):
    merged_data = {'counts': {}, 'log_bias': {}, 'logtpms': {}}
    
    for record in design.to_dict(orient='index').values():
        run = record[runs_col]
        species = record[species_col]
        log.write('Loading sample {} from species {}'.format(run, species))
        
        fpath = join(kallisto_dir, run, 'abundance.tsv')
        if not exists(fpath):
            log.write('Sample file {} not found'.format(fpath))
            continue
        
        df = pd.read_csv(fpath, sep='\t', dtype={'target_id': str}).set_index('target_id')
        df.index = [x.split('.')[0] for x in  df.index]
        if transcript2gene is not None:
            df = summarize_by_gene(df, transcript2gene)
        
        target_ids = ortho[species].dropna()
        ortho_ids = target_ids.index
        target_ids = [x.split('.')[0] for x in target_ids]
        df = df.loc[target_ids, :]
        df.index = ortho_ids 
        
        merged_data['counts'][run] = df['est_counts'].dropna()
        merged_data['logtpms'][run] = np.log(df['tpm'] + 1e-6)
        merged_data['log_bias'][run] = np.log(df['eff_length'])
    
    for f, v in merged_data.items():
        merged_data[f] = pd.DataFrame(v)
    merged_data['counts'] = merged_data['counts'].fillna(-1).astype(int)
    
    return(merged_data)
    

def main():
    description = 'Merges output from kallisto: estimated counts and effective'
    description += ' transcript length for integrated modeling of gene '
    description += 'expression evolution'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'File containing samples to merge in a single table. '
    help_msg += 'You may include several species by including a column with'
    help_msg += 'the for each sample'
    input_group.add_argument('-d', '--design', required=True,
                             help=help_msg)
    input_group.add_argument('-i', '--input_dir', required=True,
                             help='Directory containing kallisto outputs')
    input_group.add_argument('-g', '--orthologous_genes', required=True,
                             help='CSV File containing orthologous genes')
    help_msg = 'File containing correspondence bewteen transcript and gene ids'
    help_msg += ' for summarizing at the gene level'
    input_group.add_argument('-t2g', '--transcript2gene', default=None,
                             help=help_msg)

    options_group = parser.add_argument_group('Options')
    options_group.add_argument('-sp', '--species', default='species',
                               help='Column header for species (species)')
    help_msg = 'Column header for run in case to sum counts from teh same sample'
    options_group.add_argument('-r', '--runs', default=None, help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file/prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    design_fpath = parsed_args.design
    kallisto_dir = parsed_args.input_dir
    orthologs_fpath = parsed_args.orthologous_genes
    transcript2gene_fpath = parsed_args.transcript2gene
    
    species_col = parsed_args.species
    runs_col = parsed_args.runs
    output = parsed_args.output

    # Run program
    log = LogTrack()
    log.write('Start')
    design = pd.read_csv(design_fpath)
    log.write('Loaded samples data from {}'.format(design_fpath))
    ortho = pd.read_csv(orthologs_fpath, dtype=str).set_index('ortho_id')
                        
    log.write('Loaded orthologous exons from {}'.format(orthologs_fpath))

    transcript2gene = None
    if transcript2gene_fpath is not None:
        transcript2gene =  {line.strip().split()[0].split('.')[0]: line.strip().split()[1]
                            for line in open(transcript2gene_fpath)}
        msg = 'Loaded {} transcript to gene correspondence'
        log.write(msg.format(len(transcript2gene)))

    merged_data = merge_data(design, ortho, kallisto_dir,
                             runs_col=runs_col, species_col=species_col,
                             transcript2gene=transcript2gene, log=log)
    
    for suffix, df in merged_data.items():
        fpath = '{}.{}.csv'.format(output, suffix)
        df.to_csv(fpath)
        log.write('\tMerged {} written to {}'.format(suffix, fpath))

    log.finish()


if __name__ == '__main__':
    main()
