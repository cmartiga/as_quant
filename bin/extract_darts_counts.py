#!/usr/bin/env python

import argparse

from AS_quant.utils import LogTrack
import numpy as np
import pandas as pd


def extract_col_counts(counts_str):
    return(pd.DataFrame([[int(x) for x in row.split(',')]
                         for row in counts_str], index=counts_str.index))


def extract_counts(rmats_data, sample_names=None):
    i1 = extract_col_counts(rmats_data['I1'])
    i2 = extract_col_counts(rmats_data['I2'])
    s1 = extract_col_counts(rmats_data['S1'])
    s2 = extract_col_counts(rmats_data['S2'])

    inclusion = pd.concat([i1, i2], axis=1)
    skipping = pd.concat([s1, s2], axis=1)
    total = inclusion + skipping
    design = np.array([0] * i1.shape[1] + [1] * i2.shape[1])

    if sample_names is None:
        sample_names = ['g1.{}'.format(i) for i in range(i1.shape[1])]
        sample_names.extend(['g2.{}'.format(i) for i in range(i2.shape[1])])

    inclusion.columns = sample_names
    skipping.columns = sample_names
    design = pd.DataFrame({'Group': design}, index=sample_names)

    log_map_ratio = np.log(rmats_data['inc_len'] / rmats_data['skp_len'])
    return(inclusion, total, log_map_ratio, design)


def main():
    description = 'Generates a table with inclusion and skipping reads'
    description += 'with design variables for later testing'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('input', help='DARTS output file')
    parser.add_argument('-s', '--sample_names', default=None,
                        help='File containing ordered sample names')
    parser.add_argument('-o', '--output', required=True,
                        help='Output file prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    in_fpath = parsed_args.input
    out_prefix = parsed_args.output
    samples_fpath = parsed_args.sample_names

    samples_names = None
    if samples_fpath is not None:
        samples_names = [line.strip() for line in open(samples_fpath)]

    rmats_data = pd.read_csv(in_fpath, sep='\t')
    rmats_data.set_index('ID', inplace=True)

    # Init logger
    log = LogTrack()
    results = extract_counts(rmats_data, samples_names)
    inclusion, total, log_map_ratio, design = results

    inclusion.to_csv('{}.inclusion.csv'.format(out_prefix))
    total.to_csv('{}.total.csv'.format(out_prefix))
    log_map_ratio.to_csv('{}.log_map_ratio.csv'.format(out_prefix))
    design.to_csv('{}.design.csv'.format(out_prefix))

    # Read data
    log.write('Done')


if __name__ == '__main__':
    main()
