#!/usr/bin/env python
import argparse
from csv import DictWriter

import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.genome import get_annotation_db, get_sj_index
from AS_quant.models.psi import infer_splice_graph_psi


def main():
    description = 'Bayesian model to infer PSIs along genes using Splice Graphs'
    description += 'and SJ heterogeneous coverage under a Negative Binomial '
    description += 'distribution of read counts'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('sj_counts', help='SJ counts matrix')
    input_group.add_argument('-db', '--annotation_db', required=True,
                             help='Annotation DB')
    input_group.add_argument('-s', '--sj_index', default=None,
                             help='Tabix indexed file with SJ annotation')

    options_group = parser.add_argument_group('MCMC Options')
    options_group.add_argument('-N', '--n_samples', default=1000, type=int,
                               help='Number of MCMC samples (1000)')

    model_group = parser.add_argument_group('Models options')
    help_msg = 'Maximum allowed intron length in transcripts (1e6)'
    model_group.add_argument('--max_intron_length', default=1e6, type=int,
                             help=help_msg)
    model_group.add_argument('--recompile', default=False, action='store_true',
                             help='Recompile stan model before fitting')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file')

    # Parse arguments
    parsed_args = parser.parse_args()
    sj_counts_fpath = parsed_args.sj_counts
    db_fpath = parsed_args.annotation_db
    sj_index_fpath = parsed_args.sj_index

    # MCMC options
    n_samples = parsed_args.n_samples
    
    # Other options
    recompile = parsed_args.recompile
    max_intron_length = parsed_args.max_intron_length

    # Output options
    out_fpath = parsed_args.output

    # Init log
    log = LogTrack()
    log.write('Start data analysis...')
    
    sj_counts = pd.read_csv(sj_counts_fpath, sep='\t')
    log.write('Loaded counts for {} SJs'.format(sj_counts.shape[0]))
    
    db = get_annotation_db(db_fpath)
    log.write('Loaded annotation DB from {}'.format(db_fpath))
    
    sj_index = None
    if sj_index_fpath is not None:
        sj_index = get_sj_index(sj_index_fpath)
        log.write('Loaded new SJs from {}'.format(sj_index_fpath))
    
    log.write('Starting inference process')
    results = infer_splice_graph_psi(db, sj_counts, sj_index=sj_index,
                                     max_intron_length=max_intron_length,
                                     n_iter=n_samples, recompile=recompile)
    
    exons_fhand = open(out_fpath, 'w')
    exons_writer = None
    
    msg = 'Storing results at: {}'.format(out_fpath)
    log.write(msg)
    
    for exons_psi in results:
        exons_psi = exons_psi.reset_index()
        
        if exons_writer is None:
            exons_writer = DictWriter(exons_fhand, fieldnames=exons_psi.columns)
            exons_writer.writeheader()
        
        for exon_psi in exons_psi.to_dict(orient='index').values():
            exons_writer.writerow(exon_psi)
        
        exons_fhand.flush()

    log.finish()


if __name__ == '__main__':
    main()
