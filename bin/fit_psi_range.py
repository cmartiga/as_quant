#!/usr/bin/env python

import argparse

import numpy as np
import pandas as pd

from AS_quant.utils import LogTrack
from AS_quant.psi_models import PsiRangeModel


def load_binomial_data(input_fpaths, samples):
    inc_fpath, total_fpath = input_fpaths.split(',') 
    inclusion = pd.read_csv(inc_fpath, index_col=0)
    total = pd.read_csv(total_fpath, index_col=0)
    sel_samples = np.intersect1d(samples, total.columns)
    total['exon_id'] = total.index
    total.drop_duplicates('exon_id', inplace=True, keep=False)
    total.drop('exon_id', axis=1, inplace=True)
    inclusion = inclusion.loc[total.index, :]
    return(inclusion[sel_samples].fillna(0).astype(int),
           total[sel_samples].fillna(0).astype(int))


def main():
    description = 'Uses a logistic regression model to infer PSIs across the '
    description += 'different groups (e.g. tissues) and estimates the posterior'
    description += 'probability of having certain differences between '
    description += 'conditions as a proxy for AS regulatory potential. It '
    description += ' also provides estimations for PSIs across all groups'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    input_group.add_argument('input',
                             help='Input file (s) with sample observations')
    
    help_msg = 'Sample design matrix in csv format. It should include samples'
    help_msg += ' as row names and columns will be used as covariates for'
    help_msg += 'regression'
    input_group.add_argument('-d', '--design', required=True,
                             help=help_msg)
    help_msg = 'Comma separated list of covariates to take into account (all)'
    input_group.add_argument('--covariates', default=None, help=help_msg)

    obs_group = parser.add_argument_group('Observational model options')
    help_msg = 'Matrix containing log(modifier) to account for sample-specific '
    help_msg += 'fragmentation or GC bias. By default, it uses {log_bias_def} '
    help_msg += 'for every element or sample with missing data'
    obs_group.add_argument('--log_bias', default=None, help=help_msg)
    help_msg = 'Bias value to use by default for every exon. By default it uses'
    help_msg += ' log(2) to account for exons having 2 possible inclusion SJ '
    help_msg += 'per skipping SJ. Set to "mean" to replace to use rowmeans'
    obs_group.add_argument('-b', '--log_bias_def', default=np.log(2),
                           help=help_msg)

    options_group = parser.add_argument_group('MCMC Options')
    options_group.add_argument('-n', '--nthreads', default=4,
                               help='Number of threads to fit the model (4)')
    options_group.add_argument('-c', '--nchains', default=4,
                               help='Number MCMC chains to fit the model (4)')
    options_group.add_argument('-N', '--n_samples', default=2000,
                               help='Number of MCMC samples (2000)')

    model_group = parser.add_argument_group('Regression model options')
    help_msg = 'Value for which to calculate the posterior probability of '
    help_msg += 'having a larger PSI_range than this value (0.1)'
    model_group.add_argument('--psi_range', default=0.1, type=float,
                             help=help_msg)
    model_group.add_argument('--recompile', default=False, action='store_true',
                             help='Recompile stan model before fitting')

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', required=True,
                              help='Prefix for output files')

    # Parse arguments
    parsed_args = parser.parse_args()
    input_fpaths = parsed_args.input
    design_fpath = parsed_args.design
    covariates = parsed_args.covariates

    # MCMC options
    n_threads = int(parsed_args.nthreads)
    n_chains = int(parsed_args.nchains)
    n_samples = int(parsed_args.n_samples)
    
    # Bias options
    log_bias_fpath = parsed_args.log_bias
    log_bias_def = parsed_args.log_bias_def
    
    # Model options
    recompile = parsed_args.recompile
    psi_range = parsed_args.psi_range

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init log
    log = LogTrack()
    log.write('Start analysis')
    
    # Load sample and species data
    design = pd.read_csv(design_fpath, index_col=0)
    
    # Load data
    inclusion, total = load_binomial_data(input_fpaths, design.index)
    if np.any((total - inclusion) < 0):
        raise ValueError('Negative skipping values for reads found')
    common_samples = np.intersect1d(total.columns, design.index)
    if common_samples.shape[0] == 0:
        raise ValueError('No samples in commong were found in counts and design')
    
    design = design.reindex(common_samples)
    if covariates is None:
        covariates = design.columns
    else:
        covariates = covariates.split(',')
    design_dict = {}
    for variable in covariates:
        if design[variable].dtype in ('str', 'object'):
            for value in design[variable].unique():
                design_dict[value] = (design[variable] == value).astype(int)
        else:
            design_dict[variable] = design[variable]
    design = pd.DataFrame(design_dict)
    
    inclusion = inclusion[common_samples]
    total = total[common_samples]
    
    
    if log_bias_fpath is not None:
        log_bias = pd.read_csv(log_bias_fpath, index_col=0)
        log_bias = log_bias.loc[total.index, :][total.columns]
        log_bias = log_bias.replace(np.inf, np.nan).replace(-np.inf, np.nan)
        if log_bias_def == 'mean':
            log_bias.apply(lambda row: row.fillna(row.mean()), axis=1)
        else:
            log_bias.fillna(log_bias_def, inplace=True)
        log.write('Loaded exon log(bias) from {}'.format(log_bias_fpath))
    else:
        log_bias = pd.DataFrame(np.full(total.shape, log_bias_def),
                                index=total.index, columns=total.columns)
        msg = 'Missing log(bias) information. Using {} as default'
        log.write(msg.format(log_bias_def))
    msg = 'Loaded {} elements with {} samples from a total of {} groups'
    log.write(msg.format(total.shape[0], total.shape[1], design.shape[1]))
    
    # Load model
    msg = 'Loading model and performing MCMC sampling'
    log.write(msg)
    model = PsiRangeModel(design, psi_range=psi_range, recompile=recompile)
    model.observations.load(inclusion, total, log_bias)

    # Fit model        
    model.fit(chains=n_chains, n_iter=n_samples, n_jobs=n_threads)
    model.write_fit(prefix=output_prefix)
    log.write('Results saved at {}*'.format(output_prefix))
    log.finish()


if __name__ == '__main__':
    main()
