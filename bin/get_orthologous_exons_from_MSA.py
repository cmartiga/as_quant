import argparse

from AS_quant.orthologs import GeneSetCollection
from AS_quant.utils import LogTrack


def main():
    description = 'Finds orthologous exons across previously defined homologous'
    description += ' genes. It uses MCL-complete subgraphs of a best reciprocal'
    description += ' hit graph constructed based on common position along '
    description += 'gene-wise Multiple Sequence Alignemnts (MSA) '
    description += 'that must be pre-computed'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'Directory with aligned orthologs fasta files. A single fasta '
    help_msg += 'file per gene set named {gene_set_id}.fasta is expected as'
    help_msg + ' provided by align_orthologous_transcripts'
    input_group.add_argument('-f', '--fasta_dir', required=True,
                             help=help_msg)
    help_msg = 'File containing gene_ids (If not provided all fasta files in '
    help_msg += 'seqs_dir will be aligned)'
    input_group.add_argument('-g', '--gene_ids', default=None, help=help_msg)
    input_group.add_argument('-s', '--species', required=True,
                             help='File containing a species names')    
    
    options_group = parser.add_argument_group('Orthologs options')
    options_group.add_argument('-m', '--min_size', default=3, type=int,
                               help='Minimum orthogroup size (3)')
    help_msg = 'Skip the complete sub-graph step: use only MCL'
    options_group.add_argument('--skip_clique', default=False,
                               action='store_true', help=help_msg)
    options_group.add_argument('--expansion', default=2, type=float,
                               help='Expansion parameter for MCL (2)')
    options_group.add_argument('--inflation', default=2, type=float,
                               help='Inflation parameter for MCL (2)')
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file for exon orthologs ')

    # Parse arguments
    parsed_args = parser.parse_args()
    fasta_dir = parsed_args.fasta_dir
    genes_fpath = parsed_args.gene_ids
    species_fpath = parsed_args.species
    
    min_size = parsed_args.min_size
    complete_subgraph = not parsed_args.skip_clique
    expansion = parsed_args.expansion
    inflation = parsed_args.inflation
    
    out_fpath = parsed_args.output

    # Init log
    log = LogTrack()

    # Load input data
    gene_set_ids = None 
    if genes_fpath is not None:
        gene_set_ids = [line.strip() for line in open(genes_fpath)]
    species = [line.strip() for line in open(species_fpath)]
    
    gene_sets = GeneSetCollection(fasta_dir, gene_set_ids=gene_set_ids, log=log)
    gene_sets.get_exon_orthologs(min_size=min_size,
                                 complete_subgraph=complete_subgraph,
                                 expansion=expansion, inflation=inflation)
    gene_sets.write_exon_orthologs(out_fpath, species)
    log.write('Finished')


if __name__ == '__main__':
    main()
