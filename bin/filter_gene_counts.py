#!/usr/bin/env python
from copy import deepcopy
import argparse

import numpy as np
import pandas as pd

from AS_quant.event_counts import GeneCounts


def main():
    description = 'Tool to manipulate event inclusion and skipping counts'

    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'Input files prefix (expected suffixes: logtpms, counts)'
    input_group.add_argument('-i', '--input_prefix', required=True,
                             help=help_msg)

    cov_group = parser.add_argument_group('Coverage filter')
    cov_group.add_argument('-m', '--min_total', default=5, type=int,
                           help='Minimum total counts in >= 1 sample (5)')
    cov_group.add_argument('-M', '--min_total_ave', default=1, type=float,
                           help='Minimum average total counts (1)')
    
    psi_group = parser.add_argument_group('log(TPMs) filter')
    psi_group.add_argument('--min_logtpms', default=0, type=float,
                           help='Select events with TPMs >= {min_psi} > 1 sample (0)')
    psi_group.add_argument('--min_logtpms_ave', default=0, type=float,
                           help='Select events with average TPMS >= {min_psi_ave} (0)')
    
    filter_group = parser.add_argument_group('IDS filter')
    filter_group.add_argument('--gene_ids', default=None, 
                              help='File containing event ids to filter')
    
    sampling_group = parser.add_argument_group('Sampling options')
    sampling_group.add_argument('-k', '--sample_size', default=0, type=int,
                                help='Sample size (no subsampling done)')
    sampling_group.add_argument('--seed', default=0, type=float,
                                help='Random seed (0)')
    
    grouping_group = parser.add_argument_group('Grouping options')
    grouping_group.add_argument('-g', '--gene_data', default=None,
                                help='Table containing gene data for grouping')
    grouping_group.add_argument('-n', '--n_groups', default=5,
                                help='Number of groups to make (5)')
    grouping_group.add_argument('-v', '--variable', default=None,
                                help='Grouping variable')
    
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', required=True,
                              help='Prefix for output files')

    # Parse arguments
    parsed_args = parser.parse_args()
    input_prefix = parsed_args.input_prefix

    # Filtering options
    min_counts= parsed_args.min_total
    min_counts_ave = parsed_args.min_total_ave
    
    min_logtpms= parsed_args.min_logtpms
    min_logtpms_ave = parsed_args.min_logtpms_ave
    
    gene_ids_fpath = parsed_args.gene_ids
    sample_size = parsed_args.sample_size
    seed = parsed_args.seed
    
    # Grouping options
    gene_data_fpath = parsed_args.gene_data
    n_groups = parsed_args.n_groups
    grouping_variable = parsed_args.variable 

    # Output options
    output_prefix = parsed_args.output_prefix

    # Init processing
    gene_counts = GeneCounts(prefix=input_prefix)
    gene_counts.init_log()
    gene_counts.load_counts()
     
    # Filtering
    if gene_ids_fpath is not None:
        gene_ids = [line.strip() for line in open(gene_ids_fpath)]
        gene_counts.select_ids(gene_ids)
        
    gene_counts.filter_missing_samples()
    gene_counts.filter_counts_samples(min_counts=min_counts, n_samples=1, label='counts')
    gene_counts.filter_counts_average(min_counts=min_counts_ave, label='counts')
    gene_counts.filter_counts_samples(min_counts=min_logtpms, label='logtpms')
    gene_counts.filter_counts_average(min_counts=min_logtpms_ave, label='logtpms')
    
    if gene_data_fpath is not None and grouping_variable is not None:
        gene_data = pd.read_csv(gene_data_fpath, index_col=0)
        qs = np.linspace(0, 100, n_groups + 1)
        percentiles = np.percentile(gene_data[grouping_variable], qs)
        
        for subset, (l, u) in enumerate(zip(percentiles, percentiles[1:])):
            sel_rows = np.logical_and(gene_data[grouping_variable] >= l,
                                      gene_data[grouping_variable] < u)
            sel_ids = gene_data.index[sel_rows]
            subset_counts = deepcopy(gene_counts)
            subset_counts.select_ids(sel_ids)
            if sample_size > 0:
                subset_counts.sample(sample_size=sample_size,
                                            seed=seed)
            out_prefix_subset = '{}.{}.q{}'.format(output_prefix,
                                                   grouping_variable, subset)
            subset_counts.write(out_prefix_subset)
            
    else:
        if sample_size > 0:
            gene_counts.sample(sample_size=sample_size, seed=seed)
        gene_counts.write(output_prefix)
    gene_counts.log.finish()


if __name__ == '__main__':
    main()
