#!/usr/bin/env python
import argparse

from AS_quant.utils import LogTrack
import pandas as pd
from AS_quant.junctions import SpliceJunctionCounts
from os.path import join


def main():
    description = 'Merges SJ unique counts from STAR output across different '
    description += 'species. A CSV file with per sample files for each species'
    description += ' will be generated'
     
    # Create arguments
    parser = argparse.ArgumentParser(description=description)
    input_group = parser.add_argument_group('Input')
    help_msg = 'File containing samples to merge in a single table. '
    help_msg += 'You may include several species by including a column with'
    help_msg += 'the for each sample'
    input_group.add_argument('-d', '--design', required=True,
                             help=help_msg)
    input_group.add_argument('-i', '--input_dir', required=True,
                             help='Directory containing SJ files')

    options_group = parser.add_argument_group('Options')
    help_msg = 'Take into account multi-mapping reads (False)'
    options_group.add_argument('-m', '--include_multimapping', default=False,
                               action='store_true', help=help_msg)
    help_msg = 'Column header for sequencing run in the design table (run)'
    options_group.add_argument('-r', '--run_col', default='Run',
                               help=help_msg)
    help_msg = 'Column header for species. If provided, a different file'
    help_msg += ' for each species will be generated'
    options_group.add_argument('-s', '--species_col', default=None,
                               help=help_msg)
    help_msg = 'Suffix for SJ files (STAR default: SJ.out.tab)'
    options_group.add_argument('--suffix', default='SJ.out.tab',
                               help=help_msg)

    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output', required=True,
                              help='Output file/prefix')

    # Parse arguments
    parsed_args = parser.parse_args()
    design_fpath = parsed_args.design
    sj_dir = parsed_args.input_dir
    multimapping = parsed_args.include_multimapping
    run_col = parsed_args.run_col
    species_col = parsed_args.species_col
    suffix = parsed_args.suffix
    output = parsed_args.output

    # Run program
    log = LogTrack()
    log.write('Start')

    design = pd.read_csv(design_fpath)

    if species_col is None:    
        sj_counts = SpliceJunctionCounts(include_multimapping=multimapping)
        samples_fpaths_dict = {s: join(sj_dir, '{}.{}'.format(s, suffix))
                               for s in design[run_col]}
        sj_counts.read_STAR_files(samples_fpaths_dict)
        sj_counts.write_csv(output)
        log.write('\tCounts written to {}'.format(output))

    else:
        for species, samples in design.groupby(species_col)[run_col]:
            sj_counts = SpliceJunctionCounts(include_multimapping=multimapping)
            samples_fpaths_dict = {s: join(sj_dir, '{}.{}'.format(s, suffix))
                                   for s in samples}
            sj_counts.read_STAR_files(samples_fpaths_dict)
            fpath = '{}.{}.csv'.format(output, species)
            sj_counts.write_csv(fpath)
            log.write('\t{} counts written to {}'.format(species, fpath))

    log.finish()


if __name__ == '__main__':
    main()
