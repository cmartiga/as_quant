#!/usr/bin/env python

from setuptools import setup, find_packages
from AS_quant.settings import VERSION


def main():
    description = 'Quantification of alternative splicing data'
    setup(
        name='AS_quant',
        version=VERSION,
        description=description,
        author_email='cmarti@cnic.es',
        url='https://bitbucket.org/cmartiga/AS_quant',
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': [
                # Orthologs scripts
                'get_orthologous_exons = bin.get_orthologous_exons_full:main',
                'evaluate_orthologs = bin.evaluate_orthologs:main',
                'get_orthologous_exons_from_MSA = bin.get_orthologous_exons_from_MSA:main',
                'get_orthologous_exons_full = bin.get_orthologous_exons_full:main',
                'align_orthologous_transcripts = bin.align_orthologous_transcripts:main',
                'extract_orthologous_transcripts_seqs = bin.extract_orthologous_transcripts_seqs:main',
                
                # Preparing matrices for analysis
                'calc_frag_size_bias = bin.calc_frag_size_bias:main',
                'build_gtf_db = bin.build_gtf_db:main',
                'gtf_to_sj = bin.gtf_to_sj:main',
                'extract_exons_sj = bin.extract_exons_sj:main',
                'calc_exons_distances_to_ends = bin.calc_exons_distances_to_ends:main',
                'extract_exon_data = bin.extract_exon_data:main',
                'merge_sj_counts = bin.merge_sj_counts:main',
                'sj_to_exon_counts = bin.sj_to_exon_counts:main',
                'merge_sp_exon_counts = bin.merge_species_exon_counts:main',
                'filter_exon_counts = bin.filter_exon_counts:main',
                
                'extract_exon_counts = bin.extract_exon_counts:main',
                'extract_sj_counts = bin.extract_sj_counts:main',
                'parse_counts = bin.parse_counts:main',
                'merge_kallisto = bin.merge_kallisto:main',
                'filter_gene_counts = bin.filter_gene_counts:main',
                
                # Models scripts
                'infer_exon_psi = bin.infer_exon_psi:main',
                'infer_splice_graph_psi = bin.infer_splice_graph_psi:main',
                'fit_OU = bin.fit_OU:main',
                'fit_OU_by_exon = bin.fit_OU_by_exon:main',
                'fit_OU_shifts = bin.fit_OU_shifts:main',
                'fit_BM = bin.fit_BM:main',
                'sim_OU = bin.sim_OU:main',
                'fit_psi_range = bin.fit_psi_range:main',
                'fit_psi_regression = bin.fit_psi_regression:main',
                'fit_reg_model = bin.fit_reg_model:main',
                'fit_psi_mixture = bin.fit_psi_mixture:main',
                'evaluate_shifts = bin.evaluate_shifts:main',
                
                # Other
                'get_SE_transcripts = bin.get_SE_transcripts:main',
                'evaluate_psi_inference = bin.evaluate_psi_inference:main'
            ]},
        install_requires=['numpy', 'pandas', 'scipy', 'pysam', 'networkx',
                          'editdistance', 'seaborn', 'matplotlib',
                          'biotite', 'dendropy', 'markov_clustering',
                          'ete3', 'gffutils', 'pystan', 'statsmodels'], # cmdstanpy or pystan is required
        platforms='ALL',
        keywords=['bioinformatics', 'alternative splicing', 'RNA-Seq'],
        classifiers=[
            "Programming Language :: Python :: 3",
            'Intended Audience :: Science/Research',
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
    )
    return


if __name__ == '__main__':
    main()
