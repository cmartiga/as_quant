# ASquant

ASquant is a toolkit to handle comparative transcriptomic data using RNA-seq, with particular focus in the study of alternative splicing. Thus, it provides tools for equalizing annotations between different species, finding exon orthologs within groups of homologous genes, and defining exon skipping events and the splice junctions that support either the inclusion or their skipping, as well as tools for calculating species-sample specific biases introduced by the combination of the genome and transcriptome properties in each species for better estimation of splicing rates. 

However, the main motivation for building this library was the development and implementation of models of phenotypic evolution for analysing alternative splicing in a comparative setting. Thus, we have implemented Ornstein-Uhlenbeck (OU) models adapted to the particularities and data type derived from alternative splicing data. This enables the characterization of the evolutionary process and genetic forces shaping the eovlution of the splicing rates, as well as finding potential alternative splicing events that have experienced adaptive shifts along the diversification of the species under study.

## Installation

Create a python3 conda environment and activate it

```bash
conda create -n ASquant python=3.7
conda activate ASquant
```

Download the repository using git and cd into it

```bash
git clone git@bitbucket.org:cmartiga/AS_quant.git
```

Install using setuptools
```bash
cd AS_quant
python setup.py install
```

# Annotation

## Extracting all possible splice junctions in a genome

One of the problems that we face in comparative studies using transcriptomic data is the different degree and quality of the annotated transcripts in different species, which may no have anything to do with the actual amount of gene transcripts that are actually expressed in each species. For this reason, we first use an event centric approach and focus on the simplest alternative splicing events: exon skipping. Secondly, if we assume that at least most of the exons will be annotated across all species even if the combinations of them are not known, we can build a library of splicing junctions (SJ) taking into account all possible exon combinations within the longest transcript (with more annotated exons) for each species and insert them into the reference genome index before read mapping.

For this, we just need a standard GTF file 

```bash
gtf_to_sj -g GTF -a -o GTF_SJ
```

NOTE: we have worked with Ensembl and GenBank files, but it should work with any GTF or GFF file

The output of this command includes 3 different files:

-	BED file with the annotated SJ as formatted by STAR. This file can be directly as SJ library to insert in the reference genome index: OUTPUT.SJ.full.tab
-	A csv file with the set of SJ supporting inclusion and skipping for each of the annotated exons: OUPUT.exons_SJ.csv
-	A csv file with the selected transcript id for each gene and the transcript length: OUTPUT.longest_transcripts.csv


Now we can use the annotated SJ to build a reference index for each species using [STAR](https://github.com/alexdobin/STAR)

```
STAR  --runMode genomeGenerate --genomeDir INDEX  --genomeFastaFiles GENOME.fa --sjdbFileChrStartEnd GTF_SJ.SJ.full.tab --limitSjdbInsertNsj 10000000 [ADDITIONAL OPTIONS]

```

NOTE: The high number of SJ that is generated with this tool requires using the --limitSjdbInsertNsj to more than 2M overall

## Find orthologous exons

A key step when comparing different species at any level is stablishing the correspondance in the units we want to compare, in this case, the exons wihtin homologous genes that have a common ancestor and are therefore orthologs.

To do so, we require again standard annotation files in GTF format, together with genome sequences in fasta format indexed with [samtools](http://www.htslib.org/) access through pysam interface for sequence extraction. As we will restrict the search within the sets of orthologous genes, we additionally need a csv file containing gene ids on rows and species names on columns, as well as a design csv file with the genome and annotation file corresponding to each species.

We provide two alternative methods to find exon orthologs, although both are based on building a best-reciprocal hit graph and performing Markov Clustering (MCL) on the graph followed by the selection of the complete subgraph within the cluster. This last step can be skipped to, for instance, take into account within gene exon duplicates, but so far only an exon per species is selected randomly, so we recommend to avoid this option so far. The two proposed strategies differ in the way the distances are calculated

### Pairwise sequence similarity based orthologs

In this method, we use pairwise distances to build the best reciprocal hit graph. Although we provide the possibility to perform sequence alignment using [biotite](https://www.biotite-python.org/) library, this procedure is extremely slow. To accelerate the comparisons, we provide by default the use of editing distance, which is much faster and allows scaling to a larger number of species. 

```bash
get_orthologous_exons -d DESIGN -O ORHTOLOGS -a ANNOTATION_DIR -g GENOMES_DIR -o EXON_ORTHOLOGS
```

We provide options to include more than the exon sequencing in calculating similarity scores, with different weights: for instance, one can include also some intronic sequences or splice sites, as well as modify the scoring scheme for the pairwise alignments or performing the MCL


### Whole transcript multiple sequence alignment based orthologs

However, as exons become smaller, similarities would become more difficult to estimate reliably. However, in contrast with gene orthology pipelines, we expect a very strong conservation of exon order along the evolution of a gene. This order information is not taken into account in the previous strategy, but may be accommodated by using a Multiple Sequence Alignment (MSA) on whole transcripts sequences.

To do this, we first need to generate fasta files with the orthologous genes that are to be aligned in each case, with the position of introns indicated by the character "|" and ordered exon ids included in the name for later parsing. 

```bash
extract_orthologous_transcripts_seqs -d DESIGN -O ORHTOLOGS -a ANNOTATION_DIR -g GENOMES_DIR -o FASTA_DIR
```

Now, we can perform MSA using a modified scoring matrix, in which we add the character "|" with a very high score when matching to force the genes to align at the intron boundaries and help in the identification of orthologous exons. Options to modify this scores are provided and can be consulted in the help of the tool. 

NOTE: We use external tools like [MUSCLE](https://www.drive5.com/muscle/) and [MAFFT](https://mafft.cbrc.jp/alignment/software/), which must be pre-installed for using this method. You can use the -g option to align a subset of genes only and parallelize the MSA procedure in a cluster, as it may be time consuming

```bash
align_orthologous_transcripts -s FASTA_DIR -o MSA_DIR
```

Once the sequences are aligned, pairwise scores between exons can be calculated as the number of aligned positions in the MSA between every pair of species, and continue with the MCL and complete subgraph procedure as before to obtain the final csv file with exon orthologs across the provided species

```bash
get_orthologous_exons_from_MSA -O ORTHOLOGS -s MSA_DIR -o EXON_ORTHOLOGS
```


### Obtaining gene orthologs

We do not provide tools to find orthologous genes. This information may be obtained from [Ensembl Compara](https://www.ensembl.org/info/genome/compara/index.html) if the target species are all deposited there, or other external databases like [OrthoMaM](http://orthomam2.mbb.univ-montp2.fr:8080/OrthoMaM_v10b3/) or [EggNOG](http://eggnogdb.embl.de/#/app/home). This is a task that is performed by many independent tools. These are just a few of them:

-	[OrthoFinder](https://github.com/davidemms/OrthoFinder)
-	[InParanoid](http://inparanoid.sbc.su.se/cgi-bin/index.cgi)
-	[Hieranoid](http://hieranoid.sbc.su.se)
-	[OrthoGNC](http://bs.ipm.ir/softwares/orthognc)

There is a big community effort tackling this problem that came together in the [Quest for Orthologs](https://questfororthologs.org/) consortium. They have more information about existing methods and databases, as well as systematic [benchmarking](https://orthology.benchmarkservice.org) of the more widely used tools.


# Data

## Building SJ counts matrices

The basic data files that we will use for fitting OU models are matrices of read counts in csv format. We require at least 2 matrices with events in rows and samples in columns, containing the number of reads supporting exon inclusion and the total number of reads mapped to each event. If no information is found for a species, because for instance no clear exon ortholog was found, total counts would be set to 0 as if not information is provided.

-	Our tools allow to build these matrices from mapping results using [STAR](https://github.com/alexdobin/STAR) against the built reference genomes, with focus on exon skipping events
-	We only use reads mapping to the SJs, so one can only store the BED file returned with the SJ counts to avoide excesive disk usage by storing the BAM files. These files are assumed to be stored in a single directory for all mapped samples.
-	We require a design file in csv format with sample ids, species, reference species (in case the genome is not yet sequenced, to specify which close relative to use).

TODO: introduce an example table

With these, files, the first step is to build files with SJ counts for each species separately. The next command will create a file named SJ_COUNTS.<species>.csv with the number of reads mapping to each SJ

```bash
merge_sj_counts -d DESIGN -i MAPPING_DIR -o SJ_COUNTS
```

## Extend the SJ sets supporting exon inclusion and skipping

When extracting the SJs from the annotation to build the genome index, we have also obtained, for each annotated exon, the annotated SJs that support either exon inclusion or skipping. However, there are two main problems in this approach:

-	There may be some SJs that are not annotated but are detected and quantified in the mapping process
-	Exon skipping events often overlap with alternative splice site selection. This means that the annotated boundaries are not necessarily the most used in each species

To solve both these problems, we provide a tool that looks for SJs in the neighborhood of the exon boundaries within a window of certain size (~50 nt by default) and add them as SJ supporting exon inclusion. The exon then becomes a rather diffuse entity with no clear boundaries and we assume that there are no relevant differences by selecting one splice site or the other. We also expand the set of SJ supporting exon skipping by selecting all SJs that jump over the annotated exon but are still within the gene boundaries.

TODO: add explicative figure

To do this, we first need to build a indexed BED file with the SJs to perform the search. This hgas to be done for each species separately.

```bash
cut -f 1 SJ_COUNTS.<species>.csv -d ',' | sed 's/:/\t/g' | sed 's/-/\t/g' | sort -k1,1 -k2,2n -k3,3n > <species.SJ.tab>
bgzip <species>.SJ.tab
tabix -s 1 -b 2 -e 3 <species>.SJ.tab 
```

And use the indexed SJ to expand the set of annotated SJ supporting inclusion and skipping for each exon

```bash
expand_exons_sj -s <species>.SJ.tab -e <GTF>.exons_SJ.csv -l 50 -o <species>.SJ.expanded.csv
```

## Calculate sample specific exon inclusion biases

TODO: Fill

## Merge exon inclusion and skipping counts

We first need to sum the SJ counts supporting inclusion and skipping for each species separately

```bash
get_sp_exons_counts -e <species>.SJ.expanded.csv -s SJ_COUNTS.<species>.csv -o EXON_COUNTS.<species>.csv
```

And now use exon orthology relationships to merge matrices across all species, including bias files if previously calculated for each sample and exon 

```bash
merge_sp_exon_counts -d DESIGN -i EXON_COUNTS_DIR -e EXON_ORTHOLOGS --log_bias_dir LOG_BIAS_DIR -o EXON_COUNTS_FULL
```

## Filter and manipulate counts matrices

After merging counts and data for all orthologous exons, we may want to filter out exons with very little coverage, those with certain evidence of alternative usage or within a particular range of PSI values.

```bash
filter_exon_counts -i EXON_COUNTS_FULL -T 5 -s 1 -o EXON_COUNTS_FILTERED
```
We can also take random subsets of exons to reduce the computational cost of fitting the models, or, for instance, check the number of exons that are required to obtain stable estimates of the OU parameters in your particular sets of species 

```bash
filter_exon_counts -i EXON_COUNTS_FILTERED -k 1000 -o EXON_COUNTS_FILTERED.sample
```

Finally, you may also stratify the sets of exons into different groups according to certain properties, such as exon or intron length, gene expresion, etc. This information must be externally provided in a different csv file containing the values for each exonID.

```bash
filter_exon_counts -i EXON_COUNTS_FILTERED -e EXON_DATA -n 5 -v EXON_LENGTH -o EXON_COUNTS_BY_LENGTH
```

# Evolutionary analyses

## Fit an Ornstein-Uhlenbeck model for alternative splicing data

## Detect shifts in the optimal values along the phylogeny


# Citation
None yet
