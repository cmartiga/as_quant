import numpy as np


def introduce_mutations(genome_fpath, p, genome):
    with open(genome_fpath) as fhand:
        for line in fhand:
            if not line.startswith('>'):
                bases = np.array([x for x in line.strip()])
                mutation_pos = np.where(np.random.uniform(size=len(bases)) < p)[0]
                if mutation_pos.shape[0] > 0:
                    new_bases = np.random.choice(['A', 'T', 'G', 'C'],
                                                 size=mutation_pos.shape[0])
                    bases[mutation_pos] = new_bases
                    line = ''.join(bases) + '\n'
            else:
                line = '>{}{}_'.format(genome, p) + line[1:]
            yield(line)


