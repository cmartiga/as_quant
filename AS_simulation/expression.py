from AS_simulation.psi import (exon_psi_to_transcripts_psi,
                               transcript_psi_to_exon_psi)
from AS_simulation.utils import extract_transcript_seq, get_exon_seqs
import numpy as np
from AS_quant.utils import format_exon_ids


def _format_exon_id(chrom, strand, exon):
    start, end = exon
    return('{}:{}-{}{}'.format(chrom, start, end, strand))


def _get_transcripts_psi(gene_exons, chrom, strand, exons_psi, max_skipped=3):
    exon_ids = (_format_exon_id(chrom, strand, exon) for exon in gene_exons)
    exons_psi = np.array([exons_psi.get(exon_id, 1) for exon_id in exon_ids])
    res = exon_psi_to_transcripts_psi(exons_psi, max_skipped=max_skipped)
    return(res)


def get_transcript_data(transcripts, psis, gene_tpms, gene_exons, exon_seqs,
                        gene_id, min_tpms=0.001):

    i = 0
    for transcript_exons, psi in zip(transcripts, psis):
        transcript_id = '{}.{}'.format(gene_id, i)
        transcript_tpms = gene_tpms * psi
        if transcript_tpms < min_tpms:
            continue
        exons = [exon for exon, take in zip(gene_exons, transcript_exons)
                 if take]
        seq = extract_transcript_seq(exons, exon_seqs)
        if not seq:
            continue
        record = {'transcript_id': transcript_id, 'seq': seq,
                  'psi': psi, 'tpm': transcript_tpms}
        yield(record)
        i += 1


def derive_transcripts_quant(parsed_gff, genome, exons_psi, tpms, 
                             max_skipped=3):
    for gene_id, gene_data in parsed_gff.items():
        if gene_id is None:
            continue
        transcripts = gene_data.get('transcripts', {})
        chrom, strand = gene_data['chrom'], gene_data['strand']
        longest_transcript = max(transcripts,
                                 key=lambda x: len(transcripts[x]))
        gene_exons = transcripts[longest_transcript]
        if strand == '-':
            gene_exons = gene_exons[::-1]

        gene_tpms = tpms.get(gene_id, 0)
        exon_seqs = get_exon_seqs(gene_exons, gene_data['chrom'],
                                  gene_data['strand'], genome)
        transcripts, psis = _get_transcripts_psi(gene_exons, chrom, strand,
                                                 exons_psi,
                                                 max_skipped=max_skipped)
        transcript_records = list(get_transcript_data(transcripts, psis,
                                                      gene_tpms, gene_exons,
                                                      exon_seqs, gene_id))
        new_exon_psi = transcript_psi_to_exon_psi(transcripts, psis)
        exon_ids = format_exon_ids(chrom, strand, gene_exons)
        new_exon_psi = dict(zip(exon_ids, new_exon_psi))
        yield(transcript_records, new_exon_psi)
