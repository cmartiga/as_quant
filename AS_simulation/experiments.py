from random import choice

from numpy.linalg.linalg import cholesky
import numpy as np
import pandas as pd


class TSsimulator(object):
    def __init__(self, n_trajectories, n_total, start=0, end=10, n_times=100,
                 equally_spaced=True, n_samples=2, alpha=0.5, rho=5):
        self.n_trajectories = n_trajectories
        self.start = start
        self.end = end
        self.duration = end - start
        self.n_times = n_times
        self.n_total = n_total
        self.n_samples = n_samples
        self.equally_spaced = equally_spaced

        self.alpha = alpha
        self.rho = rho

        self.times = self.simulate_time_points()
        self.sim_functs = {'linear': self.simulate_linear,
                           'sin': self.simulate_sin,
                           'parable': self.simulate_parable,
                           'sigmoid': self.simulate_sigmoid,
                           'gp': self.simulate_gp}

    def sample_sim_func(self, sim_funct):
        if sim_funct == 'mix':
            simulate = choice(list(self.sim_functs.values()))
        elif sim_funct not in self.sim_functs:
            msg = 'Invalid simulation function {}'.format(sim_funct)
            raise ValueError(msg)
        else:
            simulate = self.sim_functs[sim_funct]
        return(simulate)

    def simulate(self, sim_funct='sigmoid', **kwargs):
        thetas = np.vstack([self.sample_sim_func(sim_funct)()
                            for _ in range(self.n_trajectories)])
        sel_idxs = np.random.choice(np.arange(self.n_total),
                                    size=self.n_trajectories, replace=False)
        return(self.get_full_theta_matrix(self.n_total, self.times, thetas,
                                          sel_idxs))

    def simulate_time_points(self):
        if self.equally_spaced:
            times = np.linspace(self.start, self.end, self.n_times)
        else:
            times = np.random.uniform(self.start, self.end, size=self.n_times)
        times = np.array([times] * self.n_samples).flatten()
        return(times)

    def get_full_theta_matrix(self, total_regs, times, theta, sel_idxs):
        full_theta = np.zeros((total_regs, times.shape[0]))
        full_theta[sel_idxs] = theta
        return(full_theta)

    def sample_lag(self):
        return(np.random.uniform(self.start, self.end))

    def sample_sign(self, p=0.5):
        return(np.random.choice([1, -1], p=[p, 1 - p]))

    def simulate_sigmoid(self):
        lag = self.sample_lag()
        sign = self.sample_sign()
        return(sign / (1 + np.exp(-(self.times - lag))))

    def simulate_linear(self):
        beta = np.random.normal(0.75, 0.1) / self.duration * self.sample_sign()
        return(beta * (self.times - self.sample_lag()))

    def simulate_parable(self):
        a = np.random.normal(1.1, 0.1) / self.end ** 2
        lag, sign = self.sample_lag(), self.sample_sign()
        theta = sign * (a * (self.times - lag) ** 2)
        return(theta - theta.mean())

    def simulate_sin(self):
        lag = self.sample_lag()
        theta = np.sin(2 * np.pi / self.duration * self.times - lag)
        return(theta)

    def simulate_gp(self):
        covariance = [[self.alpha * np.exp(-(t2 - t1) ** 2 / self.rho)
                       for t1 in self.times]
                      for t2 in self.times]
        delta = np.zeros((self.times.shape[0], self.times.shape[0]))
        np.fill_diagonal(delta, 1e-6)
        L = cholesky(covariance + delta)
        theta = np.dot(L, np.random.normal(size=(delta.shape[0], 1))).flatten()
        return(theta - theta.mean())


class RegEvolSimulator(object):
    def __init__(self, n):
        self.n = n

    def simulate_P1(self, mean, sd):
        self.p1 = np.random.normal(mean, sd, size=self.n)

    def simulate_cis_divergence(self, sd, mean=0):
        self.cis = np.random.normal(mean, sd, size=self.n)

    def simulate_compensation(self, mean, sd):
        self.beta = np.random.normal(mean, sd, size=self.n)

    def simulate_trans_divergence(self, sd, mean=0):
        self.trans = self.cis * self.beta + np.random.normal(mean, sd,
                                                             size=self.n)

    def calc_P2(self):
        return(self.p1 + self.cis + self.trans)

    def calc_F1(self):
        self.h1 = self.p1
        self.h2 = self.p1 + self.cis

    def simulate(self, p1_mean=5, p1_sd=3, cis_mean=0, cis_sd=1, comp_mean=0.5,
                 comp_sd=0.2, trans_mean=0, trans_sd=1):
        self.simulate_P1(p1_mean, p1_sd)
        self.simulate_cis_divergence(cis_mean, cis_sd)
        self.simulate_compensation(comp_mean, comp_sd)
        self.simulate_trans_divergence(trans_sd, trans_mean)
        self.calc_P2()
        self.calc_F1()
        return(pd.DataFrame({'P1': self.p1, 'P2': self.p2,
                             'H1': self.h1, 'H2': self.h2}))


def simulate_OU(K, N, mu_0, mu_sd, t2_over_2a, phylo_hl, sigma,
                tau2_sigma, dmatrix):
    max_t = dmatrix.max().max()
    t0 = (max_t - dmatrix) / 2 + dmatrix
    mu = np.random.normal(mu_0, mu_sd, size=K)
    X0 = np.random.normal(mu, np.sqrt(t2_over_2a))

    alpha = np.log(2) / phylo_hl

    if tau2_sigma > 0:
        tau2 = t2_over_2a * 2 * alpha
        tau2 = np.exp(np.log(tau2) + tau2_sigma * np.random.normal(size=K))
        t2_over_2a = tau2 / (2 * alpha)
    else:
        t2_over_2a = np.full(K, t2_over_2a)
    t2_over_2a = np.diagflat(t2_over_2a)
    delta = np.diagflat(np.full(N, 1e-4))

    Omega = np.exp(-alpha * dmatrix) - np.exp(-2 * alpha * t0)
    L_Omega = cholesky(Omega + delta)

    X = np.vstack([(X0 - mu) * np.exp(-alpha * max_t / 2) + mu] * N)
    X += np.dot(np.dot(L_Omega, np.random.normal(0, 1, size=(N, K))),
                t2_over_2a)
    X += sigma * np.random.normal(size=X.shape)
    X = pd.DataFrame(X, index=dmatrix.columns).transpose()
    return(X)
