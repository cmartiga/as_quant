#!/usr/bin/env python

from _collections import defaultdict
from _pickle import dump, load
from os.path import exists, join
from time import ctime
import multiprocessing
import sys
import time

from pysam import FastaFile

import numpy as np


class LogTrack(object):
    '''Logger class'''

    def __init__(self, fhand=None):
        if fhand is None:
            fhand = sys.stderr
        self.fhand = fhand
        self.start = time.time()

    def init(self, dataset_dir, script_name):
        log_fpath = join(dataset_dir, '{}.log'.format(script_name))
        self.fhand = open(log_fpath, 'w')
        self.write('Start')

    def write(self, msg, add_time=True):
        if add_time:
            msg = '[ {} ] {}\n'.format(ctime(), msg)
        else:
            msg += '\n'
        self.fhand.write(msg)
        self.fhand.flush()

    def finish(self):
        t = time.time() - self.start
        self.write('Finished succesfully. Time elapsed: {:.1f} s'.format(t))


def load_calc_results(fpath, funct, args, force=False):
    '''Function to load generally pre-computed data if the given
       path exists'''

    if not force and exists(fpath):
        with open(fpath, 'rb') as fhand:
            results = load(fhand)
    else:
        results = funct(*args)
        with open(fpath, 'wb') as fhand:
            dump(results, fhand)
    return results


def parse_gff_line(line, sep='='):
    items = line.strip().split('\t')
    chrom, _, feature, start, end, _, strand = items[:7]
    start, end = int(start) - 1, int(end)  # Convert to 0-indexed
    attrs = {x.strip().split(sep)[0]: x.strip().split(sep)[1].strip('"')
             for x in items[-1].split(';') if x}
    return {'chrom': chrom, 'feature': feature, 'strand': strand,
            'start': start, 'end': end, 'attributes': attrs}


def parse_gff(gff_fhand, sep='=', store_transcripts=True, sp=None):
    '''Returns a dictionary with exon and intron coordinates for each gene
       only for protein coding genes'''

    parsed_gff = defaultdict(dict)
    parents = {}

    for line in gff_fhand:
        if line.startswith('#'):
            continue
        record = parse_gff_line(line, sep=sep)
        attrs = record['attributes']

        gene_id = attrs.get('gene_id', None)
        parent = attrs.get('Parent', None)
        _id = attrs.get('ID', None)
        if _id is not None:
            parents[_id] = parent
        parent = parents.get(parent, parent)
        # biotype = attrs.get('biotype', None)
        if gene_id is None:
            gene_id = parent
        if gene_id is not None:
            gene_id = gene_id.split(':')[-1]

        start, end = record['start'], record['end']

        # Select protein coding genes
        parsed_gff[gene_id].update({'strand': record['strand'],
                                    'chrom': record['chrom'],
                                    'gene_id': gene_id,
                                    'species': sp})

        if 'exon' in record['feature']:
            try:
                parsed_gff[gene_id]['exons'].add((start, end))
            except KeyError:
                parsed_gff[gene_id]['exons'] = set([(start, end)])

            if store_transcripts:
                transcript_id = attrs.get(
                    'transcript_id', attrs.get('Parent', None))
                if 'transcripts' not in parsed_gff[gene_id]:
                    parsed_gff[gene_id]['transcripts'] = {}

                if transcript_id is not None:
                    try:
                        parsed_gff[gene_id]['transcripts'][transcript_id].append(
                            (start, end))
                        parsed_gff[gene_id]['transcripts'][transcript_id].sort()
                    except KeyError:
                        parsed_gff[gene_id]['transcripts'][transcript_id] = [
                            (start, end)]
    return parsed_gff


def get_gene_ends(exons):
    coords = []
    for exon in exons:
        coords.extend(exon)
    coords.sort()
    return(coords[0], coords[-1])


def write_gff_record(gene_data, fhand, source='custom'):
    gene_id, chrom, gene_data, new_transcripts = gene_data
    strand = gene_data['strand']
    start, end = get_gene_ends(gene_data['exons'])
    gene_items = [chrom, source, 'gene', start, end, '.', strand, '.',
                  'ID={}'.format(gene_id)]
    fhand.write('\t'.join([str(x) for x in gene_items]) + '\n')
    for transcript_id, exons in new_transcripts.items():
        attrs = 'ID={};Parent={};gene_id={};transcript_id={}'
        t_start, t_end = get_gene_ends(exons)
        transcript_items = [chrom, source, 'mRNA', t_start, t_end, '.',
                            strand, '.',
                            attrs.format(transcript_id, gene_id,
                                         gene_id, transcript_id)]
        fhand.write('\t'.join([str(x) for x in transcript_items]) + '\n')

        for i, (start, end) in enumerate(exons):
            attrs = 'ID={}|{};Parent={};gene_id={};transcript_id={}'
            exon_items = [chrom, source, 'exon', start + 1, end, '.', strand, '.',
                          attrs.format(transcript_id, i, transcript_id,
                                       gene_id, transcript_id)]
            fhand.write('\t'.join([str(x) for x in exon_items]) + '\n')


def load_genome(fpath):
    return(FastaFile(fpath.strip('.gz')))


def reverse_complement(seq):
    complement = dict(zip('ACTGN', 'TGACN'))
    return ''.join([complement[x] for x in seq[::-1]])


def get_seq(genome, chrom, start, end, strand):
    if chrom not in genome.references:
        return('')
    seq = genome.fetch(chrom, start, end).upper()
    if strand == '-':
        seq = reverse_complement(seq)
    return seq


def run_parallel(func, iterable, threads=None, chunks=False):
    if threads is None:
        map_func = map
    else:
        workers = multiprocessing.Pool(threads)
        if chunks:
            map_func = workers.starmap
        else:
            map_func = workers.map
    return map_func(func, iterable)


def read_fasta_file(fhand, numerical=False):
    name = ''
    seq = [] if numerical else ''
    for line in fhand:
        if line == '\n':
            continue
        if line.startswith('>'):
            if seq:
                yield name, seq
            name = line.strip()[1:]
            seq = [] if numerical else ''
        else:
            if numerical:
                seq.extend([float(x) for x in line.strip().split(' ')])
            else:
                seq += line.strip()
    if seq:
        yield name, seq


def logit(p):
    return(np.log(p / (1 - p)))


def invlogit(X):
    return(np.exp(X) / (1 + np.exp(X)))


def write_fasta_quant(transcriptome_fpath, out_fpath, transcripts_quant):
    with open(out_fpath, 'w') as fhand:
        for line in open(transcriptome_fpath):
            if line.startswith('>'):
                line = line.strip()
                tid = line.lstrip('>')
                line = '{}${}\n'.format(line, transcripts_quant[tid])
            fhand.write(line)


def simulate_SE_transcripts(parsed_gff, gene_prefix=''):

    for gene_id, gene_data in parsed_gff.items():
        gene_id = '{}{}'.format(gene_prefix, gene_data['gene_id'])
        transcripts = gene_data.get('transcripts', {}).items()
        if not transcripts:
            continue
        exons = sorted(max(transcripts, key=lambda x: len(x[1]))[1])
        transcript_id = '{}.0'.format(gene_id)
        new_transcripts = {transcript_id: exons}
        chrom = '{}{}'.format(gene_prefix, gene_data['chrom'])
        if len(exons) > 3:
            for exon in exons[1:-1]:
                exon_id = '{}:{}-{}'.format(chrom, exon[0], exon[1])
                transcript_id = '{}.{}'.format(gene_id, exon_id)
                transcript_exons = [ex for ex in exons if ex != exon]
                new_transcripts[transcript_id] = transcript_exons
        yield(gene_id, chrom, gene_data, new_transcripts)


def get_exon_seqs(exons, chrom, strand, genome):
    exon_seqs = {}
    for exon in exons:
        start, end = exon
        exon_seq = get_seq(genome, chrom, start, end, strand=strand)
        exon_seqs[exon] = exon_seq
    return(exon_seqs)


def extract_transcript_seq(exons, exon_seqs):
    seq = ''
    for exon in exons:
        seq += exon_seqs.get(exon, '')
    return(seq)


def write_transcripts(gene_data, genome, fhand):
    _, chrom, gene_data, new_transcripts = gene_data
    strand = gene_data['strand']

    for transcript_id, exons in new_transcripts.items():
        exon_seqs = get_exon_seqs(exons, chrom, strand, genome)
        seq = extract_transcript_seq(chrom, exon_seqs)
        fhand.write('>{}\n{}\n'.format(transcript_id, seq))
