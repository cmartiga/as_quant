from itertools import combinations

from AS_simulation.utils import logit, invlogit
import numpy as np


def simulate_baseline_psi(n_events, p_alt=0.2, logit_scale=True):
    n_alt = int(n_events * p_alt)
    psi = np.append(np.random.uniform(size=(1, n_alt)),
                    np.random.beta(10, 1, size=(1, n_events - n_alt)))
    if logit_scale:
        return(logit(psi))
    return(psi)


def simulate_gene_transcripts_psi(logit_mean, logit_sd, n_transcripts):
    '''
       Simulates proportions of transcripts assuming only potential
       single exon skipping event happening with a low probability.
       Whever the alternative transcripts reach a proportion of 1, both
       main and remaining transcripts have 0 proportion

       logit_mean and logit_sd are the mean and standard deviation of the
       individual exon inclusion rates
       '''
    if n_transcripts == 1:
        return(np.array([1]))

    psis = []
    for _ in range(n_transcripts - 1):
        p = 1 / (1 + np.exp(np.random.normal(logit_mean, logit_sd)))
        p = min(p, 1 - sum(psis) - p)
        if p < 0:
            psis.append(0)
        else:
            psis.append(p)
    psis = np.append([1 - sum(psis)], [psis])
    return(psis)


def simulate_exon_logit_psi(logit_mean, logit_sd, p_alternative, n_exons,
                            delta=1e-6):
    logit_1 = logit(1. - delta)
    X = np.full(n_exons, logit_1)
    alternative = np.random.uniform(size=n_exons) < p_alternative
    n_alternative = alternative.sum()
    X[alternative] = np.random.normal(logit_mean, logit_sd, size=n_alternative)
    X[0], X[-1] = logit_1, logit_1
    return(np.transpose([X]))


def simulate_exon_psi(logit_mean, logit_sd, p_alternative, n_exons, delta=0):
    psi = np.full(n_exons, 1. - delta)
    psi[0], psi[-1] = 1. - delta, 1. - delta
    alternative = np.random.uniform(size=n_exons) < p_alternative
    n_alternative = alternative.sum()
    psi[alternative] = invlogit(np.random.normal(logit_mean, logit_sd,
                                                 size=n_alternative))
    return(psi)


def get_possible_transcripts(n_exons, max_skipped=3, alt_exon_idx=None):
    if n_exons < 3:
        return(np.array([[True] * n_exons]))

    n_internal_exons = n_exons - 2
    if alt_exon_idx is None:
        alt_exon_idx = list(range(1, n_exons - 1))
    max_skipped = min(max_skipped, n_internal_exons)
    transcripts = []
    for n_exons_skipped in range(max_skipped):
        for skipped_exons in combinations(alt_exon_idx, n_exons_skipped):
            skipped_exons = np.array(skipped_exons)
            transcript_exons = np.full(n_exons, True)
            if skipped_exons.shape[0] > 0:
                transcript_exons[skipped_exons] = False

            transcripts.append(transcript_exons)
    transcripts = np.vstack(transcripts)
    return(transcripts) 


def transcript_psi_to_exon_psi(transcripts, psis):
    return(np.dot(psis, transcripts))


def exon_psi_to_transcript_psi(transcripts, psis):
    transcripts_psi = []
    for transcript_exons in transcripts:
        psi = np.prod(psis[transcript_exons])
        skipped_exons = transcript_exons == False
        if (skipped_exons).sum() > 0:
            psi = psi * np.prod(1 - psis[skipped_exons])
        transcripts_psi.append(psi)
    transcripts_psi = np.array(transcripts_psi)
    return(transcripts_psi)


def _get_transcript_psi(exon_psi, max_skipped, alternative_exons):
    n_exons = exon_psi.shape[0]
    transcripts = get_possible_transcripts(n_exons, max_skipped=max_skipped,
                                           alt_exon_idx=alternative_exons)
    transcripts_psi = exon_psi_to_transcript_psi(transcripts, exon_psi)
    return(transcripts, transcripts_psi)


def exon_psi_to_transcripts_psi(exon_psi, max_skipped=3):
    ''''Calculates transcripts PSIs from given exon PSIs

        It assumes independent exon inclusion rates along the transcript
        such that combination of skipping events can be calculated
        by multiplying the marginal probabilities for each exon.

        As the number of alternative exons increases, the number of
        combinations increases very rapidly, so we limit to a maximum
        of N exons being skipped at the same time

        '''
    alternative_exons = np.where(np.logical_and(exon_psi != 1,
                                                exon_psi != 0))[0]
    transcripts, transcripts_psi = _get_transcript_psi(exon_psi,
                                                       max_skipped,
                                                       alternative_exons)

    # Fill missing transcripts with required amounts to fit marginal exon PSIs
    transcripts_psi = transcripts_psi / transcripts_psi.sum()
    return(transcripts, transcripts_psi)
