import numpy as np


def sample_total_counts(n_events, n_samples, expected_counts, sigma=1):
    mu = np.log(expected_counts) - sigma ** 2 / 2
    lambda_events = np.exp(np.random.normal(mu, sigma, size=n_events))
    lambda_events = np.vstack([lambda_events] * n_samples).transpose()
    total = np.random.poisson(lambda_events)
    return(total)


def get_AS_reads(X, expected_counts):
    psi = np.exp(X) / (1 + np.exp(X))
    total = sample_total_counts(X.shape[0], X.shape[1], expected_counts)
    inclusion = np.random.binomial(total, psi)
    return(inclusion, total)


def get_AS_reads_uniform(X, loglambda):
    psi = np.exp(X) / (1 + np.exp(X))
    total = np.random.poisson(np.exp(loglambda), size=X.shape)
    inclusion = np.random.binomial(total, psi)
    return(inclusion, total)
