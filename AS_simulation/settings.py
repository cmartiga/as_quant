from os.path import join, abspath, dirname

BASE_DIR = abspath(join(dirname(__file__), '..'))
DEF_RUNFILE = join(BASE_DIR, 'config', 's_8_4x.runfile')

VERSION = 0.1
