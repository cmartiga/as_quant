from numpy.linalg.linalg import cholesky, LinAlgError
from scipy.stats._multivariate import invwishart

import numpy as np


def simulate_corr_matrix(size):
    corr = np.ones((size, size))
    for i in range(size):
        for j in range(i + 1, size):
            corr[i, j] = np.random.beta(2, 5)
            corr[j, i] = corr[i, j]
    return(corr)


def simulate_scale_matrix(size, n, sigma):
    scale = np.ones((size, size)) * sigma
    for i in range(size):
        for j in range(i + 1, size):
            scale[i, j] = 1 / n * sigma
            scale[j, i] = 1 / n * sigma
    return(scale)


def _simulate_sites_matrix(n_events, n_regulators, sigma=1, K=2,
                           logitmean=-2.5):
    scale = simulate_scale_matrix(size=n_regulators, n=n_regulators + 1,
                                  sigma=sigma)
    covariance = invwishart.rvs(n_regulators + 1, scale=scale)
    L = cholesky(covariance)
    X_tilde = np.random.normal(0, 1, size=(n_regulators, n_events))
    X_mean = np.random.normal(logitmean, 1, size=(n_regulators, 1))
    X = np.transpose(X_mean + np.dot(L, X_tilde))
    p = np.exp(X) / (1 + np.exp(X))
    u = np.random.uniform(size=(n_events, n_regulators))
    return((u < p).astype(int))


def add_sites_matrix_errors(sites_matrix, p_error):
    u = np.random.uniform(size=sites_matrix.shape)
    new_sites_matrix = sites_matrix.copy()
    freq_sites = sites_matrix.mean(0)

    # Set p_error relative to the number of positive and negative sites
    # The expected number of errors in each set should be the same:
    # correct by the total number of them
    p = np.vstack([p_error * freq_sites] * u.shape[0])
    p_pos = np.vstack([p_error * (1 - freq_sites)] * u.shape[0])
    p[sites_matrix == 1] = p_pos[sites_matrix == 1]
    sel_pos = u < p_error
    new_sites_matrix[sel_pos] = 1 - sites_matrix[sel_pos]
    return(new_sites_matrix)


def simulate_sites_matrix(n_events, n_regulators, sigma=1, K=2,
                          logitmean=-2.5, **kwargs):
    try:
        return(_simulate_sites_matrix(n_events, n_regulators,
                                      sigma=sigma, K=K, logitmean=logitmean))
    except LinAlgError:
        return(simulate_sites_matrix(n_events, n_regulators, sigma=sigma, K=K,
                                     logitmean=logitmean))
        