#!/usr/bin/env python
import numpy as np
import pandas as pd


def add_counts_arguments(parser):
    msg = 'CSV file with inclusion counts per sample and exon'
    parser.add_argument('inclusion', help=msg)
    msg = 'CSV file with total counts per sample and exon'
    parser.add_argument('total', help=msg)


def add_features_argument(parser):
    help_msg = 'CSV file containing regulatory features for each exon in the '
    help_msg += 'dataset'
    parser.add_argument('-f', '--features', required=True, help=help_msg)
    

def add_design_arguments(parser, add_pred=False):
    help_msg = 'Sample design matrix (it should include "sample" and '
    help_msg += '"species" columns)'
    parser.add_argument('-d', '--design', required=True, help=help_msg)
    if add_pred:
        parser.add_argument('-p', '--design_pred', default=None,
                            help='Design matrix for group PSI estimates')
    parser.add_argument('--samples', default='sample',
                        help='Column header for sample (sample)')
    parser.add_argument('--ids', default=None,
                        help='File containing ids to select for fitting')

def add_contrast_argument(parser):
    parser.add_argument('--contrast_matrix', default=None,
                        help='Contrast matrix for dPSI estimation')


def add_tree_argument(parser):
    parser.add_argument('-p', '--phylogenetic_tree', required=True,
                        help='Phylogenetic tree in newick format')
    

def add_input_arguments(parser):
    input_group = parser.add_argument_group('Input')
    add_counts_arguments(input_group)
    add_design_arguments(parser)
    add_tree_argument(parser)


def add_log_bias_arguments(parser):
    help_msg = 'File containing log bias in inclusion rates for each exon and'
    help_msg += ' sample'
    obs_group = parser.add_argument_group('Bias options')
    obs_group.add_argument('--log_bias', default=None, help=help_msg)
    help_msg = 'Bias value to use by default for every exon. By default it use'
    help_msg += 's log(2) to account for exons having 2 possible inclusion SJ '
    help_msg += 'per skipping SJ. Set to "mean" to replace to use rowmeans'
    obs_group.add_argument('-b', '--log_bias_def', default=np.log(2),
                           help=help_msg)


def add_stan_arguments(parser):
    options_group = parser.add_argument_group('stan Options')
    options_group.add_argument('-n', '--nthreads', default=4,
                               help='Number of threads to fit the model (4)')
    options_group.add_argument('-c', '--nchains', default=4,
                               help='Number MCMC chains to fit the model (4)')
    options_group.add_argument('-N', '--n_samples', default=2000,
                               help='Number of MCMC samples (2000)')
    options_group.add_argument('--recompile', default=False, action='store_true',
                               help='Recompile stan model before fitting')

def add_OU_by_element_arguments(parser):
    model_group = parser.add_argument_group('Exon level OU model options')
    model_group.add_argument('-t12', '--phylo_hl', default=None, type=float,
                             help='Phylogenetic half-life to use for shifts')
    model_group.add_argument('-t2a_m', '--log_tau2_over_2a_mean', default=None,
                             type=float, help='log tau2_over_2a prior mean')
    model_group.add_argument('-t2a_sd', '--log_tau2_over_2a_sd', default=1,
                             type=float, help='log tau2_over_2a prior std')
    model_group.add_argument('-b_m', '--log_beta_mean', default=None, type=float,
                             help='log_beta prior mean')
    model_group.add_argument('-b_sd', '--log_beta_sd', default=1, type=float,
                             help='log_beta prior std')
    help_msg = 'File containing full posterior distribution of a previous fit'
    help_msg += ' not accounting for shifts to infer the global OU parameters'
    model_group.add_argument('--prior', default=None, help=help_msg)
    return(model_group)


def add_OU_horse_shoe_argument(parser): 
    help_msg = 'Sparsity inducing prior for shifts model (0.1)'
    parser.add_argument('-r', '--rho_prior', default=0.1, type=float,
                        help=help_msg)

    
def add_output_prefix_argument(parser):
    output_group = parser.add_argument_group('Output')
    output_group.add_argument('-o', '--output_prefix', required=True,
                              help='Prefix for output files')
    

def read_counts(inclusion_fpath, total_fpath, samples=None, sel_ids=None):
    inclusion = pd.read_csv(inclusion_fpath, index_col=0)
    total = pd.read_csv(total_fpath, index_col=0)
    
    if samples is None:
        samples = total.columns
    else:
        samples = np.intersect1d(samples, total.columns)
        
    total['exon_id'] = total.index
    if sel_ids is not None:
        total = total.loc[np.intersect1d(sel_ids, total.index), :]

    total.drop_duplicates('exon_id', inplace=True, keep=False)
    total.drop('exon_id', axis=1, inplace=True)
    inclusion = inclusion.loc[total.index, :]
    return(inclusion[samples].fillna(0).astype(int),
           total[samples].fillna(0).astype(int))


def read_log_bias(log_bias_fpath, exon_ids, sample_ids,
                  log_bias_def=np.log(2), log=None):
    if log_bias_fpath is not None:
        log_bias = pd.read_csv(log_bias_fpath, index_col=0)
        if log_bias.shape[1] == 1:
            log_bias = pd.DataFrame({sample_id: log_bias.iloc[:, 0]
                                     for sample_id in sample_ids},
                                    index=log_bias.index)
        log_bias = log_bias.loc[exon_ids, :][sample_ids]
        log_bias = log_bias.replace(np.inf, np.nan).replace(-np.inf, np.nan)
        
        if log_bias_def == 'mean':
            log_bias.apply(lambda row: row.fillna(row.mean()), axis=1)
        else:
            log_bias.fillna(log_bias_def, inplace=True)
        if log is not None:
            log.write('Loaded exon log(bias) from {}'.format(log_bias_fpath))
    else:
        if isinstance(log_bias_def, str):
            msg = 'Mean default value is not available if no bias is given'
            raise ValueError(msg)
            
        log_bias = pd.DataFrame(np.full((exon_ids.shape[0], sample_ids.shape[0]),
                                        log_bias_def),
                                index=exon_ids, columns=sample_ids)
        if log is not None:
            msg = 'Missing log(bias) information. Using {} as default'
            log.write(msg.format(log_bias_def))
    return(log_bias)
