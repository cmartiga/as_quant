from _collections import defaultdict

import pandas as pd
import numpy as np

from AS_quant.settings import STAR_STRANDS, STAR_STRANDS_VALUES
from AS_quant.utils import get_tabix_overlap


def parse_interval_id(interval_id):
    strand = None
    if interval_id[-1] in ['+', '-']:
        strand = interval_id[-1]
        interval_id = interval_id[:-1]
        
    chrom, coords = interval_id.split(':')
    start, end = [int(x) for x in coords.split('-')]
    return(chrom, start, end, strand)


class SpliceJunction(object):
    def __init__(self, chrom=None, start=None, end=None, strand=None,
                 tabix_line=None, upstream_start=None, downstream_end=None):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.strand = strand
        self.upstream_start = upstream_start
        self.upstream_dist = None
        if upstream_start:
            self.upstream_dist = self.start - self.upstream_start
        self.downstream_end = downstream_end
        self.downstream_dist = None
        if downstream_end:
            self.downstream_dist = self.downstream_end - self.end 
        if tabix_line is not None:
            self.parse_tabix_line(tabix_line)
        
    @property
    def id(self):
        id_str = '{}:{}-{}'.format(self.chrom, self.start, self.end)
        if self.strand is not None:
            id_str = id_str + self.strand
        return(id_str)
    
    @property
    def STAR_SJ_line(self):
        items = [self.chrom, str(self.start + 1), str(self.end),
                 self.strand_STAR]
        line = '\t'.join(items) + '\n'
        return(line)

    @property
    def intron_length(self):
        return(self.end - self.start)
    
    def csv_line(self, sample_ids=None):
        if sample_ids is None:
            self.counts.keys()
        sorted_counts = [str(self.counts[s]) for s in sample_ids]
        return('{},{}\n'.format(self.id,  ','.join(sorted_counts)))
    
    def parse_id(self, sj_id):
        chrom, start, end, strand = parse_interval_id(sj_id)
        self.__init__(chrom, start, end, strand)
    
    def parse_tabix_line(self, line):
        items = line.strip().split()
        self.chrom = items[0]
        self.start = int(items[1]) - 1
        self.end = int(items[2]) 
        if len(items) > 3:
            self.strand = self.parse_STAR_strand(items[3])

    def parse_STAR_strand(self, value):
        try:
            return(STAR_STRANDS_VALUES[value])
        except KeyError:
            msg = 'Error in SJ format file: 3rd column expected to have strand '
            msg += 'information {0,1,2}'
            raise ValueError(msg)
    
    @property
    def strand_STAR(self):
        if not hasattr(self, '_strand_STAR'):
            self._strand_STAR = STAR_STRANDS[self.strand] 
        return(self._strand_STAR)
    
    def parse_STAR_SJ(self, line, sample_id, include_multimapping=False):
        items = line.strip().split()
        self.chrom = items[0]
        self.start = int(items[1]) - 1
        self.end = int(items[2])
        self._strand_STAR = items[3]
        self.strand =  self.parse_STAR_strand(items[3])
        
        counts = int(items[6])
        if include_multimapping:
            counts += int(items[7])
        self.counts = {sample_id: counts}
        self.max_overhang = int(items[8])
    
    def parse_csv(self, line, sample_ids):
        items = line.strip().split(',')
        self.parse_id(items[0])
        self.counts = dict(zip(sample_ids, [int(x) for x in items[1:]]))
    
    def _sj_boundaries_are_correct(self, sj, l):
        start_correct = sj.start >= self.start - l
        end_correct = sj.end <= self.end + l
        return(start_correct and end_correct)
    
    def get_overlapping_SJ(self, sj_index, window_size):
        l = window_size / 2
        lines1 = set(get_tabix_overlap(self.chrom, self.start - l + 1,
                                       self.start + l, sj_index))
        lines2 = set(get_tabix_overlap(self.chrom, self.end - l,
                                       self.end + l, sj_index))
        for line in lines1.intersection(lines2):
            sj = SpliceJunction(tabix_line=line)
            if self._sj_boundaries_are_correct(sj, l):
                yield(sj)
    
    def read_is_stranded(self, read):
        strand = '-' if read.is_reverse else '+'
        if read.is_read1 and self.strand == strand:
            stranded = 0
        elif read.is_read2 and self.strand != strand:
            stranded = 0
        else:
            stranded = 1
        return(stranded)
    
    def calc_overlaps(self, read, start, end, read_length=None,
                      exon_upstream=None, max_intron_size=1e6):
        # This allows checking for complete mapping in the intronic flank
        # without long gaps. Exonic flank is enlarged in case there is an
        # additional intron before the read finishes at either end
        if read_length is not None and exon_upstream is not None:
            if exon_upstream:
                min_pos = max(0, start - max_intron_size - 1)
                overlap_size1 = read.get_overlap(min_pos, start-1)
                overlap_size2 = read.get_overlap(end, end + read_length)
            else:
                min_pos = max(0, start - read_length - 1)
                overlap_size1 = read.get_overlap(min_pos, start-1)
                overlap_size2 = read.get_overlap(end, end + max_intron_size)
        else:
            min_pos = max(0, start - max_intron_size - 1)
            overlap_size1 = read.get_overlap(min_pos, start-1)
            overlap_size2 = read.get_overlap(end, end + max_intron_size)
        return(overlap_size1, overlap_size2)
    
    def overlaps_segments(self, overlaps, min_overhang=1):
        return(np.all([x >= min_overhang for x in overlaps]))
    
    def up_overlap_valid(self, overlaps):
        return(self.upstream_start is None or
               overlaps[0] <= self.upstream_dist)
    
    def down_overlap_valid(self, overlaps):
        return(self.downstream_end is None or
               overlaps[1] <= self.downstream_dist)
    
    def read_is_valid(self, read, overlaps, rm_duplicates, min_mapq,
                      min_overhang):
        return(self.overlaps_segments(overlaps, min_overhang=min_overhang) and
                (not rm_duplicates or not read.is_duplicate) and
                read.mapping_quality >= min_mapq and not read.is_secondary and
                self.up_overlap_valid(overlaps) and
                self.down_overlap_valid(overlaps))
    
    def get_overlapping_reads(self, bamfile, ignore_strand=False,
                              min_overhang=1, rm_duplicates=True,
                              min_mapq=255):
        for read in bamfile.fetch(self.chrom, self.start-1, self.start):
            
            # Select spliced reads
            blocks = read.get_blocks()
            if len(blocks) <= 1:
                continue
            
            # Select those matching the SJ
            for i, (b1, b2) in enumerate(zip(blocks, blocks[1:])):
                if b1[1] == self.start and b2[0] == self.end:
                    break
            else:
                continue
            
            # Calculate overhangs
            blocks_length = [x[1] - x[0] for x in blocks]
            overlaps = sum(blocks_length[:i+1]), sum(blocks_length[i+1:])
            
            # Filter reads
            if not self.read_is_valid(read, overlaps, rm_duplicates, min_mapq,
                                      min_overhang):
                continue
            
            stranded = self.read_is_stranded(read)
            if not ignore_strand and self.strand == '-':
                overlaps = overlaps[::-1]
                
            record = {'read_id': read.query_name, 'stranded': stranded,
                      'upstream_overhang': overlaps[0],
                      'downstream_overhang': overlaps[1],
                      'read_length': sum(overlaps)}
            yield(record)
    
    def calc_pos_start(self, read_length):
        start = self.start - read_length + 1 
        if self.upstream_start is not None:
            start = max(start, self.upstream_start)
        return(start)
    
    def calc_pos_end(self, read_length):
        end = self.start 
        if self.downstream_end is not None:
            d = read_length - min(read_length, self.downstream_dist)
            end = end - d
        return(end)
    
    def calc_pos(self, read_length):
        start = self.calc_pos_start(read_length)
        end = self.calc_pos_end(read_length)
        pos = np.arange(self.start - end + 1, self.start - start + 1) 
        return(pos)
        
    def _calc_mapping_profile(self, reads, read_length=None):
        positions = self.calc_pos(read_length=read_length)
        counts = {0: {k: 0 for k in positions}, 1: {k: 0 for k in positions}}
        for read in reads:
            counts[read['stranded']][read['upstream_overhang']] += 1
        
        counts = pd.DataFrame.from_dict(counts)
        counts.columns = ['forward', 'reverse']
        counts['position'] = positions
        counts = pd.melt(counts, id_vars=['position'])
        counts.columns = ['position', 'strand', 'counts']
        return(counts)
    
    def get_mapping_profile(self, bamfile, min_overhang=1, read_length=None,
                            rm_duplicates=True, min_mapq=255):
        reads = self.get_overlapping_reads(bamfile, ignore_strand=False,
                                           min_overhang=min_overhang,
                                           rm_duplicates=rm_duplicates,
                                           min_mapq=min_mapq)
        counts = self._calc_mapping_profile(reads, read_length=read_length)
        return(counts)
    
    def get_mapping_unspliced_profile(self, bamfile, read_length,
                                    min_overhang=1, rm_duplicates=True,
                                    max_intron_size=1e6, min_mapq=255):
        # Deprecated
        reads = self.get_overlapping_reads(bamfile, self.start, self.start+1,
                                          ignore_strand=True,
                                          min_overhang=min_overhang,
                                          rm_duplicates=rm_duplicates,
                                          max_intron_size=max_intron_size,
                                          min_mapq=min_mapq,
                                          read_length=read_length,
                                          exon_upstream=True)
        counts1 = self.calc_mapping_profile(reads, read_length)
        
        reads = self.get_overlapping_reads(bamfile, self.end-1, self.end+1,
                                          ignore_strand=True,
                                          min_overhang=min_overhang,
                                          rm_duplicates=rm_duplicates,
                                          max_intron_size=max_intron_size,
                                          min_mapq=min_mapq,
                                          read_length=read_length,
                                          exon_upstream=False)
        counts2 = self.calc_mapping_profile(reads, read_length)
        counts = np.hstack([counts1, counts2])
        if self.strand == '-':
            counts = counts[::-1]
            
        return(counts)


class SpliceJunctionCounts(object):
    def __init__(self, include_multimapping=False):
        self.counts = defaultdict(dict)
        self.include_multimapping = include_multimapping
        
    def read_STAR_files(self, samples_fpaths_dict):
        for sample_id, fpath in samples_fpaths_dict.items():
            with open(fpath) as lines:
                self.parse_STAR_lines(lines, sample_id) 
    
    def parse_STAR_lines(self, lines, sample_id):
        for line in lines:
            sj = SpliceJunction()
            sj.parse_STAR_SJ(line, sample_id, self.include_multimapping)
            self.counts[sample_id][sj.id] = sj.counts[sample_id]
    
    def _counts_to_dataframe(self):
        self._dataframe = pd.DataFrame(self.counts).fillna(0).astype(int)
    
    @property
    def dataframe(self):
        if not hasattr(self, '_dataframe'):
            self._counts_to_dataframe()
        return(self._dataframe)
    
    @property
    def sj_ids(self):
        return(self.dataframe.index)
    
    @property
    def sample_ids(self):
        return(self.dataframe.columns)
    
    @property
    def n_sj(self):
        return(self.dataframe.shape[0])
    
    @property
    def n_samples(self):
        return(self.dataframe.shape[1])
    
    def write_csv(self, fpath):
        self.dataframe.to_csv(fpath)
    
    def from_dataframe(self, df):
        self._dataframe = df
        self.counts = self._dataframe.to_dict()
    
    def read_csv(self, fpath):
        self._dataframe = pd.read_csv(fpath, index_col=0)
        self.counts = self._dataframe.to_dict()


def get_max_interval(sjs, extended_len):
    min_coord = min([sj[1] for sj in sjs]) - extended_len 
    max_coord = min([sj[2] for sj in sjs]) + extended_len
    return(min_coord, max_coord)


def get_overlapping_sj(chrom, start, end, tabix_index):
    try:
        overlaps = tabix_index.fetch(chrom, start, end)
    except ValueError:
        overlaps = []
        
    for line in overlaps:
        _, s, e = line.strip().split()
        yield(int(s), int(e))


def _format_sj(sj):
    chrom, start, end = sj
    return('{}:{}-{}'.format(chrom, start, end))


def extend_sjs(sj_exons, sj_index, extended_len=50):
    
    for exon, inc_sj, skp_sj in sj_exons:
        chrom, start, end, _ = parse_exon(exon)
        start_mod = max(0, start - extended_len)
        end_mod = end + extended_len
        overlapping_sj = get_overlapping_sj(chrom, start_mod,
                                            end_mod, sj_index)
        inc_sj = set([parse_sj(sj) for sj in inc_sj if sj])
        skp_sj = set([parse_sj(sj) for sj in skp_sj if sj])
        sjs = inc_sj.union(skp_sj)
        min_start, max_end = get_max_interval(sjs, extended_len)
        
        detected_inc_sj, detected_skp_sj = [], []
        for sj_start, sj_end in overlapping_sj:
            # Remove SJs that go beyond the expected SJs even if overlapping
            # the exon
            if sj_start < min_start or sj_end > max_end:
                continue
            # Remove SJ contained within the exon
            if sj_start > start_mod and sj_end < end_mod:
                continue
            new_sj = (chrom, sj_start, sj_end)
            if sj_start < start_mod and sj_end > end_mod:
                detected_skp_sj.append(_format_sj(new_sj))
            elif ((sj_start > (end - extended_len) and sj_start < end_mod) or
                  (sj_end > start_mod and sj_end < (start + extended_len))):
                detected_inc_sj.append(_format_sj(new_sj))
        
        n_new_sj = len(set(detected_inc_sj + detected_inc_sj) - sjs)
        yield(exon, ';'.join(detected_inc_sj), ';'.join(detected_skp_sj), n_new_sj)
         

def get_sj(exon1, exon2):
    exon1, exon2 = sorted([exon1, exon2])
    return(exon1[1], exon2[0])


def extract_exons_sj(exons, chrom, strand, all_sj=False):
    if not all_sj:
        for e1, e2, e3 in zip(exons, exons[1:], exons[2:]):

            alt_exon = (chrom, e2[0], e2[1], strand)
            for acceptor, skipped_exons in [(e2, []), (e3, [alt_exon])]:
                start, end = get_sj(e1, acceptor)
                sj = chrom, start, end, strand
                included_exons = [(chrom, e1[0], e1[1], strand),
                                  (chrom, acceptor[0], acceptor[1], strand)]
                yield(sj, included_exons, skipped_exons)

    else:
        if strand == '-':
            exons = exons[::-1]
        for i, e1 in enumerate(exons):
            skipped_exons = []
            for e2 in exons[(i + 1):]:
                start, end = get_sj(e1, e2)
                sj = (chrom, start, end, strand)
                included_exons = [(chrom, e1[0], e1[1], strand),
                                  (chrom, e2[0], e2[1], strand)]
                yield(sj, included_exons, skipped_exons)
                skipped_exons.append((chrom, e2[0], e2[1], strand))


def fetch_SJ(parsed_gff, all_sj=False):
    for tdata in get_gtf_longest_transcripts(parsed_gff):
        chrom, strand, _, gene_exons = tdata
        for elements in extract_exons_sj(gene_exons, chrom, strand,
                                         all_sj=all_sj):
            yield(elements)


def format_sjs(sjs):
    return(';'.join(['{}:{}-{}'.format(chrom, start, end)
                     for chrom, start, end in sjs]))


def write_exon_sj(inclusion_sj, skipping_sj, fpath):
    with open(fpath, 'w') as fhand:
        fhand.write('exon_id,inc_sj,skp_sj\n')
        for exon in inclusion_sj:
            exon_id = '{}:{}-{}{}'.format(*exon)
            exon_inc = format_sjs(inclusion_sj[exon])
            exon_skp = format_sjs(skipping_sj[exon])
            fhand.write('{},{},{}\n'.format(exon_id, exon_inc, exon_skp))



def parse_exons_sj(fpath):
    with open(fpath) as fhand:
        header = ['exon_id', 'inc_sj', 'skp_sj']
        for line in fhand:
            items = line.strip().split(',')
            record = dict(zip(header, items))
            exon = record['exon_id']
            inc_sj = record['inc_sj'].split(';')
            skp_sj = record['skp_sj'].split(';')
            yield(exon, inc_sj, skp_sj)


def _get_sj_counts(sjs, sj_counts):
    counts = {sample: sum([sj_counts[sample].get(sj, 0) for sj in sjs])
              for sample in sj_counts.keys()}
    return(counts)


def get_sj_counts(inc_sj, skp_sj, sj_counts):
    inc_counts = _get_sj_counts(inc_sj, sj_counts)
    skp_counts = _get_sj_counts(skp_sj, sj_counts)
    return(inc_counts, skp_counts)

