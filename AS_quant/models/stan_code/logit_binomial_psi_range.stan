// Exon level model to infer maximal range of between tissues PSI

data {
    int<lower=1> N; // number of samples
    int<lower=1> C; // number of covariates
    
    vector[N] log_bias;

    // Matrices with correspondance between samples and tissue    
    matrix<lower=0, upper=1>[N, C] covariates;
    
    // Inclusion and total reads for exon skipping events in singleton and duplicates
    int <lower=0> inclusion[N]; 
    int <lower=0> total[N];
}

parameters {
    real alpha;
    vector[C] beta;

    real <lower=0> sigma;
    vector[N] X_raw;
}

transformed parameters {
    vector[N] X;
    X = alpha + covariates * beta + sigma * X_raw + log_bias;
}

model {
    alpha ~ normal(0, 2);
    beta ~ normal(0, 3);
    sigma ~ gamma(2, 0.5);
    X_raw ~ normal(0, 1);

    // Likelihood
    inclusion ~ binomial_logit(total, X);
}

generated quantities {
    vector[C] psi;
    real min_psi;
    real psi_range;

    psi = inv_logit(alpha + beta);
    min_psi = min(psi);
    psi_range = max(psi) - min_psi;
}
