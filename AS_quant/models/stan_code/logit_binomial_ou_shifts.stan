data {
    int<lower=1> N; // number of individuals
    int<lower=1> W; // number of branches

    vector<lower=0>[W] branch_lengths;
    int<lower=0, upper=W> parent_branches[W];
    int <lower=1, upper=W> species[N];      // Sample species vector
    matrix<lower=0>[N, N] dmatrix;  // individuals distance matrix

    real<lower=0> rho_prior;
    
    real<lower=0> phylo_hl;
    real<lower=0> tau2_over_2a;
    real<lower=0> sigma;

    vector[N] log_bias;
    int<lower=0> inclusion[N];
    int<lower=0> total[N];
}

transformed data{
    real alpha;
    vector[W] e_alpha_t;
    vector[W] e_2alpha_t;
    
    // OU parameters
    alpha = log(2) / phylo_hl;
    e_alpha_t = exp(-alpha * branch_lengths);
    e_2alpha_t = exp(-2 * alpha * branch_lengths);
}

parameters {
    real<lower=0, upper=1> opt_psi0;
    vector[W] nodes_means_raw;
    vector[N] X_raw;
    
    // Adaptation model parameters
    real <lower=0, upper=pi()/2> delta_mu_rho_raw;          // global shrinkage
    vector<lower=0, upper=pi()/2>[W] delta_mu_sigma_raw;    // local shrinkage
    vector[W] delta_mu_raw;                                 // Branch specific shift
}

transformed parameters {
    vector[W] mu;
    vector[W] nodes_mean;
    vector[N] X;
    vector[N] psi;
    vector[W] delta_mu;
    real expected_mean;
    real expected_sd;
    
    // Get optimal values and the evolving tratis along the different branches
    delta_mu = delta_mu_raw .* tan(delta_mu_sigma_raw) * tan(delta_mu_rho_raw) * rho_prior;
    
    // Initialize values at the root
    mu[1] = logit(opt_psi0);
    nodes_mean[1] = mu[1] + sqrt(tau2_over_2a) * nodes_means_raw[1];
    
    // Iterate over the tree nodes in root-to-leaves order
    for(w in 2:W){
        mu[w] = mu[parent_branches[w]] + delta_mu[w];
        expected_mean = nodes_mean[parent_branches[w]] * e_alpha_t[w] + (1 - e_alpha_t[w]) * mu[w];
        expected_sd = sqrt(tau2_over_2a * (1 - e_2alpha_t[w]));
        
        // Store nodes trait and optimal values
        nodes_mean[w] = expected_mean + expected_sd * nodes_means_raw[w]; 
    }
    
    // Get sample means adding variability and technical biases to the species means
    X = nodes_mean[species] + sigma * X_raw + log_bias;
}

model {
    opt_psi0 ~ beta(1, 1);
    nodes_means_raw ~ normal(0, 1);
    X_raw ~ normal(0, 1);
    
    delta_mu_rho_raw ~ uniform(0, pi()/2);
    delta_mu_sigma_raw ~ uniform(0, pi()/2);
    delta_mu_raw ~ normal(0, 1);
    
    // Likelihood
    inclusion ~ binomial_logit(total, X);
}
