data {
    int<lower=1> N; // number of individuals
    int<lower=1> W; // number of branches

    vector<lower=0>[W] branch_lengths;
    int<lower=0, upper=W> parent_branches[W];
    int <lower=1, upper=W> species[N];      // Sample species vector
    matrix<lower=0>[N, N] dmatrix;  // individuals distance matrix
    
    real<lower=0> rho_prior;
    
    real<lower=0> phylo_hl;
    real<lower=0> tau2_over_2a;
    real<lower=0> sigma;

    vector[N] log_bias;
    vector[N] log_norm_factors;
    int<lower=-1> counts[N];
}

transformed data{
    real alpha;
    vector[W] e_alpha_t;
    vector[W] e_2alpha_t;
    
    alpha = log(2) / phylo_hl;
    e_alpha_t = exp(-alpha * branch_lengths);
    e_2alpha_t = exp(-2 * alpha * branch_lengths);
}



parameters {
    real mu0;
    vector[W] nodes_means_raw;
    vector[N] X_raw;
    
    // Adaptation model parameters
    real <lower=0, upper=pi()/2> delta_mu_rho_raw;          // global shrinkage
    vector<lower=0, upper=pi()/2>[W] delta_mu_sigma_raw;    // local shrinkage
    vector[W] delta_mu_raw;    
}


transformed parameters {
    vector[W] mu;
    vector[W] nodes_mean;
    vector[N] X;
    vector[W] delta_mu;
    real expected_mean;
    real expected_sd;
    
    // Get optimal values and the evolving tratis along the different branches
    delta_mu = delta_mu_raw .* tan(delta_mu_sigma_raw) * tan(delta_mu_rho_raw) * rho_prior;
    
    // Initialize values at the root
    mu[1] = mu0;
    nodes_mean[1] = mu[1] + sqrt(tau2_over_2a) * nodes_means_raw[1];
    
    // Iterate over the tree nodes in root-to-leaves order
    for(w in 2:W){
        mu[w] = mu[parent_branches[w]] + delta_mu[w];
        expected_mean = nodes_mean[parent_branches[w]] * e_alpha_t[w] + (1 - e_alpha_t[w]) * mu[w];
        expected_sd = sqrt(tau2_over_2a * (1 - e_2alpha_t[w]));
        nodes_mean[w] = expected_mean + expected_sd * nodes_means_raw[w]; 
    }
    
    // Get sample means adding variability and technical biases to the species means
    X = nodes_mean[species] + sigma * X_raw + log_bias;
}


model {
    mu0 ~ normal(0, 5);
    nodes_means_raw ~ normal(0, 1);
    X_raw ~ normal(0, 1);
    
    delta_mu_rho_raw ~ uniform(0, pi()/2);
    delta_mu_sigma_raw ~ uniform(0, pi()/2);
    delta_mu_raw ~ normal(0, 1);
    
    // Likelihood
    for (n in 1:N){
        // Handling missing genes in the data with -1 counts
        if(counts[n] >= 0){
            counts[n] ~ poisson_log(X[n]);    
        }      
    }
}
