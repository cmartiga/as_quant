data {
    int<lower=1> K; // number of exons
    int<lower=1> N; // number of individuals
    int<lower=1> C; // number of covariates
    
    matrix<lower=0>[N, N] dmatrix;        // individuals distance matrix
    matrix[N, K] log_bias;
    matrix[N, N] cmatrix[C];           // matrices with co-varying variables
    
    int<lower=0> inclusion[N, K];
    int<lower=0> total[N, K];
}

transformed data{
    real max_t;
    real offset;
    matrix <lower=0> [N, N] total_t_matrix;
    
    offset = 1e-6;
    max_t = max(dmatrix);
    total_t_matrix = (max_t - dmatrix) / 2 + dmatrix;
}

parameters {
    real mu_mean;
    real <lower=0> mu_sd;
    vector[K] mu_raw;
    vector[K] X0_raw;
    matrix[N, K] X_raw;
    vector<lower=0>[C] sigma2_cov;
    
    // New OU parametrization
    real <lower=0> phylo_hl_raw;        // Scaled Phylogenetic halflife: easier to set a prior
    real <lower=0> tau2_over_2a;        // Equilibrium variance
    real <lower=0> beta;            // Proportionality constant
}

transformed parameters {
    real phylo_hl;
    real alpha;
    real e_alpha_t;
    real sigma2;
    
    vector[K] mu;
    vector[K] X0;
    matrix[N, K] X;
    
    matrix[N, N] Sigma;
    matrix[N, N] L;
    
    // OU parameters
    phylo_hl = phylo_hl_raw * max_t;
    alpha = log(2) / phylo_hl;
    sigma2 = tau2_over_2a * 2 * alpha / beta;
    e_alpha_t = exp(-alpha * max_t / 2);
    
    // Ancestral values from the equilibrium distribution
    mu = mu_mean + mu_sd * mu_raw;
    X0 = mu + sqrt(tau2_over_2a) * X0_raw;   
    
    // Ornstein Uhlenbeck covariance
    Sigma = tau2_over_2a * (exp(-alpha * dmatrix) - exp(-2 * alpha * total_t_matrix)) + diag_matrix(rep_vector(sigma2, N));
    for(c in 1:C)
        Sigma = Sigma + sigma2_cov[c] * cmatrix[c];
    L = cholesky_decompose(Sigma);
    X = rep_matrix((X0 - mu) * e_alpha_t + mu, N)' + L * X_raw  + log_bias; 
}

model {
    mu_mean ~ normal(2, 3);
    mu_sd ~ gamma(2, 0.5);
    sigma2_cov ~ gamma(2, 0.5);
    beta ~ lognormal(3, 1);
    phylo_hl_raw ~ gamma(2, 4);
    tau2_over_2a ~ gamma(2, 3);
    
    mu_raw ~ normal(0, 1);
    X0_raw ~ normal(0, 1);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    for (n in 1:N)
        inclusion[n] ~ binomial_logit(total[n], X[n]);
}
