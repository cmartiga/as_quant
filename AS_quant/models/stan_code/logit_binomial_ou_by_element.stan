data {
    int<lower=1> N; // number of individuals
    int<lower=1> W; // number of branches

    vector<lower=0>[W] branch_lengths;
    int<lower=0, upper=W> parent_branches[W];
    int <lower=1, upper=W> species[N];      // Sample species vector

    real<lower=0> phylo_hl;
    real<lower=0> log_tau2_over_2a_mean;
    real<lower=0> log_tau2_over_2a_sd;
    real<lower=0> log_beta_mean;
    real<lower=0> log_beta_sd;

    vector[N] log_bias;
    int<lower=0> inclusion[N];
    int<lower=0> total[N];
}

transformed data{
    real alpha;
    vector[W] e_alpha_t;
    vector[W] e_2alpha_t;
    
    // OU parameters
    alpha = log(2) / phylo_hl;
    e_alpha_t = exp(-alpha * branch_lengths);
    e_2alpha_t = exp(-2 * alpha * branch_lengths);
}

parameters {
    real<lower=0> tau2_over_2a;
    real<lower=0> beta;
    
    real<lower=0, upper=1> opt_psi0;
    vector[W] nodes_means_raw;
    vector[N] X_raw;
}

transformed parameters {
    real mu;
    real sigma;
    vector[W] nodes_mean;
    vector[N] X;
    real expected_mean;
    real expected_sd;
    real x0_branch;
    
    // Initialize values at the root
    mu = logit(opt_psi0);
    nodes_mean[1] = mu + sqrt(tau2_over_2a) * nodes_means_raw[1];
    
    // Iterate over the tree nodes in root-to-leaves order
    for(w in 2:W){
        x0_branch = nodes_mean[parent_branches[w]]; 
        expected_mean = x0_branch * e_alpha_t[w] + (1 - e_alpha_t[w]) * mu;
        expected_sd = sqrt(tau2_over_2a * (1 - e_2alpha_t[w]));
        
        // Store nodes trait and optimal values
        nodes_mean[w] = expected_mean + expected_sd * nodes_means_raw[w]; 
    }
    
    // Get sample means adding variability and technical biases to the species means
    sigma = sqrt(tau2_over_2a * 2 * alpha / beta);
    X = nodes_mean[species] + sigma * X_raw + log_bias;
}

model {
    tau2_over_2a ~ lognormal(log_tau2_over_2a_mean, log_tau2_over_2a_sd);
    beta ~ lognormal(log_beta_mean, log_beta_sd);

    opt_psi0 ~ beta(1, 1);
    nodes_means_raw ~ normal(0, 1);
    X_raw ~ normal(0, 1);
    
    // Likelihood
    inclusion ~ binomial_logit(total, X);
}
