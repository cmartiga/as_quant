data {
    int<lower=1> K;                         // number of exons
    
    matrix[1, K] log_bias;                // log mappability ratio of inc-skp reads
    
    int<lower=0> inclusion[1, K];
    int<lower=0> total[1, K];
}

parameters {
    real X_mean;
    real <lower=0> X_sd;
    vector[K] X_raw;
    real <lower=0, upper=1> theta;
}

transformed parameters {
    vector[K] X_real;
    vector[K] X_obs;

    X_real = X_mean + X_sd * X_raw;
    X_obs = X_real + log_bias[1]';
}

model {
    X_mean ~ normal(5, 3);
    X_sd ~ gamma(2, 1);
    X_raw ~ normal(0, 1);
    theta ~ beta(1, 1);
    
    // 1-inflated PSI distribution
    for(k in 1:K)
        if(inclusion[1, k] == total[1, k])
            target += log_sum_exp(log(theta), log(1 - theta) + binomial_logit_lpmf(inclusion[1, k] | total[1, k], X_obs[k]));
        else
            target += log(1 - theta) + binomial_logit_lpmf(inclusion[1, k] | total[1, k], X_obs[k]);
}


generated quantities{
    vector[K] psi;
    real<lower=0, upper=1> p_const;
    
    for(k in 1:K){
        if(inclusion[1, k] == total[1, k]){
            p_const = theta / (theta  + exp(log1m(theta) + binomial_logit_lpmf(inclusion[1, k] | total[1, k], X_obs[k])));
            psi[k] = p_const + (1 - p_const) * inv_logit(X_real[k]);
        }
        else{
            psi[k] = inv_logit(X_real[k]);
        }
    }
}
