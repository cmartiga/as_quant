data {
    int<lower=1> N; // number of individuals
    int<lower=1> W; // number of branches
    int<lower=1> S; // number of species

    vector<lower=0>[W] branch_lengths;
    int<lower=1, upper=W> branch_order[W];              
    int<lower=0, upper=W> parent_branches[W];
    int <lower=1, upper=W> species[N];      // Sample species vector
    matrix<lower=0>[S, S] sp_dmatrix;          // Species distance matrix
    matrix<lower=0, upper=1>[N, S] samples_species;
    vector[N] log_norm_factors;

    real<lower=0> rho_prior;
    real<lower=0> phylo_hl;
    real<lower=0> beta;
    real<lower=0> sigma;
    vector[N] log_bias;

    int<lower=-1> counts[N];
}

transformed data{
    real max_t;
    real offset;
    matrix <lower=0> [S, S] total_t_matrix;
    
    real alpha;
    real tau2_over_2a;
    real phi;
    vector[W] e_alpha_t;
    matrix[S, S] Sigma;
    matrix[S, S] L;
    
    offset = 1e-6;
    max_t = max(sp_dmatrix);
    total_t_matrix = (max_t - sp_dmatrix) / 2 + sp_dmatrix;
    
    // OU parameters
    alpha = log(2) / phylo_hl;
    tau2_over_2a = beta * sigma^2 / (2 * alpha);
    phi = 1 / sigma^2;
    e_alpha_t = exp(-alpha * branch_lengths);
    Sigma = tau2_over_2a * (exp(-alpha * sp_dmatrix) - exp(-2 * alpha * total_t_matrix)) + diag_matrix(rep_vector(offset, S));
    L = cholesky_decompose(Sigma);
}

parameters {
    real<lower=0, upper=1> opt_psi0;
    real X0_raw;
    vector[S] X_raw;
    
    // Adaptation model parameters
    real <lower=0, upper=pi()/2> delta_mu_rho_raw;          // global shrinkage
    vector<lower=0, upper=pi()/2>[W] delta_mu_sigma_raw;    // local shrinkage
    vector[W] delta_mu_raw;                                 // Branch specific shift
}

transformed parameters {
    vector[W] mu;
    vector[W] nodes_means;
    vector[N] m;
    vector[W] delta_mu;
    
    // Get optimal values along the tree with a BM model with branch specific variance
    delta_mu = delta_mu_raw .* tan(delta_mu_sigma_raw) * tan(delta_mu_rho_raw) * rho_prior;
    mu[branch_order[1]] = logit(opt_psi0);
    nodes_means[branch_order[1]] = mu[branch_order[1]] + sqrt(tau2_over_2a) * X0_raw;
    for(w in 2:W){
        mu[branch_order[w]] = mu[parent_branches[branch_order[w]]] + delta_mu[branch_order[w]];
        nodes_means[branch_order[w]] = (nodes_means[parent_branches[branch_order[w]]] - mu[branch_order[w]]) * e_alpha_t[branch_order[w]] + mu[branch_order[w]];
    }
    
    // Ornstein Uhlenbeck covariance
    m = samples_species * (nodes_means[species] + L * X_raw) + log_bias + log_norm_factors;
}

model {
    opt_psi0 ~ beta(1, 1);
    X0_raw ~ normal(0, 1);
    X_raw ~ normal(0, 1);
    
    delta_mu_rho_raw ~ uniform(0, pi()/2);
    delta_mu_sigma_raw ~ uniform(0, pi()/2);
    delta_mu_raw ~ normal(0, 1);
    
    // Likelihood
    for (n in 1:N){
        if(counts[n] >= 0){
            counts[n] ~ neg_binomial_2_log(m[n], phi);    
        }      
    }
}
