data {
    int<lower=1> K;                         // number of exons
    
    vector[K] log_map_ratio;                // log mappability ratio of inc-skp reads
    
    int<lower=0> inclusion[K];
    int<lower=0> total[K];
}

parameters {
    real X_mean;
    real <lower=0> X_sd;
    vector[K] X_raw;
}

transformed parameters {
    vector[K] X_real;
    vector[K] X_obs;

    X_real = X_mean + X_sd * X_raw;
    X_obs = X_real + log_map_ratio;
}

model {
    X_mean ~ normal(5, 3);
    X_sd ~ gamma(2, 1);
    X_raw ~ normal(0, 1);
    
    inclusion ~ binomial_logit(total, X_obs);
}
generated quantities{
    vector[K] psi;
    
    psi = inv_logit(X_real);
}
