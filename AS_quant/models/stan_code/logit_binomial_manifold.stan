// Model for exon allelic manifold 

data {
    int<lower=1> K; // number of samples
    int<lower=1> N; // number of samples
    int<lower=1> C; // number of covariates
    
    matrix[N, K] log_bias;

    // Matrices with correspondance between samples and tissue    
    matrix<lower=0, upper=1>[N, C] covariates;
    
    // Inclusion and total reads for exon skipping events in singleton and duplicates
    int <lower=0> inclusion[N, K]; 
    int <lower=0> total[N, K];
}

parameters {
    real alpha_mu;
    real<lower=0> alpha_sigma;
    vector[K] alpha_raw;
    
    vector[C] beta;

    real <lower=0> sigma;
    matrix[N, K] X_raw;
}

transformed parameters {
    vector[K] alpha;
    matrix[N, K] X;
    
    alpha = alpha_mu + alpha_sigma * alpha_raw;
    X = rep_matrix(alpha, N)' + rep_matrix(covariates * beta, K) + sigma * X_raw + log_bias;
}

model {
    alpha_mu ~ normal(2, 2);
    alpha_sigma ~ normal(0, 2);
    alpha_raw ~ normal(0, 1);
    
    beta ~ normal(0, 3);
    sigma ~ gamma(2, 0.5);
    to_row_vector(X_raw) ~ normal(0, 1);

    // Likelihood
    for(n in 1:N)
        inclusion[n] ~ binomial_logit(total[n], X[n]);
}
