data {
    int<lower=1> N; // number of samples
    int<lower=1> K; // number of events
    int<lower=1> W; // number of features

    real theta_sigma_sd;    
    
    vector <lower=0, upper=1> [N] design;
    matrix <lower=0, upper=1> [K, W] features;

    matrix[N, K] log_bias;    
    int <lower=0> inclusion[N, K]; 
    int <lower=0> total[N, K];
}

parameters {
    real alpha_mu;
    real<lower=0> alpha_sigma;
    vector[K] alpha_raw;
    
    vector[W] theta_raw;
    real<lower=0> theta_sigma;
    
    real <lower=0> sigma;
    matrix[N, K] X_raw;
    
    real <lower=0> beta_sigma;
    vector[K] beta_raw;
}


transformed parameters {
    matrix[N, K] X;
    vector[W] theta;
    vector[K] alpha;
    vector[K] beta;
    
    alpha = alpha_mu + alpha_raw * alpha_sigma;
    theta = theta_sigma * theta_raw;
    beta = features * theta + beta_sigma * beta_raw;
    X = rep_matrix(alpha, N)' + design * beta' + sigma * X_raw; 
}

model {
    alpha_mu ~ normal(3, 3);
    alpha_raw ~ normal(0, 1);
    alpha_sigma ~ normal(0, 2);
    
    theta_raw ~ normal(0, 1);
    theta_sigma ~ normal(0, theta_sigma_sd);
    
    sigma ~ normal(0, 2);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    beta_sigma ~ normal(0, 2);
    beta_raw ~ normal(0, 1);
    
    for (i in 1:N)
        inclusion[i] ~ binomial_logit(total[i], X[i] + log_bias[i]);
}
