data {
    int<lower=1> K; // number of exons
    int<lower=1> N; // number of individuals
    int<lower=1> M; // number of populations
    
    matrix<lower=0, upper=1>[N, M] populations;       // matrix individuals-population
    int<lower=0> n_generations;
    vector<lower=0>[N] Ne;
    
    matrix[N, K] log_bias;
    int<lower=0> inclusion[N, K];
    int<lower=0> total[N, K];
}

parameters {
    real X0_mean;
    real <lower=0> X0_sd;
    vector[K] X0_raw;
    
    // Lande parameters
    cholesky_factor_corr[K] Lcorr;
    cholesky_factor_corr[K] Lcorr_res;
    real log_sigma2_m;
    real log_sigma2_res;
    
    matrix[K, M] pop_delta_raw;
    matrix[K, N] X_raw;
    
}

transformed parameters {
    vector[K] X0;
    matrix[M, K] pop_delta;
    matrix[N, K] res;
    matrix[N, K] X;
    vector[N] h2s2;
    real sigma2_m;
    real sigma2_res;
    vector[N] s2;
    real tau;
    
    matrix[K, K] Mcorr;
    matrix[K, K] Rcorr;
    matrix[K, K] Sigma_res;
    matrix[K, K] P;
    matrix[K, K] P_L;
    
    sigma2_m = exp(log_sigma2_m);
    sigma2_res = exp(log_sigma2_res);
    
    X0 = X0_mean + X0_sd * X0_raw;
    
    // Calculate population divergence
    tau = sqrt(2 * sigma2_m * n_generations);
    pop_delta = (tau * Lcorr * pop_delta_raw)';

    // Calculate population diversity assuming uncorrelated residual variance
    Mcorr = Lcorr * Lcorr';
    Rcorr = Lcorr_res * Lcorr_res';
    h2s2 = 2 * sigma2_m * Ne;
    Sigma_res = sigma2_res * Rcorr;
    
    for(n in 1:N){
      P = h2s2[n] * Mcorr + Sigma_res;
      P_L = cholesky_decompose(P);
      res[n,] = (P_L * X_raw[,n])'; 
    }
    
    // Sum effects
    X = rep_matrix(X0, N)' + populations * pop_delta + res + log_bias; 
}

model {
    X0_mean ~ normal(3, 3);
    X0_sd ~ gamma(2, 0.5);
    X0_raw ~ normal(0, 1);

    Lcorr ~ lkj_corr_cholesky(1);
    Lcorr_res ~ lkj_corr_cholesky(1);    
    log_sigma2_m ~ normal(-6, 5);
    log_sigma2_res ~ normal(-2, 3);
    
    to_row_vector(pop_delta_raw) ~ normal(0, 1);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    for (n in 1:N)
        inclusion[n] ~ binomial_logit(total[n], X[n]);
}

