data {
    int<lower=1> N; // number of individuals
    int<lower=1> K; // number of exons

    matrix[N, K] log_bias;
    int<lower=0> inclusion[N, K];
    int<lower=0> total[N, K];
}


parameters {
    real X0_mean;
    real <lower=0> X0_sd;
    vector[K] X0_raw;
    
    matrix[N, K] X_raw;
    
    real log_sigma2_mean;
    real<lower=0> log_sigma2_sd;
    vector[K] log_sigma2_raw;
}

transformed parameters {
    vector[K] sigma2;
    vector[K] X0;
    matrix[N, K] X;
    matrix[N, K] dX;
    
    X0 = X0_mean + X0_sd * X0_raw;
    sigma2 = exp(log_sigma2_mean + log_sigma2_sd * log_sigma2_raw);
    dX = diag_post_multiply(X_raw, sqrt(sigma2));
    X = rep_matrix(X0, N)' + dX  + log_bias;
}

model {
    X0_mean ~ normal(3, 3);
    X0_sd ~ gamma(2, 1);
    X0_raw ~ normal(0, 1);
    
    to_row_vector(X_raw) ~ normal(0, 1);
    
    log_sigma2_mean ~ normal(log(0.5), 1);
    log_sigma2_sd ~ gamma(2, 1);
    log_sigma2_raw ~ normal(0, 1);
    
    // Likelihood
    for (n in 1:N)
        inclusion[n] ~ binomial_logit(total[n], X[n]);
}
