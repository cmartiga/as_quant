// To maintain common parameter values we have used sigma2 as overdispersion
// parameter for the negative-binomial distribution and beta for the ratio
// tau2/sigma2, which now has a realitively different meaning

data {
    int<lower=1> K; // number of exons
    int<lower=1> S; // number of species
    int<lower=1> N; // number of individuals
    
    matrix<lower=0>[S, S] sp_dmatrix;        // species distance matrix
    matrix[N, K] log_bias;
    matrix<lower=0, upper=1>[N, S] samples_species;
    
    int<lower=-1> counts[N, K];
}

transformed data{
    real max_t;
    real offset;
    matrix <lower=0> [S, S] total_t_matrix;
    
    offset = 1e-6;
    max_t = max(sp_dmatrix);
    total_t_matrix = (max_t - sp_dmatrix) / 2 + sp_dmatrix;
}

parameters {
    real mu_mean;
    real <lower=0> mu_sd;
    vector[K] mu_raw;
    vector[K] X0_raw;
    matrix[S, K] X_raw;
    real <lower=0> sigma2;           // Overdispersion parameter for negative-binomial
                                     // represents intra-specific variation
    
    vector[N-1] log_norm_factors_raw;
    
    // New OU parametrization
    real <lower=0> phylo_hl_raw;        // Scaled Phylogenetic halflife: easier to set a prior
    
    // Rate variation
    real tau2_mu;              
    real <lower=0> tau2_sigma;           
    vector[K] tau2_raw;  
}

transformed parameters {
    real phylo_hl;
    real alpha;
    real e_alpha_t;
    real phi;
    vector[K] tau2_over_2a;
    
    vector[K] mu;
    vector[K] X0;
    matrix[S, K] X;
    matrix[N, K] m;
    vector[N] log_norm_factors;
    
    matrix[S, S] Omega;
    matrix[S, S] L_Omega;
    
    // OU parameters
    phylo_hl = phylo_hl_raw * max_t;
    alpha = log(2) / phylo_hl;
    e_alpha_t = exp(-alpha * max_t / 2);

    // Rate variation
    tau2_over_2a = exp(tau2_mu + tau2_sigma + tau2_raw) / (2 * alpha);
    
    // Ancestral values from the equilibrium distribution
    mu = mu_mean + mu_sd * mu_raw;
    X0 = mu + diag_matrix(sqrt(tau2_over_2a)) * X0_raw; 
    
    // Ornstein Uhlenbeck covariance
    Omega = (exp(-alpha * sp_dmatrix) - exp(-2 * alpha * total_t_matrix)) + diag_matrix(rep_vector(1e-6, S));
    L_Omega = cholesky_decompose(Omega);
    
    // Species means
    X = rep_matrix((X0 - mu) * e_alpha_t + mu, S)' + L_Omega * X_raw * diag_matrix(sqrt(tau2_over_2a));
    
    // Get samples params
    log_norm_factors[1] = 0;
    log_norm_factors[2:N] = log_norm_factors_raw;
    phi = 1 / sigma2;
    m = samples_species * X + log_bias + rep_matrix(log_norm_factors, K);
}

model {
    mu_mean ~ normal(2, 3);
    mu_sd ~ gamma(2, 0.5);
    phylo_hl_raw ~ gamma(2, 4);
    sigma2 ~ gamma(1, 1);
    log_norm_factors_raw ~ normal(0, 1);
    
    tau2_mu ~ normal(0, 2);
    tau2_sigma ~ gamma(2, 1);
    tau2_raw ~ normal(0, 1);
    
    mu_raw ~ normal(0, 1);
    X0_raw ~ normal(0, 1);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    for (n in 1:N){
        for(k in 1:K){
            if(counts[n, k] >= 0){
                counts[n, k] ~ neg_binomial_2_log(m[n, k], phi);    
            }      
        }
    }
}
