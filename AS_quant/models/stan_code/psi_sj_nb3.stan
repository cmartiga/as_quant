data {
    int<lower=1> S;                         // number of SJ
    int<lower=3> N;                         // number of nodes
    int<lower=1, upper=N-2> A;              // number of alternative nodes
    int<lower=1> P;                         // number of positions

    int<lower=1, upper=N> sources[S];        // nodes idx source for each SJ
    int<lower=1, upper=N> targets[S];        // nodes idx target for each SJ
    int<lower=1, upper=N> alternative[A];    // alternative nodes idx
    int<lower=0, upper=1> single_sources[S]; // boolean vector for single sources    
    
    int <lower=1, upper=S> pos_sj_idx[P];   // index of the SJ to which each position belongs
    int<lower=0> counts[P];                 // number of reads at each position
    
    real <lower=0> inv_phi;                 // Negative binomial inverse phi
    
    // For specifying prior on PSIs
    real <lower=0, upper=1> main_p;
    real <lower=0> main_p_c;
    real mu0;
    real muS;
}

transformed data{
    real phi;
    real psi_prior;
    
    phi = 1 / inv_phi;
    psi_prior = exp(log(main_p) / A);
}

parameters {
    real <lower=0> mu;
    
    vector<lower=0, upper=1>[A] psi_raw;
}

transformed parameters {
    vector[N] psi;
    vector[N] nodes_mu;
    vector[S] sj_mu;
    
    // Set nodes PSI
    psi = rep_vector(1-1e-4, N);
    psi[alternative] = psi_raw;
    
    // Get nodes and SJ mu
    nodes_mu = rep_vector(0, N);        // initialize every node to zero
    nodes_mu[1] = mu;                   // initialize starting node
    
    for(s in 1:S){
        if (single_sources[s] == 1){
            sj_mu[s] = nodes_mu[sources[s]];
        }
        else{
            sj_mu[s] = nodes_mu[sources[s]] * psi[targets[s]];
        }
        nodes_mu[targets[s]] += sj_mu[s];
        nodes_mu[sources[s]] -= sj_mu[s]; 
    }
}

model {
    // Prior on gene expression
    mu ~ lognormal(mu0, muS);
    psi_raw ~ beta(psi_prior * main_p_c, (1 - psi_prior) * main_p_c);
    
    counts ~ neg_binomial_2(sj_mu[pos_sj_idx], phi);
}
