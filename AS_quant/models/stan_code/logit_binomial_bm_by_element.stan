
data {
    int<lower=1> N; // number of individuals
    
    matrix<lower=0>[N, N] dmatrix;        // species distance matrix
    vector[N] log_bias;
    
    int<lower=0> inclusion[N];
    int<lower=0> total[N];
}

transformed data{
    real max_t;
    real offset;
    matrix <lower=0> [N, N] common_t;
    
    offset = 1e-6;
    max_t = max(dmatrix);
    common_t = (max_t - dmatrix) / 2;
}

parameters {
    real X0;
    
    vector[N] X_raw;
    real <lower=0> sigma2;
    real log_beta;           
}

transformed parameters {
    real tau2;

    matrix[N, N] Sigma;
    matrix[N, N] L;
    vector[N] X;
    
    // Ornstein Uhlenbeck covariance
    tau2 = sigma2 * exp(log_beta);
    Sigma = tau2 * common_t + diag_matrix(rep_vector(sigma2, N));
    
    L = cholesky_decompose(Sigma);
    
    // Species means
    X = X0 + L * X_raw;
}

model {
    X0 ~ normal(2, 3);
    sigma2 ~ gamma(1, 1);
    log_beta ~ normal(-log(max_t), 3);
    X_raw ~ normal(0, 1);
    
    inclusion ~ binomial_logit(total, X);
}
