// To maintain common parameter values we have used sigma2 as overdispersion
// parameter for the beta-binomial distribution and beta for the ratio
// tau2/sigma2, which now has a realitively different meaning

data {
    int<lower=1> K; // number of exons
    int<lower=1> S; // number of species
    int<lower=1> N; // number of individuals
    
    matrix<lower=0>[S, S] sp_dmatrix;        // species distance matrix
    matrix[N, K] log_bias;
    matrix<lower=0, upper=1>[N, S] samples_species;
    
    int<lower=0> inclusion[N, K];
    int<lower=0> total[N, K];
}

transformed data{
    real max_t;
    real offset;
    matrix <lower=0> [S, S] total_t_matrix;
    
    offset = 1e-6;
    max_t = max(sp_dmatrix);
    total_t_matrix = (max_t - sp_dmatrix) / 2 + sp_dmatrix;
}

parameters {
    real mu_mean;
    real <lower=0> mu_sd;
    vector[K] mu_raw;
    vector[K] X0_raw;
    matrix[S, K] X_raw;
    real <lower=0> sigma2;           // Overdispersion parameter for beta-binomial
                                     // represents intra-specific variation
    
    // New OU parametrization
    real <lower=0> phylo_hl_raw;        // Scaled Phylogenetic halflife: easier to set a prior
    real <lower=0> tau2_over_2a;        // Equilibrium variance
}

transformed parameters {
    real phylo_hl;
    real alpha;
    real e_alpha_t;
    real beta;
    real ab;
    
    vector[K] mu;
    vector[K] X0;
    matrix[S, K] X;
    matrix[N, K] obs_psi;
    matrix[N, K] a;
    matrix[N, K] b;
    
    matrix[S, S] Sigma;
    matrix[S, S] L;
    
    // OU parameters
    phylo_hl = phylo_hl_raw * max_t;
    alpha = log(2) / phylo_hl;
    e_alpha_t = exp(-alpha * max_t / 2);
    beta = tau2_over_2a * 2 * alpha / sigma2;
    
    // Ancestral values from the equilibrium distribution
    mu = mu_mean + mu_sd * mu_raw;
    X0 = mu + sqrt(tau2_over_2a) * X0_raw;   
    
    // Ornstein Uhlenbeck covariance
    Sigma = tau2_over_2a * (exp(-alpha * sp_dmatrix) - exp(-2 * alpha * total_t_matrix)) + diag_matrix(rep_vector(1e-6, S));
    L = cholesky_decompose(Sigma);
    
    // Species means
    X = rep_matrix((X0 - mu) * e_alpha_t + mu, S)' + L * X_raw;
    
    // Get samples params
    ab = 1 / sigma2 - 1;
    obs_psi = inv_logit(samples_species * X + log_bias);
    a = ab * obs_psi;
    b = ab - a; 
}

model {
    mu_mean ~ normal(2, 3);
    mu_sd ~ gamma(2, 0.5);
    phylo_hl_raw ~ gamma(2, 4);
    tau2_over_2a ~ gamma(2, 3);
    sigma2 ~ gamma(1, 1);
    
    mu_raw ~ normal(0, 1);
    X0_raw ~ normal(0, 1);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    for (n in 1:N)
        inclusion[n] ~ beta_binomial(total[n], a[n], b[n]);
}
