data {
    int<lower=1> N; // number of individuals
    int<lower=1> W; // number of branches

    vector<lower=0>[W] branch_lengths;
    int<lower=0, upper=W> parent_branches[W];
    int <lower=1, upper=W> species[N];      // Sample species vector

    vector[N] log_bias;
    int<lower=0> inclusion[N];
    int<lower=0> total[N];
}


parameters {
    real<lower=0, upper=1> psi0;
    vector[W-1] nodes_means_raw;
    vector[N] X_raw;
    
    real<lower=0> sigma2;
    real log_beta;
}

transformed parameters {
    real tau2;
    real X0;
    
    vector[W] nodes_mean;
    vector[N] X;
    
    real expected_sd;
    real x0_branch;
    
    tau2 = sigma2 * exp(log_beta);
    
    // Initialize values at the root
    X0 = logit(psi0);
    nodes_mean[1] = X0;
    
    // Iterate over the tree nodes in root-to-leaves order
    for(w in 2:W){
        x0_branch = nodes_mean[parent_branches[w]]; 
        expected_sd = sqrt(tau2 * branch_lengths[w]);
        
        // Store nodes trait and optimal values
        nodes_mean[w] = x0_branch + expected_sd * nodes_means_raw[w-1]; 
    }
    
    // Get sample means adding variability and technical biases to the species means
    X = nodes_mean[species] + sqrt(sigma2) * X_raw + log_bias;
}

model {
    psi0 ~ beta(1, 1);
    nodes_means_raw ~ normal(0, 1);
    X_raw ~ normal(0, 1);
    
    sigma2 ~ gamma(1, 1);
    log_beta ~ normal(-3, 3);
    
    // Likelihood
    inclusion ~ binomial_logit(total, X);
}
