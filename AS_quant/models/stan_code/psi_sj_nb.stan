data {
    int<lower=1> S;                         // number of SJ
    int<lower=3> N;                         // number of nodes
    int<lower=2> T;                         // number of transcripts
    int<lower=1> P;                         // number of SJ positions

    matrix <lower=0, upper=1>[S, T] tx_sj;      // Matrix relating transcript and SJ
    matrix <lower=0, upper=1>[T, N] tx_nd;      // Matrix relating transcript and nodes
    matrix <lower=0, upper=1>[T, N] tx_cond;    // Matrix with nodes possibility in transcripts
    
    int<lower=0> counts[S, P];
    
    real inv_phi_log_mean_prior;
    real <lower=0> inv_phi_log_sd_prior;
}

transformed data{
    real <lower=0, upper=1> main_p;
    real <lower=0> main_p_c;
    
    main_p = 0.9;
    main_p_c = 5;
    
}

parameters {
    real <lower=0> inv_phi;
    real <lower=0> mu;
    
    vector<lower=0, upper=1>[N-2] psi_raw;
    
}

transformed parameters {
    real psi_prior;
    vector[N] psi;
    vector[N] log_psi;
    vector[N] log_1psi;
    
    vector[T] tx_psi;
    vector[T] tx_mu;
    vector[S] sj_mu;
    real phi;

    psi_prior = exp(log(main_p) / (N-2));
    phi = 1 / inv_phi;
    psi[1] = 1-1e-4;
    psi[N] = 1-1e-4;
    psi[2:(N-1)] = psi_raw;
    log_psi = log(psi);
    log_1psi = log(1-psi);

    tx_psi = exp((tx_nd .* tx_cond) * log_psi + ((1 - tx_nd) .* tx_cond) * log_1psi);
    tx_mu = mu * tx_psi;    
    sj_mu = tx_sj * tx_mu;
}

model {
    inv_phi ~ lognormal(inv_phi_log_mean_prior, inv_phi_log_sd_prior);
    mu ~ lognormal(5, 2);
    psi_raw ~ beta(psi_prior * main_p_c, (1 - psi_prior) * main_p_c);
    
    for(s in 1:S)
        counts[s,] ~ neg_binomial_2(sj_mu[s], phi);
}
