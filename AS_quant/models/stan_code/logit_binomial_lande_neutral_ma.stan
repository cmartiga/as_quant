data {
    int<lower=1> K; // number of exons
    int<lower=1> N; // number of individuals
    int<lower=1> M; // number of populations
    
    matrix<lower=0, upper=1>[N, M] populations;       // matrix individuals-population
    int<lower=0> n_generations;
    vector<lower=0>[N] Ne;
    
    matrix[N, K] log_bias;
    int<lower=0> inclusion[N, K];
    int<lower=0> total[N, K];
}

parameters {
    real X0_mean;
    real <lower=0> X0_sd;
    vector[K] X0_raw;
    
    // Lande parameters
    real log_sigma2_m;
    real log_sigma2_res;
    
    matrix[M, K] pop_delta_raw;
    matrix[N, K] X_raw;
    
}

transformed parameters {
    vector[K] X0;
    matrix[M, K] pop_delta;
    matrix[N, K] X;
    vector[N] h2s2;
    real sigma2_m;
    real sigma2_res;
    vector[N] s2;
    real tau;
    
    sigma2_m = exp(log_sigma2_m);
    sigma2_res = exp(log_sigma2_res);
    
    
    X0 = X0_mean + X0_sd * X0_raw;
    tau = sqrt(2 * exp(log_sigma2_m) * n_generations);
    pop_delta = tau * pop_delta_raw;
    h2s2 = 2 * exp(log_sigma2_m) * Ne;
    s2 = h2s2 + sigma2_res;  
    X = rep_matrix(X0, N)' + populations * pop_delta + diag_matrix(sqrt(s2)) * X_raw + log_bias; 
}

model {
    X0_mean ~ normal(3, 3);
    X0_sd ~ gamma(2, 0.5);
    X0_raw ~ normal(0, 1);
    
    log_sigma2_m ~ normal(-6, 5);
    log_sigma2_res ~ normal(-2, 3);
    
    to_row_vector(pop_delta_raw) ~ normal(0, 1);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    for (n in 1:N)
        inclusion[n] ~ binomial_logit(total[n], X[n]);
}
