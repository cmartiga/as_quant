data {
    real log_bias[1];                // log mappability ratio of inc-skp reads
    
    int<lower=0> inclusion[1];
    int<lower=0> total[1];
}

parameters {
    real logit_psi;
}

model {
    logit_psi ~ normal(0, 1.5);
    inclusion[1] ~ binomial_logit(total[1], logit_psi + log_bias[1]);
}
generated quantities{
    real psi;
    psi = inv_logit(logit_psi);
}