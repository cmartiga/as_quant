data {
    // Population observations
    int<lower=0> positives;
    int<lower=positives> total;                         
    
    // data for estimating test properties
    int<lower=0> true_positives;
    int<lower=0> false_positives;
    int<lower=0> true_negatives;
    int<lower=0> false_negatives;
}

parameters {
    real <lower=0, upper=1> sensitivity;
    real <lower=0, upper=1> specificity;
    real <lower=0, upper=1> prevalence;
}

transformed parameters {
    real<lower=0, upper=1> p_positive;

    p_positive = sensitivity * prevalence + (1 - specificity) * (1 - prevalence);
}

model {
    sensitivity ~ beta(1, 1);
    specificity ~ beta(1, 1);
    prevalence ~ beta(1, 1);
    
    positives ~ binomial(total, p_positive);
    true_positives ~ binomial(true_positives + false_negatives, sensitivity);
    true_negatives ~ binomial(true_negatives + false_positives, specificity);
}
