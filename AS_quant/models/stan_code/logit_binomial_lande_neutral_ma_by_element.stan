data {
    int<lower=1> N; // number of individuals
    int<lower=1> M; // number of populations
    
    matrix<lower=0, upper=1>[N, M] populations;       // matrix individuals-population
    int<lower=0> n_generations;
    vector<lower=0>[N] Ne;
    
    vector[N] log_bias;
    int<lower=0> inclusion[N];
    int<lower=0> total[N];
}

parameters {
    real X0;
    
    // Lande parameters
    real log_sigma2_m;
    real log_sigma2_res;
    
    vector[M] pop_delta_raw;
    vector[N] X_raw;
    
}

transformed parameters {
    vector[M] pop_delta;
    vector[N] X;
    vector[N] h2s2;
    real sigma2_m;
    real sigma2_res;
    vector[N] s2;
    real tau;
    
    sigma2_m = exp(log_sigma2_m);
    sigma2_res = exp(log_sigma2_res);
    
    tau = sqrt(2 * exp(log_sigma2_m) * n_generations);
    pop_delta = tau * pop_delta_raw;
    h2s2 = 2 * exp(log_sigma2_m) * Ne;
    s2 = h2s2 + sigma2_res;  
    X = X0 + populations * pop_delta + sqrt(s2) .* X_raw + log_bias; 
}

model {
    X0 ~ normal(3, 3);
    
    log_sigma2_m ~ normal(-6, 5);
    log_sigma2_res ~ normal(-2, 3);
    
    to_row_vector(pop_delta_raw) ~ normal(0, 1);
    to_row_vector(X_raw) ~ normal(0, 1);
    
    inclusion ~ binomial_logit(total, X);
}
