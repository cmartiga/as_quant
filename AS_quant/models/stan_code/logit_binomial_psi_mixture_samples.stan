data {
    int<lower=1> K;                         // number of exons
    int<lower=1> N;                         // number of samples
    
    matrix[N, K] log_bias;                // log mappability ratio of inc-skp reads
    
    int<lower=0> inclusion[N, K];
    int<lower=0> total[N, K];
}

parameters {
    real X_mean;
    real <lower=0> X_sd;
    vector[K] X_raw;
    
    matrix[N, K] x_raw;
    real <lower=0> sigma;
    
    real <lower=0, upper=1> theta;
}

transformed parameters {
    vector[K] m;
    matrix[N, K] X;

    m = X_mean + X_sd * X_raw;
    X = rep_matrix(m, N)' + sigma * x_raw + log_bias;    
}

model {
    X_mean ~ normal(5, 3);
    X_sd ~ gamma(2, 1);
    X_raw ~ normal(0, 1);
    
    to_row_vector(x_raw) ~ normal(0, 1);
    sigma ~ normal(0, 1);
    theta ~ beta(1, 1);
    
    // 1-inflated PSI distribution
    for(k in 1:K)
        if(sum(inclusion[,k]) == sum(total[,k]))
            target += log_sum_exp(log(theta), log(1 - theta) + binomial_logit_lpmf(inclusion[,k] | total[,k], X[,k]));
        else
            target += log(1 - theta) + binomial_logit_lpmf(inclusion[,k] | total[,k], X[,k]);
}


generated quantities{
    vector[K] psi;
    real<lower=0, upper=1> p_const;
    real logp_k_given_alt;
    
    for(k in 1:K){
        if(sum(inclusion[,k]) == sum(total[,k])){
            logp_k_given_alt = binomial_logit_lpmf(inclusion[,k] | total[,k], X[,k]) + normal_lpdf(X[,k] | m[k], sigma);
            p_const = theta / (theta  + exp(log1m(theta) + logp_k_given_alt));
            psi[k] = p_const + (1 - p_const) * inv_logit(m[k]);
        }
        else{
            psi[k] = inv_logit(m[k]);
        }
    }
}
