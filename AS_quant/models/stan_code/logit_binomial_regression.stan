// Exon level model to infer PSI across conditions

data {
    int<lower=1> N; // number of samples
    int<lower=1> C; // number of covariates
    int<lower=1> P; // number of predicted conditions
    
    vector[N] log_bias;

    // Matrices with correspondance between samples and tissue    
    matrix<lower=0, upper=1>[N, C] covariates;
    matrix<lower=0, upper=1>[P, C] covariates_pred;
    
    // Inclusion and total reads for exon skipping events in singleton and duplicates
    int <lower=0> inclusion[N]; 
    int <lower=0> total[N];
}

parameters {
    real alpha;
    vector[C] beta;

    real <lower=0> sigma;
    vector[N] X_raw;
}

transformed parameters {
    vector[N] X;
    X = alpha + covariates * beta + sigma * X_raw + log_bias;
}

model {
    alpha ~ normal(0, 2);
    beta ~ normal(0, 3);
    sigma ~ gamma(2, 0.5);
    X_raw ~ normal(0, 1);

    // Likelihood
    inclusion ~ binomial_logit(total, X);
}

generated quantities {
    vector[P] psi_pred;

    psi_pred = inv_logit(alpha + covariates_pred * beta);
}
