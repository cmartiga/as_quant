import numpy as np
import pandas as pd

from AS_quant.models.basic import _BayesianModel
from AS_simulation.utils import invlogit


class AdditiveRegModel(_BayesianModel):
    def __init__(self, design, features, theta_sigma_sd=1, recompile=False):
        self.evol_model = 'add_reg'
        self.set_model_attrs(obs_model='logit_binomial', design=design,
                             recompile=recompile)
        self.design = design
        self.features = features
        
        self.observations.by_element = False
        self.theta_sigma_sd = theta_sigma_sd

        self.covariates_stan_data = {}
        self._model_params = ['theta', 'alpha', 'beta',
                              'sigma', 'beta_sigma',
                              'alpha_mu', 'alpha_sigma', 'theta_sigma']
        self.global_params = self._model_params[3:]         
        self.output_params = ['event_id']

    @property
    def by_element(self):
        return(False)
            
    @property
    def model_params(self):
        return(self._model_params)

    @property
    def n_samples(self):
        return(self.design.shape[0])
    
    @property
    def n_features(self):
        return(self.features.shape[1])
    
    @property
    def feature_names(self):
        return(self.features.columns)
    
    @property
    def n_elements(self):
        return(self.features.shape[0])
    
    @property
    def event_ids(self):
        return(self.features.index)

    @property
    def samples(self):
        return(self.design.index)

    @property
    def model_stan_data(self):
        return({'features': self.features,
                'design': self.design.iloc[:, 0],
                
                'theta_sigma_sd': self.theta_sigma_sd,
                
                'W': self.n_features,
                'N': self.observations.n_samples,
                'K': self.observations.n_elements})
        
    def simulate(self, alpha_mean, alpha_sd,
                 beta_sd, sigma, theta_sd, theta=None, seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids') or self.ids.shape[0] != self.n_elements:
            self.ids = np.array(['e{}'.format(i) for i in range(self.n_elements)])

        alpha = np.random.normal(alpha_mean, alpha_sd, size=self.n_elements)
        alpha = np.vstack([alpha] * self.n_samples).transpose()
        
        if theta is None:
            theta = np.random.normal(0, theta_sd, size=self.n_features)
            
        beta_raw = np.random.normal(0, beta_sd, size=(self.n_elements))
        beta = np.dot(self.features, theta) + beta_raw
        X_raw = np.random.normal(0, sigma, size=(self.n_elements, self.n_samples))
        X = alpha + np.dot(beta.reshape(self.n_elements, 1),
                           self.design.transpose()) + X_raw
        X = pd.DataFrame(X, index=self.ids, columns=self.samples)
        self.observations.simulate(X, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)

    def get_regulation_summary(self, theta):
        summary = pd.DataFrame({'E[theta]': theta.mean(),
                                'theta_2.5': np.percentile(theta, 2.5, axis=0),
                                'theta_97.5': np.percentile(theta, 97.5, axis=0)},
                                index=self.feature_names)
        return(summary)
    
    def _get_param_summary(self, param, label):
        summary = {'E[{}]'.format(label): param.mean(),
                   '{}_2.5'.format(label): np.percentile(param, 2.5, axis=0),
                   '{}_97.5'.format(label): np.percentile(param, 97.5, axis=0)}
        return(summary)
    
    def get_event_summary(self, alpha, beta):
        psi1 = invlogit(alpha)
        psi2 = invlogit(alpha)
        
        summary = {}
        params = [alpha, beta, psi1, psi2]
        labels = ['alpha', 'beta', 'PSI1', 'PSI2']
        for label, param in zip(labels, params):
            summary.update(self._get_param_summary(param, label))
        summary = pd.DataFrame(summary, index=self.event_ids)
        return(summary)
    
    def get_global_params_summary(self, traces):
        summary = {}
        for label in self.global_params:
            param = traces[label]
            summary[label] = {'mean': param.mean(),
                              'q2.5': np.percentile(param, 2.5, axis=0),
                              'q97.5': np.percentile(param, 97.5, axis=0)}
        summary = pd.DataFrame(summary).transpose()
        return(summary)

    def write_fit(self, prefix):
        traces = self.model_fit.extract()
        theta = pd.DataFrame(traces['theta'], columns=self.feature_names)
        theta.to_csv('{}.theta.csv'.format(prefix))
        
        reg_summary = self.get_regulation_summary(theta)
        reg_summary.to_csv('{}.regulation.csv'.format(prefix))
        
        alpha = traces['alpha']
        beta = traces['beta']
        event_summary = self.get_event_summary(alpha, beta)
        event_summary.to_csv('{}.events.csv'.format(prefix))
        
        global_summary = self.get_global_params_summary(traces)
        global_summary.to_csv('{}.global.csv'.format(prefix))


class ManifoldModel(_BayesianModel):
    def __init__(self, design, recompile=False):
        self.evol_model = 'manifold'
        self.set_model_attrs(obs_model='logit_binomial', design=design,
                             recompile=recompile)
        self.design = design
        
        self.observations.by_element = False

        self.covariates_stan_data = {}
        self._model_params = ['beta', 'sigma', 'alpha']
        self.global_params = self._model_params         
        self.output_params = ['event_id']

    @property
    def by_element(self):
        return(False)
            
    @property
    def model_params(self):
        return(self._model_params)

    @property
    def n_samples(self):
        return(self.design.shape[0])
    
    @property
    def covariate_names(self):
        return(self.design.columns)
    
    @property
    def n_covariates(self):
        return(self.design.shape[1])
    
    @property
    def event_ids(self):
        return(self.features.index)

    @property
    def samples(self):
        return(self.design.index)

    @property
    def model_stan_data(self):
        return({'design': self.design,
                'covariates': self.design,
                
                'C': self.n_covariates,
                'N': self.observations.n_samples,
                'K': self.observations.n_elements})
        
    def simulate(self, n_elements, sigma, beta, alpha_mean=0, alpha_sd=1.5,
                 alpha=None, seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids') or self.ids.shape[0] != n_elements:
            self.ids = np.array(['e{}'.format(i) for i in range(n_elements)])

        if alpha is None:
            alpha = np.random.normal(alpha_mean, alpha_sd, size=n_elements)
        alpha = np.vstack([alpha] * self.n_samples).transpose()
        
        X_raw = np.random.normal(0, sigma, size=(n_elements, self.n_samples))
        beta = np.vstack([beta] * n_elements)
        X = alpha + np.dot(beta, self.design.transpose()) + X_raw
        X = pd.DataFrame(X, index=self.ids, columns=self.samples)
        self.observations.simulate(X, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)

    def get_summary(self):
        traces = self.model_fit.extract()
        beta = traces['beta']
        summary = pd.DataFrame({'E[beta]': beta.mean(axis=0),
                                'beta_2.5': np.percentile(beta, 2.5, axis=0),
                                'beta_97.5': np.percentile(beta, 97.5, axis=0)},
                                index=self.covariate_names)
        return(summary)
    
    def write_fit(self, prefix):
        traces = self.model_fit.extract()
        beta = pd.DataFrame(traces['beta'], columns=self.covariate_names)
        beta.to_csv('{}.beta.csv'.format(prefix))
