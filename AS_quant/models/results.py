from os.path import exists

import numpy as np
import pandas as pd
import seaborn as sns
from seaborn.matrix import dendrogram
import matplotlib.pyplot as plt
from scipy.stats.mstats_basic import pearsonr
from sklearn.metrics._ranking import (roc_auc_score, average_precision_score,
                                      roc_curve)
from sklearn.metrics._classification import recall_score, precision_score                         
from sklearn.decomposition._pca import PCA


from AS_quant.utils import specificity_score
from AS_quant.plot_utils import (init_fig, arrange_plot, savefig, AXIS_LABELS,
                                 FigGrid, plot_tree_ax)


class EvolResults(object):
    def __init__(self):
        self.real_values = {}
        self.datasets_properties = {}
        self.suffixes = ['delta_mu', 'mu', 
                         'nodes_mean', 'delta_mean',
                         'nodes_psi', 'delta_psi',
                         'opt_psi', 'delta_opt_psi',
                         'delta_mu_p', 'delta_opt_psi_p',  
                         'branch_data']
    
    @property
    def n_elements(self):
        return(self.real_values['nodes_mean'].shape[0])
    
    @property
    def n_branches(self):
        return(self.real_branch_data.shape[0])
    
    @property
    def n_species(self):
        return(self.real_branch_data.dropna('sp').shape[0])
    
    @property
    def branch_ids(self):
        return(np.array(self.real_branch_data['node'].astype(str)))
    
    def get_parent(self, node_id):
        return(self.parents.get(node_id, None))
    
    def get_branch_length(self, node_id):
        return(self.branch_lengths.get(node_id, None))
    
    def set_real_values(self, dfs):
        branch_data = dfs['branch_data']
        self.real_branch_data = branch_data
        self.parents = dict(zip(branch_data['node'], branch_data['parent']))
        self.branch_lengths = dict(zip(branch_data['node'],
                                       branch_data['length']))
        self._ids = dfs['nodes_mean'].index
        self.real = dfs
    
    def _read_values_dfs(self, prefix):
        dfs = {}
        for suffix in self.suffixes:
            fpath = '{}.{}.csv'.format(prefix, suffix)
            if exists(fpath):
                dfs[suffix] = pd.read_csv(fpath, index_col=0)
        return(dfs)
                
    def read_real_values(self, prefix):
        dfs = self._read_values_dfs(prefix)
        self.set_real_values(dfs)
        
    def read_inferred_values(self, prefix):
        self.inferred = self._read_values_dfs(prefix)

    def calc_confusion_matrix(self, real, score, threshold=0.9):
        shifts = real.replace(np.nan, 0) != 0
        s = score.dropna()
        shifts = shifts.loc[s.index]
        change = s > threshold
        record = {'true_positives': np.logical_and(shifts, change).sum(),
                  'true_negatives': np.logical_and(shifts == False,
                                                   change == False).sum(),
                  'false_positives': np.logical_and(shifts == False, change).sum(),
                  'false_negatives': np.logical_and(shifts, change == False).sum()}
        return(record)

    def calc_classification_metrics(self, real, score, param_label,
                                    threshold=0.9):
        record = {}
        shifts = real.replace(np.nan, 0) != 0
        s = score.dropna()
        shifts = shifts.loc[s.index]
        p = shifts.mean()
        if p < 1 and p > 0:
            change = (s > threshold).astype(int)
            record['{}_auroc'.format(param_label)] = roc_auc_score(shifts, s)
            record['{}_sens'.format(param_label)] = recall_score(shifts, change)
            record['{}_spec'.format(param_label)] = specificity_score(shifts, change)
            record['{}_prec'.format(param_label)] = precision_score(shifts, change)
            record['{}_ap'.format(param_label)] = average_precision_score(shifts, s)
        return(record)

    def _evaluation(self, data, param_label, tree_data=None,
                    threshold=0.9):
        if data.shape[0] < 3:
            return({})
        rho = pearsonr(data['real'], data['inferred'])[0]
        mae = np.abs(data['real'] - data['inferred']).mean()
        record = {'{}_rho'.format(param_label): rho,
                  '{}_mae'.format(param_label): mae}
        if 'score' in data.columns:
            metrics = self.calc_classification_metrics(data['real'],
                                                       data['score'],
                                                       param_label,
                                                       threshold)
            record.update(metrics)
            if tree_data is not None:
                metrics = self.calc_classification_metrics(tree_data['real'],
                                                           tree_data['score'],
                                                           param_label + '_tree',
                                                           threshold)
                record.update(metrics)
        return(record)
    
    def get_tree_data(self):
        real = self.real['delta_opt_psi']
        sel_rows = real.index
        score = self.inferred['delta_opt_psi_p'].reindex(sel_rows)
        tree_data = pd.DataFrame({'real': np.any(real.replace(np.nan, 0) != 0, axis=1).astype(int),
                                  'score': score.max(1)})
        return(tree_data.dropna())
        
    def get_data(self, param):
        df = self.inferred[param]
        real = self.real[param]
        
        if '{}_p'.format(param) in self.inferred:
            score = self.inferred['{}_p'.format(param)]
            data = pd.DataFrame({'real': pd.melt(real)['value'],
                                 'inferred': pd.melt(df)['value'],
                                 'score': pd.melt(score)['value']}).dropna() 
            tree_data = pd.DataFrame({'real': np.any(real.replace(np.nan, 0) != 0, axis=1).astype(int),
                                      'score': score.max(1)})
                                 
        else:
            data = pd.DataFrame({'real': pd.melt(real)['value'],
                                 'inferred': pd.melt(df)['value']}).dropna()
            tree_data = None
        return(data, tree_data)
    
    
    def evaluate_dataset(self, threshold=0.9):
        record = {}
        for k in self.inferred.keys():
            if k == 'branch_data' or k.endswith('_p'):
                continue
            data, tree_data = self.get_data(k)
            vs = self._evaluation(data, param_label=k, tree_data=tree_data,
                                  threshold=threshold)
            record.update(vs)
        return(record)
    
    # TODO: fix this with new model interface
#     def calculate_shifts_prevalence(self, positives, negatives, tree_data,
#                                     threshold=0.9, recompile=False):
#         data = self.calc_confusion_matrix(tree_data['real'], tree_data['score'],
#                                           threshold=threshold)
#         data['positives'] = positives
#         data['total'] = negatives + positives
#         params = ['sensitivity', 'specificity', 'prevalence']
#         
#         model = get_model('prevalence', recompile=recompile)
#         fit = model.sampling(data=data, pars=params,
#                              chains=4, iter=2000,
#                              n_jobs=4, control={'adapt_delta': 0.95})
#         traces = get_posterior_df(fit, params=params)[0]
#         summary = get_summary_df(fit)
#         return(traces, summary)
    
    def _scatterplot(self, param, axes, vmax=None, show_colorbar=True):
        data = self.inferred[param]
        real = self.real[param]
        common_ids = np.intersect1d(data.index, real.index)
        data = data.reindex(common_ids)
        real = real.reindex(common_ids)
        
        data = pd.melt(data)
        data['real'] = pd.melt(real)['value']
        data = data.dropna()
        
        branch_data = self.inferred['branch_data']
        ages = {node: age for node, age in zip(branch_data['node'].astype(str),
                                               branch_data['age'])}
        data['age'] = [ages[x] for x in data['variable']]
        data.dropna(inplace=True)
        r = pearsonr(data['real'], data['value'])[0] 
        sc = axes.scatter(data['real'], data['value'], s=5,
                          c=data['age'], lw=0.1, vmax=vmax)
        xlims = axes.get_xlim()
        ylims = axes.get_ylim()
        lims = min(xlims[0], ylims[0]), max(xlims[1], ylims[1])
        axes.text(lims[0] + 0.1 * (lims[1] - lims[0]),
                  lims[1] - 0.1 * (lims[1] - lims[0]),
                  r'$\rho$={:.2f}'.format(r))
        axes.plot(lims, lims, lw=0.5, alpha=0.4,
                  linestyle='--', c='grey')
        if show_colorbar:
            plt.colorbar(sc, ax=axes, orientation='horizontal', shrink=0.8,
                         label='Node age (my)')
        param_label = AXIS_LABELS.get(param, param)
        xlabel = 'Real {}'.format(param_label)
        ylabel = 'Inferred {}'.format(param_label)
        arrange_plot(axes, xlabel=xlabel, ylabel=ylabel, xlims=lims, ylims=lims)
    
    def plot_dataset_evaluation(self, fpath, params=None, vmax=None):
        if params is None:
            params = [x for x in self.inferred.keys()
                      if x != 'branch_data' and not x.endswith('_p')]
        n_plots = len(params)
        
        fig, subplots = init_fig(1, n_plots, rowsize=3.5, colsize=3)
        
        for axes, param in zip(subplots, params):
            self._scatterplot(param, axes, vmax=vmax)
        
        savefig(fig, fpath)
    
    def calc_pca(self, n_components=2, param='nodes_mean'):
        pca = PCA(n_components=n_components)
        x = self.inferred[param].transpose()
        self.pcs = pd.DataFrame(pca.fit_transform(x), index=x.index,
                                columns=['PC{}'.format(i+1)
                                         for i in range(n_components)])
        self.inferred['PCA'] = self.pcs
        self.exp_var = pca.explained_variance_ratio_ * 100
        self.pcs_dict = self.pcs.to_dict(orient='index')
    
    def plot_pca_tree(self, n_components=2, param='nodes_mean',
                      show_labels=False, fpath=None, axes=None):
        self.calc_pca(n_components=n_components, param=param)
        branch_data = self.inferred['branch_data']
        
        if axes is None:
            fig, axes = init_fig(1, 1, colsize=10, rowsize=10)
        
        for node, parent, label in zip(branch_data['node'].astype(str),
                                       branch_data['parent'].astype(str),
                                       branch_data['sp']):
            if parent == 'nan':
                continue
                
            xs = (self.pcs_dict[node]['PC1'], self.pcs_dict[parent]['PC1'])
            ys = (self.pcs_dict[node]['PC2'], self.pcs_dict[parent]['PC2'])
            axes.plot(xs, ys, lw=1.5, c='black', zorder=0)
            
            if show_labels and isinstance(label, str) and label != 'nan':
                label = label.replace("'", "").replace('_', ' ').capitalize()
                axes.text(xs[0], ys[0], label, fontsize=6)
        axes.scatter(self.pcs['PC1'], self.pcs['PC2'], s=10, c='orange',
                     edgecolor='black', lw=0.5, zorder=1)
        
        arrange_plot(axes, xlabel='PC1 ({:.2f} %)'.format(self.exp_var[0]),
                     ylabel='PC2 ({:.2f} %)'.format(self.exp_var[1]))
        sns.despine(ax=axes)
        
        if fpath is not None:
            savefig(fig, fpath)
    
    def plot_tree_param(self, param='nodes_psi', axes=None, fpath=None):
        branch_data = self.inferred['branch_data']
        data = self.inferred[param]
        ages = (-branch_data.set_index('node')['age']).to_dict()
        
        if axes is None:
            fig, axes = init_fig(1, 1)
        
        for node, parent in zip(branch_data['node'].astype(str),
                                branch_data['parent'].astype(str)):
            if parent == 'nan':
                continue
            
            xs = [ages[node], ages[parent]]
            yss = zip(data[node], data[parent])
            
            for ys in yss: 
                axes.plot(xs, ys, lw=0.5, c='grey', alpha=0.2)
        
        arrange_plot(axes, xlabel='Time (my)',
                     ylabel=AXIS_LABELS.get(param, param))
        
        sns.despine(ax=axes)
        if fpath is not None:
            savefig(fig, fpath)
    
    def heatmap(self, param='nodes_psi', order=None, axes=None,
                fpath=None, figsize=(5, 5), sel_elements=None, xlabel=''):
        species_data = self.inferred['branch_data'].dropna(subset=['sp'])
        data = self.inferred[param]
        if sel_elements is not None:
            data = data.reindex(sel_elements)
        m = data[species_data['node']].transpose()
        m.index = [x.strip("'") for x in species_data['sp']]
        if order is not None:
            m = m.reindex(order)
            
        d = dendrogram(m, metric='euclidean', method='ward', label=False,
                       axis=1, ax=None, linkage=None)
        sorted_species = d.reordered_ind
        m = m.iloc[:, sorted_species]
        
        if axes is None:
            fig, axes = init_fig(1, 1, figsize=figsize)
        
        sns.heatmap(m, ax=axes, cmap='Blues', xticklabels=False,
                    yticklabels=False,
                    cbar_kws={'label': AXIS_LABELS.get(param, param),
                              'shrink': 0.6})
        axes.set_xlabel(xlabel)
        
        if fpath is not None:
            savefig(fig, fpath)
    
    def tree_heatmap(self, tree, fpath, order=None, param='nodes_psi',
                     figsize=(10, 10), sel_elements=None, xlabel=''):
        grid_fig = FigGrid(figsize=figsize)
        axes = grid_fig.new_axes(xend=35)
        plot_tree_ax(tree, axes)
        
        axes = grid_fig.new_axes(xstart=60)
        self.heatmap(param=param, order=order, axes=axes,
                     sel_elements=sel_elements, xlabel=xlabel)
        
        grid_fig.savefig(fpath)
    
    def _plot_roc_curve(self, data, label, axes, color='black'):
        y_true = (data['real'] != 0).astype(int)
        fpr, tpr, _ = roc_curve(y_true, data['score'])
        roc_auc = roc_auc_score(y_true, data['score'])
        label = '{} AUC={:.2f}'.format(label, roc_auc)
        axes.plot(fpr, tpr, c=color, label=label)
    
    def plot_roc_curve(self, param, axes, show_branch_data=True):
        data, tree_data = self.get_data(param)
        if show_branch_data:
            self._plot_roc_curve(data, label='Branch', axes=axes,
                                 color='purple')
        if tree_data is not None:
            self._plot_roc_curve(tree_data, label='Tree', axes=axes,
                                 color='orange')
        arrange_plot(axes, xlims=(0, 1), ylims=(0, 1),
                     xlabel='False Positive rate',
                     ylabel='True positive rate',
                     showlegend=tree_data is not None,
                     legend_loc=4)
        axes.plot((0, 1), (0, 1), lw=0.5, c='grey')
    
    
class EvolResultsSets(object):
    def __init__(self, log=None):
        self.log = log
    
    def read_datasets_results(self, params_table, real_prefix=None,
                              inferred_prefix=None):
        self.results_dict = {}
        datasets_properties = {} 
        
        p = params_table.set_index('dataset').to_dict(orient='index').items()
        for dataset, dataset_properties in p:
            
            results = EvolResults()
            if real_prefix is None:
                d_real_prefix = '{}'.format(dataset)
            else:
                d_real_prefix = '{}.{}'.format(real_prefix, dataset)
            results.read_real_values(d_real_prefix)
                
            if inferred_prefix is None:
                d_inferred_prefix = '{}'.format(dataset)
            else:
                d_inferred_prefix = '{}.{}'.format(inferred_prefix, dataset)
            results.read_inferred_values(d_inferred_prefix)
            self.results_dict[dataset] = results
            
            datasets_properties[dataset] = dataset_properties
            if self.log is not None:
                self.log.write('\tDataset {} loaded'.format(dataset))
        
        self.datasets_properties = pd.DataFrame.from_dict(datasets_properties,
                                                          orient='index')
            
    def evaluate_datasets(self, threshold=0.9):
        metrics = []
        for dataset, results in self.results_dict.items():
            d_metrics = results.evaluate_dataset(threshold=threshold)
            d_metrics['dataset'] = dataset
            metrics.append(d_metrics)
        self.metrics = pd.DataFrame(metrics).set_index('dataset')
        if self.log is not None:
            self.log.write('\tTotal datasets evaluated: {}'.format(len(metrics)))
    
    def write_evaluation(self, fpath):
        self.metrics.to_csv(fpath)
        if self.log is not None:
            self.log.write('Evaluation metrics written to {}'.format(fpath))
    
    def plot(self, fpath, params=None, experiments=None):
        df = pd.concat([self.metrics, self.datasets_properties], axis=1)
        if experiments is None:
            experiments = df['experiment'].unique()
        if params is None:
            params = ['nodes_psi_rho', 'delta_psi_rho', 'opt_psi_rho',
                      'delta_opt_psi_rho']
        
        fig, subplots = init_fig(experiments.shape[0],
                                 ncol=len(params), colsize=3, rowsize=3)
        if len(params) == 1:
            subplots = [[axes] for axes in subplots]
        if experiments.shape[0] == 1:
            subplots = [subplots]
        
        for experiment, ax_row in zip(experiments, subplots):
            data = df.loc[df['experiment'] == experiment, :]
            for param, axes in zip(params, ax_row):
                sns.barplot(x=experiment, y=param, data=data, n_boot=1, ax=axes,
                            edgecolor='black', linewidth=1.2)
                arrange_plot(axes, xlabel=AXIS_LABELS.get(experiment, experiment),
                             ylabel=AXIS_LABELS.get(param, param))
                if '_rho' in param:
                    axes.set_ylim((0, 1))
            
        savefig(fig, fpath)
        
        if self.log is not None:
            self.log.write('Evaluation metrics plotted to {}'.format(fpath))
    
    def plot_by_dataset(self, prefix, params=None, vmax=None):
        if params is None:
            params = ['nodes_psi', 'delta_psi', 'opt_psi', 'delta_opt_psi']
        for dataset, results in self.results_dict.items():
            fpath = '{}.{}.png'.format(prefix, dataset)
            results.plot_dataset_evaluation(fpath, params, vmax=vmax)
            if self.log is not None:
                msg = '\tEvaluation of dataset {} plotted to {}'.format(dataset,
                                                                        fpath)
                self.log.write(msg)
