from csv import DictWriter

from AS_quant.models.basic import _BayesianModel
from AS_simulation.utils import invlogit
from scipy.linalg.matfuncs import expm
from Bio.Phylo.BaseTree import Clade, Tree

import numpy as np
import pandas as pd


def branch_data_to_tree(branch_data, node_label_field='node',
                        character_field=None):
    nodes = {}
    for record in branch_data.sort_values('rank').to_dict(orient='index').values():
        parent_id = record['parent']
        node_id, node_label = record['node'], record[node_label_field]
        
        if node_label is None or isinstance(node_label, float):
            taxon = ''
        else:
            taxon = node_label.strip("'")
        node = Clade(branch_length=record['length'], name=taxon, clades=[])
        if character_field is not None:
            node.character = record[character_field]
        
        if parent_id is not None and not isinstance(parent_id, float):
            nodes[parent_id].clades.append(node)
        nodes[node_id] = node
    tree = Tree(nodes['n0'], rooted=True) 
    return(tree)


class TreeCharacter(object):
    def __init__(self, tree, time_interval=0.1):
        self.tree = tree
        self.set_node_ids()
        self.network = self.to_network()
        self.nodes = [x for x in self.network.keys()]
        self.time_interval = time_interval

        self.ranks = {}
        self.trait = {}

    def set_node_ids(self):
        node_id = 0
        for node in self.tree.preorder_node_iter():
            node.id = 'n{}'.format(node_id)
            node_id += 1

    def to_network(self):
        nodes = {}
        for node in self.tree.preorder_node_iter():
            if node.parent_node is None:
                nodes[node.id] = (None, None, None)
            else:
                sp = node.taxon
                if sp is not None:
                    sp = str(sp).replace(' ', '_').lower()
                nodes[node.id] = (node.parent_node.id, node.edge.length, sp)
        return(nodes)

    @property
    def nodes_ages(self):
        ages = {}
        for node, (parent, l, _) in self.network.items():
            if l is None:
                l = 0
            ages[node] = ages.get(parent, 0) + l
        return(ages)

    @property
    def network_size(self):
        return(np.nansum([x[1] for x in self.network.values()
                          if x[0] is not None]))

    def __str__(self):
        lines = []
        for node, (parent, l, _) in self.network.items():
            lines.append('{} --{}-- {}'.format(node, l, parent))
        return('\n'.join(lines))

    def get_ancestors(self, node_id):
        if node_id is None:
            return([])
        else:
            parent = self.network[node_id][0]
            return(self.get_ancestors(parent) + [node_id])

    def get_node_rank(self, node_id):
        rank = self.ranks.get(node_id, None)
        if rank is not None:
            return(rank)

        parent = self.network[node_id][0]
        if parent is None:
            return(0)
        else:
            return(self.get_node_rank(parent) + 1)

    def get_branches_data(self):
        data = []
        ages = self.nodes_ages
        for node, (p, l, sp) in self.network.items():
            if p is None:
                l = 0
            record = {'node': node, 'parent': p, 'length': l,
                      'rank': self.get_node_rank(node), 'sp': sp,
                      'age': ages[node],
                      'parent_idx1': 0 if p is None else int(p.strip('n')) + 1}
            data.append(record)
        data = pd.DataFrame(data)
        data['age'] = data['age'].max() - data['age']
        return(data)

    def evolve_trait(self, n):
        self.trait['n0'] = self.get_trait_root(n)
        for node in self.network.keys():
            self.simulate_branch(node)


class TreeCTMC(TreeCharacter):
    def init_rates_matrix(self, freqs, global_rate=1,
                          ex_rates_matrix=None, Q=None):
        self.n_elements = freqs.shape[0]
        self.freqs = freqs
        if Q is not None:
            self.Q = Q * global_rate
        elif ex_rates_matrix is not None:
            self.Q = self.get_Q(ex_rates_matrix, freqs, global_rate)
        else:
            raise ValueError('Provide either an Q or ex_rates matrix')
        self.elements = np.arange(self.n_elements)

    def get_Q(self, rates_matrix, freqs, global_rate):
        Q = global_rate * rates_matrix * np.vstack([freqs] * self.n_elements)
        for i in range(self.n_elements):
            Q[i, i] = -np.sum([Q[i, j] for j in range(self.n_elements)
                               if j != i])
        return(Q)

    def get_trait_root(self, n):
        x = np.random.choice(self.elements, size=n, replace=True)
        return({'x': x})

    def simulate_trait(self, x0, t):
        P = expm(self.Q * t)
        x = [np.random.choice(self.elements, p=P[v], replace=True) for v in x0]
        x = np.array(x)
        return(x)

    def simulate_branch(self, node_id):
        if node_id in self.trait:
            return(self.trait[node_id])

        parent, time, _ = self.network[node_id]
        ancestor = self.trait.get(parent, None)
        if ancestor is None:
            ancestor = self.simulate_branch(parent)

        x0 = ancestor['x']
        x = self.simulate_trait(x0, time)
        self.trait[node_id] = {'x': x}
        return(self.trait[node_id])

    def get_nodes_trait_table(self):
        data = {}
        nodes = []
        for node in range(len(self.network)):
            nodes.append(node)
            p, _, _ = self.network[node]
            if p is None:
                p = -1
            data[node] = self.trait[node]['x']
        data = pd.DataFrame(data, columns=nodes)
        return(data)

    def get_species_data(self):
        species_data = {}
        for node in self.tree.leaf_node_iter():
            species = node.taxon.label.strip("'")
            species_data[species] = self.trait[node.id]['x']
        X = pd.DataFrame(species_data)
        return(X)


class TreeExon(TreeCTMC):
    def set_params(self, exonization, stablishment, pseudo_loss, loss,
                   exons_p=0.9, pseudo_exon_p=0.05,):
        freqs = np.array([exons_p * (1 - pseudo_exon_p),
                          exons_p * pseudo_exon_p, 1 - exons_p])
        Q = np.array([[-loss, 0, loss],
                      [stablishment, -stablishment - pseudo_loss, pseudo_loss],
                      [0, exonization, -exonization]])
        self.init_rates_matrix(freqs, Q=Q)


class TreeContinuousTrait(TreeCharacter):
    def evolve_trait(self, n):
        self.trait['n0'] = self.get_trait_root(n_elements=n)
        for node in self.network.keys():
            self.simulate_branch(node)

    def get_sample_data(self, samples, species_dict):
        species_data = {}
        for node in self.tree.leaf_node_iter():
            species = node.taxon.label.strip("'")
            species_data[species] = self.trait[node.id]['x']
        X = pd.DataFrame({sample: species_data[species_dict[sample]]
                          for sample in samples})
        X += np.random.normal(0, self.sigma, size=X.shape)
        return(X)


class TreeBM(TreeContinuousTrait):
    def set_bm_params(self, X0_mean, X0_sd, tau2, sigma):
        self.X0_mean = X0_mean
        self.X0_sd = X0_sd
        self.tau2 = tau2
        self.sigma = sigma
        self.beta = tau2 / sigma ** 2

    def init_X0(self, n_elements):
        X0 = np.random.normal(self.X0_mean, self.X0_sd, size=n_elements)
        return(X0)

    def get_trait_root(self, n_elements):
        x = self.init_X0(n_elements)
        return({'x': x, 'x0': np.full(x.shape, np.nan),
                'delta_x': np.full(x.shape, np.nan)})

    def simulate_BM(self, x0, tau2, t):
        x = np.random.normal(x0, np.sqrt(tau2 * t))
        return(x)

    def simulate_branch(self, node_id):
        if node_id in self.trait:
            return(self.trait[node_id])

        parent, time, _ = self.network[node_id]
        ancestor = self.trait.get(parent, None)
        if ancestor is None:
            ancestor = self.simulate_branch(parent)

        x0 = ancestor['x']
        x = self.simulate_BM(x0, self.tau2, time)
        self.trait[node_id] = {'x': x, 'delta_x': x - x0, 'x0': x0}
        return(self.trait[node_id])

    def get_nodes_traits_tables(self):
        data = {'nodes_mean': {}, 'delta_mean': {},
                'nodes_psi': {}, 'delta_psi': {}}

        for node in self.nodes:
            p, _, _ = self.network[node]
            if p is None:
                p = -1

            data['nodes_mean'][node] = self.trait[node]['x']
            data['delta_mean'][node] = self.trait[node]['delta_x']
            data['nodes_psi'][node] = invlogit(self.trait[node]['x'])
            data['delta_psi'][node] = data['nodes_psi'][node] - \
                invlogit(self.trait[node]['x0'])

        data = {k: pd.DataFrame(v, columns=self.nodes)
                for k, v in data.items()}
        return(data)


class TreeOU(TreeContinuousTrait):
    def set_ou_params(self, mu0_mean, mu0_sd, phylo_hl, tau2_over_2a, sigma,
                      adapt_p=0):
        self.mu0_mean = mu0_mean
        self.mu0_sd = mu0_sd
        self.phylo_hl = phylo_hl
        self.tau2_over_2a = tau2_over_2a
        self.sigma = sigma
        self.alpha = np.log(2) / phylo_hl
        self.beta = tau2_over_2a * 2 * self.alpha / sigma ** 2
        self.adapt_p = adapt_p

    def init_mu0(self, n_elements):
        mu0 = np.random.normal(self.mu0_mean, self.mu0_sd, size=n_elements)
        return(mu0)

    def get_trait_root(self, n_elements):
        mu0 = self.init_mu0(n_elements)
        x = np.random.normal(mu0, np.sqrt(self.tau2_over_2a))
        return({'x': x, 'x0': np.full(mu0.shape, np.nan),
                'delta_x': np.full(mu0.shape, np.nan),
                'mu': mu0, 'delta_mu': np.full(mu0.shape, np.nan)})

    def simulate_OU(self, x0, mu, alpha, tau2_over_2a, t):
        exp_x = x0 * np.exp(-alpha * t) + mu * (1 - np.exp(-alpha * t))
        sd_x = np.sqrt(tau2_over_2a * (1 - np.exp(-2 * alpha * t)))
        x = np.random.normal(exp_x, sd_x)
        return(x)

    def simulate_branch(self, node_id):
        if node_id in self.trait:
            return(self.trait[node_id])

        parent, time, _ = self.network[node_id]
        ancestor = self.trait.get(parent, None)
        if ancestor is None:
            ancestor = self.simulate_branch(parent)

        x0 = ancestor['x']
        new_psi_opt = np.random.uniform(size=x0.shape)
        mu = np.log(new_psi_opt / (1 - new_psi_opt))
        shift = np.random.uniform(size=x0.shape) < self.adapt_p
        delta_mu = (mu - ancestor['mu']) * shift
        mu = ancestor['mu'] + delta_mu
        x = self.simulate_OU(x0, mu, self.alpha, self.tau2_over_2a, time)
        self.trait[node_id] = {'x': x, 'mu': mu, 'delta_mu': delta_mu,
                               'delta_x': x - x0, 'x0': x0}
        return(self.trait[node_id])

    def get_nodes_traits_tables(self):
        data = {'mu': {}, 'delta_mu': {},
                'nodes_mean': {}, 'delta_mean': {},
                'nodes_psi': {}, 'delta_psi': {},
                'opt_psi': {}, 'delta_opt_psi': {}}
        for node in self.nodes:
            p, _, _ = self.network[node]
            if p is None:
                p = -1

            data['nodes_mean'][node] = self.trait[node]['x']
            data['delta_mean'][node] = self.trait[node]['delta_x']
            data['nodes_psi'][node] = invlogit(self.trait[node]['x'])
            data['delta_psi'][node] = data['nodes_psi'][node] - \
                invlogit(self.trait[node]['x0'])
            data['mu'][node] = self.trait[node]['mu']
            data['delta_mu'][node] = self.trait[node]['delta_mu']
            data['opt_psi'][node] = invlogit(self.trait[node]['mu'])
            opt_psi0 = invlogit(
                self.trait[node]['mu'] - self.trait[node]['delta_mu'])
            data['delta_opt_psi'][node] = data['opt_psi'][node] - opt_psi0

        data = {k: pd.DataFrame(v, columns=self.nodes)
                for k, v in data.items()}
        return(data)


class _ContinuousTraitModel(_BayesianModel):
    def _get_covariate_matrix(self, design, variable):
        values = design[variable].unique()
        m = pd.DataFrame({value: (design[variable] == value).astype(int)
                          for value in values})
        return(np.dot(m, m.transpose()))

    def set_model_attrs(self, obs_model, tree, design, covariates=None,
                        recompile=False):
        self.tree = tree
        self.design = design
        self.samples = design.index
        self.species = design['species'].unique()
        self.n_species = self.species.shape[0]
        self.species_dict = design['species'].to_dict()

        self.obs_model_label = obs_model
        self.set_obs_model(obs_model)
        self.model_label = '{}_{}'.format(obs_model, self.evol_model)
        if covariates is not None:
            self.covariates_m = {c: self._get_covariate_matrix(design, c)
                                 for c in covariates}
            self.model_label += '_cov'
            if hasattr(self, '_model_params'):
                self._model_params.append('sigma2_cov')

        self.recompile = recompile

    @property
    def covariates_stan_data(self):
        if hasattr(self, 'covariates_m'):
            return({'cmatrix': [v for v in self.covariates_m.values()],
                    'C': len(self.covariates_m)})
        else:
            return({})

    @property
    def species_matrix(self):
        if not hasattr(self, '_species_matrix'):
            m = {}
            for species in self.species:
                m[species] = (self.design['species'] == species).astype(int)
            self._species_matrix = pd.DataFrame(m, index=self.samples)
        return(self._species_matrix)

    def _calc_samples_dist_matrix(self):
        d = self.tree.phylogenetic_distance_matrix()
        taxons = {nd.taxon.label: nd.taxon for nd in self.tree.leaf_nodes()}
        m = []
        for sample1 in self.samples:
            sp1 = self.species_dict[sample1]
            row = []
            for sample2 in self.samples:
                sp2 = self.species_dict[sample2]
                if sp1 == sp2:
                    row.append(0)
                else:
                    row.append(d.distance(taxons[sp1], taxons[sp2]))
            m.append(row)
        return(pd.DataFrame(m, index=self.samples, columns=self.samples))

    def _calc_species_dist_matrix(self):
        d = self.tree.phylogenetic_distance_matrix()
        taxons = {nd.taxon.label: nd.taxon for nd in self.tree.leaf_nodes()}
        m = []
        for sp1 in self.species:
            row = []
            for sp2 in self.species:
                if sp1 == sp2:
                    row.append(0)
                else:
                    row.append(d.distance(taxons[sp1], taxons[sp2]))
            m.append(row)
        return(pd.DataFrame(m, index=self.species, columns=self.species))

    @property
    def samples_dist_matrix(self):
        if not hasattr(self, '_samples_distance_matrix'):
            self._distance_matrix = self._calc_samples_dist_matrix()
        return(self._distance_matrix)

    @property
    def species_dist_matrix(self):
        if not hasattr(self, '_species_distance_matrix'):
            self._distance_matrix = self._calc_species_dist_matrix()
        return(self._distance_matrix)

    @property
    def tree_character(self):
        if not hasattr(self, '_tree_character'):
            self._tree_character = self.tree_character_class(self.tree)
        return(self._tree_character)

    @property
    def species_branch_data(self):
        branch_data = self.tree_character.get_branches_data()
        return(branch_data)

    def calc_delta(self, x, nodes, parent, logit=False):
        delta_x = np.full(x.shape, np.nan)
        if logit:
            p1 = invlogit(x[nodes[1:]].values)
            p2 = invlogit(x[parent[1:]].values)
            delta_x[:, 1:] = (p1 - p2)
        else:
            delta_x[:, 1:] = (x[nodes[1:]].values - x[parent[1:]].values)
        return(delta_x)

    def calc_delta_p(self, posterior_delta):
        delta_mu_p = np.vstack([np.nanmean(posterior_delta > 0, axis=0),
                                np.nanmean(posterior_delta < 0, axis=0)])
        delta_mu_p = np.max(delta_mu_p, axis=0)
        return(delta_mu_p)

    def write_shifts(self, prefix):
        for field, df in self.tree_character.get_nodes_traits_tables().items():
            fpath = '{}.{}.csv'.format(prefix, field)
            df.index = self.ids
            df.to_csv(fpath)

    def write_simulations(self, prefix):
        self.observations.write(prefix)
        self.write_shifts(prefix)
        fpath = '{}.branch_data.csv'.format(prefix)
        self.species_branch_data.to_csv(fpath)


class _OUmodel(_ContinuousTraitModel):
    @property
    def tree_character_class(self):
        return(TreeOU)

    def simulate(self, n_elements, mu0_mean, mu0_sd, tau2_over_2a, phylo_hl,
                 sigma, adapt_p=0, seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids'):
            self.ids = ['e{}'.format(i) for i in range(n_elements)]

        self.tree_character.set_ou_params(mu0_mean, mu0_sd, phylo_hl,
                                          tau2_over_2a, sigma, adapt_p)
        self.tree_character.evolve_trait(n_elements)
        X = self.tree_character.get_sample_data(
            self.samples, self.species_dict)
        X.index = self.ids
        self.observations.simulate(X, **kwargs)


class _BMmodel(_ContinuousTraitModel):
    @property
    def tree_character_class(self):
        return(TreeBM)

    def simulate(self, n_elements, X0_mean, X0_sd, tau2, sigma,
                 seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids'):
            self.ids = ['e{}'.format(i) for i in range(n_elements)]
        self.tree_character.set_bm_params(X0_mean, X0_sd, tau2, sigma)
        self.tree_character.evolve_trait(n_elements)
        X = self.tree_character.get_sample_data(
            self.samples, self.species_dict)
        X.index = self.ids
        self.observations.simulate(X, **kwargs)


class BMmodelByElement(_BMmodel):
    def __init__(self, tree, design, obs_model,
                 covariates=None, recompile=False):
        self.evol_model = 'bm_by_element2'
        self.by_element = True
        self.set_model_attrs(obs_model=obs_model, covariates=covariates,
                             tree=tree, design=design, recompile=recompile)
        self.observations.by_element = self.by_element
        self.model_params = ['X0', 'log_beta', 'sigma2', 'tau2']
        self.output_params = ['X0', 'beta', 'sigma2', 'tau2']
        self.logit = 'logit' in obs_model
        if self.logit:
            self.output_params.append('psi0')

    def process_model_fit(self, fit):
        return(self._process_model_fit(fit, logit=self.logit))

    @property
    def _model_stan_data_cov_matrix(self):
        return({'dmatrix': self.samples_dist_matrix,
                'N': self.observations.n_elements})

    @property
    def model_stan_data(self):
        branch_data = self.species_branch_data
        species = [str(x).strip("'") for x in branch_data['sp']]
        species = [species.index(self.species_dict[sample]) + 1
                   for sample in self.samples]
        data = {'species': species,
                'branch_lengths': branch_data['length'],
                'W': branch_data.shape[0],
                'S': self.species_dist_matrix.shape[0],
                'parent_branches': branch_data['parent_idx1']}
        return(data)

    def _process_model_fit(self, fit, logit=False):
        traces = fit.extract()

        processed = {'X0': traces['X0'].mean(),
                     'tau2': traces['tau2'].mean(),
                     'beta': np.exp(traces['log_beta']).mean(),
                     'sigma2': traces['sigma2'].mean()}
        if logit:
            processed['psi0'] = invlogit(traces['X0']).mean(0)

        return(processed)

    def write_fit(self, prefix):
        fpath = '{}.branch_data.csv'.format(prefix)
        self.species_branch_data.to_csv(fpath)

        fieldnames = ['exon_id'] + self.output_params
        fpath = '{}.fit.csv'.format(prefix)
        with open(fpath, 'w') as fhand:
            writer = DictWriter(fhand, fieldnames=fieldnames)
            writer.writeheader()
            for event_id, fit in zip(self.observations.ids, self.model_fit):
                processed_fit = self.process_model_fit(fit)
                processed_fit['exon_id'] = event_id
                writer.writerow(processed_fit)
                fhand.flush()


class BasicOUmodel(_OUmodel):
    def __init__(self, tree, design, obs_model, covariates=None,
                 recompile=False, rate_variation=False):
        self.evol_model = 'ou'
        self.by_element = False
        self._model_params = ['phylo_hl', 'mu_mean', 'mu_sd', 'sigma2']
        if rate_variation:
            self.evol_model += '_varR'
            self._model_params.extend(['tau2_mu', 'tau2_sigma'])
        else:
            self._model_params.extend(['tau2_over_2a', 'beta'])
        self.set_model_attrs(obs_model=obs_model, tree=tree, design=design,
                             covariates=covariates, recompile=recompile)
        self._model_params.extend(self.observations.out_params)
        self.observations.by_element = self.by_element

    @property
    def model_params(self):
        return(self._model_params)

    @property
    def model_stan_data(self):
        return({'dmatrix': self.samples_dist_matrix,
                'sp_dmatrix': self.species_dist_matrix,
                'samples_species': self.species_matrix,
                'S': self.species_dist_matrix.shape[0],
                'N': self.observations.n_elements,
                'K': self.observations.n_samples})

    def write_fit(self, prefix):
        self.model.write_fit(prefix)


class OUmodelByElement(_OUmodel):
    def __init__(self, tree, design, obs_model, phylo_hl,
                 log_tau2_over_2a_mean, log_tau2_over_2a_sd,
                 log_beta_mean, log_beta_sd,
                 covariates=None, recompile=False):
        self.evol_model = 'ou_by_element'
        self.by_element = True
        self.set_OU_global_params(phylo_hl, log_tau2_over_2a_mean,
                                  log_tau2_over_2a_sd, log_beta_mean,
                                  log_beta_sd)
        self.set_model_attrs(obs_model=obs_model, covariates=covariates,
                             tree=tree, design=design, recompile=recompile)
        self.observations.by_element = self.by_element
        self.model_params = ['mu', 'nodes_mean', 'tau2_over_2a', 'beta']
        self.output_params = ['nodes_mean', 'nodes_psi',
                              'delta_mean', 'delta_psi',
                              'params']

    def process_model_fit(self, fit):
        return(self._process_model_fit(fit, logit=True))

    def set_OU_global_params(self, phylo_hl, log_tau2_over_2a_mean,
                             log_tau2_over_2a_sd, log_beta_mean, log_beta_sd):
        self.phylo_hl = phylo_hl
        self.log_tau2_over_2a_mean = log_tau2_over_2a_mean
        self.log_tau2_over_2a_sd = log_tau2_over_2a_sd
        self.log_beta_mean = log_beta_mean
        self.log_beta_sd = log_beta_sd

    @property
    def model_stan_data(self):
        branch_data = self.species_branch_data
        species = [str(x).strip("'") for x in branch_data['sp']]
        species = [species.index(self.species_dict[sample]) + 1
                   for sample in self.samples]
        data = {'species': species,
                'branch_lengths': branch_data['length'],
                'W': branch_data.shape[0],
                'S': self.species_dist_matrix.shape[0],
                'parent_branches': branch_data['parent_idx1'],
                'phylo_hl': self.phylo_hl,
                'log_tau2_over_2a_mean': self.log_tau2_over_2a_mean,
                'log_tau2_over_2a_sd': self.log_tau2_over_2a_sd,
                'log_beta_mean': self.log_beta_mean,
                'log_beta_sd': self.log_beta_sd}
        return(data)

    def _init_writers(self, prefix):
        self.writers = {}
        for param in self.output_params:
            fpath = '{}.{}.csv'.format(prefix, param)

            self.writers[param] = open(fpath, 'w')
            if param == 'params':
                colnames = ['tau2_over_2a', 'tau2', 'beta', 'sigma2',
                            'mu', 'opt_psi']
            else:
                colnames = self.species_branch_data['node']

            header = 'exon_id,{}\n'.format(','.join(str(x) for x in colnames))
            self.writers[param].write(header)

    def _process_model_fit(self, fit, logit=False):
        traces = fit.extract()
        nodes = self.species_branch_data['node']
        parent = self.species_branch_data['parent']

        mu = traces['mu']
        tau2_over_2a = traces['tau2_over_2a']
        beta = traces['beta']
        alpha = np.log(2) / self.phylo_hl
        tau2 = tau2_over_2a * 2 * alpha
        sigma2 = tau2 / beta

        opt_psi = invlogit(mu)
        x = pd.DataFrame(traces['nodes_mean'], columns=nodes)
        delta_x = self.calc_delta(x, nodes, parent)
        processed = {'nodes_mean': x.mean(0),
                     'delta_mean': delta_x.mean(0),
                     'params': [tau2_over_2a.mean(), tau2.mean(),
                                beta.mean(), sigma2.mean(),
                                mu.mean(), opt_psi.mean()]
                     }

        if logit:
            processed['nodes_psi'] = invlogit(x).mean(0)
            processed['delta_psi'] = self.calc_delta(x, nodes, parent,
                                                     logit=True).mean(0)

        return(processed)

    def _write_processed_fit(self, event_id, processed_fit):
        for field, values in processed_fit.items():
            line = '{},{}\n'.format(event_id,
                                    ','.join([str(x) for x in values]))
            self.writers[field].write(line)
            self.writers[field].flush()

    def write_fit(self, prefix):
        fpath = '{}.branch_data.csv'.format(prefix)
        self.species_branch_data.to_csv(fpath)

        self._init_writers(prefix)
        for event_id, fit in zip(self.observations.ids, self.model_fit):
            processed_fit = self.process_model_fit(fit)
            self._write_processed_fit(event_id, processed_fit)


class ShiftsOUmodel(OUmodelByElement):
    def __init__(self, tree, design, obs_model,
                 phylo_hl, log_tau2_over_2a_mean, log_tau2_over_2a_sd,
                 log_beta_mean, log_beta_sd,
                 rho_prior=0.1,
                 covariates=None, recompile=False):
        self.rho_prior = rho_prior
        self.evol_model = 'ou_shifts'
        self.by_element = True
        self.set_OU_global_params(phylo_hl, log_tau2_over_2a_mean,
                                  log_tau2_over_2a_sd,
                                  log_beta_mean, log_beta_sd,)
        self.set_model_attrs(obs_model=obs_model, covariates=covariates,
                             tree=tree, design=design, recompile=recompile)
        self.observations.by_element = self.by_element
        self.model_params = ['mu', 'delta_mu', 'nodes_mean']
        self.output_params = ['nodes_mean', 'nodes_psi',
                              'mu', 'delta_mu', 'delta_mu_p',
                              'delta_mean', 'delta_psi',
                              'opt_psi', 'delta_opt_psi', 'delta_opt_psi_p']

    @property
    def model_stan_data(self):
        branch_data = self.species_branch_data
        species = [str(x).strip("'") for x in branch_data['sp']]
        species = [species.index(self.species_dict[sample]) + 1
                   for sample in self.samples]
        t2a = np.exp(self.log_tau2_over_2a_mean)
        sigma = np.sqrt(t2a * 2 * np.log(2) / self.phylo_hl / np.exp(self.log_beta_mean))
        data = {'species': species,
                'dmatrix': self.samples_dist_matrix,
                'branch_lengths': branch_data['length'],
                'W': branch_data.shape[0],
                'S': self.species_dist_matrix.shape[0],
                'parent_branches': branch_data['parent_idx1'],
                'rho_prior': self.rho_prior, 'phylo_hl': self.phylo_hl,
                'tau2_over_2a': t2a, 'sigma': sigma}
        return(data)

    def _init_writers(self, prefix):
        self.writers = {}
        for param in self.output_params:
            fpath = '{}.{}.csv'.format(prefix, param)
            self.writers[param] = open(fpath, 'w')
            colnames = self.species_branch_data['node']
            header = 'exon_id,{}\n'.format(','.join(str(x) for x in colnames))
            self.writers[param].write(header)

    def _process_model_fit(self, fit, logit=False):
        traces = fit.extract()
        nodes = self.species_branch_data['node']
        parent = self.species_branch_data['parent']

        x = pd.DataFrame(traces['nodes_mean'], columns=nodes)
        delta_x = self.calc_delta(x, nodes, parent, logit=False)

        mu = pd.DataFrame(traces['mu'], columns=nodes)
        delta_mu = traces['delta_mu']
        processed = {'nodes_mean': x.mean(0),
                     'delta_mean': delta_x.mean(0),
                     'mu': mu.mean(0),
                     'delta_mu': np.nanmean(delta_mu, axis=0),
                     'delta_mu_p': self.calc_delta_p(delta_mu)}
        if logit:
            opt_psi = invlogit(mu)
            delta_opt_psi = self.calc_delta(mu, nodes, parent, logit=True)

            processed['nodes_psi'] = invlogit(x).mean(0)
            processed['delta_psi'] = self.calc_delta(x, nodes, parent,
                                                     logit=True).mean(0)
            processed['opt_psi'] = opt_psi.mean(0)
            processed['delta_opt_psi'] = delta_opt_psi.mean(0)
            processed['delta_opt_psi_p'] = self.calc_delta_p(delta_opt_psi)

        return(processed)
