from csv import DictWriter
from os.path import join

import numpy as np
import pandas as pd

from AS_simulation.psi import simulate_exon_logit_psi
from AS_quant.models.basic import _BayesianModel, get_backend_model
from AS_quant.settings import STAN_CODE_DIR
from AS_quant.genome import Genome
from AS_quant.qc import SpliceJunctionProfiles
from AS_quant.utils import calc_sj_mu, get_single_sources
from AS_simulation.utils import invlogit


class _RegressionModel(_BayesianModel):
    @property
    def model_params(self):
        return(self._model_params)

    @property
    def n_samples(self):
        return(self.design.shape[0])

    @property
    def samples(self):
        return(self.design.index)

    @property
    def n_covariates(self):
        return(self.design.shape[1])

    @property
    def model_stan_data(self):
        return({'covariates': self.design,
                'C': self.n_covariates,
                'N': self.observations.n_samples,
                'K': self.observations.n_elements})

    @property
    def by_element(self):
        return(True)

    def simulate(self, n_elements, alpha_mean, alpha_sd,
                 beta_mean, beta_sd, sigma,
                 seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids') or self.ids.shape[0] != n_elements:
            self.ids = np.array(['e{}'.format(i) for i in range(n_elements)])

        alpha = np.random.normal(alpha_mean, alpha_sd, size=n_elements)
        alpha = np.vstack([alpha] * self.n_samples).transpose()
        betas = np.random.normal(beta_mean, beta_sd, size=(n_elements,
                                                           self.n_covariates))
        X_raw = np.random.normal(0, sigma, size=(n_elements, self.n_samples))
        X = alpha + np.dot(betas, self.design.transpose()) + X_raw
        X = pd.DataFrame(X, index=self.ids, columns=self.samples)
        self.observations.simulate(X, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)


class RegressionModel(_RegressionModel):
    def __init__(self, design, design_pred=None, recompile=False,
                 contrast_matrix=None):
        self.evol_model = 'regression'
        self.set_model_attrs(obs_model='logit_binomial', design=design,
                             recompile=recompile)
        self.observations.by_element = True
        self.design_pred = design_pred
        if design_pred is None:
            self.design_pred = design.drop_duplicates()

        self._model_params = ['psi_pred', 'alpha', 'beta']        
        self.output_params = ['event_id']
        self.contrast_matrix = contrast_matrix
        for group in self.design_pred.index:
            self.output_params.append('{}_PSI'.format(group))
            self.output_params.append('{}_PSI_q0.025'.format(group))
            self.output_params.append('{}_PSI_q0.975'.format(group))
    
    @property
    def n_groups_pred(self):
        return(self.design_pred.shape[0])
    
    @property
    def model_stan_data(self):
        return({'covariates': self.design,
                'covariates_pred': self.design_pred,
                'C': self.n_covariates,
                'P': self.n_groups_pred,
                'N': self.observations.n_samples,
                'K': self.observations.n_elements})

    def _get_param_posterior_summary(self, values):
        m = values.mean()
        lower = np.percentile(values, 2.5)
        upper = np.percentile(values, 97.5)
        return(m, lower, upper)
    
    def _get_param_posterior_summary_dict(self, values, prefix):
        m, l, u = self._get_param_posterior_summary(values)
        result = {prefix: m, '{}_q0.025'.format(prefix): l,
                  '{}_q0.975'.format(prefix): u}
        return(result)

    def _posterior_summary(self, indexes, names, variable, posterior):
        processed = {}
        for i, group in zip(indexes, names):
            prefix = '{}_{}'.format(group, variable)
            s = self._get_param_posterior_summary_dict(posterior[:, i], prefix)
            processed.update(s)
        return(processed)

    def summarize_coeff(self, event_id, fit):
        processed = {'event_id': event_id}
        traces = fit.extract()
        
        # Get alpha summary
        alpha = traces['alpha']
        e_alpha = np.exp(alpha)
        alpha = self._get_param_posterior_summary_dict(alpha, 'alpha')
        e_alpha = self._get_param_posterior_summary_dict(e_alpha, 'e_alpha')
        processed.update(alpha)
        processed.update(e_alpha)
        
        # Get betas summary
        beta = traces['beta']
        e_beta = np.exp(beta)
        beta = self._posterior_summary(range(self.design.shape[1]),
                                       self.design.columns, 'beta', beta)
        e_beta = self._posterior_summary(range(self.design.shape[1]),
                                       self.design.columns, 'e_beta', e_beta)
        processed.update(beta)
        processed.update(e_beta)
        return(processed)

    def summarize_psi(self, event_id, fit):
        traces = fit.extract()
        psi = traces['psi_pred']
        processed = {'event_id': event_id}
        data = self._posterior_summary(range(self.design_pred.shape[0]),
                                       self.design_pred.index, 'PSI', psi)
        processed.update(data)
        return(processed)
    
    def make_contrasts(self, pred_psi, contrast_matrix):
        dpsi = np.dot(pred_psi, contrast_matrix)
        return(dpsi)
    
    def summarize_dpsi(self, event_id, fit):
        traces = fit.extract()
        dpsi = self.make_contrasts(traces['psi_pred'], self.contrast_matrix)
        processed = {'event_id': event_id}
        data = self._posterior_summary(range(self.contrast_matrix.shape[1]),
                                       self.contrast_matrix.columns, 'dPSI', dpsi)
        processed.update(data)
        return(processed)

    def _get_writer(self, writer, fpath, record, fhand=None):
        if writer is None:
            fhand = open(fpath, 'w')
            fieldnames = [x for x in record.keys()]
            writer = DictWriter(fhand, fieldnames=fieldnames)
            writer.writeheader()
        return(writer, fhand)

    def write_fit(self, prefix):
        fpath_psi = '{}.psi.csv'.format(prefix)
        fpath_dpsi = '{}.dpsi.csv'.format(prefix)
        fpath_coeff = '{}.coeff.csv'.format(prefix)
        
        coeff_fhand = None
        psi_fhand = None
        dpsi_fhand = None
        
        coeff_writer = None
        psi_writer = None
        dpsi_writer = None
        
        for event_id, fit in zip(self.observations.ids, self.model_fit):
            coeff_summary = self.summarize_coeff(event_id, fit)
            coeff_writer, coeff_fhand = self._get_writer(coeff_writer, fpath_coeff,
                                                         coeff_summary, coeff_fhand)
            coeff_writer.writerow(coeff_summary) 

            psi_summary = self.summarize_psi(event_id, fit)            
            psi_writer, psi_fhand = self._get_writer(psi_writer, fpath_psi,
                                                     psi_summary, psi_fhand)            
            psi_writer.writerow(psi_summary)
            
            if self.contrast_matrix is not None:
                dpsi_summary = self.summarize_dpsi(event_id, fit)
                dpsi_writer, dpsi_fhand = self._get_writer(dpsi_writer, fpath_dpsi,
                                                           dpsi_summary, dpsi_fhand)
                dpsi_writer.writerow(dpsi_summary)

        psi_fhand.close()
        coeff_fhand.close()
        if self.contrast_matrix is not None:
            dpsi_fhand.close()


class PsiRangeModel(_RegressionModel):
    def __init__(self, design, psi_range=0.1, recompile=False):
        self.evol_model = 'psi_range'
        self.set_model_attrs(obs_model='logit_binomial', design=design,
                             recompile=recompile)
        self.observations.by_element = self.by_element
        self._model_params = ['psi', 'psi_range', 'min_psi']
        self.output_params = ['event_id', 'p(PSI_range>0.1)',
                              'p(0.05<PSI<0.95)_max',  'E[PSI_min]',
                              'E[PSI_range]']
        self.psi_range = psi_range
        self.output_params.extend(['E[{}_PSI]'.format(f)
                                   for f in self.covariates])

    def process_model_fit(self, event_id, fit):
        traces = fit.extract()
        p_alt = np.logical_and(traces['psi'] > 0.05,
                               traces['psi'] < 0.95).mean(axis=0).max()

        psi = traces['psi'].mean(0)
        processed = {'event_id': event_id}
        processed.update({'p(PSI_range>{})'.format(self.psi_range): np.mean(traces['psi_range'] > self.psi_range),
                          'p(0.05<PSI<0.95)_max': p_alt,
                          'E[PSI_min]': traces['min_psi'].mean(0),
                          'E[PSI_range]': traces['psi_range'].mean()})
        processed.update({'E[{}_PSI]'.format(f): p
                          for f, p in zip(self.covariates, psi)})
        return(processed)

    def _write_processed_fit(self, event_id, processed_fit):
        for field, values in processed_fit.items():
            line = '{},{}\n'.format(event_id,
                                    ','.join([str(x) for x in values]))
            self.writers[field].write(line)

    def write_fit(self, prefix):
        fpath = '{}.summary.csv'.format(prefix)
        with open(fpath, 'w') as fhand:
            writer = DictWriter(fhand, fieldnames=self.output_params)
            writer.writeheader()
            for event_id, fit in zip(self.observations.ids, self.model_fit):
                processed_fit = self.process_model_fit(event_id, fit)
                writer.writerow(processed_fit)


class _PsiModel(_BayesianModel):
    @property
    def model_params(self):
        return(self._model_params)

    @property
    def n_samples(self):
        return(1)

    @property
    def samples(self):
        return(self.design.index)

    @property
    def n_covariates(self):
        return(0)

    @property
    def model_stan_data(self):
        return({})

    def simulate(self, n_elements, mean, sd, p_alternative,
                 seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids') or self.ids.shape[0] != n_elements:
            self.ids = np.array(['e{}'.format(i) for i in range(n_elements)])

        X = simulate_exon_logit_psi(mean, sd, p_alternative, n_elements)
        X = pd.DataFrame(X, index=self.ids, columns=self.samples)
        self.observations.simulate(X, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)


class ExonPsiModel(_PsiModel):
    def __init__(self, recompile=False):
        design = pd.DataFrame({'v1': [1]}, index=['s1'])
        self.evol_model = 'exon_psi'
        self.set_model_attrs(obs_model='logit_binomial', design=design,
                             recompile=recompile)
        self.observations.by_element = self.by_element
        self._model_params = ['psi']
        self.output_params = ['event_id', 'psi', 'psi_0.025', 'psi_0.975']
    
    @property
    def by_element(self):
        return(True)

    def process_model_fit(self, event_id, fit):
        traces = fit.extract()
        psi = traces['psi']
        processed = {'event_id': event_id}
        processed.update({'psi': psi.mean(),
                          'psi_0.025': np.percentile(psi, 2.5),
                          'psi_0.975': np.percentile(psi, 97.5)})
        return(processed)

    def _write_processed_fit(self, event_id, processed_fit):
        for field, values in processed_fit.items():
            line = '{},{}\n'.format(event_id,
                                    ','.join([str(x) for x in values]))
            self.writers[field].write(line)

    def write_fit(self, prefix):
        fpath = '{}.summary.csv'.format(prefix)
        data = []
        with open(fpath, 'w') as fhand:
            writer = DictWriter(fhand, fieldnames=self.output_params)
            writer.writeheader()
            for event_id, fit in zip(self.observations.ids, self.model_fit):
                processed_fit = self.process_model_fit(event_id, fit)
                writer.writerow(processed_fit)
                data.append(processed_fit)
        data = pd.DataFrame(data).set_index('event_id')
        return(data)
                

class MixturePsiModel(_BayesianModel):
    def __init__(self, n_samples=3, recompile=False):
        design = pd.DataFrame({'v1': [1] * n_samples},
                              index=['s{}'.format(i) for i in range(n_samples)])
        self.n_samples = n_samples
        self.variable = n_samples > 1
        self.evol_model = 'psi_mixture_samples' if self.variable else 'psi_mixture'
        self.set_model_attrs(obs_model='logit_binomial', design=design,
                             recompile=recompile)
        self.observations.by_element = False
        self._model_params = ['psi', 'theta', 'X_mean', 'X_sd']
        if self.variable:
            self._model_params.append('sigma')
        self.output_params = ['event_id', 'psi', 'psi_0.025', 'psi_0.975']
    
    @property
    def model_params(self):
        return(self._model_params)

    @property
    def samples(self):
        return(self.design.index)

    @property
    def n_covariates(self):
        return(0)

    @property
    def model_stan_data(self):
        return({})

    def simulate(self, n_elements, mean, sd, p_alternative, sigma,
                 mean_coverage, seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids') or self.ids.shape[0] != n_elements:
            self.ids = np.array(['e{}'.format(i) for i in range(n_elements)])

        mean = mean + sd * np.random.normal(size=(n_elements))
        X = np.vstack([mean] * self.n_samples).transpose()
        constitutive = np.random.uniform(size=(n_elements)) > p_alternative
        self.psi = invlogit(mean)
        self.psi[constitutive] = 1
        if self.variable:
            X = np.random.normal(X, sigma)
        psi = invlogit(X)
        psi[constitutive, :] = 1
        psi = pd.DataFrame(psi, index=self.ids, columns=self.samples)
        self.observations.simulate(psi, mean_coverage=mean_coverage,
                                   is_psi=True, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)
    
    @property
    def covariates_stan_data(self):
        return({})
    
    @property
    def by_element(self):
        return(False)

    def write_psi(self, fpath):
        psi = self.model.get_param_posterior('psi')
        output = {'event_id': self.observations.ids,
                  'psi': psi.mean(axis=0),
                  'psi_0.025': np.percentile(psi, 2.5, axis=0),
                  'psi_0.975': np.percentile(psi, 97.5, axis=0)}
        output = pd.DataFrame(output).set_index('event_id')
        output.to_csv(fpath)
        return(output)
    
    def write_summary(self, fpath=None):
        summary = self.model.summary
        sel_rows = [not x.startswith('psi') for x in summary.index]
        summary = summary.loc[sel_rows, :]
        if fpath is not None:
            summary.to_csv(fpath)
        return(summary)
    
    def write_fit(self, prefix):
        fpath = '{}.psi.csv'.format(prefix)
        self.write_psi(fpath)
        fpath = '{}.global.csv'.format(prefix)
        self.write_summary(fpath)
    
    
class SpliceGraphModel(object):
    def __init__(self, recompile=False, inv_phi=1.8, main_p=0.9, main_p_c=5,
                 mu0=5, muS=3):
        self.model_label = 'psi_sj_nb3'
        self.recompile = recompile
        self.inv_phi = inv_phi
        self.model_stan_data = {'inv_phi': inv_phi,
                                'main_p': main_p, 'main_p_c': main_p_c,
                                'mu0': mu0, 'muS': muS}
        
        self.model_params = ['psi', 'mu']
        self.model_fit = None
    
    @property
    def edges(self):
        for s, t in zip(self.source, self.target):
            yield(s, t)
    
    def find_alternative_nodes(self):
        single_sources = set(get_single_sources(self.source))
        single_target = set(get_single_sources(self.target))
        
        alternative_nodes = []
        for s, t in self.edges:
            if ((s not in single_sources or t not in single_target) and 
                (t != self.n_nodes - 1)):
                alternative_nodes.append(t)
        return(np.unique(alternative_nodes))
    
    def load_sj_data(self, source, target): #, exon_nodes):
        self.source = source
        self.target = target
        self.n_nodes = target.max() + 1
        alternative = np.array(list(self.find_alternative_nodes())).astype(int)
        self.n_sj = source.shape[0]
#         exon_nodes = np.array([i  for i, x in enumerate(alternative)
#                                if x in exon_nodes])
        self.single_sources = list(get_single_sources(source))
        single_sources = [int(s in self.single_sources) for s in source] 
        self.model_stan_data.update({'sources': source + 1,
                                     'targets': target + 1,
                                     'alternative': alternative + 1,
#                                      'exon_nodes': exon_nodes + 1,
                                     'single_sources': single_sources,
                                     
                                     'A': alternative.shape[0],
                                     'S': self.n_sj,
                                     'N': self.n_nodes,
#                                      'E': exon_nodes.shape[0]
                                     })
    
    def load_splice_graph(self, splice_graph):
        self.splice_graph = splice_graph
        self.load_sj_data(splice_graph.edges_data['source'],
                          splice_graph.edges_data['target'])
    
    def load_counts(self, counts, pos_sj_idx):
        self.model_stan_data.update({'P': counts.shape[0],
                                     'pos_sj_idx': pos_sj_idx + 1,
                                     'counts': counts})
        
    def simulate(self, psi, read_length, mu, inv_phi=None, seed=None):
        if seed is not None:
            np.random.seed(seed)
        if inv_phi is None:
            inv_phi = self.inv_phi

        sj_mu = calc_sj_mu(self.source, self.target, psi, mu)
        size = (read_length, sj_mu.shape[0])
        if inv_phi == 0:
            counts = np.random.poisson(sj_mu, size=size)
        else:
            n = 1 / inv_phi
            p = n / (n + sj_mu)
            counts = np.random.negative_binomial(n, p, size=size)
        
        counts = pd.melt(pd.DataFrame(counts))
        self.load_counts(counts['value'], counts['variable'])
        return(counts['value'].values, counts['variable'].values)
    
    def fit(self, chains=4, n_iter=1000, adapt_delta=0.95, 
            max_treedepth=10, metric='diag', show_progress=False):
        self.chains = chains
        self.n_iter = n_iter
        self.model_fpath = join(STAN_CODE_DIR, '{}.stan'.format(self.model_label))
        self.model = get_backend_model()(self.model_fpath,
                                         compile_stan=self.recompile,
                                         params=self.model_params)
        self.model_fit = self.model.fit(self.model_stan_data,
                                        n_iter=n_iter, chains=chains,
                                        adapt_delta=adapt_delta,
                                        max_treedepth=max_treedepth,
                                        metric=metric,
                                        show_progress=show_progress)

    def process_fit(self, gene_id=None, node_ids=None):
        summary = self.model.summary.iloc[:,[0,3,7,8,9]]
        exons = summary.loc[[s.startswith('psi') for s in summary.index], :].copy()
        if node_ids is not None:
            exons.index = node_ids
            
#         node_types = np.array(['Exon'] + ['AltSS'] * (exons.shape[0] - 2) + ['Exon'])
#         node_types[self.exon_nodes] = 'Exon' 
        if gene_id is not None:
            exons['gene_id'] = gene_id
#         exons['node_type'] = node_types
        return(exons)
    
    def calc_pos_sj_idx(self, counts, splice_graph):
        edges_idxs = {x: i for i, x in enumerate(splice_graph.edges_data['id'])}
        return(np.array([edges_idxs[sj_id] for sj_id in counts['sj_id']]))
    
    def infer_gene_psi(self, splice_graph, counts, n_iter=1000):
        self.load_splice_graph(splice_graph)
        pos_sj_idx = self.calc_pos_sj_idx(counts, splice_graph)
        self.load_counts(counts['counts'].astype(int).values, pos_sj_idx)
        self.fit(n_iter=n_iter)
        return(self.process_fit(gene_id=splice_graph.gene_id,
                                node_ids=splice_graph.nodes_data['id']))
    

def infer_splice_graph_psi(db, sj_counts, sj_index=None, max_intron_length=1e6,
                           inv_phi=None, n_iter=1000, recompile=False):
    genome = Genome(db, whole_gene=True)
    model = SpliceGraphModel(recompile=recompile)
    if inv_phi is None:
        profiles = SpliceJunctionProfiles(sj_counts)
        inv_phi = profiles.calc_overdispersion()
    
    for gene_id, counts in sj_counts.groupby('gene_id'):
        gene = genome.get_gene(gene_id)
        gene.get_exons(db, sj_index=sj_index)
        
        if gene is None or gene.n_exons < 3:
            continue
        splice_graph = gene.calc_splice_graph(max_intron_length=max_intron_length)
        res = model.infer_gene_psi(splice_graph, counts, n_iter=n_iter)
        yield(res)
        