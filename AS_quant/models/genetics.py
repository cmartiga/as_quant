from csv import DictWriter

import numpy as np
import pandas as pd
from scipy.linalg.decomp_cholesky import cholesky

from AS_simulation.utils import invlogit
from AS_quant.models.basic import _BayesianModel


def simulate_design_MA(n_pops, n_ind_pop=2, Ne=[5]):
    populations = [str('pop{}'.format(i)) for i in range(n_pops)] * n_ind_pop
    n = len(populations)
    dfs = []
    for ne in Ne:
        design = pd.DataFrame({'population': populations + ['x'],
                               'ancestral': [0] * n + [1]},
                              index=['s{}'.format(i) for i in range(n + 1)])
        design['Ne'] = ne
        dfs.append(design)
    return(pd.concat(dfs))


class MutAccLineContinuousTrait(_BayesianModel):
    def __init__(self, design, n_generations, obs_model='logit_binomial',
                 recompile=False, by_element=False, mv=False):
        self.by_element = by_element
        self.evol_model = 'lande_neutral_ma'
        self.model_params = ['sigma2_m', 'sigma2_res']
        self.output_params = ['sigma2_m', 'sigma2_res']
        if by_element:
            self.evol_model += '_by_element'
            self.model_params.extend(['X0', 'pop_delta'])
            self.output_params.extend(['X0', 'psi0'])
        elif mv:
            self.evol_model += '_mv'
            self.model_params.extend(['Mcorr', 'Rcorr'])
        self.covariates_stan_data = {}
        self.set_model_attrs(obs_model=obs_model, design=design,
                             n_generations=n_generations, recompile=recompile)
    
    @property
    def model_stan_data(self):
        data = {'M': self.n_populations,
                'populations': self.populations,
                'n_generations': self.n_generations,
                'Ne': self.Ne}
        return(data)
    
    def _get_covariate_matrix(self, design, variable):
        values = design[variable].unique()
        m = pd.DataFrame({value: (design[variable] == value).astype(int)
                          for value in values})
        return(m)

    def set_model_attrs(self, obs_model, design, n_generations, recompile=False):
        self.design = design
        self.samples = design.index
        self.n_samples = self.samples.shape[0]
        self.Ne = design['Ne']
        self.n_generations = n_generations
        anc_pop = design.loc[design['ancestral'] ==1, 'population'].unique()
        pop = self._get_covariate_matrix(design, variable='population')
        self.populations = pop.drop(anc_pop, axis=1)
        if self.by_element:
            self.output_params.extend(['dpsi.{}'.format(pop)
                                       for pop in self.populations.columns])
            self.output_params.extend(['dX.{}'.format(pop)
                                       for pop in self.populations.columns])
        
        self.n_populations = self.populations.shape[1]
        self.obs_model_label = obs_model
        self.set_obs_model(obs_model)
        self.model_label = '{}_{}'.format(obs_model, self.evol_model)
        self.recompile = recompile
        
    def simulate(self, n_elements, mu0_mean, mu0_sd, sigma2_m, sigma2_res=0,
                 Mcorr=None, Rcorr=None, seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids'):
            self.ids = ['e{}'.format(i) for i in range(n_elements)]
        
        # Genetic correlations
        if Mcorr is None:
            Mcorr = np.eye(n_elements)
        if Rcorr is None:
            Rcorr = np.eye(n_elements)
        Mcorr_L = cholesky(Mcorr, lower=True)

        X0 = np.random.normal(mu0_mean, mu0_sd, size=n_elements)
        
        # Calculate evolutionary change
        pop_delta_sd = np.sqrt(2 * sigma2_m * self.n_generations)
        pop_delta_L = pop_delta_sd * Mcorr_L
        pop_delta_raw = np.random.normal(size=(n_elements, self.n_populations))
        pop_delta = np.dot(pop_delta_L, pop_delta_raw).transpose()
        delta = np.dot(self.populations, pop_delta)

        # Population diversity
        Ps = [2 * sigma2_m * Ne * Mcorr + Rcorr * sigma2_res
              for Ne in self.Ne]
        P_L = [cholesky(P, lower=True) for P in Ps]
        X_raw = np.random.normal(size=(n_elements, self.n_samples))
        res = np.vstack([np.dot(P_L[i], X_raw[:, i]).transpose()
                         for i in range(self.n_samples)])
        X = np.vstack([X0] * self.n_samples) + delta + res
        X = pd.DataFrame(X, columns=self.ids, index=self.samples).transpose()
        self.observations.simulate(X, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)
    
    @property
    def posterior(self):
        return(self.model.get_posterior_df())
    
    def write_fit_global(self, prefix):
        self.model.write_summary('{}.summary.csv'.format(prefix))
        self.model.write_fit('{}'.format(prefix))
        self.posterior.to_csv('{}.fit.csv'.format(prefix))
    
    def process_model_fit(self, fit):
        traces = fit.extract()
        X0 = traces['X0']
        psi0 = invlogit(X0)
        pop_delta = traces['pop_delta']
        psi = invlogit((pop_delta.transpose() + X0).transpose())
        delta_psi = (psi.transpose() - psi0).transpose() 
        processed = {'X0': X0.mean(),
                     'psi0': psi0.mean(),
                     'sigma2_m': traces['sigma2_m'].mean(),
                     'sigma2_res': traces['sigma2_res'].mean()}
        for i, pop in enumerate(self.populations.columns):
            processed['dpsi.{}'.format(pop)] = delta_psi[:, i].mean()
            processed['dX.{}'.format(pop)] = pop_delta[:, i].mean()
        return(processed)
    
    def write_fit_by_element(self, prefix):
        fieldnames = ['exon_id'] + self.output_params
        fpath = '{}.fit.csv'.format(prefix)
        with open(fpath, 'w') as fhand:
            writer = DictWriter(fhand, fieldnames=fieldnames)
            writer.writeheader()
            for event_id, fit in zip(self.observations.ids, self.model_fit):
                processed_fit = self.process_model_fit(fit)
                processed_fit['exon_id'] = event_id
                writer.writerow(processed_fit)
                fhand.flush()
    
    def write_fit(self, prefix):
        if self.by_element:
            self.write_fit_by_element(prefix)
        else:
            self.write_fit_global(prefix)
            

class PopulationNeutralTrait(_BayesianModel):
    def __init__(self, n_samples, obs_model='logit_binomial',
                 recompile=False, by_element=False):
        self.by_element = by_element
        if by_element:
            raise ValueError('Option is not implemented yet')
        self.evol_model = 'pop_trait'
        self.model_params = ['sigma2', 'X0', 'dX']
        self.output_params = ['sigma2', 'X0', 'psi0', 'dX', 'dpsi']
        self.covariates_stan_data = {}
        self.set_model_attrs(n_samples=n_samples, 
                             obs_model=obs_model, recompile=recompile)
    
    @property
    def model_stan_data(self):
        data = {}
        return(data)
    
    def _get_covariate_matrix(self, design, variable):
        values = design[variable].unique()
        m = pd.DataFrame({value: (design[variable] == value).astype(int)
                          for value in values})
        return(m)

    def set_model_attrs(self, n_samples, obs_model, recompile=False):
        self.n_samples = n_samples
        self.obs_model_label = obs_model
        self.set_obs_model(obs_model)
        self.model_label = '{}_{}'.format(obs_model, self.evol_model)
        self.recompile = recompile
        
    def simulate(self, n_elements, Ne, mu0_mean, mu0_sd, sigma2_m, sigma2_res=0,
                 Mcorr=None, Rcorr=None, seed=None, **kwargs):
        if seed is not None:
            np.random.seed(seed)
        if not hasattr(self, 'ids'):
            self.ids = ['e{}'.format(i) for i in range(n_elements)]
        
        # Genetic correlations
        if Mcorr is None:
            Mcorr = np.eye(n_elements)
        if Rcorr is None:
            Rcorr = np.eye(n_elements)

        X0 = np.random.normal(mu0_mean, mu0_sd, size=n_elements)
        
        # Population diversity
        P = 2 * sigma2_m * Ne * Mcorr + Rcorr * sigma2_res
        P_L = cholesky(P, lower=True)
        X_raw = np.random.normal(size=(n_elements, self.n_samples))
        res = np.dot(P_L, X_raw).transpose()
        X = np.vstack([X0] * self.n_samples) + res
        X = pd.DataFrame(X, columns=self.ids).transpose()
        self.observations.simulate(X, **kwargs)

    def write_simulations(self, prefix):
        self.observations.write(prefix)
    
    @property
    def posterior(self):
        return(self.model.get_posterior_df())
    
    def process_output(self):
        s2 = self.model.get_param_posterior('sigma2')
        X0 = self.model.get_param_posterior('X0')
        dX = self.model.get_param_posterior('dX')
        
        colnames = ['{}.dX'.format(s) for s in self.observations.samples]
        dX = pd.DataFrame(dX.mean(0).transpose(),
                          index=self.observations.ids, columns=colnames)
        data = pd.DataFrame({'sigma2': s2.mean(0),
                             'X0': X0.mean(0)},
                             index=self.observations.ids).join(dX)
        return(data)
        
    def write_fit_global(self, prefix):
        self.model.write_summary('{}.summary.csv'.format(prefix))
        self.model.write_fit('{}'.format(prefix))
        df = self.process_output()
        df.to_csv('{}.fit.csv'.format(prefix))
    
    def write_fit(self, prefix):
        if self.by_element:
            self.write_fit_by_element(prefix)
        else:
            self.write_fit_global(prefix)
