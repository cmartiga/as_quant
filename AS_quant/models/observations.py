#!/usr/bin/env python
from AS_simulation.utils import invlogit, logit

import numpy as np
import pandas as pd


class ObservationsNormal(object):
    def load(self, X, log_bias):
        self.X = X.transpose()
        self.log_bias = log_bias.transpose()
        self.samples = self.X.index
        self.ids = self.X.columns
        self.n_elements = self.X.shape[1]
        self.n_samples = self.X.shape[0]

        if self.log_bias.shape != self.X.shape:
            msg = 'Sizes of log_bias and counts matrices are different'
            raise ValueError(msg)

    @property
    def out_params(self):
        return([])

    @property
    def stan_data(self):
        stan_data = {'X_obs': self.X,
                     'N': self.X.shape[0], 'log_bias': self.log_bias}
        if not self.by_element:
            stan_data['K'] = self.X.shape[1]
        return(stan_data)

    @property
    def iter_fields(self):
        return(['X', 'log_bias'])

    def simulate(self, X, **kwargs):
        log_bias = np.random.normal(np.log(2), 0.1, size=X.shape)
        log_bias = pd.DataFrame(log_bias, index=X.index, columns=X.columns)
        Xobs = pd.DataFrame(X + log_bias, index=X.index, columns=X.columns)
        self.load(Xobs, log_bias)

    def write(self, prefix):
        fpath = '{}.X.csv'.format(prefix)
        self.X.transpose().to_csv(fpath)
        fpath = '{}.log_bias.csv'.format(prefix)
        self.log_bias.transpose().to_csv(fpath)


class ObservationsAS(object):
    def load(self, inclusion, total, log_bias):
        self.inclusion = inclusion.transpose()
        self.total = total.transpose()
        self.log_bias = log_bias.transpose()
        self.samples = self.total.index
        self.ids = self.total.columns
        self.n_elements = self.total.shape[1]
        self.n_samples = self.total.shape[0]

        if self.log_bias.shape != self.inclusion.shape or self.inclusion.shape != self.total.shape:
            msg = 'Sizes of log_bias and counts matrices are different'
            raise ValueError(msg)

    @property
    def out_params(self):
        return([])

    @property
    def stan_data(self):
        stan_data = {'inclusion': self.inclusion, 'total': self.total,
                     'N': self.total.shape[0], 'log_bias': self.log_bias}
        if not self.by_element:
            stan_data['K'] = self.total.shape[1]
        return(stan_data)

    @property
    def iter_fields(self):
        return(['inclusion', 'total', 'log_bias'])

    def simulate(self, X, total=None, mean_coverage=10,
                 exponential_cov=False, is_psi=False, **kwargs):
        if total is None:
            if exponential_cov:
                cov = np.random.exponential(mean_coverage, size=X.shape[0])
                cov = np.vstack([cov] * X.shape[1]).transpose()
                total = np.random.poisson(cov)
            else:
                total = np.random.poisson(mean_coverage, size=X.shape)
        total = pd.DataFrame(total, index=X.index, columns=X.columns)
        if total.shape != X.shape:
            raise ValueError('X and total sizes must be the same')
        log_bias = np.random.normal(np.log(2), 0.1, size=total.shape)
        log_bias = pd.DataFrame(log_bias, index=X.index, columns=X.columns)
        
        if is_psi:
            self.real_psi = X
            obs_psi = invlogit(logit(X) + log_bias)
            obs_psi[X == 1] = 1
            obs_psi[X == 0] = 0
        else:
            self.real_psi = invlogit(X) 
            obs_psi = invlogit(X + log_bias)
        
        inclusion = np.random.binomial(total, obs_psi)
        inclusion = pd.DataFrame(inclusion, index=X.index, columns=X.columns)
        self.load(inclusion, total, log_bias)
    
    @property
    def logit_psi_hat(self):
        return(np.log((self.inclusion + 1) / (1 + self.total - self.inclusion)) - self.log_bias)

    def write(self, prefix):
        fpath = '{}.inclusion.csv'.format(prefix)
        self.inclusion.transpose().to_csv(fpath)
        fpath = '{}.total.csv'.format(prefix)
        self.total.transpose().to_csv(fpath)
        fpath = '{}.log_bias.csv'.format(prefix)
        self.log_bias.transpose().to_csv(fpath)
        fpath = '{}.logit_psi_hat.csv'.format(prefix)
        self.logit_psi_hat.transpose().to_csv(fpath)
        fpath = '{}.real_psi.csv'.format(prefix)
        self.real_psi.to_csv(fpath)


class ObservationsGE(object):
    def load(self, counts, log_eff_len, log_norm_factors=None):
        self.counts = counts.transpose().astype(int)
        self.log_bias = log_eff_len.transpose()
        self.samples = self.counts.index
        self.ids = self.counts.columns
        self.n_elements = self.counts.shape[1]
        self.n_samples = self.counts.shape[0]

        if log_norm_factors is None:
            log_norm_factors = np.full(self.counts.shape[0], 0)
        self.log_norm_factors = log_norm_factors

        if self.log_bias.shape != self.counts.shape:
            msg = 'Sizes of log_eff_len and counts matrices are different'
            raise ValueError(msg)

    @property
    def out_params(self):
        return(['log_norm_factors'])

    @property
    def stan_data(self):
        stan_data = {'counts': self.counts,
                     'N': self.counts.shape[0],
                     'log_bias': self.log_bias,
                     'log_norm_factors': self.log_norm_factors}
        if not self.by_element:
            stan_data['K'] = self.counts.shape[1]
        return(stan_data)

    @property
    def iter_fields(self):
        return(['counts', 'log_bias'])

    def simulate(self, X, **kwargs):
        counts = np.random.poisson(np.exp(X))
        counts = pd.DataFrame(counts, index=X.index, columns=X.columns)

        log_lens = np.random.normal(np.log(2000), 0.58, size=X.shape[0])
        log_bias = np.vstack([log_lens] * counts.shape[1]).transpose()
        log_bias += np.random.normal(0, 0.2, size=log_bias.shape)
        log_bias = pd.DataFrame(log_bias, index=X.index, columns=X.columns)
        self.load(counts, log_bias)

    def write(self, prefix):
        fpath = '{}.counts.csv'.format(prefix)
        self.counts.transpose().to_csv(fpath)
        fpath = '{}.log_bias.csv'.format(prefix)
        self.log_bias.transpose().to_csv(fpath)
