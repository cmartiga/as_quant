from os.path import join, lexists

import pandas as pd
try:
    from cmdstanpy import CmdStanModel
    BACKEND = 'cmdstanpy'
except ImportError:
    import pystan
    BACKEND = 'pystan'

from AS_quant.utils import write_pickle, load_pickle
from AS_quant.settings import STAN_CODE_DIR
from AS_quant.models.observations import (ObservationsAS, ObservationsGE,
                                          ObservationsNormal)


class cmdStanModel(object):
    def __init__(self, stan_fpath, compile_stan=False, params=None):
        self.stan_fpath = stan_fpath
        self.stan_model = CmdStanModel(stan_file=self.stan_fpath,
                                       compile=compile_stan)
        self.stanfit = None
        self.summary = None
        self.diagnose = None
        self.model_params = params
        if self.model_params is None:
            self.model_params = params
    
    def fit(self, data, n_iter=1000, chains=4, adapt_delta=0.95,
            max_treedepth=10, metric='diag', show_progress=False,
            **kwargs):
        self.stanfit = self.stan_model.sample(data, iter_warmup=n_iter,
                                              iter_sampling=n_iter,
                                              chains=chains,
                                              adapt_delta=adapt_delta,
                                              max_treedepth=max_treedepth,
                                              metric=metric,
                                              show_progress=show_progress)
        self.summary = self.stanfit.summary()
        self.diagnose = self.stanfit.diagnose()
        self.posterior = self.get_posterior_df()
        return(self.stanfit)
    
    def check_fit(self):
        if self.stanfit is None:
            msg = 'Fit model first for extracting posterior dataframe'
            raise ValueError(msg)
    
    def get_param_posterior(self, param):
        return(self.stanfit.stan_variable(param))
    
    def get_posterior_df(self):
        self.check_fit()
        traces_dict = {}
        for param in self.model_params:
            sample = self.get_param_posterior(param)
            if len(sample.shape) == 2 and sample.shape[1] > 1:
                for i in range(sample.shape[1]):
                    traces_dict['{}{}'.format(param, i + 1)] = sample[:, i]
            elif len(sample.shape) == 3:
                for i in range(sample.shape[1]):
                    for j in range(sample.shape[2]):
                        value = sample[:, i, j]
                        traces_dict['{}{}{}'.format(param, i + 1, j + 1)] = value
            else:
                traces_dict[param] = sample
        return(pd.DataFrame(traces_dict))
    
    def write_summary(self, fpath):
        self.check_fit()
        self.summary.to_csv(fpath)
    
    def write_fit(self, output_dir):
        self.check_fit()
        self.stanfit.save_csvfiles(output_dir)
        self.write_summary('{}.summary.csv'.format(output_dir))


class pyStanModel(object):
    def __init__(self, stan_fpath, compile_stan=False, params=None):
        self.stan_fpath = stan_fpath
        self.compiled_fpath = '{}.p'.format(stan_fpath)
        if not lexists(self.compiled_fpath) or compile_stan:
            self.stan_model = pystan.StanModel(file=self.stan_fpath)
            write_pickle(self.stan_model, self.compiled_fpath)
        else:
            # Load stored model
            self.stan_model = load_pickle(self.compiled_fpath)

        self.stanfit = None
        self.summary = None
        self.diagnose = None
        self.model_params = params
    
    def fit(self, data, n_iter=1000, chains=4, adapt_delta=0.95,
            max_treedepth=10, **kwargs):
        control = {'adapt_delta': adapt_delta,
                   'max_treedepth': max_treedepth}
        self.stanfit = self.stan_model.sampling(data, pars=self.model_params,
                                                iter=2*n_iter, warmup=n_iter,
                                                chains=chains, n_jobs=chains,
                                                control=control)
        self.calc_summary()
        self.posterior = self.get_posterior_df()
        return(self.stanfit)

    def calc_summary(self):
        fit_summary = dict(self.stanfit.summary(pars=self.model_params))
        df = pd.DataFrame(fit_summary['summary'],
                          index=fit_summary['summary_rownames'],
                          columns=fit_summary['summary_colnames'])
        self.summary = df
    
    def check_fit(self):
        if self.stanfit is None:
            msg = 'Fit model first for extracting posterior dataframe'
            raise ValueError(msg)
    
    def get_param_posterior(self, param):
        return(self.stanfit.extract(param)[param])
    
    def get_posterior_df(self):
        self.check_fit()
        traces_dict = {}
        for param in self.model_params:
            sample = self.get_param_posterior(param)
            if len(sample.shape) == 2:
                if sample.shape[1] > 1:
                    traces_dict[param] = sample[:, 0]
                else:
                    for i in range(sample.shape[1]):
                        traces_dict['{}{}'.format(param, i + 1)] = sample[:, i]
            elif len(sample.shape) == 3:
                for i in range(sample.shape[1]):
                    for j in range(sample.shape[2]):
                        value = sample[:, i, j]
                        traces_dict['{}{}{}'.format(param, i + 1, j + 1)] = value
            else:
                traces_dict[param] = sample 
        return(pd.DataFrame(traces_dict))
    
    def write_summary(self, fpath):
        self.check_fit()
        self.summary.to_csv(fpath)
    
    def write_fit(self, output_dir):
        self.check_fit()
        self.posterior.to_csv('{}.fit.csv'.format(output_dir))
        self.write_summary('{}.summary.csv'.format(output_dir))


def get_backend_model():
    if BACKEND == 'cmdstanpy':
        return(cmdStanModel)
    elif BACKEND == 'pystan':
        return(pyStanModel)


class _BayesianModel(object):
    def set_obs_model(self, obs_model):
        self.observations = self.obs_models.get(obs_model, None)
        if self.observations is None:
            msg = 'Error in observation models. Only {} allowed'
            raise ValueError(msg.format(self.observations.keys()))
        self.observations = self.observations()
        self.observations.by_element = self.by_element

    def set_model_attrs(self, obs_model, design, recompile=False):
        self.design = design
        self.sample_names = design.index.to_list()
        self.covariates = design.columns.to_list()
        self.obs_model_label = obs_model
        self.set_obs_model(obs_model)
        self.model_label = '{}_{}'.format(obs_model, self.evol_model)
        self.recompile = recompile

    @property
    def obs_models(self):
        return({'logit_binomial': ObservationsAS,
                'logit_betabinomial': ObservationsAS,
                'log_poisson': ObservationsGE,
                'log_negativebinomial': ObservationsGE,
                'normal': ObservationsNormal})

    def get_stan_data_by_element(self):
        obs_data = self.observations.stan_data
        model_data = self.model_stan_data

        for element_id in self.observations.ids:
            data_element = model_data.copy()
            for field in obs_data.keys():
                if field in self.observations.iter_fields:
                    data_element[field] = obs_data[field].loc[:, element_id]
                else:
                    data_element[field] = obs_data[field]

            data_element['N'] = obs_data['N']
            yield(data_element)

    def fix_stan_data_types(self, stan_data):
        for k, v in stan_data.items():
            if isinstance(v, (pd.DataFrame, pd.Series)):
                v = v.values
            stan_data[k] = v

    def get_stan_data(self):
        stan_data = self.model_stan_data
        stan_data.update(self.observations.stan_data)
        stan_data.update(self.covariates_stan_data)
        self.fix_stan_data_types(stan_data)
        return(stan_data)

    def fit(self, chains=4, n_iter=1000, adapt_delta=0.95, 
            max_treedepth=10, metric='diag', show_progress=False):
        self.chains = chains
        self.n_iter = n_iter
        self.model_fpath = join(STAN_CODE_DIR, '{}.stan'.format(self.model_label))
        self.model = get_backend_model()(self.model_fpath,
                                         compile_stan=self.recompile,
                                         params=self.model_params)
        
        if self.by_element:
            self.model_fit = (self.model.fit(data, n_iter=n_iter, chains=chains,
                                             adapt_delta=adapt_delta,
                                             max_treedepth=max_treedepth,
                                             metric=metric, show_progress=False)
                              for data in self.get_stan_data_by_element())
        else:
            self.model_fit = self.model.fit(self.get_stan_data(), n_iter=n_iter, chains=chains,
                                            adapt_delta=adapt_delta,
                                            max_treedepth=max_treedepth,
                                            metric=metric,
                                            show_progress=show_progress)
