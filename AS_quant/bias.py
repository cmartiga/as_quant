import pandas as pd
import numpy as np

from scipy.stats._discrete_distns import nbinom
from scipy.stats.kde import gaussian_kde

from AS_quant.utils import get_gtf_longest_transcripts, get_seq, GC_CONTENT


def calc_sj_n_positions(read_len, overhang, d_start=np.inf, d_end=np.inf):
    if isinstance(d_start, float) or isinstance(d_start, int):
        return(np.min([read_len - overhang, d_start, d_end]) - overhang)
    elif hasattr(d_start, 'shape'):
        if np.isinf(d_end):
            d_end = np.full(d_start.shape, d_end)
        v = np.vstack([np.full(d_start.shape, read_len - overhang),
                       d_start, d_end])
        return(v.min(0) - overhang)
    else:
        raise ValueError('Unknown input type for d_start: {}'.format(type(d_start)))


def calc_sj_n_fragments(read_len, overhang, d_start, d_end, fragment_size):
    n_sj = calc_sj_n_positions(read_len, overhang, d_start, d_end)
    if hasattr(n_sj, 'shape'):
        x = np.full(n_sj.shape, read_len - overhang - fragment_size)
        v = np.vstack([n_sj, d_end + x, d_start + x])
        z = np.zeros(n_sj.shape)
        n1 = np.max(np.vstack([z, np.min(v[:2, :], axis=0)]), axis=0)
        n2 = np.max(np.vstack([z, np.min(v[[0,2], :], axis=0)]), axis=0)
    else:
        x = read_len - overhang - fragment_size
        n1 = np.max([0, np.min([n_sj, d_end + x])])
        n2 = np.max([0, np.min([n_sj, d_start + x])])
    return(n1 + n2)


def calc_exon_n_positions(read_len, overhang, exon_length,
                          d_start=np.inf, d_end=np.inf):
    n_sj_1 = calc_sj_n_positions(read_len, overhang, d_start, d_end)
    n_sj_2 = calc_sj_n_positions(read_len, overhang, exon_length, d_end)
    return(n_sj_1 + n_sj_2)


def calc_exon_n_fragments(read_len, overhang, d_start, d_end, fragment_size,
                          exon_length, no_exon_length=False):
    n_sj_1 = calc_sj_n_fragments(read_len, overhang, d_start,
                                 d_end + exon_length, fragment_size)
    
    if no_exon_length:
        n_sj_2 = calc_sj_n_fragments(read_len, overhang, d_start + exon_length,
                                     d_end, fragment_size)
    else:
        n_sj = calc_sj_n_positions(read_len, overhang, exon_length, d_end)
        
        if hasattr(n_sj, 'shape'):
            v = np.vstack([n_sj, d_end + read_len - overhang - fragment_size,
                           d_start + exon_length + read_len - overhang - fragment_size])
            z = np.zeros(n_sj.shape)
            n_for = np.vstack([z, v[:2, :].min(0)]).max(0)
            n_rev = np.vstack([z, v[[0, 2], :].min(0)]).max(0)
            n_sj_2 = n_for + n_rev
        else:
            n_for = np.max([0, min(n_sj, d_end + read_len - overhang - fragment_size)])
            n_rev = np.max([0, min(n_sj, d_start + exon_length + read_len - overhang - fragment_size)])
            n_sj_2 = n_for + n_rev
    return(n_sj_1 + n_sj_2)


def calc_eff_inc_len(read_len, overhang, d_start, exon_length, d_end,
                     fragment_size, fragment_size_p, no_exon_length=False):
    eff_len = np.vstack([p * calc_exon_n_fragments(read_len, overhang, d_start,
                                                   d_end, f, exon_length,
                                                   no_exon_length=no_exon_length)
                         for f, p in zip(fragment_size, fragment_size_p)]).sum(0)
    return(eff_len)


def calc_eff_skp_len(read_len, overhang, d_start, d_end,
                     fragment_size, fragment_size_p):
    eff_len = np.vstack([p * calc_sj_n_fragments(read_len, overhang,
                                              d_start, d_end, f)
                         for f, p in zip(fragment_size, fragment_size_p)]).sum(0)
    return(eff_len)


def calc_eff_len_table(exon_dist, read_len, overhang,
                       fragment_size, fragment_size_p,
                       no_exon_length=False):
    idxs = np.logical_and(exon_dist['d_start'] > 0, exon_dist['d_end'] > 0)
    exon_dist = exon_dist.loc[idxs, :]
    inc_eff_len = calc_eff_inc_len(read_len, overhang, exon_dist['d_start'],
                                   exon_dist['exon_length'],
                                   exon_dist['d_end'], fragment_size,
                                   fragment_size_p,
                                   no_exon_length=no_exon_length)
    skp_eff_len = calc_eff_skp_len(read_len, overhang, exon_dist['d_start'],
                                   exon_dist['d_end'], fragment_size,
                                   fragment_size_p)
    log_ratio = np.log(inc_eff_len) - np.log(skp_eff_len)
    data = {'exon_id': exon_dist['exon_id'], 'inc_eff_len': inc_eff_len,
            'skp_eff_len': skp_eff_len, 'log_eff_len_ratio': log_ratio,
            'exon_length': exon_dist['exon_length']}
    return(pd.DataFrame(data))


def get_exons_seqs(chrom, exons, genome):
    return([get_seq(genome, chrom, start, end, strand='+')
            for start, end in exons])
    
    
def get_fragment_size_distrib(empirical_fragments_fpath, fragment_length_mean,
                              fragment_length_sd, read_length, adaptor_len,
                              fragment_length_min):
    if empirical_fragments_fpath is None:
        fragment_lengths, weights = get_fragment_lengths(fragment_length_mean,
                                                         fragment_length_sd,
                                                         read_length,
                                                         min_frag_len=fragment_length_min)
    else:
        fragments = pd.read_csv(empirical_fragments_fpath, sep='\t',
                                names=['l', 'r', 'n'], header=None)
        fragments['l'] = fragments['l'] + 2*(read_length + adaptor_len) 
        fragments = fragments[fragments['n'] > 0]
        weights = fragments['n'] / fragments['n'].sum()
        kde = gaussian_kde(fragments['l'], weights=weights)
        fragment_lengths = np.arange(max(fragment_length_min, fragments['l'].min()),
                                     fragments['l'].max())
        weights = kde.pdf(fragment_lengths)
        weights = weights / weights.sum()
    return(fragment_lengths, weights)
    
    
def get_transcript_fragment_seq(exons, length, seq,
                                go_upstream=False):
    seq_len = len(seq)
    if seq_len >= length or len(exons) == 0:
        return(seq)
    else:
        if go_upstream:
            remaining_len = max(length - seq_len, 0)
            seq = exons[-1][-remaining_len:] + seq
            return(get_transcript_fragment_seq(exons[:-1], length, seq, 
                                               go_upstream))
        else:
            remaining_len = min(length - seq_len, len(exons[0]))
            seq = seq + exons[0][:remaining_len]
            return(get_transcript_fragment_seq(exons[1:], length, seq, 
                                               go_upstream))


def calc_pcr_eff(gc, min_eff, max_eff, alpha):
    eff = min_eff + (max_eff - min_eff) * (1 - gc ** alpha) ** alpha
    return(eff)


def get_meta_fragments(upstream, exon, downstream, fragment_len, overhang,
                       def_seq=''):
    extra_len = fragment_len - overhang
    seq1 = get_transcript_fragment_seq(upstream, length=extra_len, seq=def_seq,
                                       go_upstream=True)
    seq2 = get_transcript_fragment_seq(downstream, length=extra_len, seq=def_seq,)
    inc_max_fragment = seq1 + exon + seq2
    start = len(seq1)
    end = start + len(exon)
    skp_max_fragment = seq1 + seq2
    return(inc_max_fragment, skp_max_fragment, start, end)


def get_sj_overlap(meta_fragment_len, sj_pos, fragment_len, read_len, overhang):
    '''Returns a vector of True/False for positions in which a fragment with
       reads overlapping the SJs starts
    '''
    sj_overlap = np.full(meta_fragment_len, fill_value=False)
    
    for pos in sj_pos:
        # Fragment ends maps to SJ
        start = max(0, pos - fragment_len + overhang)
        end = max(0, pos - fragment_len + read_len - overhang + 1)
        sj_overlap[start:end] = True 
        
        # Fragment start maps to SJ
        start = max(0, pos - read_len + overhang)
        end = max(0, pos - overhang + 1)
        sj_overlap[start:end] = True
    
    # In case of short last exon
    max_position = max(0, meta_fragment_len - fragment_len - overhang + 2)
    sj_overlap[max_position:] = False
         
    return(sj_overlap)

def count_gc(fragment):
    return(fragment.count('G') + fragment.count('C'))


def calc_gc(fragment):
    return(count_gc(fragment) / float(len(fragment)))


def calc_fragments_gc_new(meta_fragment, sj_overlap, fragment_len):
    idxs = np.where(sj_overlap)[0]
    if idxs.shape[0] == 0:
        return(np.array([]))
    prev_idx = idxs[0]
    seq0 = meta_fragment[idxs[0]:idxs[0]+fragment_len]
    prev_base = GC_CONTENT.get(seq0[0], 0)
    gcs = [count_gc(seq0)]
    prev_gc = gcs[-1]
    
    for start in idxs[1:]:
        end = start + fragment_len
        base = GC_CONTENT.get(meta_fragment[end], 0)
        if prev_idx == start - 1:
            gc = prev_gc - prev_base + base
        else:
            gc = count_gc(meta_fragment[start:end]) 
        
        gcs.append(gc)
        prev_gc = gc
        prev_base = base
        prev_idx = start
        
    return(np.array(gcs) / float(fragment_len))


def calc_fragments_gc(meta_fragment, sj_overlap, fragment_len):
    return(np.array([calc_gc(meta_fragment[start:start + fragment_len])
                     for start in np.where(sj_overlap)[0]]))


def get_fragment_lengths(fragment_length_mean, fragment_length_sd,
                         read_length, min_frag_len=0, q=0.01):
    # Fragment lengths drawn from NB distribution
    p = fragment_length_mean / fragment_length_sd**2
    n = fragment_length_mean**2 / (fragment_length_sd**2 - fragment_length_mean)
    distrib = nbinom(p=p, n=n)
    min_frag_len = max(min_frag_len, read_length)
    max_len = distrib.ppf(1 - q)
    fragment_lengths = np.arange(min_frag_len, max_len).astype(int)
    weights = distrib.pmf(fragment_lengths)
    weights = weights / weights.sum() # Normalize to sum to 1
    return(fragment_lengths, weights)


def get_exons(parsed_gff, genome):
    
    for chrom, strand, _, exons in get_gtf_longest_transcripts(parsed_gff):
        n_exons = len(exons)
        if n_exons < 3:
            continue
        exon_seqs = get_exons_seqs(chrom, exons, genome=genome)
        
        for i in range(1, n_exons - 1):  # Iterate over internal exons
            start, end = exons[i]
            exon_id = '{}:{}-{}{}'.format(chrom, start, end, strand)
            exon_data = {'exon_id': exon_id,
                         'exon': exon_seqs[i],
                         'upstream': exon_seqs[:i],
                         'downstream':exon_seqs[i + 1:]}
            yield(exon_data)


class BiasCalculator(object):
    def __init__(self, fragment_lengths, weights, max_fragment_len,
                 read_length, overhang, pcr_cycles, min_eff, max_eff, alpha):
        self.fragment_lengths = fragment_lengths
        self.weights = weights
        self.max_fragment_len = max_fragment_len
        self.read_length = read_length
        self.overhang= overhang
        self.pcr_cycles = pcr_cycles
        self.min_eff = min_eff
        self.max_eff = max_eff
        self.alpha = alpha
    
    def run_gc_pcr(self, gc, w):
        eff = calc_pcr_eff(gc, self.min_eff, self.max_eff, self.alpha)
        pcr = w * np.power(1 + eff, self.pcr_cycles)
        return(pcr)
    
    def try_log_funct_ratio(self, inc, skp, funct):
        try:
            return(np.log(funct(inc) / funct(skp)))
        except ZeroDivisionError:
            return(np.nan)
    
    def __call__(self, exon_data):
        # Get meta-fragments for inclusion and skipping events
        res = get_meta_fragments(exon_data['upstream'], exon_data['exon'],
                                 exon_data['downstream'],
                                 self.max_fragment_len, self.overhang) 
        inc_max_fragment, skp_max_fragment, exon_start, exon_end = res
        
        inc_gc = []
        skp_gc = []
        
        inc_w = []
        skp_w = []
        
        for frag_len, w in zip(self.fragment_lengths, self.weights):
            
            # Inclusion SJ overlapping positions
            inc_sj_overlap = get_sj_overlap(len(inc_max_fragment),
                                            [exon_start, exon_end],
                                            frag_len,
                                            self.read_length, self.overhang)
            frag_gc = calc_fragments_gc_new(inc_max_fragment, inc_sj_overlap, frag_len)
            inc_gc.append(frag_gc * w)
            inc_w.append([w] * frag_gc.shape[0])
            
            # Skipping SJ overlapping positions
            skp_sj_overlap = get_sj_overlap(len(skp_max_fragment), [exon_start],
                                            frag_len, self.read_length,
                                            self.overhang)
            frag_gc = calc_fragments_gc_new(skp_max_fragment, skp_sj_overlap, frag_len)
            skp_gc.append(frag_gc * w)
            skp_w.append([w] * frag_gc.shape[0])
    
        inc_w = np.concatenate(inc_w)
        skp_w = np.concatenate(skp_w)
        inc_gc = np.concatenate(inc_gc)
        skp_gc = np.concatenate(skp_gc)
        inc_pcr = self.run_gc_pcr(inc_gc, inc_w)
        skp_pcr = self.run_gc_pcr(skp_gc, skp_w)
    
        exon_data = {'exon_id': exon_data['exon_id'],
                     'log_gc_ratio': self.try_log_funct_ratio(inc_gc, skp_gc, funct=np.nanmean),
                     'log_gc_pcr_bias': self.try_log_funct_ratio(inc_pcr, skp_pcr, funct=np.nanmean),
                     'log_fragment_ratio': self.try_log_funct_ratio(inc_w, skp_w, funct=np.nansum)}
        return(exon_data)
    

def get_exon_tech_variables(parsed_gff, genome, read_length,
                            fragment_lengths, weights,
                            pcr_cycles, min_eff, max_eff,
                            alpha, overhang=1):
    max_fragment_len = fragment_lengths.max()
    exons = get_exons(parsed_gff, genome)
    _calc_bias = BiasCalculator(fragment_lengths, weights, max_fragment_len,
                                read_length, overhang, pcr_cycles, min_eff,
                                max_eff, alpha)
    for exon in exons:
        yield(_calc_bias(exon))
