#!/usr/bin/env python
from Bio import Phylo
from matplotlib.gridspec import GridSpec

import matplotlib as mpl
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


# Define palettes
ELEMENT_PAL = {'Gene': 'darkred', 'Exon': 'darkorange'}

# Labels
AXIS_LABELS = {'delta_mu': r'$\Delta\mu$',
               'delta_opt_psi': r'$\Delta\Psi_{opt}$',
               'delta_mean': r'$\Delta X$',
               'delta_psi': r'$\Delta\Psi$',
               'nodes_mean': r'$X$',
               'nodes_psi': r'$\Psi$',
               'mu': r'$\mu$',
               'opt_psi': r'$\Psi_{opt}$',
               
               'mu_mean': r'Optimal value mean $\mu_{0}$',
               'mu_sd': r'Optimal value SD $\mu_{SD}$',
               'phylo_hl': r'Phylogenetic half-life $t_{\frac{1}{2}} (my)$',
               'sigma2': r'Intra-specific variance $\sigma^{2}$',
               
               'delta_mu_rho': r'$\rho_{\Delta\mu}$',
               'delta_opt_psi_rho': r'$\rho_{\Delta\Psi_{opt}}$',
               'delta_mean_rho': r'$\rho_{\Delta X}$',
               'delta_psi_rho': r'$\rho_{\Delta\Psi}$',
               'nodes_mean_rho': r'$\rho_{X}$',
               'nodes_psi_rho': r'$\rho_{\Psi}$',
               'mu_rho': r'$\rho_{\mu}$',
               'opt_psi_rho': r'$\rho_{\Psi_{opt}}$',
               
               'delta_mu_auroc': r'$AUROC_{\Delta\mu}$',
               'delta_opt_psi_auroc': r'$AUROC_{\Delta\Psi_{opt}}$',
               'delta_opt_psi_tree_auroc': r'Tree $AUROC_{\Delta\Psi_{opt}}$',
               'delta_mu_ap': r'$AP_{\Delta\mu}$',
               'delta_opt_psi_ap': r'$AP_{\Delta\Psi_{opt}}$',
               
               'mean_coverage': 'Average total reads',
               'tau2_over_2a': r'Equilibrium variance $\frac{\tau^{2}}{2\alpha}$',
               'n_elements': 'Number of elements',
               }


# Functions
def init_fig(nrow=1, ncol=1, figsize=None, style='white',
             colsize=3, rowsize=3):
    sns.set_style(style)
    if figsize is None:
        figsize = (colsize * ncol, rowsize * nrow)
    fig, axes = plt.subplots(nrow, ncol, figsize=figsize)
    return(fig, axes)


def savefig(fig, fpath, tight=True):
    if tight:
        fig.tight_layout()
    fig.savefig(fpath, format=fpath.split('.')[-1], dpi=180)
    plt.close()


def create_patches_legend(axes, colors_dict, loc=1, **kwargs):
    axes.legend(handles=[mpatches.Patch(color=color, label=label)
                         for label, color in sorted(colors_dict.items())],
                loc=loc, **kwargs)


def set_boxplot_colorlines(axes, color):
    # From
    # https://stackoverflow.com/questions/36874697/how-to-edit-properties-of-whiskers-fliers-caps-etc-in-seaborn-boxplot
    for i, artist in enumerate(axes.artists):
        artist.set_edgecolor(color)
        artist.set_facecolor('None')

        # Each box has 6 associated Line2D objects (to make the whiskers, fliers, etc.)
        # Loop over them here, and use the same colour as above
        for line in axes.lines:
            line.set_color(color)
            line.set_mfc(color)
            line.set_mec(color)


def arrange_plot(axes, xlims=None, ylims=None, xlabel=None, ylabel=None,
                 showlegend=False, legend_loc=None, hline=None, vline=None,
                 rotate_xlabels=False, cols_legend=1, rotation=90,
                 legend_frame=True, title=None, ticklabels_size=None,
                 yticklines=False, despine=True, legend_fontsize=10):
    if xlims is not None:
        axes.set_xlim(xlims)
    if ylims is not None:
        axes.set_ylim(ylims)
    if title is not None:
        axes.set_title(title)

    if xlabel is not None:
        axes.set_xlabel(xlabel)
    if ylabel is not None:
        axes.set_ylabel(ylabel)

    if showlegend:
        axes.legend(loc=legend_loc, ncol=cols_legend,
                    frameon=legend_frame, fancybox=legend_frame,
                    fontsize=legend_fontsize)
    elif axes.legend_ is not None:
        axes.legend_.set_visible(False)

    if hline is not None:
        xlims = axes.get_xlim()
        axes.plot(xlims, (hline, hline), linewidth=1, color='grey',
                  linestyle='--')
        axes.set_xlim(xlims)

    if vline is not None:
        ylims = axes.get_ylim()
        axes.plot((vline, vline), ylims, linewidth=1, color='grey',
                  linestyle='--')
        axes.set_ylim(ylims)

    if rotate_xlabels:
        axes.set_xticklabels(axes.get_xticklabels(), rotation=rotation)
    if ticklabels_size is not None:
        for tick in axes.xaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
        for tick in axes.yaxis.get_major_ticks():
            tick.label.set_fontsize(ticklabels_size)
    if yticklines:
        xlims = axes.get_xlim()
        for y in axes.get_yticks():
            axes.plot(xlims, (y, y), lw=0.2, alpha=0.1, c='lightgrey')

    if despine:
        sns.despine(ax=axes)


def plot_post_pred_ax(x, q, axes, color):
    for i in range(int((q.shape[0] - 1) / 2)):
        axes.fill_between(x, q[i, :], q[-(i + 1), :], facecolor=color,
                          interpolate=True, alpha=0.1)
    axes.plot(x, q[int(q.shape[0] / 2), :], color=color, linewidth=2)


def add_panel_label(axes, label, fontsize=20, yfactor=0.015, xfactor=0.25):
    xlims, ylims = axes.get_xlim(), axes.get_ylim()
    x = xlims[0] - (xlims[1] - xlims[0]) * xfactor
    y = ylims[1] + (ylims[1] - ylims[0]) * yfactor
    axes.text(x, y, label, fontsize=fontsize)


def plot_tree_ax(tree, axes, cmap='Blues', vmin=None, vmax=None,
                 color_by_char=False, xlims=None):
    if color_by_char:
        values = [clade.character for clade in tree.find_clades()
                  if hasattr(clade, 'character')]
        if vmin is None:
            vmin = np.min(values)
        if vmax is None:
            vmax = np.max(values)

        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        cmap = mpl.cm.get_cmap(cmap)

        for clade in tree.find_clades():
            clade.color = tuple(int(x * 255)
                                for x in cmap(norm(clade.character))[:3])

    for clade in tree.find_clades():
        if str(clade.name).startswith('Inner'):
            clade.name = None
    Phylo.draw(tree, axes=axes, do_show=False,
               label_func=lambda x: str(x).replace('_', ' ').capitalize())
    sns.despine(ax=axes, left=True, bottom=False)
    axes.set_ylabel('')
    axes.set_yticklabels([])
    if xlims is not None:
        axes.set_xlim(xlims)
    axes.set_xlabel('Divergence time (m.y.)')
#     xticklabels = [''] + [str(int(x)) for x in axes.get_xticks()[1:]][::-1]
#     axes.set_xticklabels(xticklabels)
#     axes.set_xticks([])
    axes.set_yticks([])
    return(vmin, vmax)


class FigGrid(object):
    def __init__(self, figsize=(11, 9.5), xsize=100, ysize=100):
        self.fig = plt.figure(figsize=figsize)
        self.gs = GridSpec(xsize, ysize, wspace=1, hspace=1)
        self.xsize = 100
        self.ysize = 100

    def new_axes(self, xstart=0, xend=None, ystart=0, yend=None):
        if xend is None:
            xend = self.xsize
        if yend is None:
            yend = self.ysize
        return(self.fig.add_subplot(self.gs[ystart:yend, xstart:xend]))

    def savefig(self, fpath):
        savefig(self.fig, fpath, tight=False)
