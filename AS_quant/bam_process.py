#!/usr/bin/env python
import pysam
from AS_quant.utils import parse_sj


def load_bamfile(fpath):
    bamfile = pysam.AlignmentFile(fpath, "rb")
    return(bamfile)


def get_SJ_overlapping_reads(chrom, start, end, overhang, bamfile,
                             indexed_1=False):
    for record in bamfile.fetch(chrom, start - overhang, start):
        if record.is_secondary:
            continue
        overlap_size1 = record.get_overlap(start - overhang - indexed_1,
                                           start - indexed_1)
        overlap_size2 = record.get_overlap(end, end + overhang)
        if overlap_size1 < overhang or overlap_size2 < overhang:
            continue
        yield(record.query_name, record.reference_start, record.is_duplicate)


def get_SJ_set_overlapping_reads(sjs, overhang, bamfile, indexed_1=False):
    reads = set()
    for chrom, start, end in sjs: 
        sj_reads = get_SJ_overlapping_reads(chrom, start, end,overhang, bamfile,
                                            indexed_1=indexed_1)
        for read in sj_reads:
            reads.add(read)
    return(reads)


def filter_sjs(sjs, selected_sj=None):
    if selected_sj is not None:
        sjs = [sj for sj in sjs
               if '{}:{}-{}'.format(sj[0], sj[1], sj[2]) in selected_sj]
    return(sjs)


def get_SE_counts(inc_sj, skp_sj, overhang, bamfile, detected_sj=None,
                  indexed_1=False):
    inc_sj = filter_sjs(inc_sj, detected_sj)
    skp_sj = filter_sjs(skp_sj, detected_sj)
    inc_reads = get_SJ_set_overlapping_reads(inc_sj, overhang, bamfile,
                                             indexed_1=indexed_1)
    skp_reads = get_SJ_set_overlapping_reads(skp_sj, overhang, bamfile,
                                             indexed_1=indexed_1)
    counts = {'inc_reads': len(inc_reads), 'skp_reads': len(skp_reads),
              'inc_reads_no_dups': len([x for x in inc_reads if not x[2]]),
              'skp_reads_no_dups': len([x for x in skp_reads if not x[2]])}
    return(counts)


def get_exons_SE_counts(exons_sj, overhang, bamfile, detected_sj=None,
                        indexed_1=False):
    for exon_id, exon_data in exons_sj.to_dict(orient='index').items():
        inc_sj = [parse_sj(sj) for sj in exon_data['inc_sj'].split(';') if sj]
        skp_sj = [parse_sj(sj) for sj in exon_data['skp_sj'].split(';') if sj]
        record = get_SE_counts(inc_sj, skp_sj, overhang, bamfile,
                               detected_sj=detected_sj, indexed_1=indexed_1)
        record['exon_id'] = exon_id
        yield(record)
    
