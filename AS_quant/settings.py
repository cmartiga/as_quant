from os.path import join, abspath, dirname

# Package version
VERSION = '0.1.0'

# Directories
BASE_DIR = abspath(join(dirname(__file__)))
TEST_DATA_DIR = join(BASE_DIR, '..', 'test', 'test_data')
BIN_DIR = join(BASE_DIR, '..', 'bin')
CONFIG_DIR = join(BASE_DIR, '..', 'config')
MODELS_DIR = join(BASE_DIR, 'models')
COMPILED_DIR = join(MODELS_DIR, 'compiled')
STAN_CODE_DIR = join(MODELS_DIR, 'stan_code')
RAXML_PATH = join(COMPILED_DIR, 'raxml')

# Model labels
LOGIT_OU = 'logit_ou'
AVAILABLE_MODELS = [LOGIT_OU]
MODELS_LABELS = {}
STAR_STRANDS = {'+': '1', '-': '2', None: '0'}
STAR_STRANDS_VALUES = {'1': '+', '2': '-', '0': None}

# Event types
ES = 'exon_skipping'
IR = 'intron_retention'
AD = 'alternative_donor'
AA = 'alternative_acceptor'
MXE = 'mutually_exclusive'
RMATS_TYPES = [ES, IR, AD, AA, MXE]
