#!/usr/bin/env python
from os.path import exists
from csv import DictWriter, DictReader
from tempfile import NamedTemporaryFile
from warnings import warn
from _collections import defaultdict

import numpy as np
import pandas as pd
from gffutils import create_db, FeatureDB
from pysam import TabixFile, FastaFile

from AS_quant.junctions import SpliceJunction, parse_interval_id
from AS_quant.utils import get_tabix_overlap, calc_sj_mu
from Cython.Compiler.Parsing import p_genexp
from itertools import cycle


def fix_ncbi_gff(fpath, out_fhand):
    with open(fpath) as fhand:
        for line in fhand:
            if line.startswith('#'):
                out_fhand.write(line)
                continue
            # some files have some cDNA_match features without Dbxref id
            if 'Dbxref' not in line: 
                continue
            line = line.replace('cds-', '').replace('GeneID:', '').replace('id-', '')
            line = line.replace('rna-', '').replace('gene-', '').replace('exon-', '')
            items = line.strip().split('\t')
            attrs = dict([item.split('=') for item in items[-1].split(';')])
            
            if 'RNA' in items[2] or 'gene_segment' in items[2]:
                items[2] = 'transcript'
                attrs['Parent'] = attrs['Dbxref'].split(',')[0]
                attrs['transcript_id'] = attrs['Parent']
            elif items[2] == 'gene':
                attrs['ID'] = attrs['Dbxref']
            elif items[2] in ['exon', 'CDS']:
                attrs['transcript_id'] = attrs['Parent'] 
            attrs['gene_id'] = attrs['Dbxref'].split(',')[0]
            items[-1] = ';'.join(['{}={}'.format(k, v)
                                  for k, v in attrs.items()])
            line = '\t'.join(items) + '\n'
            out_fhand.write(line)


def _check_genbank_gtf(gtf_fpath, max_lines=1000):
    with open(gtf_fpath) as fhand:
        for i, line in enumerate(fhand):
            if 'Dbxref' in line:
                return(True)
            if i >= max_lines:
                return(False)


def get_annotation_db(db_fpath, gtf_fpath=None, force=False):
    if exists(db_fpath) and not force:
        db = FeatureDB(db_fpath)
    elif gtf_fpath:
        is_genbank = _check_genbank_gtf(gtf_fpath)
        if is_genbank:
            fhand = NamedTemporaryFile('w')
            fix_ncbi_gff(gtf_fpath, fhand)
            gtf_fpath = fhand.name
        db = create_db(gtf_fpath, dbfn=db_fpath, force=force, keep_order=True,
                       merge_strategy='merge', sort_attribute_values=True,
                       disable_infer_transcripts=True)
    else:
        raise ValueError('DB file must exist or GTF should be provided')
    return(db)


def get_genome_fastafile(fastafile_fpath):
    return(FastaFile(fastafile_fpath.rstrip('.gz')))


def get_sj_index(sj_fpath):
    sj_index = TabixFile(sj_fpath)
    return(sj_index)


class ExonsSJReader(object):
    def __init__(self, fhand):
        self.fhand = fhand
        self.reader = DictReader(fhand)
    
    def split_sj(self, record):
        record = {'exon_id': record['exon_id'],
                  'inclusion_sj': record['inclusion_sj'].split(';'),
                  'skipping_sj': record['skipping_sj'].split(';')}
        return(record)

    def __iter__(self):
        for record in self.reader:
            yield(self.split_sj(record))


class GenomeInterval(object):
    def __init__(self, chrom=None, start=None, end=None, strand=None,
                 interval_id=None):
        if interval_id is not None:
            chrom, start, end, strand = parse_interval_id(interval_id)
        self.chrom = chrom
        self.start = start
        self.end = end
        self.length = self.end - self.start
        self.strand = strand
        self.id = '{}:{}-{}{}'.format(self.chrom, self.start, self.end,
                                      self.strand)
    
    def __str__(self):
        return(self.id)
    
    def fetch_sequence(self, fastafile, strand=None):
        self.seq = get_seq(fastafile, self.chrom, self.start, self.end,
                           self.strand if strand is None else strand)
        return(self.seq)


class Exon(GenomeInterval):
    def __init__(self, chrom=None, starts=[], ends=[], strand=None,
                 exon_id=None, has_donor=True, has_acceptor=True,
                 seq=None):
        if exon_id is not None:
            chrom, starts, ends, strand = self.parse_exon_id(exon_id)
        self.chrom = chrom
        self.starts = starts
        self.ends = ends
        self.strand = strand
        self.inclusion_sj = {}
        self.skipping_sj = {}
        self.has_donor = has_donor
        self.has_acceptor = has_acceptor
        self.is_alt_ss = not has_acceptor or not has_donor
        self.seq = seq
    
    def __str__(self):
        return(self.id)
    
    @property
    def length(self):
        return(max(self.ends) - min(self.starts))
    
    @property
    def is_3n(self):
        return(self.length % 3 == 0)
    
    @property
    def length0(self):
        return(self.ends[0] - self.starts[0])
    
    @property
    def id(self):
        return('{}:{}-{}{}'.format(self.chrom, '$'.join([str(x) for x in self.starts]),
                                   '$'.join([str(x) for x in self.ends]), self.strand))
    
    def parse_exon_id(self, exon_id):
        strand = None
        if exon_id[-1] in ['+', '-']:
            strand = exon_id[-1]
            exon_id = exon_id[:-1]
            
        chrom, coords = exon_id.split(':')
        starts, ends = [sorted([int(y) for y in x.split('$')])
                        for x in coords.split('-')]
        return(chrom, starts, ends, strand)
    
    def get_exon_SJs(self, exon):
        exons = sorted([self, exon], key=lambda x: x.starts[0])
        for start in exons[0].ends:
            for end in exons[1].starts:
                yield(SpliceJunction(self.chrom, start, end, self.strand))
    
    def up_overlap(self, start, end, sj):
        return(sj.start < start and sj.end < end)
    
    def down_overlap(self, start, end, sj):
        return(sj.start > start and sj.end > end)
    
    def get_overlapping_SJ(self, pos, sj_index, window_size):
        l = window_size / 2
        start, end = pos - l + 1, pos + l 
        lines = set(get_tabix_overlap(self.chrom, start, end, sj_index))
        for line in lines:
            sj = SpliceJunction(tabix_line=line)
            yield(sj)
    
    def add_inclusion_sj(self, sj):
        self.inclusion_sj[sj.id] = sj
        
    def add_skipping_sj(self, sj):
        self.skipping_sj[sj.id] = sj
    
    def _expand_sj(self, splice_junctions, sj_index, window_size=50):
        new_sjs = {}
        for sj in splice_junctions.values():
            overlapping_sjs = sj.get_overlapping_SJ(sj_index,
                                                    window_size=window_size)
            for overlapping_sj in overlapping_sjs:
                new_sjs[overlapping_sj.id] = overlapping_sj
        return(new_sjs)
    
    def expand_sj(self, sj_index, window_size=50):
        self.inclusion_sj = self._expand_sj(self.inclusion_sj, sj_index,
                                            window_size=window_size)
        self.skipping_sj = self._expand_sj(self.skipping_sj, sj_index,
                                            window_size=window_size)
    
    def get_exon_frame(self, upstream_exon=None):
        # TODO: consider doing a CDS class separately
        if hasattr(self, 'frame'):
            return(self.frame)
        
        if upstream_exon is None:
            return(0)
        else:
            frame = upstream_exon.frame + (upstream_exon.length % 3)
            if frame >= 3:
                frame = frame - 3
            self.frame = frame
            return(frame)
    
    def get_intervals_seq(self, positions, seq, sep='', min_pos=0):
        interval_seq = ''
        for start, end in zip(positions, positions[1:]):
            start, end = start - min_pos, end - min_pos
            if interval_seq:
                interval_seq = interval_seq + sep 
            interval_seq = interval_seq + seq[start:end]
        return(interval_seq)
    
    def fetch_sequence(self, fastafile, strand=None):
        seq = get_seq(fastafile, self.chrom, self.starts[0], self.ends[-1],
                      self.strand if strand is None else strand)
        exon_seq = ''
        if len(self.starts) > 1:
            interval_seq = self.get_intervals_seq(self.starts, seq, sep='{',
                                                  min_pos=self.starts[0])
            exon_seq = exon_seq + interval_seq + '{'
        positions = [self.starts[-1], self.ends[0]]
        exon_seq = exon_seq + self.get_intervals_seq(positions, seq,
                                                     min_pos=self.starts[0])
        if len(self.ends) > 1:
            interval_seq = self.get_intervals_seq(self.ends, seq, sep='}',
                                                  min_pos=self.starts[0])
            exon_seq = exon_seq + '}' + interval_seq 
        self.seq = exon_seq
        return(exon_seq)
    
    def _get_ss_seq(self, pos, fastafile, upstream_bases=4, downstream_bases=6):
        start = pos - upstream_bases
        end = pos + downstream_bases
        if self.strand == '-':
            start = pos - downstream_bases
            end = pos + upstream_bases 
        seq = get_seq(fastafile, self.chrom, start, end, self.strand)
        return(seq)
    
    def get_5ss_seqs(self, fastafile, exonic_bases=4, intronic_bases=6):
        sites = self.ends if self.strand == '+' else self.starts
        seqs = [self._get_ss_seq(pos, fastafile, upstream_bases=exonic_bases,
                                 downstream_bases=intronic_bases)
                for pos in sites]
        return(seqs)

    def get_3ss_seqs(self, fastafile, exonic_bases=3, intronic_bases=9):
        sites = self.starts if self.strand == '+' else self.ends
        seqs = [self._get_ss_seq(pos, fastafile, upstream_bases=intronic_bases,
                                 downstream_bases=exonic_bases)
                for pos in sites]
        return(seqs)

    def split(self, is_first=False, is_last=False):
        ends = [([x], 'donor') for x in self.ends]
        starts = [([x], 'acceptor') for x in self.starts]
        if is_first:
            splice_sites = [(self.starts, None)] + ends
        elif is_last: 
            splice_sites = starts + [(self.ends, None)]
        else:
            splice_sites = starts + ends
        
        seq, ref = None, self.starts[0]
        if self.seq is not None:
            seq = self.seq.replace('{', '').replace('}', '')
        
        for (s1, stype1), (s2, stype2) in zip(splice_sites, splice_sites[1:]):
            exon = Exon(self.chrom, starts=s1, ends=s2, strand=self.strand,
                        has_donor=stype2 == 'donor',
                        has_acceptor=stype1 == 'acceptor')
            if seq is not None:
                exon.seq = seq[s1[0]-ref:s2[0]-ref]
            yield(exon)
        
    def calc_n_pos_sj(self, read_length, overhang):
        return(self.length - max(0, self.length - read_length + overhang))
    
    def get_possible_exons(self):
        for start in self.starts:
            for end in self.ends:
                exon = Exon(self.chrom, [start], [end], self.strand)
                yield(exon)
    

class ExonDB(Exon):
    def __init__(self, feature):
        self.feature = feature
        self.chrom = feature.seqid
        self.starts = [feature.start - 1]
        self.ends = [feature.end]
        try:
            self.frame = int(feature.frame)
        except ValueError:
            self.frame = np.nan
            
        self.strand = feature.strand
        self.inclusion_sj = {}
        self.skipping_sj = {}


class Gene(object):
    def __init__(self, gene_id=None, exons=None):
        if exons is not None:
            self.init(gene_id, exons)
    
    def init(self, gene_id, exons):
        self.chrom = exons[0].chrom
        self.strand = exons[0].strand
        self.start = min([exon.starts[0] for exon in exons])
        self.end = max([exon.ends[-1] for exon in exons])
        self.inclusion_sj = None
        self.skipping_sj = None
        self.id = gene_id
        self.exons = {exon.id: exon for exon in exons}
        self.exon_ids = [x for x in self.exons.keys()]
    
    def __str__(self):
        return(self.id)
    
    @property
    def n_exons(self):
        return(len(self.exons))
    
    @property
    def length(self):
        return(sum([exon.length for exon in self.exons.values()]))
        
    def parse_fasta_id(self, seq_id):
        items = seq_id.split()
        self.species = items[0] 
        gene_id = items[1]
        exon_ids = items[2].split('^')
        exons = [Exon(exon_id=exon_id) for exon_id in exon_ids]
        self.init(gene_id, exons)
    
    def parse_fasta(self, seq_id, seq, exon_sep='|'):
        self.parse_fasta_id(seq_id)
        self.tx_seq = seq
        self.tx_id = self.build_tx_id(self.exons)
        self.exon_ids = np.array([x for x in self.exons.keys()])
        for exon_id, seq in zip(self.exon_ids, seq.split(exon_sep)):
            self.exons[exon_id].seq = seq
        self.calc_base_exon_array(exon_sep=exon_sep)
        
    
    def build_tx_id(self, exons):
        return('^'.join([exon.id for exon in exons.values()]))
    
    def get_all_splice_junctions(self):
        for i, exon_id_1 in enumerate(self.exon_ids):
            exon1 = self.exons[exon_id_1]
            skipped_exons = []
            for exon_id_2 in self.exon_ids[(i + 1):]:
                exon2 = self.exons[exon_id_2]
                for sj in exon1.get_exon_SJs(exon2):
                    yield(sj)
                    exon1.add_inclusion_sj(sj)
                    exon2.add_inclusion_sj(sj)
                    for skipped_exon in skipped_exons:
                        skipped_exon.add_skipping_sj(sj)
                skipped_exons.append(exon2)
        
    def get_exons_splice_junctions(self, sj_index=None, window_size=50):
        for exon_id, exon in self.exons.items():
            if sj_index is not None:
                exon.expand_sj(sj_index, window_size=window_size)
            yield(exon_id, exon.inclusion_sj, exon.skipping_sj)
    
    def calc_exons_dist_to_ends(self):
        exon_ids = [exon_id for exon_id in self.exons.keys()]
        exon_lengths = [exon.length for exon in self.exons.values()]
        
        for i, (exon_id, exon_length) in enumerate(zip(exon_ids, exon_lengths)):
            d_start = sum(exon_lengths[:i])
            d_end = sum(exon_lengths[(i+1):])
            record = {'exon_id': exon_id, 'exon_length': exon_length,
                      'd_start': d_start, 'd_end': d_end}
            yield(record)
    
    def calc_base_exon_array(self, exon_sep='|'):
        exon_number = 0
        transcript_exons = []
        exon_ids = []
        if self.exon_ids.shape[0] == 0:
            self.base_exon_number_array = []
            self.base_exon_ids_array = []
            return
        
        exon_id = self.exon_ids[0]
        for nc in self.tx_seq:
            if nc == '-':
                exon = np.nan
                exon_id = None
            elif nc == exon_sep:
                exon = np.nan
                exon_number += 1
                exon_id = self.exon_ids[exon_number]
            else:
                exon = exon_number
                exon_id = self.exon_ids[exon_number]
            transcript_exons.append(exon)
            exon_ids.append(exon_id)
        self.base_exon_number_array = np.array(transcript_exons)
        self.base_exon_ids_array = np.array(exon_ids)
    
    def get_upstream_exon(self, i, sorted_exons):
        if i > 0:
            prev_exon = sorted_exons[i-1]
        else:
            prev_exon = None
        return(prev_exon)
    
    
    def get_downstream_exon(self, i, sorted_exons):
        if i < len(sorted_exons) - 1:
            next_exon = sorted_exons[i + 1]
        else:
            next_exon = None
        return(next_exon)
    
    def get_intron_size(self, exon1, exon2):
        if exon1 is None or exon2 is None:
            return(np.nan)
        exon1, exon2 = sorted([exon1, exon2], key=lambda x: x.starts[0])
        return(exon2.starts[0] - exon1.ends[-1])
    
    def get_exon_data(self, fastafile=None):
        sorted_exons = sorted(self.exons.values(), key=lambda x: x.starts[0],
                              reverse=self.strand == '-')
        dist_to_ends = {x['exon_id']: x for x in self.calc_exons_dist_to_ends()}
        for i, exon in enumerate(sorted_exons):
            
            up_exon = self.get_upstream_exon(i, sorted_exons)
            down_exon = self.get_downstream_exon(i, sorted_exons)
            up_intron_len = self.get_intron_size(up_exon, exon)
            down_intron_len = self.get_intron_size(exon, down_exon)
            
            acceptor, donor = None, None
            if fastafile is not None:
                acceptor = ';'.join(exon.get_3ss_seqs(fastafile))
                donor = ';'.join(exon.get_5ss_seqs(fastafile))
            
            exon_data = {'exon_id': exon.id, 'gene_id': self.id,
                         'chrom': exon.chrom, 'start': exon.starts[0],
                         'end': exon.ends[-1], 'strand': exon.strand,
                         'exon_number': i, 'exon_length': exon.length,
                         '3n': exon.is_3n,
                         'upstream_intron_length': up_intron_len,
                         'downstream_intron_length': down_intron_len,
                         'frame': exon.get_exon_frame(up_exon),
                         '3ss': acceptor, '5ss': donor}
            exon_data.update(dist_to_ends[exon.id])
            
            if exon_data['exon_number'] == 0:
                exon_data['3ss'] = None
            if exon_data['d_end'] == 0:
                exon_data['5ss'] = None
            yield(exon_data)
    
    def sj_is_not_contained(self, sj):
        return(sj.start <= self.start or sj.end >= self.end)
    
    def find_SJ_bam(self, bamfile):
        reads = bamfile.fetch(self.chrom, self.start, self.end)
        introns = bamfile.find_introns(reads)
        for start, end in introns:
            sj = SpliceJunction(self.chrom, start, end, self.strand)
            if self.sj_is_not_contained(sj):
                continue
            yield(sj)
    
    def find_SJ_tabix(self, sj_index):
        for line in get_tabix_overlap(self.chrom, self.start,
                                      self.end, sj_index):
            sj = SpliceJunction(tabix_line=line)
            if self.sj_is_not_contained(sj):
                continue
            yield(sj)
    
    def get_splice_junctions(self, bamfile=None, sj_index=None):
        if bamfile is not None:
            for sj in self.find_SJ_bam(bamfile):
                yield(sj)
        
        if sj_index is not None:
            for sj in self.find_SJ_tabix(sj_index):
                yield(sj)
    
    def get_sorted_splice_sites(self, exons_list=None, bamfile=None,
                                sj_index=None):
        if exons_list is None:
            exons_list = [self.exons]
        coords = {}
        for exons in exons_list:
            for exon in exons.values():
                for start in exon.starts:
                    coords[start] = 'start'
                for end in exon.ends:
                    coords[end] = 'end'
        
        for sj in self.get_splice_junctions(bamfile=bamfile, sj_index=sj_index):
            coords[sj.start] = 'end'
            coords[sj.end] = 'start'
                
        coords = sorted(coords.items(), key=lambda x:x[0])
        return(coords)

    def get_exons(self, fastafile=None, bamfile=None, sj_index=None):
        splice_sites = self.get_sorted_splice_sites([self.exons],
                                                    bamfile=bamfile,
                                                    sj_index=sj_index)
        if splice_sites[0][1] == 'end':
            splice_sites = splice_sites[1:]
        exons = {}
        
        starts, ends = [], []
        prev_st = None
        for s, st in splice_sites:
            if st == 'end':
                ends.append(s)
                
            if st == 'start':
                if prev_st == 'end':
                    exon = Exon(chrom=self.chrom, starts=starts, ends=ends,
                                strand=self.strand)
                    if fastafile:
                        exon.fetch_sequence(fastafile)
                    exons[exon.id] = exon
                    starts, ends = [], []
                starts.append(s) 
            prev_st = st
        
        if ends:
            exon = Exon(chrom=self.chrom, starts=starts, ends=ends,
                        strand=self.strand)
            if fastafile:
                exon.fetch_sequence(fastafile)
            exons[exon.id] = exon
        self.exons = exons
        return(exons)
    
    def sort_exons(self, exons):
        if self.strand == '-':
            exons = {exon.id: exon for exon in list(exons.values())[::-1]}
        return(exons)

    def calc_splice_graph(self, max_intron_length=1e6):
        sg = SpliceGraph(self, max_intron_length=max_intron_length)
        return(sg)


class SpliceGraph(object):
    def __init__(self, gene=None, max_intron_length=1e6):
        if gene is not None:
            self.build(gene, max_intron_length=max_intron_length)
    
    def calc_sg_nodes(self, gene):
        exons_list = [x for x in gene.exons.values()]
        exon_number = 0
        
        # First exon
        for exon_fragment in exons_list[0].split(is_first=True):
                yield(exon_number, exon_fragment)
        exon_number += 1
                
        #  Inner exons
        for exon in exons_list[1:-1]:
            for exon_fragment in exon.split():
                yield(exon_number, exon_fragment)
            exon_number += 1
        
        # Final exons
        for exon_fragment in exons_list[-1].split(is_last=True):
                yield(exon_number, exon_fragment)
    
    def _edge_is_valid(self, source_node, target_node, max_intron_length):
        c1 = source_node.has_donor or source_node.ends[0] == target_node.starts[0]
        c2 = target_node.has_acceptor or source_node.ends[0] == target_node.starts[0]
        intron_length = target_node.starts[0] - source_node.ends[0]
        return(c1 and c2 and intron_length < max_intron_length)
    
    def calc_upstream_start(self, i):
        while(i > 0 and self.nodes_list[i].is_alt_ss):
            i -= 1
        return(self.nodes_list[i].starts[0])
    
    def calc_downstream_end(self, i):
        while(i < self.n_nodes - 1 and self.nodes_list[i].is_alt_ss):
            i += 1
        return(self.nodes_list[i].ends[-1])
    
    def build(self, gene, max_intron_length):
        self.gene_id = gene.id
        self.nodes_exon = []
        self.nodes_list = []
        for exon, node in self.calc_sg_nodes(gene):
            self.nodes_exon.append(exon)
            self.nodes_list.append(node)
        
        self.nodes_ids = [node.id for node in self.nodes_list]
        self.nodes = {node.id: node for node in self.nodes_list}
        
        self.n_nodes = len(self.nodes_list)
        self.edges = {}
        self.graph = defaultdict(list)
        
        edges_data = []
        
        for i in range(self.n_nodes):
            source_node = self.nodes_list[i]
            upstream_start = self.calc_upstream_start(i)
            for j in range(i+1, self.n_nodes):
                target_node = self.nodes_list[j]
                
                if not self._edge_is_valid(source_node, target_node,
                                           max_intron_length=max_intron_length):
                    continue
                self.graph[i].append(j)
                
                sj = SpliceJunction(chrom=gene.chrom, start=source_node.ends[0],
                                    end=target_node.starts[0], strand=gene.strand,
                                    upstream_start=upstream_start,
                                    downstream_end=self.calc_downstream_end(j))
                edges_data.append({'id': sj.id, 'source': i, 'target': j})
                self.edges[sj.id] = sj
        
        self.edges_ids = [x for x in self.edges.keys()]
        self.edges_data = pd.DataFrame(edges_data)
        self.n_edges = len(self.edges)
        self.edges_data['gene_id'] = gene.id
        
        self.nodes_data = pd.DataFrame({'id': self.nodes_ids, 
                                        'node': np.arange(self.n_nodes),
                                        'exon': self.nodes_exon,
                                        'type': ['AltSS' if node.is_alt_ss else 'Exon'
                                                 for node in self.nodes_list]})
        self.nodes_are_exons = self.nodes_data['type'] == 'Exon'
        self.nodes_data['gene_id'] = gene.id
    
    def _sum_strands(self, profile):
        return(profile.groupby('position')['counts'].sum().reset_index())
    
    def calc_mapping_profile(self, bamfile,
                             read_length=None, keep_stranded=False,
                             min_overhang=1, rm_duplicates=True,
                             min_mapq=255):
        counts = []
        for sj_id, sj in self.edges.items():
            profile = sj.get_mapping_profile(bamfile, min_overhang=min_overhang,
                                             read_length=read_length, 
                                             rm_duplicates=rm_duplicates, 
                                             min_mapq=min_mapq)
            if not keep_stranded:
                profile = self._sum_strands(profile)
            profile['sj_id'] = sj_id
            counts.append(profile)
        self.counts = pd.concat(counts)
        self.counts['gene_id'] = self.gene_id
        self.counts = self.counts.reset_index().drop('index', axis=1)
        return(self.counts)
    
    def calc_sj_mu(self, psi, mu=1):
        sj_mu = calc_sj_mu(self.edges_data['source'], self.edges_data['target'],
                           psi=psi, mu=mu)
        self.edges_data['mu'] = sj_mu
        return(sj_mu)
    
    def calc_sj_psi(self):
        means = self.edges_data.groupby('source')['mu'].sum().to_dict()
        self.edges_data['source_mu'] = [means[x] for x in self.edges_data['source']]
        self.edges_data['psi'] = self.edges_data['mu'] / self.edges_data['source_mu']
        self.sj_psi = defaultdict(dict)
        for s, t, p in zip(self.edges_data['source'], self.edges_data['target'],
                           self.edges_data['psi']):
            self.sj_psi[s][t] = p
        return(self.edges_data['psi'].values)
    
    def add_children(self, transcript, transcript_psi=1):
        node = transcript[-1]
        children = self.graph[node]
        if not children:
            self.transcripts.append((transcript, transcript_psi))
            return([(transcript, transcript_psi)])
        else:
            return([self.add_children(transcript + [child],
                                      transcript_psi * self.sj_psi[node][child])
                    for child in children])
    
    def calc_tx_quant(self, nodes_psi):
        self.transcripts = []
        self.calc_sj_mu(psi=nodes_psi)
        self.calc_sj_psi()
        self.add_children([0], transcript_psi=1)
        return(self.transcripts)
    
    def get_tx_seq(self, transcript):
        seq = ''.join([self.nodes_list[i].seq for i in transcript])
        return(seq)
    
    def generate_tx_seq_quant(self, nodes_psi, gene_expr):
        for i, (tx, psi) in enumerate(self.calc_tx_quant(nodes_psi)):
            seq = self.get_tx_seq(tx)
            tx_id = '{}.{}'.format(self.gene_id, i)
            tx_quant = psi * gene_expr
            yield(tx_id, tx_quant, seq)
    
    def write_tx_seq_quant(self, tx_seq_quant, fhand):
        for tx_id, tx_quant, seq in tx_seq_quant:
            line = '>{}${}\n{}\n'.format(tx_id, tx_quant, seq)
            fhand.write(line)
        

class GeneDB(Gene):
    def __init__(self, feature, db, fastafile=None, exon_feature_type='exon',
                 tx_feature_type='transcript', tx_id=None, exon_sep='|',
                 whole_gene=False):
        
        self.exon_feature_type = exon_feature_type
        self.tx_feature_type = tx_feature_type
        self.whole_gene = whole_gene

        # Essential attributes
        self.feature = feature   
        self.chrom = feature.seqid
        self.strand = feature.strand
        self.start = feature.start - 1
        self.end = feature.end
        self.inclusion_sj = None
        self.skipping_sj = None
        self.id = feature.id

        self.exons = self.get_exons(db)
        self.exon_ids = np.array([exon_id for exon_id in self.exons.keys()])
        self.tx_seq = None
        self.tx_id = tx_id
        
        # Optional attributes
        if fastafile is not None:
            self.fetch_seq(db, fastafile, exon_sep=exon_sep)
            self.calc_base_exon_array()
            for exon_id in self.exon_ids:
                self.exons[exon_id].fetch_sequence(fastafile)
    
    def get_transcript_exons(self, db, tx_id=None, fastafile=None):
        if tx_id is None:
            exons = self.longest_transcript(db)[1]
        else:
            try:
                exons = self._get_exons_from_db(db, tx_id)
            except:
                exons = self._get_exons_from_db(db)
        if fastafile is not None:
            for exon_id in exons:
                exons[exon_id].fetch_sequence(fastafile)
        return(exons)
        
    def _get_exons_from_db(self, db, tx_id=None):
        if tx_id is None:
            tx_id = self.feature.id
        exon_features = db.children(tx_id, featuretype=self.exon_feature_type,
                                    order_by='start')
        exons = [ExonDB(e) for e in exon_features]
        exons = {exon.id: exon for exon in exons}
        return(exons)
    
    def get_transcripts(self, db):
        self.tx_feature_type = None
        txs = db.children(self.feature, featuretype=self.tx_feature_type,
                          order_by='start')
        txs = [tx for tx in txs if tx.id != self.id and tx.id is not None]
        return(txs)
    
    def get_transcripts_exons(self, db):
        txs = self.get_transcripts(db)
        if not txs:
            self._exons = {}
            return({})
        txs_exons = {tx.id: self.get_transcript_exons(db, tx) for tx in txs}
        if not txs_exons:
            txs_exons = {self.id: self.get_transcript_exons(db)}
        return(txs_exons)
    
    def longest_transcript(self, db):
        txs_exons = self.get_transcripts_exons(db)
        if not txs_exons:
            return(None, txs_exons)
        txs_len = {k: sum([exon.length for exon in exons.values()])
                   for k, exons in txs_exons.items()}
        longest_tx = max(txs_len.keys(), key=lambda x: txs_len[x])
        self._exons = txs_exons[longest_tx]
        return(longest_tx, txs_exons[longest_tx])
    
    
    def get_gene_exons(self, db, fastafile=None, bamfile=None,
                       sj_index=None):
        exons = self.get_transcripts_exons(db).values()
        splice_sites = self.get_sorted_splice_sites(exons, bamfile=bamfile,
                                                    sj_index=sj_index)
        exons = {}
        
        starts, ends = [], []
        prev_st = None
        for s, st in splice_sites:
            if st == 'end':
                
                ends.append(s)
            if st == 'start':
                if prev_st == 'end':
                    exon = Exon(chrom=self.chrom, starts=starts, ends=ends,
                                strand=self.strand)
                    if fastafile:
                        exon.fetch_sequence(fastafile)
                    exons[exon.id] = exon
                    starts, ends = [], []
                starts.append(s) 
            prev_st = st
        
        if ends:
            exon = Exon(chrom=self.chrom, starts=starts, ends=ends,
                        strand=self.strand)
            if fastafile:
                exon.fetch_sequence(fastafile)
            exons[exon.id] = exon
        return(exons)

    def get_exons(self, db, fastafile=None, bamfile=None, sj_index=None):
        if self.whole_gene:
            return(self.get_gene_exons(db, fastafile=fastafile,
                                       bamfile=bamfile, sj_index=sj_index))
        else:
            return(self.get_transcript_exons(db, fastafile=fastafile))
    
    def _fetch_seq(self, exons, fastafile, exon_sep='|'):
        exons_seq = [exon.fetch_sequence(fastafile, strand='+')
                     for exon in exons.values()]
        tx_seq = exon_sep.join(exons_seq).strip(exon_sep)
        if self.strand == '-':
            tx_seq = reverse_complement(tx_seq)
        self.tx_seq = tx_seq
        self.tx_id = self.build_tx_id(exons)
    
    def fetch_transcript_seq(self, db, fastafile, tx_id=None, exon_sep='|'):
        exons = self.get_transcript_exons(db, tx_id)
        self._fetch_seq(exons, fastafile, exon_sep)
    
    def fetch_seq(self, db, fastafile, exon_sep='|'):
        exons = self.get_exons(db)
        self._fetch_seq(exons, fastafile, exon_sep)
    

class Genome(object):
    def __init__(self, db, only_cds=False, tx_feature_type='transcript',
                 fastafile=None, whole_gene=False):
        self.db = db
        self.fastafile = fastafile
        self.exon_feature_type = 'CDS' if only_cds else 'exon' 
        self.tx_feature_type = tx_feature_type
        self.whole_gene = whole_gene
    
    @property
    def n_genes(self):
        if not hasattr(self, '_n_genes'):
            self._n_genes = self.db.count_features_of_type('gene')
        return(self._n_genes)
    
    @property
    def n_exons(self):
        if not hasattr(self, '_n_exons'):
            n = self.db.count_features_of_type(self.exon_feature_type)
            self._n_exons = n
        return(self._n_exons)
    
    @property
    def genes(self):
        for gene_feature in self.db.all_features(featuretype='gene'):
            gene = GeneDB(gene_feature, self.db, fastafile=self.fastafile,
                          exon_feature_type=self.exon_feature_type,
                          tx_feature_type=self.tx_feature_type,
                          tx_id=None, exon_sep='|',
                          whole_gene=self.whole_gene)
            yield(gene)
    
    def get_gene(self, gene_id):
        try:
            gene_feature = self.db[gene_id]
        except:
            gene_feature = None
            
        if gene_feature is None:
            msg = 'Gene id {} not found'.format(gene_id)
            warn(msg)
            return(None)
        gene = GeneDB(gene_feature, self.db, fastafile=self.fastafile,
                      exon_feature_type=self.exon_feature_type,
                      tx_feature_type=self.tx_feature_type,
                      tx_id=None, exon_sep='|',
                      whole_gene=self.whole_gene)
        return(gene)
        
    def write_splice_junctions_STAR(self, fhand):
        for gene in self.genes:
            for sj in gene.get_all_splice_junctions():
                fhand.write(sj.STAR_SJ_line)
    
    def write_exons_splice_junctions(self, fhand, sj_index=None,
                                     window_size=50):
        header = 'exon_id,inclusion_sj,skipping_sj\n'
        fhand.write(header)
        for gene in self.genes:
            list(gene.get_all_splice_junctions())
            exons_sj = gene.get_exons_splice_junctions(sj_index=sj_index,
                                                       window_size=window_size)
            exons_sj = list(exons_sj)
            for exon_id, inclusion_sj, skipping_sj in exons_sj:
                items = [exon_id, ';'.join(inclusion_sj.keys()),
                         ';'.join(skipping_sj.keys())]
                line = ','.join(items) + '\n'
                fhand.write(line)
    
    def write_exons_distances(self, fhand):
        fieldnames = ['exon_id', 'd_start', 'exon_length', 'd_end']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()
        
        for gene in self.genes:
            for exon_dist in gene.calc_exons_dist_to_ends():
                writer.writerow(exon_dist)
    
    def write_exon_data(self, fhand):
        fieldnames = ['exon_id', 'gene_id', 'chrom', 'start', 'end',
                      'strand', 'exon_number', 'exon_length', '3n', 'frame',
                      'd_start', 'd_end',
                      'upstream_intron_length', 'downstream_intron_length',
                      '3ss', '5ss']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()
        for gene in self.genes:
            for record in gene.get_exon_data(fastafile=self.fastafile):
                writer.writerow(record)
    
    def calc_mapping_profile(self, bamfile, read_length, max_intron_length=1e6):
        for gene in self.genes:
            sg = gene.calc_splice_graph(max_intron_length=max_intron_length)
            counts = sg.calc_mapping_profile(bamfile, read_length=read_length)
            counts['gene_id'] = gene.id
            yield(counts)
    
    def write_mapping_profiles(self, counts_iter, fhand, log=None):
        writer = None
        for i, counts in enumerate(counts_iter):
            
            if writer is None:
                writer = DictWriter(fhand, fieldnames=counts.columns, delimiter='\t')
                writer.writeheader()
            
            for record in counts.to_dict(orient='index').values():
                writer.writerow(record)
                
            if log is not None and i % 10 == 0:
                log.write('\tSJ processed: {}'.format(i))
                fhand.flush()
    
    def get_genes(self, gene_ids):
        if gene_ids is None:
            genes = self.genes
        else:
            genes = (self.get_gene(gene_id) for gene_id in gene_ids)
        return(genes)
    
    def get_exon_length_df(self, gene_ids=None):
        genes = self.get_genes(gene_ids=gene_ids)
        data = []
        for gene in genes:
            exons = gene.get_exons(self.db)
            exons = gene.sort_exons(exons)
            for exon in exons.values():
                data.append({'gene_id': gene.id, 'exon_length': exon.length})
        return(pd.DataFrame(data))
    
    def calc_sj_per_read(self, exon_length_df, read_length, overhang,
                           gene_expression=None, mappability=1):
        gene_df = []
        for gene_id, data in exon_length_df.groupby('gene_id')['exon_length']:
            
            lengths = data.values
            n_exons = lengths.shape[0]
            t = lengths.sum() - read_length
            n = 0
             
            for i, l_i in enumerate(lengths[:-1]):
                sum_l = 0
                n_i = l_i - max(0, l_i - read_length + overhang)
                
                # rescue reads that map to the following SJs
                for j in range(i + 1, n_exons-1):
                    l_j = lengths[j]
                    sum_l += l_j
                    if sum_l > read_length - overhang:
                        break
                    n_i += max(0, read_length - overhang - sum_l)
                n += n_i
            gene_df.append({'gene_id': gene_id, 'n': n, 'total': t,
                            'p': n / t})
        gene_df = pd.DataFrame(gene_df)
        
        # Add gene expression data 
        if gene_expression is None:
            gene_df['expression'] = 1 / gene_df.shape[0]
        else:
            expr = gene_expression.to_dict()
            gene_df['expression'] = [expr.get(gene_id, 0) for gene_id in gene_expression.index]
         
        n_sum = (gene_df['n'] * gene_df['expression']).sum() * mappability
        t_sum = (gene_df['total'] * gene_df['expression']).sum()
        p = n_sum / t_sum 
        return(p)
     

def parse_seq_name_exon_ids(seq_id):
    items = seq_id.split(' ')
    data = {'species': items[0], 'transcript_id': None, 'exon_ids': None}
    if len(items) > 1:
        data['transcript_id'] = items[1]
        if len(items) > 2:
            data['exon_ids'] = items[2:]
    return(data)


def parse_fasta_lines(lines):
    seq_id = None
    seq = ''
    for line in lines:
        if not line:
            continue
        if line.startswith('>'):
            if seq_id and len(seq) > 0:
                yield(seq_id, seq)
            seq_id = line.strip().lstrip('>')
            seq = ''
        else:
            seq += line.strip()
    if seq_id is not None and len(seq) > 0:
        yield(seq_id, seq)


def parse_fasta(fpath, get_exon_ids=False):
    with open(fpath) as fhand:
        for record in parse_fasta_lines(fhand, get_exon_ids=get_exon_ids):
            yield(record)


def reverse_complement(seq):
    complement = dict(zip('ACTG{', 'TGAC}'))
    return ''.join([complement.get(x, x) for x in seq[::-1]])


def get_seq(genome, chrom, start, end, strand):
    try:
        seq = genome.fetch(chrom, start, end)
    except:
        try:
            seq = genome.fetch(chrom.lstrip('chr'), start, end)
        except:
            seq = genome.fetch('chr' + chrom, start, end)
    if strand == '-':
        seq = reverse_complement(seq)
    return(seq.upper())
