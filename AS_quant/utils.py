#!/usr/bin/env python
from _collections import defaultdict
from itertools import chain
from os.path import join
from time import ctime
import multiprocessing
import sys
import time

from ete3 import Tree
from ete3.ncbi_taxonomy.ncbiquery import NCBITaxa
from pysam import FastaFile

import numpy as np
import pandas as pd
import itertools


if (sys.version_info > (3, 0)):
    from _pickle import dump, load
else:
    from cPickle import dump, load


GC_CONTENT = {'A': 0, 'C': 1, 'T': 0, 'G': 1}


def format_exon_id(chrom, start, end, strand, sp=None):
    exon_id = '{}:{}-{}{}'.format(chrom, start, end, strand)
    if sp is not None:
        exon_id = '{}-{}'.format(sp, exon_id)
    return(exon_id)


def get_size(x):
    if hasattr(x, 'shape'):
        return(x.shape[0])
    else:
        return(len(x))


def get_single_sources(sources):
    for source, items in itertools.groupby(sorted(sources)):
        items = list(items)
        if len(items) == 1:
            yield(source)


def calc_sj_mu(source, target, psi, mu=1):
    nodes_mu = np.zeros(get_size(psi))
    sj_mu = np.zeros(get_size(source))
    nodes_mu[0] = mu
    single_sources = list(get_single_sources(source))
    for i, (s, t) in enumerate(zip(source, target)):
        p = 1 if s in single_sources else psi[t]
        m = nodes_mu[s] * p
        sj_mu[i] = m
        nodes_mu[t] += m
        nodes_mu[s] -= m
    return(sj_mu)


class LogTrack(object):
    '''Logger class'''

    def __init__(self, fhand=None):
        if fhand is None:
            fhand = sys.stderr
        self.fhand = fhand
        self.start = time.time()

    def init(self, dataset_dir, script_name):
        log_fpath = join(dataset_dir, '{}.log'.format(script_name))
        self.fhand = open(log_fpath, 'w')
        self.write('Start')

    def write(self, msg, add_time=True):
        if add_time:
            msg = '[ {} ] {}\n'.format(ctime(), msg)
        else:
            msg += '\n'
        self.fhand.write(msg)
        self.fhand.flush()

    def finish(self):
        t = time.time() - self.start
        self.write('Finished succesfully. Time elapsed: {:.1f} s'.format(t))


def run_parallel(func, iterable, threads=None, chunks=False):
    if threads is None or threads == 1:
        map_func = map
    else:
        workers = multiprocessing.Pool(threads)
        if chunks:
            map_func = workers.starmap
        else:
            map_func = workers.map
    return map_func(func, iterable)


def parse_gff_line(line, sep=None):
    items = line.strip().strip(';').split('\t')
    chrom, _, feature, start, end, _, strand = items[:7]
    start, end = int(start) - 1, int(end)  # Convert to 0-indexed
    if sep is None:
        sep = '=' if '=' in items[-1] else ' '

    attrs = {}
    for x in items[-1].strip(';').split(';'):
        items = x.strip().split(sep)
        k = items[0]
        v = items[1].strip('"')
        attrs[k] = v

    # To extract gene ids from NCBI annotation GFFs
    if 'Dbxref' in attrs:
        attrs['gene_id'] = attrs['Dbxref'].split(',')[0].split(':')[-1]

    return {'chrom': chrom, 'feature': feature, 'strand': strand,
            'start': start, 'end': end, 'attributes': attrs}


def parse_seq_name_exon_ids(seq_id):
    items = seq_id.split(' ')
    data = {'species': items[0], 'transcript_id': None, 'exon_ids': None}
    if len(items) > 1:
        data['transcript_id'] = items[1]
        if len(items) > 2:
            data['exon_ids'] = items[2:]
    return(data)


def parse_fasta_lines(lines, get_exon_ids=False):
    seq_id = None
    seq = ''
    for line in lines:
        if not line:
            continue
        if line.startswith('>'):
            if seq_id:
                yield(seq_id, seq)
            seq_id = line.strip().lstrip('>').lower()
            if get_exon_ids:
                seq_id = parse_seq_name_exon_ids(seq_id)

            seq = ''
        else:
            seq += line.strip()
    if seq_id is not None and len(seq) > 0:
        yield(seq_id, seq)


def parse_fasta(fpath, get_exon_ids=False):
    with open(fpath) as fhand:
        for record in parse_fasta_lines(fhand, get_exon_ids=get_exon_ids):
            yield(record)


def parse_gff(gff_fhand, sep=None, store_transcripts=True, sp=None,
              only_CDS=False, genes_subset=None):
    '''Returns a dictionary with exon and intron coordinates for each gene
       only for protein coding genes'''

    parsed_gff = defaultdict(dict)
    parents = {}

    for line in gff_fhand:
        if line.startswith('#'):
            continue
        record = parse_gff_line(line, sep=sep)
        attrs = record['attributes']

        gene_id = attrs.get('gene_id', None)
        parent = attrs.get('Parent', None)
        _id = attrs.get('ID', None)
        if _id is not None:
            parents[_id] = parent
        parent = parents.get(parent, parent)
        # biotype = attrs.get('biotype', None)
        if gene_id is None:
            gene_id = parent
        if gene_id is not None:
            gene_id = gene_id.split(':')[-1]

        if genes_subset is not None and gene_id not in genes_subset:
            continue

        start, end = record['start'], record['end']

        # Select protein coding genes
        parsed_gff[gene_id].update({'strand': record['strand'],
                                    'chrom': record['chrom'],
                                    'gene_id': gene_id,
                                    'species': sp})

        exon_str = 'CDS' if only_CDS else 'exon'
        if exon_str in record['feature']:
            try:
                parsed_gff[gene_id]['exons'].add((start, end))
            except KeyError:
                parsed_gff[gene_id]['exons'] = set([(start, end)])

            if store_transcripts:
                transcript_id = attrs.get(
                    'transcript_id', attrs.get('Parent', None))
                if 'transcripts' not in parsed_gff[gene_id]:
                    parsed_gff[gene_id]['transcripts'] = {}

                if transcript_id is not None:
                    # In case of using NCBI annotation GFF: remove prefix "rna-"
                    # and version information
                    if transcript_id.startswith('rna-'):
                        transcript_id = transcript_id.split(
                            '-', 1)[1].split('.')[0]
                    try:
                        parsed_gff[gene_id]['transcripts'][transcript_id].append(
                            (start, end))
                        parsed_gff[gene_id]['transcripts'][transcript_id].sort()
                    except KeyError:
                        parsed_gff[gene_id]['transcripts'][transcript_id] = [
                            (start, end)]
    return(parsed_gff)


def get_transcript_exons_dict(annotation):
    transcript_idx = {}
    for sp, parsed_gff in annotation.items():
        for gene_data in parsed_gff.values():
            chrom = gene_data['chrom']
            strand = gene_data['strand']
            sp = gene_data['species']
            if 'transcripts' not in gene_data:
                continue
            for transcript_id, exons in gene_data['transcripts'].items():
                transcript_idx[transcript_id] = {'chrom': chrom, 'strand': strand,
                                                 'exons': exons, 'species': sp}
    return(transcript_idx)


def get_gene_ends(exons):
    coords = []
    for exon in exons:
        coords.extend(exon)
    coords.sort()
    return(coords[0], coords[-1])


def get_gtf_longest_transcripts(parsed_gff):
    for gene_data in parsed_gff.values():
        chrom = gene_data['chrom']
        strand = gene_data['strand']
        transcripts = gene_data.get('transcripts', {})
        if not transcripts:
            continue
        longest_transcript = max(transcripts,
                                 key=lambda x: len(transcripts[x]))
        gene_exons = sorted(transcripts[longest_transcript])
        yield(chrom, strand, longest_transcript, gene_exons)


def format_exon_ids(chrom, strand, exons):
    for start, end in exons:
        exon_id = format_exon_id(chrom, start, end, strand)
        yield(exon_id)


def fetch_internal_exon_ids(parsed_gff):
    for gene_data in get_gtf_longest_transcripts(parsed_gff):
        chrom, strand, _, exons = gene_data
        if len(exons) < 3:
            continue
        for exon_id in format_exon_ids(chrom, strand, exons):
            yield(exon_id)


def write_gff_record(gene_data, fhand, source='custom'):
    gene_id, chrom, gene_data, new_transcripts = gene_data
    strand = gene_data['strand']
    start, end = get_gene_ends(gene_data['exons'])
    gene_items = [chrom, source, 'gene', start, end, '.', strand, '.',
                  'ID={}'.format(gene_id)]
    fhand.write('\t'.join([str(x) for x in gene_items]) + '\n')
    for transcript_id, exons in new_transcripts.items():
        attrs = 'ID={};Parent={};gene_id={};transcript_id={}'
        t_start, t_end = get_gene_ends(exons)
        transcript_items = [chrom, source, 'mRNA', t_start, t_end, '.',
                            strand, '.',
                            attrs.format(transcript_id, gene_id,
                                         gene_id, transcript_id)]
        fhand.write('\t'.join([str(x) for x in transcript_items]) + '\n')

        for i, (start, end) in enumerate(exons):
            attrs = 'ID={}|{};Parent={};gene_id={};transcript_id={}'
            exon_items = [chrom, source, 'exon', start, end, '.', strand, '.',
                          attrs.format(transcript_id, i, transcript_id,
                                       gene_id, transcript_id)]
            fhand.write('\t'.join([str(x) for x in exon_items]) + '\n')


def reverse_complement(seq):
    complement = dict(zip('ACTG', 'TGAC'))
    return ''.join([complement.get(x, x) for x in seq[::-1]])


def get_seq(genome, chrom, start, end, strand):
    seq = genome.fetch(chrom, start, end)
    if strand == '-':
        seq = reverse_complement(seq)
    return(seq.upper())


def get_transcript_seq(transcript_id, parsed_gtf, genome,
                       species, exon_sep=''):
    tdata = parsed_gtf.get(transcript_id, None)
    if tdata is None:
        return(None, None)
    exons = sorted(tdata['exons'])
    chrom, strand = tdata['chrom'], tdata['strand']

    seq = ''
    exon_ids = []
    for start, end in exons:
        exon_ids.append(format_exon_id(chrom, start, end, strand, species))
        seq += get_seq(genome, chrom, start, end, strand='+') + exon_sep
    if strand == '-':
        seq = reverse_complement(seq)
        exon_ids = exon_ids[::-1]
    seq_id = '{} {} {}'.format(species, transcript_id, ' '.join(exon_ids))
    return(seq_id, seq.strip(exon_sep))


def load_pickle(fpath):
    with open(fpath, 'rb') as fhand:
        data = load(fhand)
    return(data)


def write_pickle(data, fpath):
    with open(fpath, 'wb') as fhand:
        dump(data, fhand)
    return(data)


def calc_cum_distrib(x, min_value=None, max_value=None, n=501):
    x = np.array(x)
    x = x[np.isnan(x) == False]
    if min_value is None:
        min_value = np.nanmin(x)
    if max_value is None:
        max_value = np.nanmax(x)

    x_values = np.linspace(min_value, max_value, n)
    pcum = np.array([np.nanmean(x <= v) for v in x_values])
    return(x_values, pcum)


def load_genome(genomes_dir, fname):
    if isinstance(fname, float) or not fname:
        return(None)
    fpath = join(genomes_dir, fname)
    return(FastaFile(fpath.rstrip('.gz')))


def load_genomes(sp_data, genomes_dir, log):
    genomes_fastafiles = {}
    for sp, fname in sp_data['genome'].to_dict().items():
        genomes_fastafiles[sp] = load_genome(genomes_dir, fname)
        log.write('\tLoaded {} genome from {}'.format(sp, fname))
    return(genomes_fastafiles)


def load_annotation(annotation_dir, sp_data, log, only_CDS=False):
    annotation_data = {}
    for sp, fname in sp_data['annotation'].to_dict().items():
        if isinstance(fname, float) or not fname:
            continue
        fpath = join(annotation_dir, fname)
        sep = '=' if fpath.split('.')[-1] == 'gff' else ' '
        with open(fpath) as gff_fhand:
            parsed_gff = parse_gff(
                gff_fhand, sep=sep, sp=sp, only_CDS=only_CDS)
            msg = '\tLoaded {}: {} genes in total'
            log.write(msg.format(fpath, len(parsed_gff)))
        annotation_data[sp] = parsed_gff
    return(annotation_data)


def parse_exons_sj(fpath):
    with open(fpath) as fhand:
        for line in fhand:
            items = line.strip().split(',')
            if items[0] == 'exon_id':
                continue
            exon = items[0]
            inc_sj = items[1].split(';')
            skp_sj = items[2].split(';')
            yield(exon, inc_sj, skp_sj)


def parse_sj(sj_str):
    chrom, coords = sj_str.split(':')
    start, end = coords.split('-')
    return(chrom, int(start), int(end))


def parse_exon(exon_str, strand=True):
    if strand:
        strand = exon_str[-1]
        exon_str = exon_str[:-1]
    else:
        strand = None
    chrom, coords = exon_str.split(':')
    start, end = coords.split('-')
    return(chrom, int(start), int(end), strand)


def get_ncbi_taxonomy_tree(species):
    species_names = [x.replace('_', ' ').capitalize() for x in species]
    ncbi = NCBITaxa()
    name2taxid = ncbi.get_name_translator(species_names)
    taxids = list(chain(*[name2taxid[sp] for sp in species_names]))
    taxid2name = {taxid: sp for sp, taxid in zip(species, taxids)}
    tree = ncbi.get_topology(taxids, intermediate_nodes=False)
    for node in tree:
        node.name = taxid2name[int(node.name)]
    return(tree)


def dendropy2ete(tree):
    str_tree = tree.as_string(schema='newick').replace("'", "")
    return(Tree(str_tree.replace('[&U] ', ''), format=1))


def get_tabix_overlap(chrom, start, end, tabix_index):
    try:
        overlaps = tabix_index.fetch(chrom, start, end)
    except ValueError:
        overlaps = []
    return(overlaps)


def specificity_score(true_y, y):
    return(np.logical_and(true_y == 0, y == 0).sum() / (true_y == 0).sum())


def _extract_log_norm_factors(df):
    i = 1
    fieldname = 'log_norm_factors{}'.format(i)
    fields = []
    while(fieldname in df.columns):
        fields.append(fieldname)
        i += 1
    return(df[fields].mean())


def get_OU_global_params(phylo_hl, log_tau2_over_2a_mean,
                         log_tau2_over_2a_sd, log_beta_mean,
                         log_beta_sd, prior_fpath,
                         mu0_mean=0, mu0_sd=1,
                         log_norm_factors=None):
    '''
       Function to parse simulation parameters from either provided ones or
       a provided prior distribution. It returns a dictionary with all
       required known parameters for fitting element-wise OU models,
       w/o shifts in optimal values

    '''

    if prior_fpath is None:
        if phylo_hl is None or log_tau2_over_2a_mean is None or log_beta_mean is None:
            msg = 'Either prior or all parameters [phylo_hl, tau2_over_2a, '
            msg += 'sigma] must be provided to fit element-wise shifts model'
            raise ValueError(msg)
        else:
            # TODO: remains to clean the parameters once shifts are change
            # to free tau2
            sigma = np.sqrt(log_tau2_over_2a_mean * 2 /
                            phylo_hl * np.log(2) / log_beta_mean)
            return({'phylo_hl': phylo_hl,
                    'tau2_over_2a': np.exp(log_tau2_over_2a_mean),
                    'sigma': sigma, 'log_norm_factors': log_norm_factors,
                    'log_tau2_over_2a_mean': log_tau2_over_2a_mean,
                    'log_tau2_over_2a_sd': log_tau2_over_2a_sd,
                    'log_beta_mean': log_beta_mean,
                    'log_beta_sd': log_beta_sd,
                    'mu0_mean': mu0_mean,
                    'mu0_sd': mu0_sd})
    else:
        df = pd.read_csv(prior_fpath)
        log_norm_factors = _extract_log_norm_factors(df)
        t2a_log = np.log(df['tau2_over_2a'])
        beta = df['tau2_over_2a'] * 2 * \
            np.log(2) / df['phylo_hl'] / df['sigma2']
        log_tau2_over_2a_mean = t2a_log.mean()
        log_tau2_over_2a_sd = t2a_log.std()
        log_beta_mean = np.log(beta).mean()
        log_beta_sd = np.log(beta).std()

        return({'phylo_hl': df['phylo_hl'].mean(),
                'tau2_over_2a': df['tau2_over_2a'].mean(),
                'sigma': np.sqrt(df['sigma2']).mean(),
                'log_norm_factors': log_norm_factors,
                'log_tau2_over_2a_mean': log_tau2_over_2a_mean,
                'log_tau2_over_2a_sd': log_tau2_over_2a_sd,
                'log_beta_mean': log_beta_mean,
                'log_beta_sd': log_beta_sd,
                'mu0_mean': df['mu_mean'].mean(),
                'mu0_sd': df['mu_sd'].mean()})
