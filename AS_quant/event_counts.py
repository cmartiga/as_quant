from os.path import exists
from _collections import defaultdict
from copy import copy

import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.decomposition._pca import PCA

from AS_quant.utils import LogTrack
from AS_quant.settings import ES, IR, AA, AD, MXE, RMATS_TYPES
from AS_quant.bias import calc_exon_n_positions, calc_sj_n_positions
from AS_simulation.utils import logit
from AS_quant.plot_utils import arrange_plot, init_fig, savefig
from AS_quant.genome import Exon


class Counts(object):
    def init_log(self, fhand=None):
        self.log = LogTrack(fhand=fhand)
        self.log.write('Start data analysis...')

    @property
    def n_samples(self):
        return(self.counts[self.ref_field].shape[1])

    @property
    def samples(self):
        return(self.counts[self.ref_field].columns)

    @property
    def ids(self):
        return(self.counts[self.ref_field].index)

    @property
    def n_elements(self):
        return(self.counts[self.ref_field].shape[0])
    
    def set_ids(self, ids):
        for suffix in self.counts.keys():
            self.counts[suffix].index = ids
        
    def set_sample_ids(self, sample_ids):
        for suffix in self.counts.keys():
            self.counts[suffix].columns = sample_ids
    
    def load_elements_data(self, event_data):
        self.counts['elements_data'] = event_data.loc[self.ids, :]

    def select_samples(self, sel_samples):
        for suffix in self.counts.keys():
            v = self.fillna_values.get(suffix, np.nan)
            self.counts[suffix] = self.counts[suffix][sel_samples].fillna(v)
        
        if self.log is not None:
            self.log.write('Selected {} samples'.format(self.n_samples))
    
    def filter_missing_samples(self):
        sel_cols = self.counts[self.ref_field].sum() > 0
        sel_samples = self.counts[self.ref_field].columns[sel_cols]
        self.select_samples(sel_samples)
        if self.log is not None:
            msg = 'Filtered {} samples with > 0 counts'.format(sel_cols.sum())
            self.log.write(msg)
        
    def select_ids(self, sel_ids):
        for suffix in self.counts.keys():
            v = self.fillna_values.get(suffix, np.nan)
            self.counts[suffix] = self.counts[suffix].reindex(sel_ids).fillna(v)
        
        if self.log is not None:
            self.log.write('Selected {} elements'.format(self.n_elements))
    
    def fill_values(self):
        self.select_ids(self.ids)

    def keep_elements(self, sel_ids):
        self.keep_rows = np.array([e_id in sel_ids for e_id in self.ids])
        if self.log is not None:
            msg = 'Maintain always a set of {} elements'
            self.log.write(msg.format(self.keep_rows.sum()))

    def _group_by_samples(self, df, weights=None):
        if self.samples_runs is None:
            return(df)
        data = {}
        for sample, runs in self.samples_runs.items():
            if weights is None:
                w = pd.Series(np.ones(runs.shape), index=runs)
            else:
                w = weights[runs] / weights[runs].sum(1)
            data[sample] = np.sum(df[runs] * w, axis=1)
        return(pd.DataFrame(data))

    def filter_rows(self, sel_rows):
        filtered = {}
        sel_rows = np.logical_or(self.keep_rows, sel_rows)
        for key, value in self.counts.items():
            if len(value.shape) == 1:
                filtered[key] = value.loc[sel_rows]
            else:
                filtered[key] = value.loc[sel_rows, :]
        self.keep_rows = self.keep_rows[sel_rows]
        self.counts = filtered

    def sample(self, sample_size, seed=None):
        random_size = int(sample_size - self.keep_rows.sum())
        sel_rows = self.keep_rows.copy()

        if sample_size >= self.n_elements:
            msg = 'Sample size {} bigger than dataset: no sampling done'
            return
        elif random_size > 0:
            available_elements = np.where(self.keep_rows == False)[0]
            if seed is not None:
                np.random.seed(int(seed))
            sel_elements = np.random.choice(available_elements, size=random_size,
                                          replace=False)
            sel_rows[sel_elements] = True
            msg = 'Randomly sampled up to {} elements'
        else:
            msg = 'Keep all {} elements; sample size is smaller than kept elements'
        
        self.filter_rows(sel_rows)

        if self.log is not None:
            msg = msg.format(self.n_elements)
            self.log.write(msg)
    
    def sample_subsets(self, sample_size, subset_size=2, seed=None):
        if seed is not None:
            np.random.seed(seed)
        
        if self.log is not None:
            msg = 'Randomly sampling {} subsets of {} events'
            self.log.write(msg.format(sample_size, subset_size))
            
        for _ in range(sample_size):
            c = copy(self)
            c.sample(sample_size=2)
            yield(c)

    def filter_counts_samples(self, min_value, label, n_samples=1,
                              max_value=None):
        sel_rows = (self.counts[label] >= min_value).sum(1) >= n_samples
        if max_value is not None:
            sel_rows = np.logical_and(sel_rows,
                                      (self.counts[label] <= max_value).sum(1) >= n_samples)
        self.filter_rows(sel_rows)

        if self.log is not None:
            msg = 'Filtered {} elements with at least '
            msg += '{} {} in at least {} samples'
            self.log.write(msg.format(self.n_elements, label,
                                      min_value, n_samples))
    
    def filter_counts_funct(self, min_value, label, max_value=None,
                            funct=np.mean):
        sel_rows = funct(self.counts[label], axis=1) >= min_value
        if max_value is not None:
            sel_rows2 = funct(self.counts[label], axis=1) <= max_value
            sel_rows = np.logical_and(sel_rows, sel_rows2)
        self.filter_rows(sel_rows)

        if self.log is not None:
            msg = 'Filtered {} elements with {} > {} in average'
            msg = msg.format(self.n_elements, label, min_value)
            if max_value is not None:
                msg = 'Filtered {} elements {} > {} > {} in average'
                msg = msg.format(self.n_elements, max_value, label, min_value)
            self.log.write(msg)

    def write(self, output_prefix):
        if self.log is not None:
            msg = 'Files with {} samples and {} elements written to {}'
            self.log.write(msg.format(self.n_samples, self.n_elements,
                                      output_prefix))
        
        for suffix, m in self.counts.items():
            fpath = '{}.{}.csv'.format(output_prefix, suffix)
            m.to_csv(fpath)
    
    def _merge_counts(self):
        '''
        This method assumes that counts attribute are lists of dataframes
        '''
        for suffix, c in self.counts.items():
            v = self.fillna_values.get(suffix, np.nan)
            self.counts[suffix] = pd.concat(c, axis=1).fillna(v)
        
        if self.log is not None:
            self.log.write('Selected {} samples'.format(self.n_samples))
    
    def merge_species(self, species_counts_dict, orthologs,
                      add_sp_to_sample_ids=False):
        merged_counts = defaultdict(list)
        for species, counts in species_counts_dict.items():
            species_elements = orthologs[species].dropna()
            counts.select_ids(species_elements)
            counts.set_ids(species_elements.index)
            if add_sp_to_sample_ids:
                counts.set_sample_ids(['{}-{}'.format(species, sample)
                                       for sample in counts.samples])
            for suffix in counts.suffixes:
                if suffix in counts.counts:
                    merged_counts[suffix].append(counts.counts[suffix])
        self.counts = merged_counts
        self._merge_counts()
        if self.log is not None:
            msg = 'Merged {} species into {} events with {} samples'
            self.log.write(msg.format(len(merged_counts), self.n_elements,
                                      self.n_samples))
    
    def __getitem__(self, k):
        return(self.counts[k])


class EventsCounts(Counts):
    def __init__(self, prefix=None, samples_runs=None,
                 inclusion_suffix='inclusion', total_suffix='total',
                 skipping_suffix='skipping', keep_ids=None,
                 log_bias_suffix='log_bias'):
        self.samples_runs = samples_runs
        self.prefix = prefix
        self.ref_field = 'total'
        self.suffixes = {'inclusion': inclusion_suffix,
                         'skipping': skipping_suffix,
                         'total': total_suffix,
                         'psi': 'psi',
                         'events': 'events',
                         'log_bias': log_bias_suffix}
        self.fillna_values = {'inclusion': 0,
                              'skipping': 0,
                              'total': 0,
                              'log_bias': np.log(2)}
        self.log = None
        self.rmats_coord_parsers = {ES: self.get_exon_coords_rmats,
                                    IR: self.get_intron_coords_rmats,
                                    AA: self.get_acceptor_coords_rmats,
                                    AD: self.get_donor_coords_rmats,
                                    MXE: self.get_mxe_coords_rmats}

    def get_fpaths(self, prefix=None):
        if prefix is None:
            prefix = self.prefix
        fpaths = {}
        for label, suffix in self.suffixes.items():
            fpath = '{}.{}.csv'.format(prefix, suffix)
            if not exists(fpath):
                if label == 'inclusion':
                    raise ValueError('{} file not found'.format(fpath))
                else:
                    continue
            fpaths[label] = fpath
        if not exists(fpaths['skipping']) and not exists(fpaths['total']):
            msg = 'Either skipping or total counts must be provided'
            raise ValueError(msg)
        return(fpaths)

    def collapse_sample_runs(self):
        counts = self.counts
        weigths = counts['total'].sum(0)
        for label, df in counts.items():
            if label == 'log_bias':
                df = self._group_by_samples(df, weights=weigths)
            elif label != 'events':
                df = self._group_by_samples(df).astype(int)
            counts[label] = df
        self.counts = counts

    @property
    def raw_psi(self):
        if 'psi' not in self.counts:
            self.counts['psi'] = self.counts['inclusion'] / self.counts['total']
        return(self.counts['psi'])

    def load_counts(self, prefix=None):
        counts = {}
        fpaths = self.get_fpaths(prefix=prefix)
        for label, fpath in fpaths.items():
            df = pd.read_csv(fpath)
            index_col = df.columns[0]
            df.drop_duplicates(subset=[index_col], keep=False, inplace=True)
            df.set_index(index_col, inplace=True)
            counts[label] = df

        if 'total' not in counts:
            counts['total'] = counts['inclusion'] + counts['skipping']
        if 'skipping' not in counts:
            counts['skipping'] = counts['total'] - counts['inclusion']
        if 'events' in counts:
            for suffix in ['inclusion', 'total']:
                counts['events'][suffix] = counts[suffix].mean(1)
        
        self.counts = counts
        self.raw_psi
        self.keep_rows = np.full(self.n_elements, False)
        
        if self.log is not None:
            msg = 'Loaded {} samples and {} events'
            self.log.write(msg.format(self.n_samples, self.n_elements))

    def read_log_bias(self, log_bias_dir, bias_col='log_eff_len_ratio'):
        log_bias = {}
        if self.samples_runs is None:
            runs = self.samples
        else:
            runs = np.unique(np.hstack(self.samples_runs.values()))

        for run in runs:
            fpath = '{}/{}.{}.csv'.format(log_bias_dir, run,
                                          self.suffixes['log_bias'])
            log_bias[run] = pd.read_csv(fpath, index_col=0)[bias_col]
        log_bias = pd.DataFrame(log_bias, columns=runs)
        log_bias = log_bias.loc[log_bias.index.duplicated(keep=False) == False, :]
        self.counts['log_bias'] = log_bias.reindex(self.ids)
    
    def rm_duplicates(self):
        rownames = self.counts[self.ref_field].index
        sel_idxs = rownames.duplicated(keep=False) == False
        self.select_ids(rownames[sel_idxs])

    def load_count_matrices(self, inclusion, total, log_bias=None):
        counts = {}
        counts['inclusion'] = self._group_by_samples(inclusion)
        counts['total'] = self._group_by_samples(total)
        counts['skipping'] = counts['total'] - counts['inclusion']
        if log_bias is not None:
            counts['log_bias'] = self._group_by_samples(log_bias,
                                                        weights=counts['total'])
            
        if 'events' in counts:
            for suffix in ['inclusion', 'total']:
                counts['events'][suffix] = counts[suffix].mean(1)
        self.counts = counts
        self.keep_rows = np.full(self.n_elements, False)

        if self.log is not None:
            msg = 'Loaded {} events with {} samples'
            self.log.write(msg.format(self.n_elements, self.n_samples))
        
    def _sum_counts(self, sj_ids, sj_counts, samples):
        if not sj_ids:
            return({sample: 0 for sample in samples})
        counts = {sample: sum([sj_counts[sample].get(sj_id, 0)
                               for sj_id in sj_ids])
                  for sample in samples}
        return(counts)
    
    def _build_df(self, data, event_ids):
        df = pd.DataFrame(data)
        df['event_id'] = event_ids
        df.drop_duplicates(subset=['event_id'], keep=False, inplace=True)
        df.set_index('event_id', inplace=True)
        return(df)
    
    def build_from_sj_counts(self, sj_counts, events_sj_iter):
        sj_counts_dict = sj_counts.dataframe.to_dict()
        samples = sj_counts.dataframe.columns
        
        inclusion = []
        skipping = []
        event_ids = []
        
        for event_sj in events_sj_iter:
            event_ids.append(event_sj['exon_id'])
            inclusion.append(self._sum_counts(event_sj['inclusion_sj'],
                                              sj_counts_dict, samples))
            skipping.append(self._sum_counts(event_sj['skipping_sj'],
                                             sj_counts_dict, samples))
        
        inclusion = self._build_df(inclusion, event_ids)
        skipping = self._build_df(skipping, event_ids)
        total = inclusion + skipping
        self.load_count_matrices(inclusion, total)

    def filter_event_type(self, event_type):
        sel_rows = self.counts['events']['type'] == event_type
        self.filter_rows(sel_rows)
    
    def extract_col_counts(self, counts_str):
        return(pd.DataFrame([[x for x in row.split(',')]
                             for row in counts_str.astype(str)],
                             index=counts_str.index))
    
    def get_exon_coords_rmats(self, record):
        coords = [record['chr'],
                  record['upstreamES'], record['upstreamEE'], 
                  record['exonStart_0base'], record['exonEnd'],
                  record['downstreamES'], record['downstreamEE'], 
                  record['strand']]
        return('{}:{}-{},{}-{},{}-{}{}'.format(*coords))
    
    def get_acceptor_coords_rmats(self, record):
        chrom = record['chr']
        strand = record['strand']
        if strand == '+':
            donor, a2, a1 = (record['flankingEE'], record['shortES'],
                             record['longExonStart_0base'])
        else:
            donor, a1, a2 = (record['flankingES'], record['longExonEnd'],
                             record['shortEE'])
        return('{}:{}-{},{}{}'.format(chrom, donor, a1, a2, strand))
    
    def get_donor_coords_rmats(self, record):
        chrom = record['chr']
        strand = record['strand']
        if strand == '+':
            acceptor, d2, d1, s = (record['flankingES'], record['longExonEnd'],
                                   record['shortEE'], record['shortES'])
            return('{}:{}-{},{}-{}{}'.format(chrom, s, d1, d2, acceptor, strand))
        else:
            acceptor, d2, d1, s = (record['flankingEE'], record['shortES'],
                                   record['longExonStart_0base'],
                                   record['shortEE'])
            return('{}:{}-{},{}-{}{}'.format(chrom, acceptor, d1, d2, s, strand))
        
    
    def get_intron_coords_rmats(self, record):
        return('{}:{}-{}{}'.format(record['chr'], record['upstreamEE'],
                                   record['downstreamES'], record['strand']))
        
    def get_mxe_coords_rmats(self, record):
        e1 = '{}-{}'.format(record['1stExonStart_0base'], record['1stExonEnd'])
        e2 = '{}-{}'.format(record['2ndExonStart_0base'], record['2ndExonEnd'])
        return('{}:{},{}{}'.format(record['chr'], e1, e2,
                                         record['strand']))
    
    def get_coords_ids(self, df, event_type):
        try:
            parse = self.rmats_coord_parsers[event_type]
        except KeyError:
            msg = 'Event type {} is not supported. Try {}'
            raise ValueError(msg.format(event_type, RMATS_TYPES))
        
        coords = [parse(record)
                  for record in df.to_dict(orient='index').values()]
        return(coords)
    
    def parse_rmats(self, fpath, sample_names=None, is_darts=False,
                    event_type='exon_skipping'):
        rmats_data = pd.read_csv(fpath, sep='\t')
        rmats_data.index = self.get_coords_ids(rmats_data, event_type)
        if is_darts:
            inc_prefix, skp_prefix = 'I', 'S'
        else:
            inc_prefix, skp_prefix = 'IJC_SAMPLE_', 'SJC_SAMPLE_'
        psi_prefix = 'IncLevel'
        
        i1 = self.extract_col_counts(rmats_data['{}1'.format(inc_prefix)])
        i2 = self.extract_col_counts(rmats_data['{}2'.format(inc_prefix)])
        s1 = self.extract_col_counts(rmats_data['{}1'.format(skp_prefix)])
        s2 = self.extract_col_counts(rmats_data['{}2'.format(skp_prefix)])
        psi1 = self.extract_col_counts(rmats_data['{}1'.format(psi_prefix)])
        psi2 = self.extract_col_counts(rmats_data['{}2'.format(psi_prefix)]) 
    
        inclusion = pd.concat([i1, i2], axis=1).astype(int)
        skipping = pd.concat([s1, s2], axis=1).astype(int)
        psi = pd.concat([psi1, psi2], axis=1).replace('NA', np.nan).astype(float)
        total = inclusion + skipping
        design = np.array([0] * i1.shape[1] + [1] * i2.shape[1])
    
        if sample_names is None:
            sample_names = ['g1.{}'.format(i) for i in range(i1.shape[1])]
            sample_names.extend(['g2.{}'.format(i) for i in range(i2.shape[1])])
    
        for df in [inclusion, total, psi, skipping]:
            df.columns = sample_names
        design = pd.DataFrame({'Group': design}, index=sample_names)
    
        log_bias = np.log(rmats_data['IncFormLen'] / rmats_data['SkipFormLen'])
        log_bias = pd.DataFrame({sample: log_bias for sample in sample_names},
                                index=log_bias.index)
        self.load_count_matrices(inclusion, total, log_bias)
        self.counts['psi'] = psi
    
    def parse(self, fpath, program='rmats', sample_names=None,
              event_type='exon_skipping'):
        if program == 'rmats':
            self.parse_rmats(fpath, sample_names=sample_names, is_darts=False,
                             event_type=event_type)
        elif program == 'darts':
            self.parse_rmats(fpath, sample_names=sample_names, is_darts=True,
                             event_type=event_type)
        else:
            raise ValueError('Program {} is not supported'.format(program))
    
    def get_exon_length_rmats(self, exon_id):
        coords = [int(x) for x in exon_id.split(',')[1].split('-')]
        return(coords[1] - coords[0])
    
    def get_exon_lengths_rmats(self):
        exon_lengths = np.array([self.get_exon_length_rmats(exon_id)
                                 for exon_id in self.ids])
        return(exon_lengths)
    
    def calc_eff_length_inclusion(self, read_length, exon_lengths, overhang):
        if isinstance(read_length, (list, np.ndarray)):
            m = np.vstack([calc_exon_n_positions(r, overhang, exon_lengths)
                           for r in read_length]).T
        else:
            c = calc_exon_n_positions(read_length, overhang, exon_lengths)
            m = np.vstack([c] * self.n_samples).T
        return(m)
    
    def calc_eff_length_skipping(self, read_length, overhang):
        if isinstance(read_length, (list, np.ndarray)):
            m = np.vstack([np.full(self.n_elements, calc_sj_n_positions(r, overhang))
                           for r in read_length]).T
        else:
            c = np.full(self.n_elements, calc_sj_n_positions(read_length, overhang))
            m = np.vstack([c] * self.n_samples).T
        return(m)

    def calculate_log_bias(self, read_length, exon_lengths, overhang):
        l_inclusion = self.calc_eff_length_inclusion(read_length, exon_lengths, overhang) 
        l_skipping = self.calc_eff_length_skipping(read_length, exon_lengths, overhang)
        return(np.log(l_inclusion / l_skipping))
    
    def estimate_psi(self, l_inclusion, l_skipping):
        i = self.counts['inclusion'] * l_inclusion
        s = self.counts['skipping'] * l_skipping
        self.counts['psi'] = i / (i + s)
        return(self.counts['psi'])
    
    def _get_x_for_dim_red(self, use_logit=False, center=True):
        x = self.counts['psi'].copy()
        if use_logit:
            x = logit(x)
            x.replace([np.inf, -np.inf], np.nan, inplace=True)
        if center:
            m = x.mean(1)
            x = (x.T - m).T
        return(x)
    
    def pca(self, use_logit=False, center=True):
        x = self._get_x_for_dim_red(use_logit=use_logit, center=center)
        pca = PCA()
        pca.fit(x.dropna())
        self.variance_p = pca.explained_variance_ratio_
        self.variance = pca.explained_variance_
        pcs = ['PC{}'.format(i + 1) for i in range(pca.n_components_)]
        self.projection = pd.DataFrame(pca.components_.T, index=self.samples,
                                       columns=pcs)
    
    def plot_variance_p(self, axes, title=None):
        x = np.arange(self.variance_p.shape[0]) + 1
        axes.plot(x, self.variance_p * 100, c='purple')
        axes.scatter(x, self.variance_p * 100, c='purple')
        arrange_plot(axes, xlabel='Component', ylabel='Variance explained (%)',
                     title=title)
        
    def plot_pca(self, axes, x=1, y=2, color=None, add_labels=False,
                 title=None):
        v_x = self.variance_p[x-1] * 100
        v_y = self.variance_p[y-1] * 100
        pc_x, pc_y = 'PC{}'.format(x), 'PC{}'.format(y)
        x = self.projection[pc_x]
        y = self.projection[pc_y]
        
        if color is None:
            axes.scatter(x, y, c='purple')
            showlegend = False
        else:
            sns.scatterplot(x, y, hue=color, ax=axes)
            showlegend = True
        
        if add_labels:
            for x_i, y_i, s_i in zip(x, y, self.samples):
                axes.text(x_i, y_i, s_i)
        
        arrange_plot(axes, xlabel='{} ({:.1f}%)'.format(pc_x, v_x),
                     ylabel='{} ({:.1f}%)'.format(pc_y, v_y),
                     showlegend=showlegend,
                     hline=0, vline=0, title=title)
    
    def figure_pca(self, fpath, x=1, y=2, color=None, add_labels=False):
        fig, subplots = init_fig(1, 2, colsize=3.5)
        self.plot_variance_p(subplots[0])
        self.plot_pca(subplots[1], x=x, y=y, color=color, add_labels=add_labels)
        savefig(fig, fpath)
        
    
    def expand_ids(self):
        old_ids = []
        new_ids = []
        
        for event_id in self.ids:
            exon = Exon(exon_id=event_id)
            for new_exon in exon.get_possible_exons():
                old_ids.append(event_id)
                new_ids.append(new_exon.id)
        return(new_ids, old_ids)
    
    def expand(self):
        '''This method expands the counts by taking every possible combination
           of starts and ends in the event ids.
           
           It is meant to be used to match exon coordinates when obtained
           from CDS only exon orthologs that do not take into account variable
           splice sites 
        '''
        new_ids, old_ids = self.expand_ids()
        self.select_ids(old_ids)
        self.set_ids(new_ids)


class GeneCounts(Counts):
    def __init__(self, prefix=None, samples_runs=None,
                 counts_suffix='counts', logtpms_suffix='logtpms',
                 log_bias_suffix='log_bias', keep_ids=None):
        self.samples_runs = samples_runs
        self.prefix = prefix
        self.ref_field = 'counts'
        self.suffixes = {'counts': counts_suffix,
                         'logtpms': logtpms_suffix,
                         'log_bias': log_bias_suffix}
        self.fillna_values = {'counts': -1}
        self.log = None

    def get_fpaths(self, prefix=None):

        if prefix is None:
            prefix = self.prefix
        fpaths = {}
        for label, suffix in self.suffixes.items():
            fpath = '{}.{}.csv'.format(prefix, suffix)
            if not exists(fpath):
                raise ValueError('{} file not found'.format(fpath))
            fpaths[label] = fpath
        return(fpaths)

    def load_counts(self, prefix=None):
        counts = {}
        fpaths = self.get_fpaths(prefix=prefix)
        for label, fpath in fpaths.items():
            df = pd.read_csv(fpath, index_col=0).fillna(0)
            counts[label] = df
        counts['tpms'] = np.exp(counts['logtpms'])
        self.counts = counts
        self.keep_rows = np.full(self.n_elements, False)
        
        if self.log is not None:
            msg = 'Loaded {} samples and {} genes'
            self.log.write(msg.format(self.n_samples, self.n_elements))
