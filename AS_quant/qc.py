#!/usr/bin/env python

import numpy as np
import pandas as pd
import seaborn as sns

import statsmodels.api as sm
from statsmodels.genmod.families.family import Gaussian

from AS_quant.plot_utils import arrange_plot, init_fig, savefig
from AS_quant.junctions import SpliceJunction
from statsmodels.genmod.families.family import NegativeBinomial


class SpliceJunctionProfiles(object):
    def __init__(self, counts=None):
        if counts is not None:
            self.init(counts)
        self._intron_lengths = None
    
    def calc_m(self, counts):
        m = pd.pivot_table(counts, index='sj_id', columns='position',
                           values='counts')
        self.set_m(m)
    
    def set_m(self, m):
        self.m = m
        self.sj_counts = m.sum(1)
        self.pos_counts = m.sum(0)
    
    def init(self, counts):
        self.counts = counts.drop_duplicates()
        self.calc_m(counts)
    
    def sample(self, n):
        sel_rows = np.random.choice(self.m.index, size=n, replace=False)
        self.set_m(self.m.reindex(sel_rows))
    
    def get_SJ(self):
        for sj_id in self.m.index:
            sj = SpliceJunction()
            sj.parse_id(sj_id)
            yield(sj)
    
    def get_intron_lengths(self):
        if self._intron_lengths is None:
            self._intron_lengths = np.array([sj.intron_length
                                             for sj in self.get_SJ()])
        return(self._intron_lengths)
    
    def read(self, fpath):
        self.init(pd.read_csv(fpath, sep='\t'))
    
    def rm_missing_positions(self):
        sel_cols = self.pos_counts > 0.5 * self.pos_counts.max()
        m = self.m.loc[:, sel_cols]
        self.set_m(m)
        
    def calc_overdispersion(self):
        c = self.m
        m = c.mean(1)
        m2 = m ** 2
        v = np.var(c, axis=1)
        
        self.variances = v
        self.means = m
        
        model = sm.GLM(v-m, m2, family=Gaussian())
        results = model.fit()
        self.inv_phi = results.params[0]
        return(self.inv_phi)

    def infer_sj_coverage(self):
        c = self.m.transpose()
        log_coverage = []
        for col in c.columns:
            y = c[col].dropna().values.astype(int)
            ones = np.ones(y.shape[0])
            if y.sum() == 0:
                log_coverage.append(np.nan)
                continue
            model = sm.GLM(y, ones, family=NegativeBinomial(alpha=self.inv_phi))
            results = model.fit()
            log_coverage.append(results.params[0])
        self.log_coverage = np.array(log_coverage)
        self.coverage = np.exp(self.log_coverage)
        return(self.log_coverage)
    
    def pred_mean_var_relationship(self, mean_pred=None):
        if mean_pred is None:
            mean_pred = np.exp(np.linspace(-10, np.log(self.means.max()), 101))
        v_pred = mean_pred + self.inv_phi * mean_pred ** 2
        return(mean_pred, v_pred)

    def plot_coverage_by_position(self, axes):
        c = self.m
        data = pd.DataFrame({'counts': np.log10(c.sum()+1),
                             'position': np.arange(c.shape[1])})
        axes.plot(data['position'], data['counts'])
        arrange_plot(axes, xlabel='Splice junction position',
                     ylabel=r'$log_{10}$(Counts+1)', xlims=(0, data.shape[0]),
                     hline=0, despine=False)
    
    def plot_overdispersion(self, axes):
        self.calc_overdispersion()
        x, y = np.log(self.means), np.log(self.variances)
        ylims = (0, y.max())
        xlims = (0, x.max())
        axes.scatter(x, y, c='purple', s=5, alpha=0.2)
        axes.plot(ylims, ylims, lw=1, c='grey', label='Poisson')
        
        m_pred, v_pred = self.pred_mean_var_relationship()
        axes.plot(np.log(m_pred), np.log(v_pred), c='darkorange',
                  label='Overdispersed')

        axes.text(0.5, ylims[1] * 0.9,
                  r'$\Phi^{-1}$=' + '{:.2f}'.format(self.inv_phi))        
        arrange_plot(axes, xlims=xlims, ylims=ylims, despine=False,
                     xlabel='log(Mean)', ylabel='log(Variance)',
                     showlegend=True, legend_loc=4, legend_fontsize=9)
    
    def plot_depth_distribution(self, axes):
        c = self.sj_counts
        xlims = (0, np.log(c.max()))
        percs = np.linspace(0, 100, 101)
        qs = np.percentile(c, percs)
        
        sns.distplot(np.log(c+1), bins=30, hist=True, kde=False, 
                     color='purple', ax=axes)
        arrange_plot(axes, xlims=xlims, xlabel='log(Counts+1)',
                     ylabel='# Splice Junction', despine=False)
        
        axes = axes.twinx()
        axes.plot(np.log(qs+1), percs, c='purple', lw=1)
        arrange_plot(axes, xlims=xlims, xlabel='log(Counts+1)',
                     ylabel='Cumulative %', despine=False)
    
    def plot_coverage_vs_intron_size(self, axes):
        d = pd.DataFrame({'log_length': np.log10(self.get_intron_lengths()),
                          'log_cov': np.log(self.sj_counts + 1)}).dropna()
        xlims = (0, d['log_cov'].max())
        ylims = np.percentile(d['log_length'], [0.5, 99.5])
        axes.scatter(d['log_cov'], d['log_length'], s=5, c='purple',
                     alpha=0.005)
        
        arrange_plot(axes, xlims=xlims, ylims=ylims,
                     ylabel=r'$log_{10}$(Intron Length)',
                     xlabel='log(Counts+1)', despine=False)
    
    def plot_intron_size_distrib(self, axes):
        data = pd.DataFrame({'counts': self.sj_counts, 
                             'log_length': np.log10(self.get_intron_lengths())})
        data['interval'] = pd.cut(data['log_length'], bins=50)
        data = data.groupby('interval')['counts'].sum().reset_index()
        data['log_length'] = [(x.left + x.right)/2. for x in data['interval']]
        
        axes.plot(data['log_length'], data['counts'], c='purple', lw=1)
        xlims = (data['log_length'].min(), data['log_length'].max())
        arrange_plot(axes, ylabel=r'Counts', xlims=xlims,
                     xlabel=r'$log_{10}$(Intron Length)', despine=False)
        
    def plot_inferred_coverage(self, axes):
        self.infer_sj_coverage()
        
        sns.distplot(self.log_coverage, bins=30, hist=True, kde=False,
                     color='purple', ax=axes)
        arrange_plot(axes, ylabel=r'# Splice Junctions',
                     xlabel=r'log(Estimated coverage)', despine=False)
    
    def plot_summary(self, fpath):
        fig, subplots = init_fig(3, 2, style='ticks', colsize=3.5)
        
        self.plot_coverage_by_position(subplots[0][0])
        self.plot_depth_distribution(subplots[0][1])
        self.plot_coverage_vs_intron_size(subplots[1][1])
        self.plot_intron_size_distrib(subplots[1][0])
        self.rm_missing_positions()
        self.plot_overdispersion(subplots[2][0])
        self.plot_inferred_coverage(subplots[2][1])
        
        savefig(fig, fpath)
