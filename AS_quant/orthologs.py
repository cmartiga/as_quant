#!/usr/bin/env python
from _collections import defaultdict
from _functools import partial
from itertools import combinations
from os import mkdir, listdir
from os.path import exists, join

import seaborn as sns
import dendropy
import editdistance
import biotite.application.mafft as mafft
import biotite.application.muscle as muscle
import biotite.sequence as seq
import biotite.sequence.align as align
import markov_clustering as mc
import networkx as nx
import numpy as np
import pandas as pd
import sys

from dendropy.interop import raxml
from networkx.algorithms.clique import find_cliques
from scipy.special._basic import comb
from dendropy.calculate.treemeasure import treeness

from AS_quant.entropy import calc_H_from_counts
from AS_quant.settings import RAXML_PATH
from AS_quant.genome import (Gene, parse_fasta_lines, get_annotation_db,
                             get_genome_fastafile, Genome)
from AS_quant.models.phylo import TreeExon
from AS_quant.utils import calc_cum_distrib, dendropy2ete
from AS_quant.plot_utils import init_fig, arrange_plot, savefig


NC_MATRIX = align.SubstitutionMatrix.std_nucleotide_matrix()
PROT_MATRIX = align.SubstitutionMatrix.std_protein_matrix()


def nucleotide_alignment(seq1, seq2, gap_open=-10, gap_extension=-1):
    s1 = seq.NucleotideSequence(seq1)
    s2 = seq.NucleotideSequence(seq2)
    alignments = align.align_optimal(s1, s2, NC_MATRIX,
                                     gap_penalty=(gap_open, gap_extension))
    best_score = max([a.score for a in alignments])
    return(best_score)


def protein_alignment(p1, p2, gap_open=-10, gap_extension=-1):
    alignments = align.align_optimal(p1, p2, PROT_MATRIX,
                                     gap_penalty=(gap_open, gap_extension))
    best_score = max([a.score for a in alignments])
    return(best_score)


def translate_seq(s, frame, strand='+'):
    seq_len = len(s)
    end = int((seq_len - frame) / 3) * 3 + frame
    nc = seq.NucleotideSequence(s[frame:end])
    if strand == '-':
        nc = nc.reverse().complement()
    prot = nc.translate(complete=True)
    return(prot)


def translate_alignment(seq1, seq2, gap_open=-10, gap_extension=-1,
                        frames=(0, 1, 2), strands=['+']):
    scores = []
    for strand in strands:
        for frame1 in frames:
            p1 = translate_seq(seq1, frame1)
            for frame2 in frames:
                p2 = translate_seq(seq2, frame2, strand)
                scores.append(protein_alignment(
                    p1, p2, gap_open, gap_extension))
    return(max(scores))


def editscore(seq1, seq2):
    max_len = max(len(seq1), len(seq2))
    score = 0.
    if max_len != 0:
        d = editdistance.eval(seq1, seq2)
        score = (max_len - d) / max_len
    return(score)


class ExonCompScorer(object):
    def __init__(self, splice_sites_w=0, intronic_bases=0, phase_w=0,
                 score_function='edit', gap_opening=-10, gap_extension=-1):
        self.b = intronic_bases
        if intronic_bases == 0:
            splice_sites_w = 0
        self.splice_sites_w = splice_sites_w
        self.phase_w = phase_w

        if score_function == 'nc':
            score_function = partial(nucleotide_alignment,
                                     gap_open=gap_opening,
                                     gap_extension=gap_extension)
        elif score_function == 'prot':
            score_function = partial(translate_alignment,
                                     gap_open=gap_opening,
                                     gap_extension=gap_extension)
        elif score_function == 'edit':
            score_function = editscore
        else:
            msg = 'Unsupported seq scoring function: {}'.format(score_function)
            raise ValueError(msg)
        self.score_function = score_function

    def score_exon(self, seq1, seq2):
        return(self.score_function(seq1[self.b:-self.b - 1],
                                   seq2[self.b:-self.b - 1]))

    def score_splice_sites(self, seq1, seq2):
        if self.b == 0 or self.splice_sites_w == 0:
            return(0)
        s1 = self.score_function(seq1[:self.b], seq2[:self.b])
        s2 = self.score_function(seq1[-self.b:], seq2[-self.b:])
        return(self.splice_sites_w * (s1 + s2))

    def get_exon_phase(self, seq):
        if self.phase_w == 0:
            return(0)
        return((len(seq) - 2 * self.b) % 3)

    def score_phase(self, seq1, seq2):
        phases = [self.get_exon_phase(seq1), self.get_exon_phase(seq2)]
        return(self.phase_w * (phases[0] == phases[1]))

    def __call__(self, seq1, seq2):
        return(self.score_exon(seq1, seq2) +
               self.score_splice_sites(seq1, seq2) +
               self.score_phase(seq1, seq2))

# TODO: refactor these functions into GeneSet and GeneSetCollection methods
def get_orthologous_exons_table(exon_sets):
    data = []
    for exon_set in exon_sets:
        for gene_set_id, exon_set_id, exons in exon_set:

            record = {'gene_set_id': gene_set_id, 'exon_set_id': exon_set_id}
            for exon_id, exon in exons.items():
                record[exon['sp']] = exon_id
            data.append(record)
    return(pd.DataFrame(data))


def orthologs_to_character_matrix(orthologs, filter_up_down=True):
    species = [x for x in orthologs.columns if x != 'gene_set_id']
    
    m = defaultdict(lambda : '')
    for _, exons in orthologs.groupby('gene_set_id')[species]:
        missing_gene = np.all(exons.isnull(), axis=0).to_dict()
        for sp in species:
            if missing_gene[sp]:
                seq = '?' * exons.shape[0]
            else:
                values = (exons[sp].isnull() == False).astype(int).astype(str)
                if filter_up_down:
                    values = np.array(values)
                    miss_upstream = np.append(np.array(['1']), values[:-1])
                    miss_downstream = np.append(values[1:], np.array(['1']))
                    miss_any = np.logical_or(miss_upstream == '0',
                                             miss_downstream == '0')
                    idx = np.logical_and(values == '0', miss_any)
                    values[idx] = '?'
                seq = ''.join(values)
            m[sp] += seq
    m = dendropy.StandardCharacterMatrix.from_dict(m)
    return(m)


def calc_character_hamming_distance(character_matrix):
    distances = defaultdict(dict)
    species = character_matrix.taxon_namespace
    for taxon1, taxon2 in combinations(species, 2):
        d = np.mean([c1 != c2 for c1, c2 in zip(character_matrix[taxon1],
                                                character_matrix[taxon2])
                     if c1 != '?' and c2 != '?'])
        distances[taxon1][taxon1] = 0
        distances[taxon1][taxon2] = distances[taxon2][taxon1] = d
    dm = dendropy.PhylogeneticDistanceMatrix()
    dm.compile_from_dict(distances=distances, taxon_namespace=species)
    return(dm)


def get_substitution_matrix(intron_score=10, ss_score=10):
    '''
       Custom substitution matrix that weights differently transitions and 
       transversions and includes a character "|" for exon boundaries
       to enforce gene structure alignment
    '''
    alphabet = seq.Alphabet(['A', 'T', 'G', 'C', '|', 'N', '{', '}'])
    m = [[5, -4, -3, -4, -10, 0, -10, -10],
         [-4, 5, -4, -3, -10, 0, -10, -10],
         [-3, -4, 5, -4, -10, 0, -10, -10],
         [-4, -3, -4, 5, -10, 0, -10, -10],
         [-10, -10, -10, -10, intron_score, 0, -10, -10],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [-10, -10, -10, -10, -10, 0, ss_score, -10],
         [-10, -10, -10, -10, -10, 0, -10, ss_score]]
    matrix = align.SubstitutionMatrix(alphabet, alphabet, np.array(m))
    return(matrix)


def run_msa(transcripts_seqs, method='mafft', gap_penalty=(-10, -1),
            intron_score=10, ss_score=10):
    alphabet = seq.Alphabet(['A', 'T', 'C', 'G', '|', 'N', '{', '}'])
    matrix = get_substitution_matrix(intron_score=intron_score, 
                                     ss_score=ss_score)

    # Run MAFFT with the scoring matrix to align gene structures
    transcripts = [seq.GeneralSequence(alphabet, [x for x in s])
                   for s in transcripts_seqs]
    if method == 'mafft':
        alignment = mafft.MafftApp.align(transcripts, matrix=matrix)
    elif method == 'muscle':
        alignment = muscle.MuscleApp.align(transcripts, matrix=matrix,
                                           gap_penalty=gap_penalty)
    else:
        msg = '{} MSA method not supported. Try muscle or mafft'.format(method)
        raise ValueError(msg)
    aligned = []
    for s, trace in zip(transcripts_seqs, alignment.trace.transpose()):
        try:
            s = ''.join([s[i] if i != -1 else '-' for i in trace])
            aligned.append(s)
        except IndexError:
            print('Error ocurred when building the aligned seq', s, trace)
    return(aligned)


class GeneSet(object):
    def __init__(self, gene_set_id=None, genes_dict={}):
        self.genes_dict = genes_dict
        self.id = gene_set_id
        self.species = [x for x in genes_dict.keys()]
    
    def __getitem__(self, species):
        return(self.genes_dict.get(species, None))
    
    def __iter__(self):
        for species, gene in self.genes_dict.items():
            yield(species, gene)
    
    def write_fasta(self, fhand):
        for species, gene in self.genes_dict.items():
            lines = '>{} {} {}\n{}\n'.format(species, gene.id, gene.tx_id,
                                             gene.tx_seq)
            fhand.write(lines)
    
    def read_fasta(self, fhand):
        self.genes_dict = {}
        self.species = []
        for seq_id, seq in parse_fasta_lines(fhand):
            gene = Gene()
            gene.parse_fasta(seq_id, seq)
            self.genes_dict[gene.species] = gene
            self.species.append(gene.species)
    
    def align(self, method='muscle', gap_penalty=(-10, -1), intron_score=50, 
              ss_score=10):
        seqs = [gene.tx_seq for gene in self.genes_dict.values()]
        species = [species for species in self.genes_dict.keys()]
        if len(seqs) > 1: 
            aligned_seqs = run_msa(seqs, method=method, gap_penalty=gap_penalty,
                                   intron_score=intron_score, ss_score=ss_score)
            for sp, seq in zip(species, aligned_seqs):
                self.genes_dict[sp].tx_seq = seq
                self.genes_dict[sp].calc_base_exon_array()
    
    @property
    def exon_pos_matrix(self):
        if not hasattr(self, '_base_exon_matrix'):
            sp_arrays = {sp: gene.base_exon_ids_array for sp, gene in self}
            self._base_exon_matrix = pd.DataFrame(sp_arrays)
        return(self._base_exon_matrix) 
    
    def _add_species_to_exon_id(self, exon_id, species):
        return('{}-{}'.format(species, exon_id))
    
    def _rm_species_from_exon_id(self, exon_id):
        return(exon_id.split('-', 1))
    
    def calc_exon_alignment_scores(self):
        exon_scores = {}
        exon_ids_total = []
        for species_pair in combinations(self.species, 2):
            species_pair = list(species_pair)
            ns = self.exon_pos_matrix.groupby(species_pair)[species_pair[0]].count()
            for exon_ids, n in ns.dropna().to_dict().items():
                exon_id1, exon_id2 = exon_ids
                e1 = self._add_species_to_exon_id(exon_id1, species_pair[0])
                e2 = self._add_species_to_exon_id(exon_id2, species_pair[1])
                exon_scores[(e1, e2)] = n
                exon_scores[(e2, e1)] = n
                exon_ids_total.extend([e1, e2])
        self._exon_ids = np.unique(exon_ids_total)
        self.exon_pairwise_scores = exon_scores
        return(exon_scores)
    
    def calc_exon_pairs_scores(self, funct):
        exon_scores = {}
        exon_ids_total = []
        exon_pairs = set()
        for sp1, sp2 in combinations(self.species, 2):
            for exon1 in self[sp1].exons.values():
                for exon2 in self[sp2].exons.values():
                    e1 = self._add_species_to_exon_id(exon1.id, sp1)
                    e2 = self._add_species_to_exon_id(exon2.id, sp2)
                    if (e1, e2) in exon_pairs:
                        continue
                    s = funct(exon1.seq, exon2.seq)
                    exon_scores[(e1, e2)] = s
                    exon_scores[(e2, e1)] = s
                    exon_ids_total.extend([e1, e2])
                    exon_pairs.add((e1, e2))
                    exon_pairs.add((e2, e1))
        self._exon_ids = np.unique(exon_ids_total)
        self.exon_pairwise_scores = exon_scores
        return(exon_scores)
        
    @property
    def exon_ids(self):
        if not hasattr(self, '_exon_ids'):
            exon_ids = np.array([[x for x in xs] 
                                 for xs in self.exon_sets.values()])
            self._exon_ids = np.unique(exon_ids.flatten())
        return(self._exon_ids)
        
    @property
    def exon_sets(self):
        if not hasattr(self, '_exon_sets'):
            self._exon_sets = {}
            for sp, gene in self:
                self._exon_sets[sp] = [self._add_species_to_exon_id(exon, sp)
                                       for exon in gene.exons.keys()]
        return(self._exon_sets)
    
    def get_closest_exons(self, pairwise_scores, null_value=0):
        closest = defaultdict(tuple)
        for exon_id1 in self.exon_ids:
            def get_score(exon_id):
                return(pairwise_scores.get((exon_id1, exon_id), null_value))
            
            for species_exons in self.exon_sets.values():
                closest[exon_id1] += (max(species_exons, key=get_score),)
        return(closest)
    
    def _build_bhg(self,  closest_exons):
        graph = nx.Graph()
        seen_edges = set()
        for e1 in self.exon_ids:
            for e2 in closest_exons[e1]:
                if e1 == e2:
                    continue
                if e1 in closest_exons[e2]:
                    if (e1, e2) in seen_edges or (e2, e1) in seen_edges:
                        continue
                    seen_edges.add((e1, e2))
                    seen_edges.add((e2, e1))
                    graph.add_edge(e1, e2)
        return(graph)
    
    def get_best_reciprocal_hits_matrix(self, pairwise_scores, null_value=0):
        closest_exons = self.get_closest_exons(pairwise_scores,
                                               null_value=null_value)
        bh_graph = self._build_bhg(closest_exons)
        if len(bh_graph.edges) > 0:
            adjacency_matrix = nx.to_scipy_sparse_matrix(bh_graph, self.exon_ids)
        else:
            adjacency_matrix = None
        return(adjacency_matrix)
    
    def markov_clustering(self, adjacency_matrix, min_size,
                          complete_subgraph=True,
                          expansion=2, inflation=2):
        if adjacency_matrix is None:
            return([])
        nodes_clusters = mc.get_clusters(mc.run_mcl(adjacency_matrix,
                                                    expansion=expansion,
                                                    inflation=inflation))
        matched = []
        for nodes_idxs in nodes_clusters:
            nodes = [self.exon_ids[i] for i in nodes_idxs]
    
            # Filter only fully connected subgraphs within clusters
            if complete_subgraph:
                matrix = adjacency_matrix.todense()[nodes_idxs, :][:, nodes_idxs]
                subgraph = nx.from_numpy_array(matrix)
                cliques = list(find_cliques(subgraph))
                if cliques:
                    clique = max(cliques, key=lambda x: len(x))
                    if len(clique) >= min_size:
                        matched.append([nodes[i] for i in clique])
            else:
                if len(nodes) >= min_size:
                    matched.append(nodes)
        return(matched)
    
    def _get_exon_set(self, exon_ids):
        exon_set = {}
        for exon_id in exon_ids:
            species, exon_id = self._rm_species_from_exon_id(exon_id)
            exon = self[species].exons[exon_id]
            exon_set[species] = exon
        return(exon_set)
    
    def get_exon_orthologs(self, min_size=3, complete_subgraph=True,
                           expansion=2, inflation=2):
        self.exon_orthologs = {}
        if not hasattr(self, 'exon_pairwise_scores'):
            msg = 'Pairwise exon comparison must be done in prior to exon '
            msg += 'orthology call. Use calc_exon_alignment_scores or '
            msg += 'calc_exon_pairs_scores methods to obtain them'
            raise ValueError(msg)
        pairwise_scores = self.exon_pairwise_scores
        adj_matrix = self.get_best_reciprocal_hits_matrix(pairwise_scores)
        matched = self.markov_clustering(adj_matrix, min_size=min_size,
                                         complete_subgraph=complete_subgraph,
                                         expansion=expansion,
                                         inflation=inflation)
        for i, exon_set in enumerate(matched):
            exon_set_id = '{}.e{}'.format(self.id, i)
            self.exon_orthologs[exon_set_id] = self._get_exon_set(exon_set)
    
    def write_exon_orthologs_header(self, fhand, species=None, sep=','):
        if species is None:
            species = self.species
        line = sep.join(['gene_set_id', 'exon_set_id'] + species) + '\n'
        fhand.write(line)
    
    def _get_exon_id(self, species, exon_set):
        exon = exon_set.get(species, None)
        if exon is None:
            return('')
        return(exon.id)
    
    def write_exon_orthologs(self, fhand, species=None, sep=','):
        if species is None:
            species = self.species
        
        for exon_set_id, exon_set in self.exon_orthologs.items():
            exon_ids = [self._get_exon_id(sp, exon_set) for sp in species]
            line = sep.join([str(self.id), str(exon_set_id)] + exon_ids) + '\n'
            fhand.write(line)


class GeneSetCollection(object):
    def __init__(self, fasta_dir=None, gene_set_ids=None, log=None):
        if fasta_dir is None:
            self.gene_sets_iter =  []
        else:
            self.gene_sets_iter = self.read_from_dir(fasta_dir, gene_set_ids)
        self.aligned = False
        self.aligned_iter = []
        self.log = log
        
    def get_fasta_fpaths(self, fasta_dir, gene_set_ids=None):
        if gene_set_ids is None:
            fasta_fpaths = {fname.rstrip('.fasta'): join(fasta_dir, fname)
                            for fname in listdir(fasta_dir)
                            if fname.endswith('.fasta')}
        else:
            fasta_fpaths = {gene_id: join(fasta_dir, '{}.fasta'.format(gene_id))
                            for gene_id in gene_set_ids}
        if self.log is not None:
            msg = 'Identified a total of {} gene sets in {}'
            self.log.write(msg.format(len(fasta_fpaths), fasta_dir))
        return(fasta_fpaths)
    
    def read_from_dir(self, fasta_dir, gene_set_ids):
        fpaths = self.get_fasta_fpaths(fasta_dir, gene_set_ids)
        for gene_set_id, fasta_fpath in fpaths.items():
            gene_set = GeneSet(gene_set_id=gene_set_id)
            if not exists(fasta_fpath):
                continue
            with open(fasta_fpath) as fhand:
                gene_set.read_fasta(fhand)
            if len(gene_set.species) > 1:
                yield(gene_set)
    
    def _align(self, method='muscle', gap_penalty=(-10, -1), intron_score=50,
               ss_score=10):
        for gene_set in self.gene_sets_iter:
            try:
                gene_set.align(method=method, gap_penalty=gap_penalty,
                               intron_score=intron_score, ss_score=ss_score)
            except:
                if self.log is not None:
                    self.log.write('Error aligning {}'.format(gene_set.id))
            yield(gene_set)
    
    def align(self, method='muscle', gap_penalty=(-10, -1), intron_score=50,
              ss_score=10):
        self.aligned = True
        self.aligned_iter = self._align(method=method, gap_penalty=gap_penalty,
                                        intron_score=intron_score,
                                        ss_score=ss_score)
    
    def __iter__(self):
        if self.aligned:
            return(self.aligned_iter)
        else:
            return(self.gene_sets_iter)
    
    def write_fasta(self, fasta_dir):
        if not exists(fasta_dir):
            mkdir(fasta_dir)
        for i, gene_set in enumerate(self):
            fpath = join(fasta_dir, '{}.fasta'.format(gene_set.id))
            with open(fpath, 'w') as fhand:
                gene_set.write_fasta(fhand)
            if self.log is not None and i % 100 == 0:
                self.log.write('\tGene sets aligned: {}'.format(i+1))
    
    def _get_exon_orthologs(self, min_size=3, complete_subgraph=True,
                            expansion=2, inflation=2):
        for gene_set in self:
            gene_set.calc_exon_alignment_scores()
            gene_set.get_exon_orthologs(min_size=min_size,
                                        complete_subgraph=complete_subgraph,
                                        expansion=expansion, inflation=inflation)
            yield(gene_set)
    
    def get_exon_orthologs(self, min_size=3, complete_subgraph=True,
                           expansion=2, inflation=2):
        self.exon_orthologs = self._get_exon_orthologs(min_size=min_size,
                                                       complete_subgraph=complete_subgraph,
                                                       expansion=expansion,
                                                       inflation=inflation)
    def write_exon_orthologs(self, fpath, species):
        first = True
        with open(fpath, 'w') as fhand:
            for i, gene_set in enumerate(self.exon_orthologs):
                if first:
                    gene_set.write_exon_orthologs_header(fhand ,species)
                gene_set.write_exon_orthologs(fhand, species)
                first = False
                if self.log is not None and i % 100 == 0:
                    msg = '\tExon orthologs derived for {} genes'
                    self.log.write(msg.format(i+1))
            

class GenomeSet(object):
    def __init__(self, orthologous_genes=None, log=None, only_cds=False,
                 whole_gene=False):
        self.genomes = {}
        self.set_orthologous_genes(orthologous_genes)
        self.log = log
        self.only_cds = only_cds
        self.whole_gene = whole_gene
    
    @property
    def species(self):
        return([x for x in self.genomes.keys()])
    
    def set_orthologous_genes(self, orthologous_genes):
        self.orthologous_genes = orthologous_genes
    
    def add_genome(self, species, fasta_fpath, db_fpath):
        db = get_annotation_db(db_fpath)
        fastafile = get_genome_fastafile(fasta_fpath)
        self.genomes[species] = Genome(db, fastafile=fastafile,
                                       only_cds=self.only_cds,
                                       whole_gene=self.whole_gene)
    
    def build(self, genome_data, fastafiles_dir, annotation_dbs_dir,
              db_field='db', fastafile_field='fastafile'):
        for species, species_data in genome_data.items():
            db_fpath = join(annotation_dbs_dir, species_data[db_field])
            fasta_fpath = join(fastafiles_dir, species_data[fastafile_field])
            self.add_genome(species, fasta_fpath, db_fpath)

    def get_gene_set(self, gene_set_id, species_gene_ids_dict):
        genes_dict = {species: self.genomes[species].get_gene(gene_id)
                      for species, gene_id in species_gene_ids_dict.items()
                      if isinstance(gene_id, str)}
        genes_dict = {sp: gene for sp, gene in genes_dict.items()
                      if gene is not None}
        return(GeneSet(gene_set_id, genes_dict))

    def gene_orthologs_iter(self, species=None):
        if species is None:
            species = self.genomes.keys()
        else:
            species = np.test_intersect1d(self.genomes.keys(), species)
        if self.orthologous_genes is not None:
            gene_sets = self.orthologous_genes[species].to_dict(orient='index')
            for gene_set_id, species_gene_ids in gene_sets.items():
                gene_set = self.get_gene_set(gene_set_id, species_gene_ids)
                yield(gene_set)
    
    def write_gene_orthologs_seqs(self, outdir, species=None):
        if not exists(outdir):
            mkdir(outdir)
            
        for gene_set in self.gene_orthologs_iter(species=species):
            fpath = join(outdir, '{}.fasta'.format(gene_set.id))
            with open(fpath, 'w') as fhand:
                gene_set.write_fasta(fhand)
    
    def get_exon_orthologs(self, species=None, msa=True,  msa_method='muscle',
                           gap_penalty=(-10, -1), intron_score=50,
                           ss_score=10, splice_sites_w=0, intronic_bases=0,
                           phase_w=0, score_function='edit', min_size=3,
                           complete_subgraph=True, expansion=2, inflation=2):
        if not msa:
            if self.log is not None:
                msg = 'Using {} function to score exon pairs'
                self.log.write(msg.format(score_function))
            score = ExonCompScorer(splice_sites_w=splice_sites_w,
                                   intronic_bases=intronic_bases,
                                   phase_w=phase_w,
                                   score_function=score_function,
                                   gap_opening=gap_penalty[0],
                                   gap_extension=gap_penalty[1])
        elif self.log is not None:
            msg = 'Performing MSA with {} for exon scoring'
            self.log.write(msg.format(msa_method))
        
        for gene_set in self.gene_orthologs_iter(species=species):
            if msa:
                try:
                    gene_set.align(method=msa_method, gap_penalty=gap_penalty,
                                   intron_score=intron_score, ss_score=ss_score)
                except:
                    msg = 'Error aligning gene set: skipped {}'
                    sys.stderr.write(msg.format(gene_set.id))
                    continue
                gene_set.calc_exon_alignment_scores()
            else:
                gene_set.calc_exon_pairs_scores(score)
            gene_set.get_exon_orthologs(min_size=min_size,
                                        complete_subgraph=complete_subgraph,
                                        expansion=expansion,
                                        inflation=inflation)
            yield(gene_set)
    
    def write_exon_orthologs(self, gene_sets, fpath):
        with open(fpath, 'w') as fhand:
            first = True
            for i, gene_set in enumerate(gene_sets):
                if first:
                    gene_set.write_exon_orthologs_header(fhand,
                                                         species=self.species)
                    first = False
                gene_set.write_exon_orthologs(fhand)
                if self.log is not None and i% 10 == 0:
                    msg = '\tGenes processed: {}'.format(i+1)
                    self.log.write(msg)
                    fhand.flush()


class OrthologousExons(object):
    def __init__(self, log=None):
        self.log = log
        
    def load_exons(self, exons_df, species=None):
        if 'exon_set_id' not in exons_df.columns:
            exons_df['exon_set_id'] = exons_df.index
        else:
            exons_df.index = exons_df['exon_set_id']
        self.exons_df = exons_df
        if species is None:
            species = [x for x in exons_df.columns
                       if x != 'gene_set_id' and x != 'exon_set_id']
        self.species = species
        self.n_species = len(species)
        self.n_exons_ref = {}
    
    def read_csv(self, fpath):
        exons_df = pd.read_csv(fpath).set_index('exon_set_id')
        self.load_exons(exons_df)
    
    def __getitem__(self, species):
        return(self.exons_df[species])
    
    @property
    def n_exons(self):
        return(self.exons_df.shape[0])
    
    @property
    def gene_set_ids(self):
        return(self.exons_df['gene_set_id'])
    
    @property
    def exon_set_ids(self):
        return(self.exons_df['exon_set_id'])
    
    def filter_n_exons_per_set(self, min_exons):
        sel_rows = (self.n_species - self[self.species].isnull().sum(axis=1)) >= min_exons
        self.exons_df = self.exons_df.loc[sel_rows, :]
    
    def filter_variable_sets(self):
        sel_rows = self[self.species].isnull().sum(axis=1) < self.n_species
        self.exons_df = self.exons_df.loc[sel_rows, :]
    
    def load_n_exons_ref(self, genome, id_funct=lambda x: x):
        # TODO: ensure ids match or provide gene orthologs as well
        self.n_exons_ref = {id_funct(gene.id): len(gene.exons)
                            for gene in genome.genes}
    
    def calc_n_exons_table(self):
        n_exons = self.calc_n_exons_per_genes()
        data = pd.DataFrame({'n_exon_sets': n_exons}, index=n_exons.index)
        try:
            gene_ids = [x.split('_')[0] for x in data.index]
        except AttributeError:
            gene_ids = data.index
        
        if self.n_exons_ref:
            data['exons_ref'] = [self.n_exons_ref.get(gene_id, np.nan)
                                 for gene_id in gene_ids]
            data['p_ref'] = data['n_exon_sets'] / data['exons_ref']
        self.exon_numbers = data
        return(data)
    
    def calc_sets_sizes(self, df):
        return(self.n_species - df.isnull().sum(1))
    
    def calc_exon_sets_sizes(self):
        return(self.calc_sets_sizes(self.exons_df))
    
    def calc_n_exons_per_genes(self):
        n_exons = self.exons_df.groupby('gene_set_id')['gene_set_id'].count()
        return(n_exons)
    
    def _calc_n_exons_sp(self, sp):
        df = self.exons_df[['gene_set_id', sp]].dropna()
        return(df.groupby('gene_set_id')[sp].count())
    
    def calc_n_exons_per_species(self):
        n_exons = {sp: self._calc_n_exons_sp(sp) for sp in self.species}
        return(pd.DataFrame(n_exons))
    
    def calc_species_character(self, exons_sp, missing_gene,
                               filter_up_down=True):
        if missing_gene:
            seq = '?' * exons_sp.shape[0]
        
        else:
            values = (exons_sp.isnull() == False).astype(int).astype(str)
            
            if filter_up_down:
                values = np.array(values)
                miss_upstream = np.append(np.array(['1']), values[:-1])
                miss_downstream = np.append(values[1:], np.array(['1']))
                miss_any = np.logical_or(miss_upstream == '0',
                                         miss_downstream == '0')
                idx = np.logical_and(values == '0', miss_any)
                values[idx] = '?'
                
            seq = ''.join(values)
        return(seq)
    
    def calc_character_matrix(self, filter_up_down=True):
        m = defaultdict(lambda : '')
        for _, exons in self.exons_df.groupby('gene_set_id')[self.species]:
            for sp in self.species:
                missing_gene = np.all(exons[sp].isnull(), axis=0)
                seq = self.calc_species_character(exons[sp], missing_gene,
                                                  filter_up_down=filter_up_down)
                m[sp] += seq
        m = dendropy.StandardCharacterMatrix.from_dict(m)
        return(m)
    
    @property
    def genes_df(self):
        if not hasattr(self, '_genes_df'):
            genes_dict = {}
            for gene_set_id, exons in self.exons_df.groupby('gene_set_id'):
                genes_dict[gene_set_id] = exons[self.species].isnull().mean(0) < 1
            self._genes_df = pd.DataFrame.from_dict(genes_dict, orient='index')
            self._genes_df.replace(False, np.nan, inplace=True)
        return(self._genes_df)

    def _get_exon_set_ids(self, gene_set_ids):
        exon_ids = []
        prev_gene = None
        n = 0
        for gid in gene_set_ids:
            if prev_gene is None or prev_gene == gid:
                n += 1 
            else:
                n = 0
            exon_id = '{}.e{}'.format(gid, n)
            prev_gene = gid
            exon_ids.append(exon_id)
        return(exon_ids)

    def _sim_exons_df(self, n_genes, exons_per_gene_mean, species):
        genes = ['g{}'.format(i) for i in np.arange(n_genes)]
        n_exons = np.random.poisson(exons_per_gene_mean, size=n_genes)
        gene_ids = np.hstack([[gene_id] * n_exon
                              for gene_id, n_exon in zip(genes, n_exons)])
        exons_df = {'gene_set_id': gene_ids}
        exons_df['exon_set_id'] = self._get_exon_set_ids(exons_df['gene_set_id'])
        exons_df = pd.DataFrame(exons_df).set_index('exon_set_id')
        
        for sp in species:
            strand = np.random.choice(['+', '-'])
            exon_sizes = np.random.poisson(120, size=exons_df.shape[0])
            intron_sizes = np.random.poisson(1000, size=exons_df.shape[0])
            starts = [np.sum(intron_sizes[:i]) + np.sum(exon_sizes[:i-1])
                      for i in range(exons_df.shape[0])]
            ends = starts + exon_sizes
            chrom = 1
            exons = ['{}:{}-{}{}'.format(chrom, start, end, strand)
                     for start, end in zip(starts, ends)]
            if strand == '-':
                exons = exons[::-1]
            exons_df[sp] = exons
        return(exons_df) 

    def simulate(self, tree, n_genes, exonization,stablishment, pseudo_loss,
                 loss, exons_p, exons_per_gene_mean=5, seed=None,
                 missing_gene_p=0):
        if seed is not None:
            np.random.seed(0)
        species = tree.taxon_namespace
        exons_df = self._sim_exons_df(n_genes, exons_per_gene_mean, species)
        exon_tree = TreeExon(tree)
        exon_tree.set_params(exonization=exonization, stablishment=stablishment,
                             pseudo_loss=pseudo_loss, loss=loss,
                             exons_p=exons_p)
        exon_tree.evolve_trait(n=exons_df.shape[0])
        gene_ids = exons_df['gene_set_id']
        exons_df.drop('gene_set_id', inplace=True, axis=1)
        exon_loss = np.array(exon_tree.get_species_data() == 2)
        exons_df[exon_loss] = np.nan
        exons_df['gene_set_id'] = gene_ids
        
        if missing_gene_p > 0:
            genes = gene_ids.unique()
            for sp in species:
                lost_genes = genes[np.random.uniform(size=genes.shape) < missing_gene_p]
                sel_rows = np.array([x in lost_genes
                                     for x in exons_df['gene_set_id']])
                exons_df.loc[sel_rows, sp] = np.nan
        
        self.load_exons(exons_df)
    
    def calc_phylogeny_exons(self, method='nj', filter_up_down=True):
        '''
        Function to estimate phylogeny using exon presence as discrete character
        It may use ML with a substitution model through RAxML, but it is quite slow
    
        It provides faster alternative methods such as those provided directly
        by dendropy: UPGMA and NJ, based on distances.
        '''
        
        if self.log is not None:
            self.log.write('\tCalculating exon phylogeny using {}'.format(method))
        
        m = self.calc_character_matrix(filter_up_down=filter_up_down)
        if method == 'nj':
            dm = calc_character_hamming_distance(m)
            tree = dm.nj_tree()
        elif method == 'upgma':
            dm = calc_character_hamming_distance(m)
            tree = dm.upgma_tree()
        elif method == 'raxml':
            rx = raxml.RaxmlRunner(raxml_path=RAXML_PATH)
            raxml_args = ['-m', 'BINGAMMAI', '-N', '250']
            tree = rx.estimate_tree(m, raxml_args)
        else:
            raise ValueError('Method {} not supported'.format(method))
        self.exon_tree = tree
        return(self.exon_tree)
    
    def calc_contiguity_metrics(self, entropy_estimator='mm'):
        if self.log is not None:
            self.log.write('\tCalculating exon graph contiguity metrics')
        metrics = []
        for gene_set_id, exons_df in self.exons_df.groupby('gene_set_id'):
            if exons_df.shape[0] > 1:
                graph = MetaExonGraph()
                graph.load_exons(exons_df, gene_set_id=gene_set_id)
                metrics.append(graph.calc_metrics(entropy_estimator=entropy_estimator))
        self.contiguity_metrics = pd.DataFrame(metrics).set_index('gene_set_id').replace(-np.inf, np.nan).replace(np.inf, np.nan)
        return(self.contiguity_metrics)
        
    def calc_sets_size_table(self):
        if self.log is not None:
            self.log.write('\tCalculating exon sets sizes')
        exons_sizes = self.calc_exon_sets_sizes()
        gene_sizes = self.calc_sets_sizes(self.genes_df)
        gene_sizes.index = gene_sizes.index.astype(str)
        gene_sizes = gene_sizes.to_dict()
        gene_sizes = [gene_sizes.get(g, None)
                      for g in self.exons_df['gene_set_id'].astype(str)]
    
        sizes = pd.DataFrame({'exons': exons_sizes, 'genes': gene_sizes},
                             index=exons_sizes.index)
        sizes['p'] = sizes['exons'] / sizes['genes']
        self.sets_sizes = sizes
        return(sizes)
    
    def calc_treeness(self, dn_tree):
        try:
            return(treeness(dn_tree))
        except ZeroDivisionError:
            return(np.nan)
    
    def calc_metrics(self, species_tree=None, entropy_estimator='mm',
                     phylogeny_method='nj'):
        self.calc_sets_size_table()
        self.calc_n_exons_table()
        self.calc_contiguity_metrics(entropy_estimator=entropy_estimator)
        self.calc_phylogeny_exons(method=phylogeny_method, filter_up_down=True)
        
        dn_tree = dendropy.Tree.get(data=self.exon_tree.as_string(schema='newick'),
                                    schema='newick')
        p_main_path = self.contiguity_metrics['main_path_counts'].sum() / self.contiguity_metrics['total_counts'].sum()
        
        summary = {'Median number of exons per gene': np.median(self.exon_numbers['n_exon_sets']),
                   'Total number of exon sets': self.n_exons,
                   'Mean orthologs continuity Entropy': self.contiguity_metrics['relative_entropy'].mean(),
                   'Mean edge Entropy': self.contiguity_metrics['edge_entropy'].mean(),
                   'Proportion of edges on main path': p_main_path,
                   'Exon phylogeny treeness': self.calc_treeness(dn_tree),
                   'Median gene set size':  np.median(self.sets_sizes['genes']),
                   'Median exon set size': np.median(self.sets_sizes['exons']),
                   'Median set size ratio': np.median(self.sets_sizes['p'])}
            
        if species_tree is not None:
            rf = dendropy2ete(self.exon_tree).robinson_foulds(species_tree)
            rf, max_rf = rf[:2]
            summary['Species tree RF distance'] = rf / max_rf
        
        if 'exons_ref' in self.exon_numbers.columns:
            n = np.nanmedian(self.exon_numbers['exons_ref'])
            p = np.nanmedian(self.exon_numbers['p_ref'])
            summary['Median number of exons in reference genome'] = n  
            summary['Median number of exons ratio'] = p
        self.global_metrics = summary 
        return(summary)
    
    def write_metrics(self, out_prefix):
        if self.log is not None:
            self.log.write('\tWriting results to {}.*'.format(out_prefix))
        
        fpath = '{}.exons.csv'.format(out_prefix)
        self.sets_sizes.to_csv(fpath)
    
        fpath = '{}.genes.csv'.format(out_prefix)
        df = pd.concat([self.exon_numbers, self.contiguity_metrics], axis=1)
        df.to_csv(fpath)
        
        fpath = '{}.global.csv'.format(out_prefix)
        with open(fpath, 'w') as fhand:
            for k, v in self.global_metrics.items():
                fhand.write('{}: {}\n'.format(k, v))
        
        fpath = '{}.tree.nh'.format(out_prefix)
        self.exon_tree.write(path=fpath, schema='newick')
    
    def plot_sets_sizes(self, axes):
        sets_sizes = self.sets_sizes
        max_value = sets_sizes['genes'].max()
        exons_sizes, ps = calc_cum_distrib(sets_sizes['exons'], min_value=0,
                                           max_value=max_value)
        axes.plot(exons_sizes, ps, label='Exons', color='darkorange')
        genes_sizes, ps = calc_cum_distrib(sets_sizes['genes'], min_value=0,
                                           max_value=max_value)
        axes.plot(genes_sizes, ps, label='Genes', color='darkred')
        arrange_plot(axes, xlabel='# elements', ylabel='P(set size < x)',
                     showlegend=True, legend_loc=2, cols_legend=1,
                     xlims=(0, max_value), ylims=(0, 1),
                     legend_frame=True, despine=True, legend_fontsize=9)
    
    def plot_set_size_ratio(self, axes):
        exons_sizes, ps = calc_cum_distrib(self.sets_sizes['p'])
        axes.plot(exons_sizes, ps, c='black')
        arrange_plot(axes, xlabel='Set size ratio (exon/gene)',
                     ylabel='P(set size < x)', despine=True, xlims=(0, 1),
                     ylims=(0, 1))
    
    def plot_exon_number(self, axes):
        n_exons = self.exon_numbers
        max_value = n_exons['n_exon_sets'].max()
        n_orthologs, ps = calc_cum_distrib(n_exons['n_exon_sets'], min_value=0,
                                           max_value=max_value)
        axes.plot(n_orthologs, ps, label='Orthologs', color='cyan')
    
        if 'exons_ref' in n_exons.columns:
            n_ref, ps = calc_cum_distrib(n_exons['exons_ref'], min_value=0,
                                         max_value=max_value)
            axes.plot(n_ref, ps, label='Reference', color='purple')
            
        arrange_plot(axes, xlabel='# exons per gene', ylabel='P(exon number < x)',
                     showlegend=True, legend_loc=4, cols_legend=1,
                     xlims=(0, max_value), ylims=(0, 1),
                     legend_frame=True, despine=True, legend_fontsize=9)
    
    def plot_n_exons_by_species(self, axes):
        n_exons_sp = self.n_exons_sp
        max_value = self.exon_numbers['n_exon_sets'].max()
        for sp in self.species:
            n_orthologs, ps = calc_cum_distrib(n_exons_sp[sp], min_value=0,
                                               max_value=max_value)
            axes.plot(n_orthologs, ps)
    
        arrange_plot(axes, xlabel='# exons per gene', ylabel='P(exon number < x)',
                     showlegend=True, legend_loc=4, cols_legend=1,
                     xlims=(0, max_value), ylims=(0, 1),
                     legend_frame=True, despine=True, legend_fontsize=9)
    
    def plot_n_exons_by_species_to_ref(self, axes):
        n_exons_sp = self.n_exons_sp
        for sp in self.species:
            values = n_exons_sp[sp] / n_exons_sp['exons_ref']
            n_orthologs, ps = calc_cum_distrib(values, min_value=0, max_value=1)
            axes.plot(n_orthologs, ps)
    
        arrange_plot(axes, xlabel='Exon number ratio (orthologs/reference)',
                     ylabel='P(exon number ratio < x)', despine=True,
                     xlims=(0, 1), ylims=(0, 1))
    
    def plot_set_sizes_to_ref(self, axes):
        n_exons_ratio, ps = calc_cum_distrib(self.exon_numbers['p_ref'])
        axes.plot(n_exons_ratio, ps, c='black')
        arrange_plot(axes, xlabel='Exon number ratio (orthologs/reference)',
                     ylabel='P(exon number ratio < x)', despine=True, xlims=(0, 1),
                     ylims=(0, 1))
    
    def plot_relative_entropy_dist(self, axes):
        contiguity = self.contiguity_metrics
        sns.distplot(contiguity['relative_entropy'].dropna(), bins=30,
                     hist=True, kde=False, ax=axes)
        arrange_plot(axes, xlabel='Relative exon graph entropy',
                     ylabel='Number of gene sets', despine=True, xlims=(0, 1))
    
    def plot_metrics(self, fpath):
        if self.log is not None:
            self.log.write('\tPlotting results to {}'.format(fpath))
        
        fig, subplots = init_fig(2, 2, colsize=3, rowsize=2.3)
        self.plot_sets_sizes(subplots[0][0])
        self.plot_set_size_ratio(subplots[0][1])
        self.plot_exon_number(subplots[1][0])
        self.plot_relative_entropy_dist(subplots[1][1])
        savefig(fig, fpath)
    

class MetaExonGraph(object):
    def load_exons(self, exons_df, directed=True, species=None, gene_set_id=None):
        self.exons_df = exons_df
        self.gene_set_id = gene_set_id
        self.n_exons = exons_df.shape[0]
        self.build(exons_df, species=species, directed=directed)

    def _get_exon_start(self, exon_id):
        try:
            return(int(exon_id.split(':')[1].split('-')[0].split('$')[0]))
        except IndexError:
            sys.stderr.write('Invalid exon_id found: {}\n'.format(exon_id))
            return(None)
    
    def sort_exons(self, exons):
        if not exons:
            return([])
        strands = np.unique([x[-1] for x in exons])
        if strands.shape[0] > 1:
            msg = 'More than one strand value found at exons: {}'.format(exons)
            raise ValueError(msg)
        strand = strands[0]
        sorted_exons = sorted(exons, key=self._get_exon_start,
                              reverse=strand == '-')
        return(sorted_exons)

    def calc_sets_sizes(self):
        return(self.exon_orthologs_df.shape[1] - self.exon_orthologs_df.isnull().sum(1))

    def build(self, exons_df, species=None, directed=True):
        graph = defaultdict(lambda: defaultdict(lambda: 0))
        self.n_exons = exons_df.shape[0]
        
        if self.n_exons <= 1:
            return(np.nan)
        
        if species is None:
            species = [x for x in exons_df.columns
                       if x != 'gene_set_id' and x != 'exon_set_id']
        self.species = species
        
        # Iterate over all species
        nodes = []
        for sp in species:
            sp_exons = {v: k for k, v in exons_df[sp].dropna().to_dict().items()}
            sorted_exons = [exon for exon in self.sort_exons(sp_exons.keys())]
            sorted_exons = [sp_exons[exon] for exon in sorted_exons]
            for e1, e2 in zip(sorted_exons, sorted_exons[1:]):
                graph[e1][e2] += 1
                nodes.extend([e1, e2])
                if not directed:
                    graph[e2][e1] += 1
        self.nodes = np.unique(nodes)
        self.graph = graph
    
    def nodes_children(self):
        return(self.graph.items())
    
    def children(self):
        return(self.graph.values())
    
    def calc_counts_on_main_path(self):
        total = 0
        n_main = 0 
        for children in self.children():
            targets = [x for x in children.values()]
            n_main += np.max(targets)
            total += np.sum(targets)
        return(n_main, total)
    
    def calc_graph_edge_entropy(self, estimator='mm'):
        entropies = []
        for children in self.children():
            targets = np.array([x for x in children.values()])
            entropies.append(calc_H_from_counts(targets, estimator=estimator))
        return(np.mean(entropies))
    
    def calc_min_entropy(self):
        min_entropy_p = np.full(self.n_exons - 1, 1. / (self.n_exons - 1))
        min_entropy = - np.sum(min_entropy_p * np.log2(min_entropy_p))
        return(min_entropy)
    
    def calc_max_entropy(self):
        n_comb = comb(self.n_exons, 2, exact=True)        
        max_entropy_p = np.full(n_comb, 1. / n_comb)
        max_entropy = - np.sum(max_entropy_p * np.log2(max_entropy_p))
        return(max_entropy)
    
    def calc_entropy(self, estimator='mm'):
        if self.n_exons <= 1:
            return(np.nan)
        paths_df = pd.DataFrame(self.graph, index=self.nodes,
                                columns=self.nodes).fillna(0).values
        values = np.array([paths_df[i, j] + paths_df[j, i]
                           for i, j in combinations(range(paths_df.shape[0]), 2)])
        entropy = calc_H_from_counts(values[values != 0], estimator=estimator)
        return(entropy)
    
    def calc_gene_contiguity_relative_H(self, estimator='mm'):
        if self.n_exons <= 1:
            return(np.nan)
        min_entropy = self.calc_min_entropy()
        max_entropy = self.calc_max_entropy()
        entropy = self.calc_entropy(estimator=estimator)
        relative_entropy = (entropy - min_entropy) / (max_entropy - min_entropy)
        return(relative_entropy)
    
    def calc_metrics(self, entropy_estimator='mm'):
        c = self.calc_gene_contiguity_relative_H(estimator=entropy_estimator)
        k, n = self.calc_counts_on_main_path()
        edge_h = self.calc_graph_edge_entropy()
        metrics = {'gene_set_id': self.gene_set_id, 'relative_entropy': c,
                   'main_path_counts': k, 'total_counts': n, 
                   'edge_entropy': edge_h}
        return(metrics)


def calc_n_exons_per_gene_gtf(gtf):
    n_exons = {}
    for gene_id, gene_data in gtf.items():
        transcripts = gene_data.get('transcripts', {})
        if not transcripts:
            continue
        longest_transcript = max(transcripts,
                                 key=lambda x: len(transcripts[x]))
        n_exons[gene_id] = len(transcripts[longest_transcript])
    return(n_exons)


def calc_n_exons_per_species(exon_orthologs, species, n_exons_global=None):
    n_exons = {sp: exon_orthologs[['gene_set_id', sp]].dropna().groupby('gene_set_id')[sp].count()
               for sp in species}
    n_exons = pd.DataFrame(n_exons)
    if n_exons_global is not None and 'exons_ref' in n_exons_global.columns:
        n_exons['exons_ref'] = n_exons_global.loc[n_exons.index, 'exons_ref']
    return(n_exons)
