import unittest
from tempfile import NamedTemporaryFile

import numpy as np
import pandas as pd

from AS_quant.models.genetics import (MutAccLineContinuousTrait, 
                                      simulate_design_MA,
    PopulationNeutralTrait)


def calc_rel_error(real, inferred):
    return(np.mean(np.abs(inferred-real)/real))


def calc_abs_error(real, inferred):
    return(np.mean(np.abs(inferred-real)))


class MATests(unittest.TestCase):    
    def test_simulate(self):
        design = simulate_design_MA(n_pops=5, n_ind_pop=3, Ne=[5])
        n_generations = 400
        sigma2_m = 1e-4
        
        m = MutAccLineContinuousTrait(design, n_generations=n_generations,
                                      obs_model='logit_binomial',
                                      recompile=False)
        m.simulate(n_elements=5, mu0_mean=0, mu0_sd=1, sigma2_m=sigma2_m,
                   sigma2_res=0.1, seed=0, mean_coverage=100)
        
        with NamedTemporaryFile('w') as fhand:
            m.write_simulations(fhand.name)
    
    def test_fit(self):
        design = simulate_design_MA(n_pops=5, n_ind_pop=3, Ne=[5])
        n_generations = 400
        sigma2_m = 1e-4
        sigma2_res = 0.1
        
        m = MutAccLineContinuousTrait(design, n_generations=n_generations,
                                      obs_model='logit_binomial',
                                      recompile=False)
        m.simulate(n_elements=5, mu0_mean=0, mu0_sd=1, sigma2_m=sigma2_m,
                   sigma2_res=sigma2_res, seed=0, mean_coverage=100)
        m.fit()
            
        assert(calc_rel_error(sigma2_m, m.posterior['sigma2_m'].mean()) < 1)
        assert(calc_rel_error(sigma2_res, m.posterior['sigma2_res'].mean()) < 1)
    
    def test_fit_variable_Ne(self):
        design = simulate_design_MA(n_pops=5, n_ind_pop=3, Ne=[5, 50])
        n_generations = 400
        sigma2_m = 1e-4
        sigma2_res = 0.1
        
        m = MutAccLineContinuousTrait(design, n_generations=n_generations,
                                      obs_model='logit_binomial',
                                      recompile=False)
        m.simulate(n_elements=5, mu0_mean=0, mu0_sd=1, sigma2_m=sigma2_m,
                   sigma2_res=sigma2_res, seed=0, mean_coverage=100)
        m.fit()
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(fhand.name)
            
        assert(calc_rel_error(sigma2_m, m.posterior['sigma2_m'].mean()) < 1)
        assert(calc_rel_error(sigma2_res, m.posterior['sigma2_res'].mean()) < 1)
    
    def test_fit_by_element(self):
        design = simulate_design_MA(n_pops=5, n_ind_pop=3, Ne=[1, 5, 50])
        n_generations = 400
        sigma2_m = 2e-4
        sigma2_res = 0.1
        
        m = MutAccLineContinuousTrait(design, n_generations=n_generations,
                                      recompile=False, by_element=True)
        m.simulate(n_elements=2, mu0_mean=0, mu0_sd=1, sigma2_m=sigma2_m,
                   sigma2_res=sigma2_res, seed=0, mean_coverage=100)
        m.fit()
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(fhand.name)
            result = pd.read_csv('{}.fit.csv'.format(fhand.name), index_col=0)
            assert(calc_rel_error(sigma2_m, result['sigma2_m']) < 1)
            assert(calc_rel_error(sigma2_res, result['sigma2_res']) < 1)
            assert('dX.pop0' in result.columns)
            assert('dpsi.pop0' in result.columns)
    
    def test_fit_mv_model(self):
        design = simulate_design_MA(n_pops=5, n_ind_pop=5, Ne=[10, 50])
        n_generations = 400
        sigma2_m = 1e-3
        sigma2_res = 0.1
        rho = 0.5
        
        m = MutAccLineContinuousTrait(design, n_generations=n_generations,
                                      obs_model='logit_binomial',
                                      recompile=False, mv=True)
        Mcorr = np.array([[1, rho],
                          [rho, 1]])
        m.simulate(n_elements=2, mu0_mean=0, mu0_sd=1, sigma2_m=sigma2_m,
                   sigma2_res=sigma2_res, Mcorr=Mcorr, seed=0,
                   mean_coverage=100)
        m.fit()

        assert(calc_rel_error(sigma2_m, m.posterior['sigma2_m'].mean()) < 2)
        assert(calc_rel_error(sigma2_res, m.posterior['sigma2_res'].mean()) < 2)
        assert(calc_abs_error(rho, m.posterior['Mcorr12'].mean()) < 0.25)
        assert(calc_abs_error(rho, m.posterior['Rcorr12'].mean()) < 0.25)
        
    def test_population_neutral_trait_simulate(self):
        m = PopulationNeutralTrait(n_samples=10)
        m.simulate(n_elements=20, Ne=100, mu0_mean=0, mu0_sd=1,
                   sigma2_m=1e-4, sigma2_res=0.1, seed=0)
        with NamedTemporaryFile('w') as fhand:
            m.write_simulations(fhand.name)
            
    def test_population_neutral_trait_fit(self):
        m = PopulationNeutralTrait(n_samples=4, recompile=False)
        m.simulate(n_elements=20, Ne=1000, mu0_mean=0, mu0_sd=1,
                   sigma2_m=1e-4, sigma2_res=0.1, seed=0)
        m.fit(chains=4, n_iter=1000)
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(fhand.name)

if __name__ == '__main__':
    import sys
    sys.argv = ['', 'MATests']
    unittest.main()
