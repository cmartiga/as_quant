import unittest
from tempfile import NamedTemporaryFile

import pandas as pd
import numpy as np

from AS_quant.models.regulation import AdditiveRegModel, ManifoldModel


class RegTests(unittest.TestCase):
    def test_add_reg_model(self):
        design = pd.DataFrame({'v2': [0, 0, 0, 1, 1, 1]},
                              index=['s{}'.format(i) for i in range(6)])
        features = pd.DataFrame({'reg1': [0] * 15 + [1] * 5,
                                 'reg2': [0] * 10 + [1] * 5 + [0] * 5},
                              index=['e{}'.format(i) for i in range(20)])
        theta = [0.5, 1]
        m = AdditiveRegModel(design, features, recompile=False)
        m.simulate(alpha_mean=0, alpha_sd=1, beta_sd=0.2, theta_sd=0.5,
                   sigma=0.2, theta=theta, seed=1)
        m.fit()
        
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(prefix=fhand.name)
            fpath = '{}.theta.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (4000, 2))
            assert(np.all(results.columns == ['reg1', 'reg2']))
            
            fpath = '{}.regulation.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(np.all(results['theta_2.5'] < theta) and 
                   np.all(results['theta_97.5'] > theta))
            assert(results.shape == (2, 3))
            assert(np.all(results.index == ['reg1', 'reg2']))
            
            fpath = '{}.events.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (20, 12))
            
            fpath = '{}.global.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (5, 3))
        
    def test_manifold_model(self):
        design = pd.DataFrame({'v2': [0, 0, 0, 1, 1, 1]},
                              index=['s{}'.format(i) for i in range(6)])
        m = ManifoldModel(design, recompile=False)
        m.simulate(n_elements=20, alpha_mean=0, alpha_sd=1, sigma=0.2,
                   beta=[1], seed=1)
        m.fit()
        summary = m.get_summary()
        assert(summary['beta_2.5'][0] < 1 and summary['beta_97.5'][0] > 1)
            
        

if __name__ == '__main__':
    import sys
    sys.argv = ['', 'RegTests']
    unittest.main()
