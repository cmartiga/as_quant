#!/usr/bin/env python

import unittest
import numpy as np
import pandas as pd
from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
from os.path import join
from subprocess import check_call
from AS_quant.junctions import SpliceJunction, SpliceJunctionCounts
from AS_quant.genome import get_sj_index
from AS_quant.event_counts import EventsCounts
from tempfile import NamedTemporaryFile


class EventsCountsTests(unittest.TestCase):
    def test_exon_counts_build(self):
        sj_counts = SpliceJunctionCounts()
        sj_counts.from_dataframe(pd.DataFrame([[5, 6], [10, 12], [10, 4]],
                                              index=['sj1', 'sj2', 'sj3'],
                                              columns=['s1', 's2']))
        
        events_sj_iter = [{'exon_id': 'e1',
                           'inclusion_sj': ['sj1', 'sj2'],
                           'skipping_sj': ['sj3']},
                          {'exon_id': 'e2',
                           'inclusion_sj': ['sj1', 'sj2'],
                           'skipping_sj': ['sj3']}]
        exon_counts = EventsCounts()
        exon_counts.build_from_sj_counts(sj_counts, events_sj_iter)
        assert(np.allclose(exon_counts.counts['inclusion'], [[15, 18], [15, 18]]))
        assert(np.allclose(exon_counts.counts['skipping'], [[10, 4], [10, 4]]))
    
    def test_merge_exon_counts_species(self):
        orthologs = pd.DataFrame({'species_a': ['exon1a', 'exon2a', np.nan],
                                  'species_b': ['exon1b', 'exon2b', 'exon3b']},
                                  index=['exon1', 'exon2', 'exon3'])
        inclusion_a = pd.DataFrame({'s1a': [18, 15],
                                    's2a': [17, 12]},
                                    index=['exon1a', 'exon2a'])
        log_bias_a = pd.DataFrame({'s1a': [0.54, 0.68],
                                   's2a': [0.54, 0.68]},
                                   index=['exon1a', 'exon2a'])
        inclusion_b = pd.DataFrame({'s1b': [19, 20, 25],
                                    's2b': [23, 15, 10]},
                                    index=['exon1b', 'exon2b', 'exon3b'])
        log_bias_b = pd.DataFrame({'s1b': [0.68, 0.78, 0.85],
                                   's2b': [0.68, 0.78, 0.85]},
                                   index=['exon1b', 'exon2b', 'exon3b'])
        
        a_counts = EventsCounts()
        a_counts.load_count_matrices(inclusion_a, inclusion_a, log_bias_a)
        b_counts = EventsCounts()
        b_counts.load_count_matrices(inclusion_b, inclusion_b, log_bias_b)
        species_counts_dict = {'species_a': a_counts, 'species_b': b_counts}
        
        merged_counts = EventsCounts()
        merged_counts.merge_species(species_counts_dict, orthologs)
        expected_counts = [[18, 17, 19, 23], [15, 12, 20, 15], [0, 0, 25, 10]]
        expected_bias = [[0.54, 0.54, 0.68, 0.68], [0.68, 0.68, 0.78, 0.78],
                         [np.log(2), np.log(2), 0.85, 0.85]]
        assert(np.allclose(merged_counts.counts['inclusion'], expected_counts))
        assert(np.allclose(merged_counts.counts['inclusion'], expected_counts))
        assert(np.allclose(merged_counts.counts['skipping'], 0))
        assert(np.allclose(merged_counts.counts['log_bias'], expected_bias))
    
    def test_expand_Exons(self):
        exon_ids = ['chr1:10$13-20+', 'chr2:10$13-20$23+']
        counts = pd.DataFrame({'s1a': [18, 15], 's2a': [17, 12]},
                              index=exon_ids)
        log_bias = pd.DataFrame({'s1a': [0.54, 0.68],
                                 's2a': [0.54, 0.68]},
                                index=exon_ids)
        events = EventsCounts()
        events.load_count_matrices(counts, counts, log_bias)
        events.expand()
        
        expanded_ids = ['chr1:10-20+', 'chr1:13-20+', 'chr2:10-20+',
                        'chr2:10-23+', 'chr2:13-20+', 'chr2:13-23+']
        expanded_counts = np.array([[18, 17], [18, 17], [15, 12],
                                    [15, 12], [15, 12], [15, 12]])
        assert(np.all(events.ids == expanded_ids))
        assert(np.all(events.counts['inclusion'] == expanded_counts))
    
    def test_parse_exon_rmats_coords(self):
        # Positive strand
        record = {'chr': 'chrIV', 'strand': '+',
                  'exonStart_0base': 78775, 'exonEnd': 78969,
                  'upstreamES': 77602, 'upstreamEE': 77618,
                  'downstreamES': 80148, 'downstreamEE': 80499}
        donors = EventsCounts()
        event_id = donors.get_exon_coords_rmats(record)
        assert(event_id == 'chrIV:77602-77618,78775-78969,80148-80499+')
        
        # Negative strand
        record = {'chr': 'chrIV', 'strand': '-',
                  'exonStart_0base': 10474, 'exonEnd': 10549,
                  'upstreamES': 9225, 'upstreamEE': 9409,
                  'downstreamES': 10634, 'downstreamEE': 10870}
        event_id = donors.get_exon_coords_rmats(record)
        assert(event_id == 'chrIV:9225-9409,10474-10549,10634-10870-')
    
    def test_parse_donor_rmats_coords(self):
        # Positive strand
        record = {'chr': 'chr22', 'strand': '+',
                  'longExonStart_0base': 7938, 'longExonEnd': 7993,
                  'shortES': 7938, 'shortEE': 7989,
                  'flankingES': 9325, 'flankingEE': 9434}
        donors = EventsCounts()
        event_id = donors.get_donor_coords_rmats(record)
        assert(event_id == 'chr22:7938-7989,7993-9325+')
        
        # Negative strand
        record = {'chr': 'chr22', 'strand': '-', 
                  'longExonStart_0base': 7076, 'longExonEnd': 7329,
                  'shortES': 7162, 'shortEE': 7329,
                  'flankingES': 883, 'flankingEE': 1106}
        event_id = donors.get_donor_coords_rmats(record)
        assert(event_id == 'chr22:1106-7076,7162-7329-')
    
    def test_parse_exon_counts(self):
        fpath = join(TEST_DATA_DIR, 'SE.MATS.JC.txt')
        counts = EventsCounts()
        sample_names = ['s{}'.format(i) for i in range(54)]
        counts.parse(fpath, program='rmats', sample_names=sample_names,
                     event_type='exon_skipping')
        assert('psi' in counts.counts)
        with NamedTemporaryFile('w') as fhand:
            counts.write(fhand.name)
        
    def test_parse_intron_counts(self):
        fpath = join(TEST_DATA_DIR, 'RI.MATS.JC.txt')
        counts = EventsCounts()
        sample_names = ['s{}'.format(i) for i in range(54)]
        counts.parse(fpath, program='rmats', sample_names=sample_names,
                     event_type='intron_retention')
        assert('psi' in counts.counts)
        with NamedTemporaryFile('w') as fhand:
            counts.write(fhand.name)
    
    def test_parse_donor_counts(self):
        fpath = join(TEST_DATA_DIR, 'A5SS.MATS.JC.txt')
        counts = EventsCounts()
        counts.parse(fpath, program='rmats', 
                     event_type='alternative_donor')
        assert('psi' in counts.counts)
        with NamedTemporaryFile('w') as fhand:
            counts.write(fhand.name)
    
    def test_sample_counts(self):
        fpath = join(TEST_DATA_DIR, 'SE.MATS.JC.txt')
        counts = EventsCounts()
        sample_names = ['s{}'.format(i) for i in range(54)]
        counts.parse(fpath, program='rmats', sample_names=sample_names,
                     event_type='exon_skipping')
        
        n = counts.n_elements
        counts.sample(sample_size=n)
        assert(counts.n_elements == n)
        
        n = 100
        counts.sample(sample_size=n)
        assert(counts.n_elements == n)
        
    def test_filter_counts(self):
        fpath = join(TEST_DATA_DIR, 'SE.MATS.JC.txt')
        counts = EventsCounts()
        sample_names = ['s{}'.format(i) for i in range(54)]
        counts.parse(fpath, program='rmats', sample_names=sample_names,
                     event_type='exon_skipping')
        
        counts.filter_counts_funct(min_value=20, label='total')
        assert(counts.n_elements == 907)
        
        counts.filter_counts_funct(min_value=2, label='skipping')
        assert(counts.n_elements == 747)
    
    def test_sj_to_exon_counts_bin(self):
        sj_counts_fpath = join(TEST_DATA_DIR, 'sj_counts_for_exons.csv')
        exons_sj_fpath = join(TEST_DATA_DIR, 'exons_sj_for_exons.csv')
        bin_fpath = join(BIN_DIR, 'sj_to_exon_counts.py')
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, '-s', sj_counts_fpath, 
                   '-e', exons_sj_fpath, '-o', fhand.name]
            check_call(cmd)
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, '-s', sj_counts_fpath, 
                   '-e', exons_sj_fpath, '-o', fhand.name,
                   '--log_bias_dir', TEST_DATA_DIR]
            check_call(cmd)
    
        # With duplicated exon ids
        sj_counts_fpath = join(TEST_DATA_DIR, 'SJ.counts.short.csv')
        exons_sj_fpath = join(TEST_DATA_DIR, 'short.exons_SJ.csv')
        with NamedTemporaryFile('w') as fhand:        
            cmd = ['python', bin_fpath, '-s', sj_counts_fpath, 
                   '-e', exons_sj_fpath, '-o', fhand.name,
                   '--log_bias_dir', TEST_DATA_DIR]
            check_call(cmd)


class SJcountsTests(unittest.TestCase):
    def test_SJ_class(self):
        line = 'chr1\t10\t56\t1\t0\t1\t10\t2\t30\n'
        sj = SpliceJunction()
        sj.parse_STAR_SJ(line, sample_id='s1', include_multimapping=False)

        assert(sj.chrom == 'chr1')
        assert(sj.start == 9)
        assert(sj.end == 56)
        assert(sj.strand == '+')
        assert(sj.counts['s1'] == 10)
        assert(sj.id == 'chr1:9-56+')
        
        sj.parse_STAR_SJ(line, sample_id='s1', include_multimapping=True)
        assert(sj.counts['s1'] == 12)
        
        line = 'chr1:9-56+,13,23\n'
        sample_ids = ['s1', 's2']
        sj.parse_csv(line, sample_ids)
        
        assert(sj.chrom == 'chr1')
        assert(sj.start == 9)
        assert(sj.end == 56)
        assert(sj.strand == '+')
        assert(sj.counts['s1'] == 13)
        assert(sj.counts['s2'] == 23)
        assert(sj.id == 'chr1:9-56+')
        assert(sj.csv_line(sample_ids) == line)
        assert(sj.STAR_SJ_line == 'chr1\t10\t56\t1\n')
    
    def test_sj_overlap(self):
        '''
        # SJ file
        chr18   5956335 5969392 2
        chr18   5956388 5960093 2
        chr18   5956388 5969392 2
        chr18   5956388 6093354 2
        chr18   5956395 5969392 2
        chr18   5960151 6091354 2
        chr18   5960151 6091354 2
        chr18   5960157 5969372 2
        chr18   5960157 5969372 2
        chr18   5960157 5969392 2
        chr18   5960157 6093354 2
        chr18   5960209 6093354 2
        chr18   5969563 6093354 2
        '''
        
        sj_fpath = join(TEST_DATA_DIR, 'test.short.SJ.tab.gz')
        sj_index = get_sj_index(sj_fpath)
        sj = SpliceJunction('chr18', 5956385, 5969392, strand='-')
        overlaps = list(sj.get_overlapping_SJ(sj_index, window_size=50))
        assert(len(overlaps) == 2)
        assert(overlaps[0].chrom == 'chr18')
        assert(overlaps[0].start in [5956387, 5956394])
        assert(overlaps[0].end == 5969392)
         
        overlaps = list(sj.get_overlapping_SJ(sj_index, window_size=1))
        assert(len(overlaps) == 0)
         
        sj = SpliceJunction('chr18', 5960156, 5969392, strand='-')
        overlaps = list(sj.get_overlapping_SJ(sj_index, window_size=50))
        assert(len(overlaps) == 2)
        
        sj = SpliceJunction('chr18', 5960156, 6093354, strand='-')
        overlaps = list(sj.get_overlapping_SJ(sj_index, window_size=50))
        assert(len(overlaps) == 1)
        
        sj = SpliceJunction('chr18', 5960159, 6093354, strand='-')
        overlaps = list(sj.get_overlapping_SJ(sj_index, window_size=100))
        assert(len(overlaps) == 2)
    
    def test_SJ_set(self):
        lines = ['chr1\t10\t56\t1\t0\t1\t10\t2\t30\n',
                 'chr1\t80\t152\t0\t0\t1\t6\t0\t6\n',
                 'chr1\t234\t257\t2\t0\t1\t5\t0\t36\n']
        
        sj_counts = SpliceJunctionCounts()
        sj_counts.parse_STAR_lines(lines, sample_id='s1')
        sj_counts.parse_STAR_lines(lines, sample_id='s2')
        
        expected_counts = np.array([[10, 10], [6, 6], [5, 5]])
        expected_ids = ['chr1:9-56+', 'chr1:79-152', 'chr1:233-257-']
        assert(np.all(sj_counts.dataframe == expected_counts))
        assert(np.all(sj_counts.sj_ids == expected_ids))
        assert(np.all(sj_counts.sample_ids == ['s1', 's2']))
    
    def test_sj_merge_bin(self):
        design_fpath = join(TEST_DATA_DIR, 'design_sj_merge.csv')
        bin_fpath = join(BIN_DIR, 'merge_sj_counts.py')
        out_fpath = join(TEST_DATA_DIR,'sj_counts.csv')
        cmd = ['python', bin_fpath, '-d', design_fpath, '-i', TEST_DATA_DIR,
               '-o', out_fpath, '--suffix', 'SJ.out.tab']
        check_call(cmd)
        
        sj_counts = SpliceJunctionCounts()
        sj_counts.read_csv(out_fpath) 
        expected_counts = np.array([[10, 10], [6, 6], [5, 5]])
        expected_ids = ['chr1:9-56+', 'chr1:79-152', 'chr1:233-257-']
        assert(np.all(sj_counts.dataframe == expected_counts))
        assert(np.all(sj_counts.sj_ids == expected_ids))
        assert(np.all(sj_counts.sample_ids == ['s1', 's2']))
    
    def test_merge_bin(self):
        orthologs_fpath = join(TEST_DATA_DIR, 'exon_orthologs2.csv')
        input_prefix = join(TEST_DATA_DIR, 'exon_counts')
        bin_fpath = join(BIN_DIR, 'merge_species_exon_counts.py')
        out_prefix = join(TEST_DATA_DIR, 'exon_counts')
        cmd = ['python', bin_fpath, input_prefix, '-e', orthologs_fpath,
               '-o', out_prefix]
        check_call(cmd)
        
        inc = pd.read_csv(join(TEST_DATA_DIR, 'exon_counts.inclusion.csv'),
                          index_col=0)
        skp = pd.read_csv(join(TEST_DATA_DIR, 'exon_counts.skipping.csv'),
                          index_col=0)
        bias = pd.read_csv(join(TEST_DATA_DIR, 'exon_counts.log_bias.csv'),
                           index_col=0)
        
        assert(np.allclose(inc, [[5, 5], [7, 7], [10, 0]]))
        assert(np.allclose(skp, [[5, 5], [7, 7], [0, 0]]))
        assert(np.allclose(bias, [[0.69, 0.56], [0.85, 0.67]]))
        
        orthologs_fpath = join(TEST_DATA_DIR, 'exon_orthologs.test.csv')
        cmd = ['python', bin_fpath, input_prefix, '-e', orthologs_fpath,
               '-o', out_prefix]
        check_call(cmd)
    

if __name__ == '__main__':
#     import sys;sys.argv = ['', 'SJcountsTests', 'EventsCountsTests']
    import sys;sys.argv = ['', 'EventsCountsTests.test_expand_Exons']
    unittest.main()
