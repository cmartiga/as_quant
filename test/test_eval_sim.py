import unittest
from os.path import join
from subprocess import check_call
from tempfile import NamedTemporaryFile

import numpy as np
import pandas as pd

from AS_quant.evol_models import EvolResults, EvolResultsSets
from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
from Bio import Phylo


class EvalSimTests(unittest.TestCase):
    def test_evaluation_psi_ou(self):
        sim_dir = join(TEST_DATA_DIR, 'test_ou')
        results = EvolResults()
        results.read_real_values(prefix=join(sim_dir, 'test_ou.real'))
        results.read_inferred_values(prefix=join(sim_dir, 'test_ou'))
        metrics = results.evaluate_dataset()
        assert(len(metrics) == 12) 
        assert(metrics['delta_psi_rho'] > 0.5)
        assert(metrics['nodes_psi_rho'] > 0.8)
        assert(metrics['mu_rho'] > 0.9)
    
    def test_evaluation_psi_ou_shits(self):    
        sim_dir = join(TEST_DATA_DIR, 'test_ou')
        results = EvolResults()
        results.read_real_values(prefix=join(sim_dir, 'test_shift.real'))
        results.read_inferred_values(prefix=join(sim_dir, 'test_shift'))
        metrics = results.evaluate_dataset()
        assert(metrics['delta_psi_rho'] > 0.5)
        assert(metrics['nodes_psi_rho'] > 0.7)
        assert(metrics['mu_rho'] > 0.5)
        assert(metrics['delta_mu_rho'] > 0.2)
        assert(metrics['delta_opt_psi_auroc'] > 0.6)
        assert(metrics['delta_opt_psi_ap'] > 0.1)
        
    def test_plots_ou_results(self):
        sim_dir = join(TEST_DATA_DIR, 'test_ou')
        tree_fpath = join(TEST_DATA_DIR, 'study.tree.nh')
        order = ['homo_sapiens', 'mus_musculus', 'pan_troglodytes']
        
        tree = Phylo.read(tree_fpath, format='newick')
        results = EvolResults()
        results.read_real_values(prefix=join(sim_dir, 'test_ou.real'))
        results.read_inferred_values(prefix=join(sim_dir, 'test_ou'))
        
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            params = ['nodes_psi', 'delta_psi']
            results.plot_dataset_evaluation(fhand.name, params=params)
  
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            results.plot_pca_tree(fpath=fhand.name)
           
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            results.plot_tree_param(fpath=fhand.name)
         
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            results.heatmap(fpath=fhand.name)
            results.heatmap(fpath=fhand.name, order=order)
          
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            results.tree_heatmap(tree, fhand.name)

        results.read_real_values(prefix=join(sim_dir, 'data.13'))
        results.read_inferred_values(prefix=join(sim_dir, 'fit.13'))
        
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            params = ['nodes_psi', 'delta_psi', 'opt_psi', 'delta_opt_psi']
            results.plot_dataset_evaluation(fhand.name, params=params)

    def test_plots_ou_shifts_results(self):
        sim_dir = join(TEST_DATA_DIR, 'test_ou')
        results = EvolResults()
        results.read_real_values(prefix=join(sim_dir, 'test_shift.real'))
        results.read_inferred_values(prefix=join(sim_dir, 'test_shift'))
        
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            params = ['nodes_psi', 'delta_psi', 'opt_psi', 'delta_opt_psi']
            results.plot_dataset_evaluation(fhand.name, params=params)
        
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            results.plot_tree_param(fpath=fhand.name, param='delta_opt_psi')
        
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            results.plot_pca_tree(fpath=fhand.name, param='opt_psi')
        # TODO: Add phylogeny plot with color by exon PSI or PC
        
    def test_eval_shifts_sets(self):
        test_dir = join(TEST_DATA_DIR, 'test_ou')
        params_fpath = join(test_dir, 'simulations.config.csv')
        real_prefix = join(test_dir, 'data')
        inferred_prefix = join(test_dir, 'shifts')
        params_table = pd.read_csv(params_fpath)
        
        res_sets = EvolResultsSets()
        res_sets.read_datasets_results(params_table, real_prefix,
                                       inferred_prefix)
        res_sets.evaluate_datasets()

        assert(np.all(res_sets.metrics['opt_psi_rho'] > 0.5))
        assert(np.all(res_sets.metrics['delta_psi_rho'] > 0.5))
        assert(np.all(res_sets.metrics['nodes_psi_rho'] > 0.8))
        
        with NamedTemporaryFile('w') as fhand:
            res_sets.write_evaluation(fhand.name)
            
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            res_sets.plot(fhand.name)
        
    def test_shifts_results_bin(self):
        test_dir = join(TEST_DATA_DIR, 'test_ou')
        params_fpath = join(test_dir, 'simulations.config.csv')
        real_prefix = join(test_dir, 'data')
        inferred_prefix = join(test_dir, 'shifts')
        bin_fpath = join(BIN_DIR, 'evaluate_shifts.py')
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, params_fpath, '-r', real_prefix,
                   '-i', inferred_prefix, '-o', fhand.name]
            check_call(cmd)


if __name__ == '__main__':
    import sys
    sys.argv = ['', 'EvalSimTests']
    unittest.main()
