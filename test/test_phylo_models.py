from os.path import join
from subprocess import check_call
from tempfile import NamedTemporaryFile
import unittest

from AS_quant.models.phylo import (BasicOUmodel, ShiftsOUmodel, TreeCTMC,
                                   TreeExon, OUmodelByElement, TreeOU,
                                   BMmodelByElement, branch_data_to_tree)
from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
import dendropy
import numpy as np
import pandas as pd
from Bio import Phylo


def get_test_tree():
    tree_str = '(((homo_sapiens:76.0,mus_musculus:76.0)Inner5:13.0,'
    tree_str += 'bos_taurus:89.0)Inner4:84.0,monodelphis_domestica:173.0)'
    tree_str += 'Inner2:427.0;'
    species = ['homo_sapiens', 'mus_musculus',
               'bos_taurus', 'monodelphis_domestica']
    tree = dendropy.Tree.get_from_string(tree_str, schema='newick',
                                         preserve_underscores=True)
    tree.retain_taxa_with_labels(species)
    return(tree, species)


def get_test_design(species, n_samples_per_sp):
    n = n_samples_per_sp * len(species)
    design = pd.DataFrame({'species': species * n_samples_per_sp},
                          index=['s{}'.format(i) for i in range(1, n + 1)])
    return(design)


class OUTests(unittest.TestCase):
    def test_branch_data_to_tree(self):
        tree = get_test_tree()[0]
        tree_ou = TreeOU(tree)
        branch_data = tree_ou.get_branches_data()
        tree = branch_data_to_tree(branch_data, node_label_field='sp')
        with NamedTemporaryFile('w') as fhand:
            Phylo.write(tree, file=fhand.name, format='newick')
            tree = [line.strip() for line in open(fhand.name)][0]
            exp = '(((homo_sapiens:76.00000,mus_musculus:76.00000):13.00000,bos_taurus:89.00000):84.00000,monodelphis_domestica:173.00000):0.00000;'
            assert(tree == exp)
        
        # Store variable as character for plotting
        tree = branch_data_to_tree(branch_data, node_label_field='sp',
                                   character_field='age')
        with NamedTemporaryFile('w') as fhand:
            Phylo.write(tree, file=fhand.name, format='newick')
            tree = [line.strip() for line in open(fhand.name)][0]
            exp = '(((homo_sapiens:76.00000,mus_musculus:76.00000):13.00000,bos_taurus:89.00000):84.00000,monodelphis_domestica:173.00000):0.00000;'
            assert(tree == exp)
        
    def test_tree_ou_simulate(self):
        tree = get_test_tree()[0]
        tree_ou = TreeOU(tree)
        branch_data = tree_ou.get_branches_data()
        assert(np.all(branch_data['node'] == ['n0', 'n1', 'n2', 'n3',
                                              'n4', 'n5', 'n6']))
        assert(np.all(branch_data['parent'][1:] == ['n0', 'n1', 'n2', 'n2',
                                                    'n1', 'n0']))
        assert(np.allclose(branch_data['age'], [173, 89, 76, 0, 0, 0, 0]))

        np.random.seed(0)
        tree_ou.set_ou_params(mu0_mean=0, mu0_sd=1, phylo_hl=10,
                              tau2_over_2a=1, sigma=0.2, adapt_p=0.1)
        tree_ou.evolve_trait(n=10)
        dfs = tree_ou.get_nodes_traits_tables()
        m = dfs['nodes_mean']
        exp_delta_means = pd.DataFrame({'1': m['n1'] - m['n0'],
                                        '2': m['n2'] - m['n1'],
                                        '3': m['n3'] - m['n2'],
                                        '4': m['n4'] - m['n2'],
                                        '5': m['n5'] - m['n1'],
                                        '6': m['n6'] - m['n0']})
        assert(np.allclose(dfs['delta_mean'].iloc[:, 1:], exp_delta_means))
        assert(np.all(np.isnan(dfs['delta_mu']['n0'])))
        assert(np.all(np.isnan(dfs['delta_psi']['n0'])))

    def test_simulate_ou_data_AS(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)

        ou_as = BasicOUmodel(tree, design, obs_model='logit_binomial')
        ou_as.simulate(n_elements=10, mu0_mean=0, mu0_sd=1, tau2_over_2a=3,
                       phylo_hl=1, sigma=0.4, seed=0)
        assert(ou_as.observations.inclusion.shape == (8, 10))
        assert(ou_as.observations.total.shape == (8, 10))
        assert(np.all(ou_as.observations.inclusion.index == design.index))

        ou_as.simulate(n_elements=10, mu0_mean=0, mu0_sd=1, tau2_over_2a=3,
                       phylo_hl=1, sigma=0.4, seed=0, exponential_cov=True)

    def test_simulate_shifts_AS(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)
        shifts_as = ShiftsOUmodel(tree, design, obs_model='logit_binomial',
                                  phylo_hl=1, tau2_over_2a=1, sigma=0.5)
        shifts_as.simulate(n_elements=10, mu0_mean=0, mu0_sd=1, tau2_over_2a=3,
                           phylo_hl=1, sigma=0.4, adapt_p=0.1)
        assert(shifts_as.observations.inclusion.shape == (8, 10))
        assert(shifts_as.observations.total.shape == (8, 10))
        assert(np.all(shifts_as.observations.inclusion.index == design.index))

    def test_simulate_ou_data_GE(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)

        ou_ge = BasicOUmodel(tree, design, obs_model='log_poisson')
        ou_ge.simulate(n_elements=10, mu0_mean=0, mu0_sd=1, tau2_over_2a=3,
                       phylo_hl=1, sigma=0.4, seed=0)
        assert(ou_ge.observations.counts.shape == (8, 10))
        assert(np.all(ou_ge.observations.counts.index == design.index))

    def test_fit_global_ou_binomial(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)

        ou_as = BasicOUmodel(tree, design, obs_model='logit_binomial')
        ou_as.simulate(n_elements=20, mu0_mean=0, mu0_sd=1, tau2_over_2a=3,
                       phylo_hl=20, sigma=0.4, seed=0, coverage=100)
        ou_as.fit()
        s = ou_as.model.summary.to_dict()
        assert(s['2.5%']['phylo_hl'] < 20 and s['97.5%']['phylo_hl'] > 20)
        assert(s['2.5%']['tau2_over_2a'] <
               3 and s['97.5%']['tau2_over_2a'] > 3)
        assert(s['2.5%']['sigma2'] < 0.16 and s['97.5%']['sigma2'] > 0.16)
        assert(s['2.5%']['mu_mean'] < 0. and s['97.5%']['mu_mean'] > 0)
        assert(s['2.5%']['mu_sd'] < 1 and s['97.5%']['mu_sd'] > 1)

        with NamedTemporaryFile('w') as fhand:
            ou_as.write_fit(prefix=fhand.name)

    def test_fit_global_ou_nb(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)

        # Negative binomial
        ou_ge = BasicOUmodel(tree, design, obs_model='log_negativebinomial',
                             recompile=False)
        ou_ge.simulate(n_elements=20, mu0_mean=6, mu0_sd=1, tau2_over_2a=3,
                       phylo_hl=20, sigma=0.5, adapt_p=0, seed=0)
        ou_ge.fit()
        s = ou_ge.model.summary.to_dict()
        assert(s['2.5%']['phylo_hl'] < 20 and s['97.5%']['phylo_hl'] > 20)
        assert(s['2.5%']['tau2_over_2a'] <
               3 and s['97.5%']['tau2_over_2a'] > 3)
        assert(s['2.5%']['sigma2'] < 0.25 and s['97.5%']['sigma2'] > 0.25)
        assert(s['2.5%']['mu_mean'] < 0. and s['97.5%']['mu_mean'] > 0)
        assert(s['2.5%']['mu_sd'] < 1 and s['97.5%']['mu_sd'] > 1)

    def test_fit_global_ou_poisson(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)

        ou_ge = BasicOUmodel(tree, design, obs_model='log_poisson',
                             recompile=False)
        ou_ge.simulate(n_elements=20, mu0_mean=6, mu0_sd=1, tau2_over_2a=3,
                       phylo_hl=20, sigma=0.5, adapt_p=0, seed=0)
        ou_ge.fit()
        s = ou_ge.model.summary.to_dict()
        assert(s['2.5%']['phylo_hl'] < 20 and s['97.5%']['phylo_hl'] > 20)
        assert(s['2.5%']['tau2_over_2a'] <
               3 and s['97.5%']['tau2_over_2a'] > 3)
        assert(s['2.5%']['sigma2'] < 0.25 and s['97.5%']['sigma2'] > 0.25)
        assert(s['2.5%']['mu_mean'] < 0. and s['97.5%']['mu_mean'] > 0)
        assert(s['2.5%']['mu_sd'] < 1 and s['97.5%']['mu_sd'] > 1)

    def test_shifts_binomial(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)
        shifts_as = ShiftsOUmodel(tree, design, obs_model='logit_binomial',
                                  phylo_hl=20, tau2_over_2a=1, sigma=0.5,
                                  recompile=False)
        shifts_as.simulate(n_elements=20, mu0_mean=6, mu0_sd=1, tau2_over_2a=3,
                           phylo_hl=20, sigma=0.5, adapt_p=0.1, seed=0)
        shifts_as.fit()
        with NamedTemporaryFile('w') as fhand:
            shifts_as.write_fit(fhand.name)

    def test_shifts_poisson(self):
        tree, species = get_test_tree()
        design = get_test_design(species, n_samples_per_sp=3)
        shifts_as = ShiftsOUmodel(tree, design, obs_model='log_poisson',
                                  phylo_hl=20, tau2_over_2a=1, sigma=0.5,
                                  recompile=False)
        shifts_as.simulate(n_elements=20, mu0_mean=6, mu0_sd=1, tau2_over_2a=3,
                           phylo_hl=20, sigma=0.5, adapt_p=0.1, seed=0)
        shifts_as.fit()
        with NamedTemporaryFile('w') as fhand:
            shifts_as.write_fit(fhand.name)

    def test_simulate_ctmc(self):
        tree, _ = get_test_tree()
        np.random.seed(0)
        pi = np.array([0.8, 0.1, 0.1])
        er = np.array([[0, 0.2, 0.1],
                       [0.2, 0, 0.2],
                       [0.1, 0.2, 0]])
        tree_ctmc = TreeCTMC(tree)
        tree_ctmc.init_rates_matrix(ex_rates_matrix=er, freqs=pi)
        tree_ctmc.evolve_trait(n=5)
        m = tree_ctmc.get_species_data()
        assert(m.shape == (5, 4))

        # Exon tree
        exon_tree = TreeExon(tree)
        exon_tree.set_params(exonization=0.2, stablishment=0.1,
                             pseudo_loss=0.1, loss=0.02)
        exon_tree.evolve_trait(n=10)
        m = exon_tree.get_species_data()
        assert(m.shape == (10, 4))

    def test_simulate_OU_bin(self):
        design_fpath = join(TEST_DATA_DIR, 'design.ou.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.ou.nh')
        bin_fpath = join(BIN_DIR, 'sim_OU.py')

        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, '-d', design_fpath, '-p', tree_fpath,
                   '-C', '20', '--exponential_depth', '-N', '10',
                   '-m', 'logit_binomial', '-o', fhand.name, '--seed', '0',
                   '-m0', '0', '-m0s', '0.2', '-t12', '20', '-t2a', '5',
                   '-s', '0.2', '-a', '0.2']
            check_call(cmd)
            total = pd.read_csv('{}.1.total.csv'.format(fhand.name),
                                index_col=0)
            assert(total.shape == (10, 8))

    def test_simulate_OU_bin_prior(self):
        design_fpath = join(TEST_DATA_DIR, 'design.ou.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.ou.nh')
        prior_fpath = join(TEST_DATA_DIR, 'ou_fit.csv')
        bin_fpath = join(BIN_DIR, 'sim_OU.py')

        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, '-d', design_fpath, '-p', tree_fpath,
                   '-C', '20', '--exponential_depth', '-N', '10',
                   '-m', 'logit_binomial', '-o', fhand.name, '--seed', '0',
                   '--prior', prior_fpath]
            check_call(cmd)
            total = pd.read_csv('{}.1.total.csv'.format(fhand.name),
                                index_col=0)
            assert(total.shape == (10, 8))

    def test_fit_OU_bin(self):
        design_fpath = join(TEST_DATA_DIR, 'design.ou.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.ou.nh')
        bin_fpath = join(BIN_DIR, 'fit_OU.py')
        inc_fpath = join(TEST_DATA_DIR, 'ou_data.1.inclusion.csv')
        tot_fpath = join(TEST_DATA_DIR, 'ou_data.1.total.csv')
        log_bias_fpath = join(TEST_DATA_DIR, 'ou_data.1.log_bias.csv')

        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, inc_fpath, tot_fpath,
                   '-d', design_fpath, '-p', tree_fpath,
                   '--log_bias', log_bias_fpath, '-o', fhand.name]
            check_call(cmd)

    def test_fit_OU_shifts_bin(self):
        design_fpath = join(TEST_DATA_DIR, 'design.ou.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.ou.nh')
        bin_fpath = join(BIN_DIR, 'fit_OU_shifts.py')
        inc_fpath = join(TEST_DATA_DIR, 'ou_data.1.inclusion.csv')
        tot_fpath = join(TEST_DATA_DIR, 'ou_data.1.total.csv')
        prior_fpath = join(TEST_DATA_DIR, 'ou_data.fit.csv')
        log_bias_fpath = join(TEST_DATA_DIR, 'ou_data.1.log_bias.csv')

        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, inc_fpath, tot_fpath,
                   '-d', design_fpath, '-p', tree_fpath,
                   '--log_bias', log_bias_fpath, '-o', fhand.name,
                   '--prior', prior_fpath]
            check_call(cmd)

    def test_ou_by_element(self):
        tree_fpath = join(TEST_DATA_DIR, 'simulations', 'tree.nh')
        design_fpath = join(TEST_DATA_DIR, 'simulations', 'design.csv')
        tree = dendropy.Tree.get(path=tree_fpath, schema='newick')
        design = pd.read_csv(design_fpath)

        m = OUmodelByElement(tree, design, obs_model='logit_binomial',
                             phylo_hl=10,
                             log_tau2_over_2a_mean=0, log_tau2_over_2a_sd=0.5,
                             log_beta_mean=4, log_beta_sd=1,
                             recompile=False)
        m.simulate(n_elements=1, mu0_mean=0, mu0_sd=1,
                   tau2_over_2a=1, phylo_hl=20, sigma=0.3, seed=0,
                   mean_coverage=50)
        m.fit(chains=4, n_iter=1000)

        with NamedTemporaryFile('w') as fhand:
            m.write_fit(prefix=fhand.name)
            fpath = '{}.params.csv'.format(fhand.name)
            data = pd.read_csv(fpath)
            colnames = ['exon_id', 'tau2_over_2a', 'tau2', 'beta', 'sigma2',
                        'mu', 'opt_psi']
            assert(np.all(data.columns == colnames))

    def test_ou_by_element_bin(self):
        design_fpath = join(TEST_DATA_DIR, 'design.ou.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.ou.nh')
        bin_fpath = join(BIN_DIR, 'fit_OU_by_exon.py')
        inc_fpath = join(TEST_DATA_DIR, 'ou_data.1.inclusion.csv')
        tot_fpath = join(TEST_DATA_DIR, 'ou_data.1.total.csv')
        prior_fpath = join(TEST_DATA_DIR, 'ou_data.fit.csv')
        log_bias_fpath = join(TEST_DATA_DIR, 'ou_data.1.log_bias.csv')

        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, inc_fpath, tot_fpath,
                    '-d', design_fpath, '-p', tree_fpath,
                    '--log_bias', log_bias_fpath, '-o', fhand.name,
                    '--prior', prior_fpath]
            check_call(cmd)
            fpath = '{}.params.csv'.format(fhand.name)
            data = pd.read_csv(fpath)
            colnames = ['exon_id', 'tau2_over_2a', 'tau2', 'beta', 'sigma2',
                        'mu', 'opt_psi']
            assert(np.all(data.columns == colnames))

    def test_ou_shifts_by_element(self):
        tree_fpath = join(TEST_DATA_DIR, 'simulations', 'tree.nh')
        design_fpath = join(TEST_DATA_DIR, 'simulations', 'design.csv')
        tree = dendropy.Tree.get(path=tree_fpath, schema='newick')
        design = pd.read_csv(design_fpath)

        m = ShiftsOUmodel(tree, design, obs_model='logit_binomial',
                          phylo_hl=10, tau2_over_2a=1, sigma=0.3,
                          recompile=False)
        m.simulate(n_elements=1, mu0_mean=0, mu0_sd=1,
                   tau2_over_2a=1, phylo_hl=20, sigma=0.3, seed=0,
                   mean_coverage=50, adapt_p=0.01)
        m.fit(chains=4, n_iter=1000, n_jobs=4)

        with NamedTemporaryFile('w') as fhand:
            m.write_fit(prefix=fhand.name)


class BMTests(unittest.TestCase):
    def test_bm(self):
        tree_fpath = join(TEST_DATA_DIR, 'simulations', 'tree.nh')
        design_fpath = join(TEST_DATA_DIR, 'simulations', 'design.csv')
        tree = dendropy.Tree.get(path=tree_fpath, schema='newick')
        design = pd.read_csv(design_fpath)

        m = BMmodelByElement(tree, design, obs_model='logit_binomial',
                             recompile=False)
        m.simulate(n_elements=1, X0_mean=0, X0_sd=1,
                   tau2=0.01, sigma=0.4, seed=0, mean_coverage=50)
        m.fit()
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(prefix=fhand.name)

    def test_fit_BM_bin(self):
        design_fpath = join(TEST_DATA_DIR, 'design.ou.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.ou.nh')
        bin_fpath = join(BIN_DIR, 'fit_BM.py')
        in_fpaths = [join(TEST_DATA_DIR, 'ou_data.1.inclusion.csv'),
                     join(TEST_DATA_DIR, 'ou_data.1.total.csv')]
        in_fpaths_str = ','.join(in_fpaths)
        log_bias_fpath = join(TEST_DATA_DIR, 'ou_data.1.log_bias.csv')

        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, in_fpaths_str, '-d', design_fpath,
                   '-p', tree_fpath, '-m', 'logit_binomial', '--log_bias',
                   log_bias_fpath, '-o', fhand.name]
            check_call(cmd)


if __name__ == '__main__':
    import sys
    sys.argv = ['', 'OUTests', 'BMTests']
#     sys.argv = ['', 'OUTests']
    unittest.main()
