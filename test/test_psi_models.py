import unittest
from os.path import join, exists
from subprocess import check_call
from tempfile import NamedTemporaryFile

import pandas as pd
import numpy as np
from scipy.stats.mstats_basic import pearsonr

from AS_quant.settings import BIN_DIR, TEST_DATA_DIR
from AS_quant.genome import get_annotation_db
from AS_quant.models.psi import (PsiRangeModel, ExonPsiModel, MixturePsiModel,
                                 SpliceGraphModel, infer_splice_graph_psi,
                                 RegressionModel)


class PsiTests(unittest.TestCase):
    def xtest_simulation(self):
        m = MixturePsiModel(recompile=False)
        m.simulate(n_elements=30, mean=0, sd=1, p_alternative=0.5, seed=1,
                   mean_coverage=20)
        m.observations.write(join(TEST_DATA_DIR, 'sim_test'))
        assert(exists(join(TEST_DATA_DIR, 'sim_test.inclusion.csv')))
        assert(exists(join(TEST_DATA_DIR, 'sim_test.total.csv')))
        assert(exists(join(TEST_DATA_DIR, 'sim_test.log_bias.csv')))
        
    def xtest_psi_range(self):
        design = pd.DataFrame({'v1': [1] * 2 + [0] * 6,
                               'v2': [0] * 2 + [1] * 2 + [0] * 4,
                               'v3': [0] * 4 + [1] * 2 + [0] * 2,
                               'v4': [0] * 6 + [1] * 2},
                              index=['s{}'.format(i) for i in range(8)])
        m = PsiRangeModel(design)
        m.simulate(n_elements=10, alpha_mean=0, alpha_sd=1,
                   beta_mean=0, beta_sd=0.5, sigma=0.4, seed=1)
        m.fit()
        m.write_fit(prefix=join(TEST_DATA_DIR, 'test_psi_range'))
        
    def test_psi_regression(self):
        design = pd.DataFrame({'v1': [1, 1, 1, 1, 1, 1],
                               'v2': [0, 0, 1, 1, 1, 1],
                               'v3': [0, 0, 0, 0, 1, 1]},
                              index=['s{}'.format(i) for i in range(6)])
        design_pred = pd.DataFrame({'v1': [1, 1, 1],
                                    'v2': [0, 1, 1],
                                    'v3': [0, 0, 1]},
                                    index=['g1', 'g2', 'g3'])
        m = RegressionModel(design, design_pred=design_pred)
        m.simulate(n_elements=5, alpha_mean=0, alpha_sd=1,
                   beta_mean=0, beta_sd=0.5, sigma=0.4, seed=1)
        m.fit()
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(prefix=fhand.name)
            fpath = '{}.psi.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (5, 9))
            
            fpath = '{}.coeff.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (5, 24))
            
        # Test dPSI contrasts
        contrast_matrix = pd.DataFrame({'v3_vs_v1': [-1, 0, 1],
                                        'v2_vs_v1': [-1, 1, 0]},
                                        index=['v1', 'v2', 'v3'])
        m = RegressionModel(design, design_pred=design_pred,
                            contrast_matrix=contrast_matrix)
        m.simulate(n_elements=5, alpha_mean=0, alpha_sd=1,
                   beta_mean=0, beta_sd=0.5, sigma=0.4, seed=1)
        m.fit()
        with NamedTemporaryFile('w') as fhand:
            m.write_fit(prefix=fhand.name)
            fpath = '{}.dpsi.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (5, 6))
    
    def test_psi_regression_bin(self):
        bin_fpath = join(BIN_DIR, 'fit_psi_regression.py')
        inc_fpath = join(TEST_DATA_DIR, 'counts.inclusion.csv')
        total_fpath = join(TEST_DATA_DIR, 'counts.total.csv')
        design_fpath = join(TEST_DATA_DIR, 'counts.design.csv')
        design_pred_fpath = join(TEST_DATA_DIR, 'counts.design_pred.csv')
        
        with NamedTemporaryFile('w') as fhand:
            cmd = [bin_fpath, inc_fpath, total_fpath,
                   '-d',  design_fpath, '--design_pred', design_pred_fpath, 
                   '-o', fhand.name]
            print(' '.join(cmd))
            cmd = [bin_fpath, '-h']
            check_call(cmd)
            fpath = '{}.summary.csv'.format(fhand.name)
            results = pd.read_csv(fpath, index_col=0)
            assert(results.shape == (5, 9))
    
    def xtest_exon_psi(self):
        m = ExonPsiModel(recompile=False)
        m.simulate(n_elements=10, n_samples=3, mean=0, sd=1,
                   p_alternative=1, seed=1)
        m.fit()
        result = m.write_fit(prefix=join(TEST_DATA_DIR, 'test_exon_psi'))
        r = pearsonr(m.observations.real_psi, result['psi'])[0]
        assert(r > 0.9)
        
    def test_exon_psi_mixture(self):
        # With a single sample
        m = MixturePsiModel(n_samples=1, recompile=False)
        m.simulate(n_elements=50, mean=0, sd=1, p_alternative=0.5, seed=1,
                   sigma=0.2, mean_coverage=20)
        m.fit()
        result = m.write_psi(join(TEST_DATA_DIR, 'test_psi_mixture.csv'))
        r = pearsonr(m.psi, result['psi'])[0]
        assert(r > 0.95)
        
        s = m.write_summary()
        assert(s.loc['theta', '2.5%'] < 0.5 and s.loc['theta', '97.5%'] > 0.5)
        
        # With several samples
        m = MixturePsiModel(n_samples=3, recompile=False)
        m.simulate(n_elements=50, mean=0, sd=1, p_alternative=0.5, seed=1,
                   sigma=0.2, mean_coverage=10)
        m.fit()
        result = m.write_psi(join(TEST_DATA_DIR, 'test_psi_mixture.csv'))
        r = pearsonr(m.psi, result['psi'])[0]
        assert(r > 0.95)
        
        s = m.write_summary()
        assert(s.loc['theta', '2.5%'] < 0.5 and s.loc['theta', '97.5%'] > 0.5)
        assert(s.loc['sigma', '2.5%'] < 0.2 and s.loc['sigma', '97.5%'] > 0.2)
    
    def xtest_infer_psi_bin(self):
        inc_fpath = join(TEST_DATA_DIR, 'psi_test.inclusion.csv')
        total_fpath = join(TEST_DATA_DIR, 'psi_test.total.csv')
        log_bias_fpath = join(TEST_DATA_DIR, 'psi_test.log_bias.csv')
        bin_fpath = join(BIN_DIR, 'infer_exon_psi.py')
        out_fpath = join(TEST_DATA_DIR, 'psi_test.output.csv')
        
        cmd = ['python', bin_fpath, '-i', inc_fpath, '-t', total_fpath,
               '-e', log_bias_fpath, '-f', 's1', '-o', out_fpath]
        check_call(cmd)
        
    def test_splice_graph_nodes_types(self):
        m = SpliceGraphModel(recompile=False, inv_phi=1.8)
        
        # Simple event
        source = np.array([0, 0, 1])
        target = np.array([1, 2, 2])
        m.load_sj_data(source, target)
        assert(m.model_stan_data['alternative'][0] == 2)
        
        # With alternative splice site
        source = np.array([0, 0, 0, 1, 2])
        target = np.array([1, 2, 3, 2, 3])
        m.load_sj_data(source, target)
        assert(np.all(m.model_stan_data['alternative'] == [2, 3]))
        
        # Complex event
        source = np.array([0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 5])
        target = np.array([1, 3, 4, 5, 6, 2, 3, 4, 5, 6, 4, 5, 6, 5, 6])
        m.load_sj_data(source, target)
        assert(np.all(m.model_stan_data['alternative'] == [2, 4, 5, 6]))
    
    def test_splice_graph_model_1exon(self):
        source = np.array([0, 0, 1])
        target = np.array([1, 2, 2])
        psi = np.array([1-1e-6, 0.5, 1-1e-6])
        read_length = 20
        
        m = SpliceGraphModel(recompile=False, inv_phi=1.8)
        m.load_sj_data(source, target)
        counts, idx = m.simulate(psi=psi, read_length=read_length,
                                 mu=5, seed=0)
        
        exp_idx = [0] * read_length + [1] * read_length + [2] * read_length
        assert(counts.shape[0] == 3 * read_length)
        assert(np.all(idx == exp_idx))
        
        m.fit()
        nodes_psi = m.process_fit()
        assert(nodes_psi.iloc[1, 1] < 0.5 and nodes_psi.iloc[1, 2] > 0.5)
        
        psi = np.array([1-1e-6, 0.2, 1-1e-6])
        m.simulate(psi=psi, read_length=read_length, mu=5, seed=0)
        m.fit()
        nodes_psi = m.process_fit()
        assert(nodes_psi.iloc[1, 1] < 0.2 and nodes_psi.iloc[1, 2] > 0.2)
        
        psi = np.array([1-1e-6, 0.1, 1-1e-6])
        m.simulate(psi=psi, read_length=read_length, mu=5, seed=0)
        m.fit()
        nodes_psi = m.process_fit()
        assert(nodes_psi.iloc[1, 1] < 0.1 and nodes_psi.iloc[1, 2] > 0.1)
        
    
    def test_splice_graph_model_2exons(self):
        source = np.array([0, 0, 0, 1, 1, 2])
        target = np.array([1, 2, 3, 2, 3, 3])
        psi = np.array([1-1e-6, 0.5, 0.5, 1-1e-6])
        read_length = 10
        
        m = SpliceGraphModel(recompile=False, inv_phi=1.8)
        m.load_sj_data(source, target)
        m.simulate(psi=psi, read_length=read_length*10, mu=5, seed=0)
        m.fit()
        nodes_psi = m.process_fit()
        assert(nodes_psi.iloc[1, 1] < 0.5 and nodes_psi.iloc[1, 2] > 0.5)
        assert(nodes_psi.iloc[2, 1] < 0.5 and nodes_psi.iloc[2, 2] > 0.5)
    
    def test_splice_graph_model_alt_ss(self):
        source = np.array([0, 0, 0, 1, 2])
        target = np.array([1, 2, 3, 2, 3])
        psi = np.array([1-1e-6, 0.5, 0.9, 1-1e-6])
        read_length = 100
        
        m = SpliceGraphModel(recompile=False, inv_phi=1.5)
        m.load_sj_data(source, target)
        m.simulate(psi=psi, read_length=read_length, mu=5, seed=0)
        m.fit()
        nodes_psi = m.process_fit()
        assert(nodes_psi.iloc[1, 1] < 0.5 and nodes_psi.iloc[1, 2] > 0.5)
        assert(nodes_psi.iloc[2, 1] < 0.9 and nodes_psi.iloc[2, 2] > 0.9)
        
    def test_splice_graph_model_3exons(self):
        source = np.array([0, 0, 0, 0, 1, 1, 1, 2, 2, 3])
        target = np.array([1, 2, 3, 4, 2, 3, 4, 3, 4, 4])
        psi = np.array([1-1e-6, 0.5, 0.5, 0.5, 1-1e-6])
        read_length = 10
        
        m = SpliceGraphModel(recompile=False, inv_phi=1.8)
        m.load_sj_data(source, target)
        m.simulate(psi=psi, read_length=read_length*10, mu=5, seed=0)
        m.fit()
        nodes_psi = m.process_fit()
        assert(nodes_psi.iloc[1, 1] < 0.5 and nodes_psi.iloc[1, 2] > 0.5)
        assert(nodes_psi.iloc[2, 1] < 0.5 and nodes_psi.iloc[2, 2] > 0.5)
        assert(nodes_psi.iloc[3, 1] < 0.5 and nodes_psi.iloc[3, 2] > 0.5)
    
    def test_splice_graph_model_complex(self):
        source = np.array([0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 5])
        target = np.array([1, 3, 4, 5, 6, 2, 3, 4, 5, 6, 4, 5, 6, 5, 6])
        psi = np.array([1-1e-6, 0.9, 1-1e-6, 0.5,
                        0.9, 0.8, 1-1e-6])
        
        m = SpliceGraphModel(recompile=False, inv_phi=1.5)
        m.load_sj_data(source, target)
        m.simulate(psi=psi, read_length=100, mu=5, seed=0)
        m.fit()
        nodes_psi = m.process_fit()
        for i in [1, 3, 4, 5]:
            assert(nodes_psi.iloc[i, 1] < psi[i] and nodes_psi.iloc[i, 2] > psi[i])
    
    def test_infer_sg_psi(self):
        db_fpath = join(TEST_DATA_DIR, 'cel.gtf.db')
        counts_fpath = join(TEST_DATA_DIR, 'SRR7155123.counts.tsv')
        
        sj_counts = pd.read_csv(counts_fpath, sep='\t')
        db = get_annotation_db(db_fpath)
        
        for nodes_psi in infer_splice_graph_psi(db, sj_counts, recompile=False):
            assert(nodes_psi.shape[0] < 10)
    
    def test_infer_sg_psi_bin(self):
        db_fpath = join(TEST_DATA_DIR, 'cel.gtf.db')
        counts_fpath = join(TEST_DATA_DIR, 'SRR7155123.counts.tsv')
        bin_fpath = join(BIN_DIR, 'infer_splice_graph_psi.py')

        with NamedTemporaryFile('w') as fhand:
            out_fpath = fhand.name        
            cmd = ['python', bin_fpath, counts_fpath, '-db', db_fpath,
                   '-o', out_fpath]
            check_call(cmd)
            exons_psi = pd.read_csv(out_fpath)
            assert(exons_psi.shape[0] == 12)
        

if __name__ == '__main__':
    import sys
    sys.argv = ['', 'PsiTests.test_exon_psi_mixture']
    unittest.main()
