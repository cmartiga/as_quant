#!/usr/bin/env python
import unittest
from tempfile import NamedTemporaryFile
from os.path import join
from subprocess import check_call

import numpy as np
import pandas as pd

from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
from AS_quant.bam_process import load_bamfile, get_SE_counts
from AS_quant.junctions import SpliceJunction
from AS_quant.genome import Gene, Exon, Genome
from AS_quant.qc import SpliceJunctionProfiles
from bin.evaluate_psi_inference import (plot_psi_inference_evaluation,
                                        merge_inferred_and_real_psi)


class SJCountsTests(unittest.TestCase):
    def test_get_SJ_reads(self):
        fpath = join(TEST_DATA_DIR, 'reads.MD.bam')
        bamfile = load_bamfile(fpath)
        
        sj = SpliceJunction(chrom='chr18', start=321807, end=331666, strand='+')
        reads = sj.get_overlapping_reads(bamfile)
        n_reads = len(list(reads))
        assert(n_reads == 820)
        
        sj = SpliceJunction(chrom='chr18', start=321807, end=331666, strand='+',
                            upstream_start=321757)
        reads = sj.get_overlapping_reads(bamfile)
        n_reads = len(list(reads))
        assert(n_reads == 455)
    
    def test_get_SJ_reads_profile(self):
        fpath = join(TEST_DATA_DIR, 'reads.MD.bam')
        bamfile = load_bamfile(fpath)
        sj = SpliceJunction(chrom='chr18', start=321807, end=331666, strand='+')
        counts = sj.get_mapping_profile(bamfile, read_length=101)
        assert(counts.shape == (200, 3))
        assert(counts['counts'].sum() == 820)
        s_counts = counts.groupby(['strand'])['counts'].sum()
        assert(np.all(s_counts == [413, 407]))
        
        # With upstream start
        sj = SpliceJunction(chrom='chr18', start=321807, end=331666, strand='+',
                            upstream_start=321757)
        counts = sj.get_mapping_profile(bamfile, read_length=101)
        assert(counts.shape == (100, 3))
        assert(counts['counts'].sum() == 455)

        # Ensure there is no overlap with a slightly shifted SJ        
        sj = SpliceJunction(chrom='chr18', start=321808, end=331663, strand='+')
        counts = sj.get_mapping_profile(bamfile, read_length=101)
        assert(counts['counts'].sum() == 0)
    
    def test_calc_positions(self):
        sj = SpliceJunction(chrom='chr1', start=50, end=100, strand='+',
                            upstream_start=25, downstream_end=125)
        assert(sj.downstream_dist == 25)
        assert(sj.upstream_dist == 25)
        
        assert(sj.calc_pos_start(read_length=10) == 41)
        assert(sj.calc_pos_start(read_length=15) == 36)
        assert(sj.calc_pos_start(read_length=20) == 31)
        assert(sj.calc_pos_start(read_length=30) == 25)
        
        assert(sj.calc_pos_end(read_length=10) == 50)
        assert(sj.calc_pos_end(read_length=15) == 50)
        assert(sj.calc_pos_end(read_length=20) == 50)
        assert(sj.calc_pos_end(read_length=30) == 45)
        
        pos = sj.calc_pos(read_length=10)
        exp = np.arange(1, 10)
        assert(np.all(pos == exp))
        
        pos = sj.calc_pos(read_length=20)
        exp = np.arange(1, 20)
        assert(np.all(pos == exp))
        
        pos = sj.calc_pos(read_length=30)
        exp = np.arange(6, 26)
        assert(np.all(pos == exp))
        
        pos = sj.calc_pos(read_length=50)
        assert(pos.shape[0] == 0)
        
        sj = SpliceJunction(chrom='chr1', start=50, end=100, strand='+',
                            upstream_start=0, downstream_end=125)
        assert(sj.calc_pos_start(read_length=10) == 41)
        assert(sj.calc_pos_start(read_length=15) == 36)
        assert(sj.calc_pos_start(read_length=20) == 31)
        assert(sj.calc_pos_start(read_length=30) == 21)
        
        assert(sj.calc_pos_end(read_length=10) == 50)
        assert(sj.calc_pos_end(read_length=15) == 50)
        assert(sj.calc_pos_end(read_length=20) == 50)
        assert(sj.calc_pos_end(read_length=30) == 45)
        assert(sj.calc_pos_end(read_length=50) == 25)
        
        pos = sj.calc_pos(read_length=50)        
        exp = np.arange(26, 50)
        assert(np.all(pos == exp))
    
    def test_get_splice_graph_profile(self):
        fpath = join(TEST_DATA_DIR, 'reads.MD.bam')
        bamfile = load_bamfile(fpath)
        
        # Test with number of positions with easy coordinates
        exons = [Exon(chrom='chr18', starts=[5], ends=[20, 25], strand='+'),
                 Exon(chrom='chr18', starts=[50], ends=[60, 80], strand='+'),
                 Exon(chrom='chr18', starts=[100], ends=[105, 120], strand='+')]
        gene = Gene(gene_id='gene1', exons=exons)
        sg = gene.calc_splice_graph()
        counts = sg.calc_mapping_profile(bamfile, read_length=10)
        sj = counts.groupby(['sj_id'])['sj_id'].count()
        assert(sj.shape[0] == 8)
        assert(np.all(sj == 9))
        
        counts = sg.calc_mapping_profile(bamfile, read_length=20)
        sj = counts.groupby(['sj_id'])['sj_id'].count()
        assert(sj.shape[0] == 8)
        assert(np.all(sj == [15, 15, 5, 19, 9, 10, 10, 19]))
        
        # With real coordinates
        exon = Exon(chrom='chr18', starts=[321807], ends=[371666], strand='+')
        gene = Gene(gene_id='gene1', exons=[exon])
        gene.get_exons(bamfile=bamfile)
        sg = gene.calc_splice_graph()
        counts = sg.calc_mapping_profile(bamfile, read_length=101)
        sj = counts.groupby(['sj_id'])['sj_id'].count()
        assert(sj.shape[0] == 58)
        assert(sj.min() == 97)
        assert(counts['counts'].sum() == 3698)
        
    def test_get_SJ_reads_genome(self):
        fpath = join(TEST_DATA_DIR, 'reads.MD.bam')
        bamfile = load_bamfile(fpath)
        
        class GenomeHack(Genome):
            def __init__(self):
                exon = Exon(chrom='chr18', starts=[321807],
                            ends=[371666], strand='+')
                gene = Gene(gene_id='gene1', exons=[exon])
                gene.get_exons(bamfile=bamfile)
                self.whole_gene = True
                self._genes = [gene]
            
            @property
            def genes(self):
                return(self._genes)
        
        genome = GenomeHack()
        profiles = genome.calc_mapping_profile(bamfile, read_length=101)
        
        with NamedTemporaryFile('w') as fhand:
            genome.write_mapping_profiles(profiles, fhand)
            fhand.flush()
            counts = pd.read_csv(fhand.name, sep='\t').reset_index()
            sj = counts.groupby(['sj_id'])['sj_id'].count()
            assert(sj.shape[0] == 58)
            assert(sj.min() == 97)
            assert(counts['counts'].sum() == 3698)
    
    def test_get_SJ_reads_genome_bin(self):
        bam_fpath = join(TEST_DATA_DIR, 'test.bam')
        db_fpath = join(TEST_DATA_DIR, 'cel.gtf.db')
        bin_fpath = join(BIN_DIR, 'extract_sj_counts.py')
        
        with NamedTemporaryFile('w') as fhand:
            out_fpath = fhand.name
            cmd = ['python', bin_fpath, db_fpath, bam_fpath, '-o', 
                   out_fpath, '-r', '150']
            check_call(cmd)
            counts = pd.read_csv(out_fpath, sep='\t')
            assert(counts.shape == (2218, 4))
            assert(counts['counts'].sum() == 1969)
    
    def test_sj_profile_qc(self):
        fpath = join(TEST_DATA_DIR, 'SRR7155123.counts.tsv')
        p = SpliceJunctionProfiles()
        p.read(fpath)
        p.sample(n=15)
        p.calc_overdispersion()
        p.infer_sj_coverage()
        
        with NamedTemporaryFile('w', suffix='.png') as fhand:
            fpath = fhand.name
            p.plot_summary(fpath)
        

class ExonCountsTests(unittest.TestCase):
    def test_random_primers_dG(self):
        bin_fpath = join(BIN_DIR, 'calc_random_primers_mfe.py')
        cmd = ['python', bin_fpath]
        check_call(cmd)
    
    def test_fragment_length(self):
        exons_pos = [[1,2,3,4,5], [10,11,12,13,14], [30,31,32,32,34,35,36,37],
                     [101,102,103,104,105], [110,111,112,113,114],
                     [130,131,132,132,134,135,136,137]]
        r1_pos = [3,4,5,10,11,12]
        r2_pos = [101,102,103,104,105]
        
        # In full transcript
        transcript = [True] * 6
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(f_len == 21)
        assert(d_s == 2)
        assert(d_e == 13)
        
        # With exon 3 skipping
        transcript = [True, True, False, True, True, True]
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(f_len == 13)
        assert(d_s == 2)
        assert(d_e == 13)
        
        # With exon 2 skipping: fragment can not be generated
        transcript = [True, False, True, True, True, True]
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(f_len == 0)
        assert(d_s == 0)
        assert(d_e == 0)
        
        # With overlapping reads
        r1_pos = [3,4,5,10,11,12]
        r2_pos = [10,11,12,13,14,30]
        transcript = [True] * 6
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(f_len == 9)
        assert(d_s == 2)
        assert(d_e == 25)
        transcript = transcript = [True, True, True, False, True, True]
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(d_e == 20)
        transcript = transcript = [True, True, False, True, True, True]
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(f_len == 0)
        
        # With a single read from PE merging
        r1_pos = [3,4,5,10,11,12,13,14,30]
        r2_pos = None
        transcript = [True] * 6
        t_pos = get_transcript_pos(exons_pos, transcript)
        f_len, d_s, d_e = calc_fragment_length(r1_pos, r2_pos, t_pos)
        assert(f_len == 9)
        assert(d_s == 2)
        assert(d_e == 25)
    
    def test_get_exon_reads(self):
        fpath = join(TEST_DATA_DIR, 'reads.MD.bam')
        bamfile = load_bamfile(fpath)
        inc_sj = [('chr18', 333144, 480706), ('chr18', 331778, 480706),
                  ('chr18', 321808, 480706),('chr18', 480758, 500507),
                  ('chr18', 335231, 480706), ('chr18', 320065, 480706),
                  ('chr18', 348164, 480706), ('chr18', 357523, 480706),
                  ('chr18', 347342, 480706)]
        skp_sj = [('chr18', 331778, 500507), ('chr18', 321808, 500507),
                  ('chr18', 320065, 500507), ('chr18', 357523, 500507),
                  ('chr18', 348164, 500507), ('chr18', 347342, 500507),
                  ('chr18', 333144, 500507), ('chr18', 335231, 500507)]
        
        counts = get_SE_counts(inc_sj, skp_sj, 8, bamfile, indexed_1=True)
        assert(counts['inc_reads'] == 1447)
        assert(counts['skp_reads'] == 299)
        assert(counts['inc_reads_no_dups'] == 1344)
        assert(counts['skp_reads_no_dups'] == 279)
        
        detected_sj = set(['chr18:333144-480706', 'chr18:331778-480706',
                           'chr18:331778-500507'])
        counts = get_SE_counts(inc_sj, skp_sj, 8, bamfile, detected_sj,
                               indexed_1=True)
        assert(counts['inc_reads'] == 0)
        assert(counts['skp_reads'] == 0)
        
        detected_sj = set(['chr18:480758-500507', 'chr18:357523-480706',
                           'chr18:357523-500507'])
        counts = get_SE_counts(inc_sj, skp_sj, 8, bamfile, indexed_1=True)
        assert(counts['inc_reads'] == 1447)
        assert(counts['skp_reads'] == 299)
    
    def test_get_exon_reads_bin(self):
        fpath = join(TEST_DATA_DIR, 'reads.MD.bam')
        exons_sj_fpath = join(TEST_DATA_DIR, 'exons_sj.csv')
        bin_fpath = join(BIN_DIR, 'extract_exon_counts.py')
        out_fpath = join(TEST_DATA_DIR,'exon_counts.csv')
        cmd = ['python', bin_fpath, fpath, '-e', exons_sj_fpath, '-O', '8', 
               '-o', out_fpath]
        check_call(cmd)
    
    def test_psi_inference_evaluation(self):
        real_psi_fpath = join(TEST_DATA_DIR, 'real_psi.csv')
        inferred_psi_fpath = join(TEST_DATA_DIR, 'inferred_psi.csv')
        counts_fpath = join(TEST_DATA_DIR, 'exon_counts_psi.csv')
        gc_fpath = join(TEST_DATA_DIR, 'gc_bias.csv')
        exon_dist_fpath = join(TEST_DATA_DIR, 'exon_distances.csv')
        out_fpath = join(TEST_DATA_DIR, '{}.png'.format(inferred_psi_fpath))
        
        psi = pd.read_csv(inferred_psi_fpath, index_col='exon_id')
        real_psi = pd.read_csv(real_psi_fpath, index_col='exon_id')
        counts = pd.read_csv(counts_fpath, index_col='exon_id')
        gc_data = pd.read_csv(gc_fpath, index_col='exon_id')
        exon_dist = pd.read_csv(exon_dist_fpath, index_col='exon_id')
        merged = merge_inferred_and_real_psi(psi, real_psi, counts, gc_data,
                                             exon_dist)
        plot_psi_inference_evaluation(merged, out_fpath)
    
    def test_psi_inference_evaluation_bin(self):
        real_psi_fpath = join(TEST_DATA_DIR, 'real_psi.csv')
        inferred_psi_fpath = join(TEST_DATA_DIR, 'inferred_psi.csv')
        counts_fpath = join(TEST_DATA_DIR, 'exon_counts_psi.csv')
        gc_fpath = join(TEST_DATA_DIR, 'gc_bias.csv')
        exon_dist_fpath = join(TEST_DATA_DIR, 'exon_distances.csv')
        out_fpath = join(TEST_DATA_DIR, '{}.png'.format(inferred_psi_fpath))
        
        bin_fpath = join(BIN_DIR, 'evaluate_psi_inference.py')
        cmd = ['python', bin_fpath, inferred_psi_fpath, '-r', real_psi_fpath,
                '-c', counts_fpath, '-d', exon_dist_fpath, '-g', gc_fpath, 
               '-o', out_fpath]
        check_call(cmd)
        

if __name__ == '__main__':
    import sys;sys.argv = ['', 'SJCountsTests.test_get_SJ_reads_genome_bin']
    unittest.main()
