#!/usr/bin/env python
import unittest
from tempfile import NamedTemporaryFile
from os.path import join
from subprocess import check_call

from pysam import FastaFile
from scipy.special._basic import comb
import numpy as np
import pandas as pd

from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
from AS_quant.genome import (get_annotation_db, Genome, get_sj_index,
                             get_genome_fastafile, reverse_complement, GeneDB,
                             Exon, Gene, SpliceGraph)

class GenomeTests(unittest.TestCase):
    def test_load_annotation_db(self):
        gtf_fpath = join(TEST_DATA_DIR, 'test.gtf')
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        
        db = get_annotation_db(db_fpath, gtf_fpath, force=True)
        gene_ids = ['L3MBTL4', 'L3MBTL4-AS1']
        for gene, exp_gene_id in zip(db.all_features(featuretype='gene'),
                                     gene_ids):
            assert(gene.id == exp_gene_id)
    
    def test_genbank_db(self):
        # With genbank GFF format
        gtf_fpath = join(TEST_DATA_DIR, 'genbank.fixed.gff')
        db_fpath = join(TEST_DATA_DIR, 'genbank.fixed.db')
        
        db = get_annotation_db(db_fpath, gtf_fpath, force=True)
        for gene in db.all_features(featuretype='gene'):
            exons = [x for x in db.children(gene, featuretype='exon')]
            assert(len(exons) > 0)
        
        # Without fixing
        gtf_fpath = join(TEST_DATA_DIR, 'genbank.gff')
        db_fpath = join(TEST_DATA_DIR, 'genbank.db')
        db = get_annotation_db(db_fpath, gtf_fpath, force=True)
        for gene in db.all_features(featuretype='gene'):
            exons = [x for x in db.children(gene, featuretype='exon')]
            assert(len(exons) > 0)
        
        # Get item
        genome = Genome(db)
        gene = genome.get_gene('110210974')
        exons = gene.exons
        assert(len(exons) == 5)
            
        # With some lines without Dbxref
        gtf_fpath = join(TEST_DATA_DIR, 'genbank2.gff')
        db_fpath = join(TEST_DATA_DIR, 'genbank2.db')
        db = get_annotation_db(db_fpath, gtf_fpath, force=True)
        for gene in db.all_features(featuretype='gene'):
            exons = [x for x in db.children(gene, featuretype='exon')]
            assert(len(exons) > 0)
    
    def test_longest_transcript(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        gene = GeneDB(db['L3MBTL4'], db)
        longest_tx = gene.longest_transcript(db)
        assert(longest_tx[0] == 'NR_158611')
        
    def test_gene_seq(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        
        gene = GeneDB(db['L3MBTL4'], db, whole_gene=False)
        assert(len(gene.exons) == 17)
        
        gene = GeneDB(db['L3MBTL4'], db, whole_gene=True)
        assert(len(gene.exons) == 25)
    
    def test_genome_get_p_reads_sj(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        genome = Genome(db)
        exon_df = genome.get_exon_length_df()
        p = genome.calc_sj_per_read(exon_df, 100, 8)
        assert(p == 0.2213607806442989)
        
        expression = pd.Series([0.2, 0.8], index=['L3MBTL4', 'L3MBTL4-AS1'])
        p = genome.calc_sj_per_read(exon_df, 100, 8, gene_expression=expression)
        assert(p == 0.16570356472795497)
        
        p = genome.calc_sj_per_read(exon_df, 150, 8)
        assert(p == 0.3446687440726189)
    
    def test_genome_get_exon_length_df(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        genome = Genome(db)
        df = genome.get_exon_length_df()
        lengths = np.array([2347, 59, 103, 55, 105, 136, 92, 155, 77, 86, 111,
                            115, 103, 174, 170, 63, 1682, 130, 143, 1775])
        assert(df.shape[0] == 20)
        assert(np.all(df['exon_length'] == lengths))
    
    def test_genome_get_p_reads_sj_large_genome(self):
        db_fpath = join(TEST_DATA_DIR, 'Homo_sapiens.GRCh38.92.gtf.db')
        db = get_annotation_db(db_fpath)
        genome = Genome(db, only_cds=True)
        df = genome.get_exon_length_df()
        print(df)
        
        read_lengths = np.arange(36, 300)
        p = []
        for r in read_lengths:
            print(r)
            p.append(genome.calc_sj_per_read(df, r, 8))
            print(p[-1])
            data = pd.DataFrame({'r': read_lengths[:len(p)], 'p': p})
            data.to_csv(join(TEST_DATA_DIR, 'read_lengths_p_sj.csv'))
    
    def test_exon_split(self):
        exon = Exon(chrom='chr1', starts=[10, 15], ends=[30, 35], strand='+')
        segments = list(exon.split())
        assert(len(segments) == 3)
        expected_ids = ['chr1:10-15+', 'chr1:15-30+', 'chr1:30-35+']
        assert(np.all([x.id for x in segments] == expected_ids))
        assert(segments[0].has_donor == False and segments[0].has_acceptor)
        assert(segments[1].has_donor and segments[1].has_acceptor)
        assert(segments[2].has_donor and segments[2].has_acceptor == False)
        
        exon = Exon('1', [10, 20], [30, 40], '+')
        fragments = [f.id for f in exon.split()]
        assert(fragments == ['1:10-20+', '1:20-30+', '1:30-40+'])
        
        fragments = [f.id for f in exon.split(is_first=True)]
        assert(fragments == ['1:10$20-30+', '1:30-40+'])
        
        fragments = [f.id for f in exon.split(is_last=True)]
        assert(fragments == ['1:10-20+', '1:20-30$40+'])
    
    def test_exon_splice_site_seqs(self):
        fasta_fpath = join(TEST_DATA_DIR, 'exon_seq_test.fa')
        fastafile = FastaFile(fasta_fpath)
        
        exon = Exon('chr1', [12], [17], '+')
        donors = exon.get_5ss_seqs(fastafile)
        assert(donors == ['CCCCGTGGGG'])
        acceptors = exon.get_3ss_seqs(fastafile)
        assert(acceptors == ['TTTTTTTAGCCC'])
        
        exon = Exon('chr1', [12], [17], '-')
        donors = exon.get_5ss_seqs(fastafile)
        assert(donors == ['GGGGCTAAAA'])
        acceptors = exon.get_3ss_seqs(fastafile)
        assert(acceptors == ['CCCCCCCACGGG'])
    
    
    def test_get_sg_nodes(self):
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30, 35], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = SpliceGraph()
        nodes = [n[1] for n in sg.calc_sg_nodes(gene)]
        expected_ids = ['chr1:0-5+', 'chr1:10-15+', 'chr1:15-30+',
                        'chr1:30-35+', 'chr1:50-60+']
        expected_acceptors = [False, True, True, False, True]
        expected_donors = [True, False, True, True, False]
        
        assert(np.all([n.id for n in nodes] == expected_ids))
        assert(np.all([n.has_acceptor for n in nodes] == expected_acceptors))
        assert(np.all([n.has_donor for n in nodes] == expected_donors))
    
    def test_get_sg(self):
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30, 35], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        
        expected_nodes = ['chr1:0-5+', 'chr1:10-15+', 'chr1:15-30+',
                          'chr1:30-35+', 'chr1:50-60+']
        assert(np.all(sg.nodes_ids == expected_nodes))
        
        expected_edges = ['chr1:5-10+', 'chr1:5-15+', 'chr1:5-50+',
                          'chr1:15-15+', 'chr1:30-30+', 'chr1:30-50+',
                          'chr1:35-50+']
        assert(np.all(sg.edges_ids == expected_edges))
        
        expected_graph = {0: [1, 2, 4], 1: [2], 2: [3, 4], 3: [4]}
        for k, v in sg.graph.items():
            assert(v == expected_graph[k])
    
    def test_calc_sg_genedb(self):
        db_fpath = join(TEST_DATA_DIR, 'cel.gtf.db')
        db = get_annotation_db(db_fpath)
        genome = Genome(db, whole_gene=True)
        gene = genome.get_gene('WBGene00002061')
        sg = gene.calc_splice_graph()
        expected = ['V:6587$6588$6592-6754-', 'V:6912-7110-', 'V:7157-7384-',
                    'V:7384-7390-', 'V:7390-7393-', 'V:7432-7609-',
                    'V:7650-7822-']
        assert(sg.nodes_ids == expected)
    
    def test_sg_calc_sj_mu(self):
        # With alternative acceptor
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        nodes_psi = [1, 0.5, 0.9, 1]
        sj_mu = sg.calc_sj_mu(nodes_psi)
        expected = [0.5, 0.45, 0.05, 0.5, 0.95]
        assert(np.allclose(sj_mu, expected))
        
        # With alternative acceptor and donor
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30, 35], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        nodes_psi = [1, 0.5, 0.9, 0.5, 1]
        sj_mu = sg.calc_sj_mu(nodes_psi)
        expected = [0.5, 0.45, 0.05, 0.5, 0.475, 0.475, 0.475]
        assert(np.allclose(sj_mu, expected))
        
        # With several alternative acceptors
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15, 20], ends=[30], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        nodes_psi = [1, 0.5, 0.5, 0.5, 1]
        sj_mu = sg.calc_sj_mu(nodes_psi)
        expected = [0.5, 0.25, 0.125, 0.125, 0.5, 0.75, 0.875]
        assert(np.allclose(sj_mu, expected))
    
    def test_sg_calc_sj_psi(self):
        # With alternative acceptor
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        nodes_psi = [1, 0.5, 0.9, 1]
        sg.calc_sj_mu(nodes_psi)
        sj_psi = sg.calc_sj_psi()
        expected = [0.5, 0.45, 0.05, 1, 1]
        assert(np.allclose(sj_psi, expected))
    
    def test_sg_get_transcripts(self):
        # Simple case
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        nodes_psi = [1, 0.5, 0.9, 1]
        transcripts = sg.calc_tx_quant(nodes_psi=nodes_psi)
        assert(len(transcripts) == 3)
        assert(np.sum([x[1] for x in transcripts]) == 1)
        
        # More complex scenario
        exons = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+'),
                 Exon(chrom='chr1', starts=[10, 15], ends=[30, 35], strand='+'),
                 Exon(chrom='chr1', starts=[50], ends=[60], strand='+')]
        gene = Gene(gene_id='g1', exons=exons)
        sg = gene.calc_splice_graph()
        nodes_psi = [1, 0.5, 0.9, 0.5, 1]
        transcripts = sg.calc_tx_quant(nodes_psi=nodes_psi)
        assert(len(transcripts) == 5)
        assert(np.sum([x[1] for x in transcripts]) == 1)
        
    def test_sg_tx_seq(self):
        es = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+', seq='A'*5),
              Exon(chrom='chr1', starts=[10, 15], ends=[20], strand='+',
                   seq='T' * 5 + 'C' * 5),
              Exon(chrom='chr1', starts=[50], ends=[60], strand='+', seq='G'*10)]
        gene = Gene(gene_id='g1', exons=es)
        sg = gene.calc_splice_graph()
        seq = sg.get_tx_seq([0, 1, 2, 3])
        assert(seq == 'AAAAATTTTTCCCCCGGGGGGGGGG')
        
        seq = sg.get_tx_seq([0, 2, 3])
        assert(seq == 'AAAAACCCCCGGGGGGGGGG')
        
        seq = sg.get_tx_seq([0, 3])
        assert(seq == 'AAAAAGGGGGGGGGG')
    
    def test_sg_generate_tx_seq_quant(self):
        es = [Exon(chrom='chr1', starts=[0], ends=[5], strand='+', seq='A'*5),
              Exon(chrom='chr1', starts=[10, 15], ends=[20], strand='+',
                   seq='T' * 5 + 'C' * 5),
              Exon(chrom='chr1', starts=[50], ends=[60], strand='+', seq='G'*10)]
        gene = Gene(gene_id='g1', exons=es)
        sg = gene.calc_splice_graph()
        psi = [1, 0.5, 0.5, 1]
        expected = {'g1.0': 2.5, 'g1.1': 1.25, 'g1.2': 1.25}
        for tx, mu, _ in sg.generate_tx_seq_quant(nodes_psi=psi, gene_expr=5):
            assert(mu == expected[tx])
    
    def test_get_splice_junctions(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        gene = GeneDB(db['L3MBTL4'], db)
        sjs = list(gene.get_all_splice_junctions())
        assert(len(sjs) == comb(17, 2))
        assert(sjs[0].id == 'chr18:5956387-5960093-')
        assert(sjs[-1].id == 'chr18:6312056-6356683-')
    
    def test_get_exon_distances(self):
        gtf_fpath = join(TEST_DATA_DIR, 'test.short.gtf')
        db_fpath = join(TEST_DATA_DIR, 'test.short.db')
        db = get_annotation_db(db_fpath, gtf_fpath)
        gene = GeneDB(db['L3MBTL4'], db, whole_gene=True)
        expected_dist = {'chr18:5954705-5956387-': {'d_start': 0, 'd_end': 410},
                         'chr18:5960093-5960156$5960159-': {'d_start': 1682, 'd_end': 344},
                         'chr18:5969392-5969562-': {'d_start': 1748, 'd_end': 174},
                         'chr18:6093354-6093528-': {'d_start': 1918, 'd_end': 0}}
        for record in gene.calc_exons_dist_to_ends():
            expected = expected_dist[record['exon_id']]
            for k, v in expected.items():
                assert(record[k] == v)
    
    def test_get_exon_splice_junctions(self):
        gtf_fpath = join(TEST_DATA_DIR, 'test.short.gtf')
        db_fpath = join(TEST_DATA_DIR, 'test.short.db')
        db = get_annotation_db(db_fpath, gtf_fpath)
        
        gene = GeneDB(db['L3MBTL4'], db, whole_gene=True)
         
        list(gene.get_all_splice_junctions())
        exons_sj = gene.get_exons_splice_junctions()
        
        inc_sjs = {'chr18:5954705-5956387-': ('chr18:5956387-5960093-',
                                              'chr18:5956387-5969392-',
                                              'chr18:5956387-6093354-'),
                   'chr18:5960093-5960156$5960159-':
                                             ('chr18:5956387-5960093-',
                                              'chr18:5960156-5969392-',
                                              'chr18:5960156-6093354-',
                                              'chr18:5960159-5969392-',
                                              'chr18:5960159-6093354-'),
                   'chr18:5969392-5969562-': ('chr18:5956387-5969392-',
                                              'chr18:5960156-5969392-',
                                              'chr18:5960159-5969392-',
                                              'chr18:5969562-6093354-'),
                   'chr18:6093354-6093528-': ('chr18:5956387-6093354-',
                                              'chr18:5960156-6093354-',
                                              'chr18:5960159-6093354-',
                                              'chr18:5969562-6093354-')}
        
        skp_sjs = {'chr18:5954705-5956387-': (),
                   'chr18:5960093-5960156$5960159-': ('chr18:5956387-5969392-',
                                                      'chr18:5956387-6093354-'),
                   'chr18:5969392-5969562-': ('chr18:5956387-6093354-',
                                              'chr18:5960156-6093354-',
                                              'chr18:5960159-6093354-'),
                   'chr18:6093354-6093528-': ()}
        
        
        for exon_id, inc, skp in exons_sj:
            assert(len(inc) == len(inc_sjs[exon_id]))
            for inc_sj in inc:
                assert(inc_sj in inc_sjs[exon_id]) 
                 
            assert(len(skp) == len(skp_sjs[exon_id]))
            for skp_sj in skp:
                assert(skp_sj in skp_sjs[exon_id])
    
    def test_get_new_exon_splice_junctions(self):
        db_fpath = join(TEST_DATA_DIR, 'test.short.db')
        db = get_annotation_db(db_fpath)
        gene = GeneDB(db['L3MBTL4'], db, whole_gene=True)
        list(gene.get_all_splice_junctions())
        
        # With new junctions. Report only those that are found in SJ index
        ''' New Junctions
        chr18:5956395-5969392-
        chr18:5960209-6093354-
        chr18:5960157-5969372-
        chr18:5960151-6091354-  Not overlapping
        '''
        sj_fpath = join(TEST_DATA_DIR, 'test.short.SJ.tab.gz')
        sj_index = get_sj_index(sj_fpath)
        exons_sj = gene.get_exons_splice_junctions(sj_index=sj_index,
                                                   window_size=100)
        
        inc_sjs = {'chr18:5954705-5956387-': ('chr18:5956387-5960093-',
                                              'chr18:5956387-5969392-',
                                              'chr18:5956387-6093354-',
                                              'chr18:5956394-5969392-'),
                                              
                   'chr18:5960093-5960156$5960159-': ('chr18:5956387-5960093-',
                                                      'chr18:5960156-5969392-',
                                                      'chr18:5960156-6093354-',
                                                      'chr18:5960156-5969372-',
                                                      'chr18:5960208-6093354-'),
                   
                   'chr18:5969392-5969562-': ('chr18:5956387-5969392-',
                                              'chr18:5960156-5969392-',
                                              'chr18:5969562-6093354-',
                                              'chr18:5956394-5969392-',
                                              'chr18:5960156-5969372-'),
                   
                   'chr18:6093354-6093528-': ('chr18:5956387-6093354-',
                                              'chr18:5960156-6093354-',
                                              'chr18:5969562-6093354-',
                                              'chr18:5960208-6093354-')}
        
        skp_sjs = {'chr18:5954705-5956387-': (),
                   'chr18:5960093-5960156$5960159-': ('chr18:5956387-5969392-',
                                                      'chr18:5956387-6093354-',
                                                      'chr18:5956394-5969392-'),
                   
                   'chr18:5969392-5969562-': ('chr18:5956387-6093354-',
                                              'chr18:5960156-6093354-',
                                              'chr18:5960208-6093354-'),
                   'chr18:6093354-6093528-': ()}
        
        
        for exon_id, inc, skp in exons_sj:
            assert('chr18:5960150-6091354-' not in inc)
            assert('chr18:5960150-6091354-' not in skp)
            
            for inc_sj in inc:
                assert(inc_sj in inc_sjs[exon_id])
            assert(len(inc) == len(inc_sjs[exon_id])) 

            for skp_sj in skp:
                assert(skp_sj in skp_sjs[exon_id])
            assert(len(skp) == len(skp_sjs[exon_id]))
    
    def test_genome_write_splice_junctions(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        genome = Genome(db)
        with NamedTemporaryFile('w') as fhand:
            genome.write_splice_junctions_STAR(fhand)
    
    def test_genome_write_exon_splice_junctions(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        sj_fpath = join(TEST_DATA_DIR, 'test.short.SJ.tab.gz')
        db = get_annotation_db(db_fpath)
        sj_index = get_sj_index(sj_fpath)
        genome = Genome(db)
        with NamedTemporaryFile('w') as fhand:
            genome.write_exons_splice_junctions(fhand)
            
        with NamedTemporaryFile('w') as fhand:
            genome.write_exons_splice_junctions(fhand, sj_index=sj_index)
    
    def test_genome_write_exon_distances(self):
        db_fpath = join(TEST_DATA_DIR, 'test.db')
        db = get_annotation_db(db_fpath)
        genome = Genome(db)
        with NamedTemporaryFile('w') as fhand:
            genome.write_exons_distances(fhand)
    
    def test_build_gtf_db_bin(self):
        gtf_fpath = join(TEST_DATA_DIR, 'test.gtf')
        bin_fpath = join(BIN_DIR, 'build_gtf_db.py')
        out_fpath = join(TEST_DATA_DIR, 'test.gtf.db')
        cmd = ['python', bin_fpath, gtf_fpath, '-o', out_fpath, '-f']
        check_call(cmd)
    
    def test_gtf_to_sj_bin(self):
        db_fpath = join(TEST_DATA_DIR, 'test.gtf.db')
        bin_fpath = join(BIN_DIR, 'gtf_to_sj.py')
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name]
            check_call(cmd)
            
        db_fpath = join(TEST_DATA_DIR, 'no_transcript.gff.db')
        bin_fpath = join(BIN_DIR, 'gtf_to_sj.py')
        with NamedTemporaryFile('w') as fhand:
            out_fpath = fhand.name
            cmd = ['python', bin_fpath, db_fpath, '-o', out_fpath]
            check_call(cmd)
    
    def test_calc_exons_distances_bin(self):
        db_fpath = join(TEST_DATA_DIR, 'test.gtf.db')
        bin_fpath = join(BIN_DIR, 'calc_exons_distances_to_ends.py')
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name]
            check_call(cmd)
    
    def test_extract_exons_sj_bin(self):
        db_fpath = join(TEST_DATA_DIR, 'test.gtf.db')
        bin_fpath = join(BIN_DIR, 'extract_exons_sj.py')
        sj_fpath = join(TEST_DATA_DIR, 'test.short.SJ.tab.gz')
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name]
            check_call(cmd)
            
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name,
                   '-s', sj_fpath]
            check_call(cmd)
            
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name,
                   '-s', sj_fpath, '--whole_gene']
            check_call(cmd)
        
        db_fpath = join(TEST_DATA_DIR, 'no_transcript.gff.db')
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name]
            check_call(cmd)
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name,
                   '--whole_gene']
            check_call(cmd)
        
        db_fpath = join(TEST_DATA_DIR, 'test2.gff.db')
        bin_fpath = join(BIN_DIR, 'extract_exons_sj.py')
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name]
            check_call(cmd)
    
    def test_get_intervals_seq(self):
        exon = Exon(chrom='chr1', starts=[10], ends=[20], strand='+')
        seq = 'AAAATTTTCC'
        interval_seq = exon.get_intervals_seq([10, 14, 18, 20], seq=seq,
                                              sep='-', min_pos=10)
        assert(interval_seq == 'AAAA-TTTT-CC')
        
    def test_get_exon_seq(self):
        fasta_fpath = join(TEST_DATA_DIR, 'chimp.fasta')
        fastafile = get_genome_fastafile(fasta_fpath)
        exon = Exon(chrom='3', starts=[10, 14], ends=[20], strand='+')
        seq = exon.fetch_sequence(fastafile)
        assert(seq == 'CGCC{GGCGCT')
    
    def test_transcript_seqs(self):
        db_fpath = join(TEST_DATA_DIR, 'chimp.gtf.db')
        fasta_fpath = join(TEST_DATA_DIR, 'chimp.fasta')
        db = get_annotation_db(db_fpath)
        fastafile = get_genome_fastafile(fasta_fpath) 
        genome = Genome(db, fastafile=fastafile)
        gene = genome.get_gene(gene_id='ENSPTRG00000014558')
        seq1 = gene.tx_seq
        gene.strand = '-'
        gene.fetch_transcript_seq(db, fastafile)
        seq2 = gene.tx_seq
        assert(reverse_complement(seq1) == seq2)
        assert(seq1.count('|') == 26)
        n_alt2 = seq1.count('{') + seq1.count('}')
        assert(n_alt2 == 0)
        assert(len(gene.tx_seq) == 7560)
        
    def test_gene_seqs(self):
        db_fpath = join(TEST_DATA_DIR, 'chimp.gtf.db')
        fasta_fpath = join(TEST_DATA_DIR, 'chimp.fasta')
        db = get_annotation_db(db_fpath)
        fastafile = get_genome_fastafile(fasta_fpath) 
        genome = Genome(db, fastafile=fastafile, whole_gene=True)
        gene = genome.get_gene(gene_id='ENSPTRG00000014558')
        gene.fetch_seq(db, fastafile)
        n_alt2 = gene.tx_seq.count('{') + gene.tx_seq.count('}')
        assert(n_alt2 == 4)
        assert(gene.tx_seq.count('|') == 28)
        assert(len(gene.tx_seq) > 7560)
    
    def test_get_exon_data(self):
        db_fpath = join(TEST_DATA_DIR, 'chimp.gtf.db')
        fastafile = FastaFile(join(TEST_DATA_DIR, 'chimp.fasta'))
        db = get_annotation_db(db_fpath)
        genome = Genome(db, only_cds=True, fastafile=fastafile)
        for gene in genome.genes:
            for exon_data in gene.get_exon_data(fastafile=fastafile):
                l1 = exon_data['upstream_intron_length']
                l2 = exon_data['downstream_intron_length']
                assert(exon_data['3ss'] is None or exon_data['3ss'][7:9] == 'AG')
                assert(exon_data['5ss'] is None or exon_data['5ss'][4:6] == 'GT')
                assert(l1 != l2)
        
        with NamedTemporaryFile('w') as fhand:
            genome.write_exon_data(fhand)
    
    def test_get_exon_data_bin(self):
        db_fpath = join(TEST_DATA_DIR, 'chimp.gtf.db')
        bin_fpath = join(BIN_DIR, 'extract_exon_data.py')
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, db_fpath, '-o', fhand.name,
                   '--only_cds']
            check_call(cmd)


if __name__ == '__main__':
    import sys;sys.argv = ['', 'GenomeTests.test_genome_get_p_reads_sj_large_genome']
    unittest.main()
