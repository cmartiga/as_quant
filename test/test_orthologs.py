#!/usr/bin/env python
import unittest
from subprocess import check_call
from tempfile import TemporaryDirectory, NamedTemporaryFile
from os.path import join

import networkx as nx
import numpy as np
import pandas as pd
import dendropy

from AS_quant.orthologs import (nucleotide_alignment, translate_alignment,
                                ExonCompScorer, run_msa, GenomeSet, GeneSet,
                                OrthologousExons, MetaExonGraph)
from AS_quant.utils import dendropy2ete
from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
from AS_quant.genome import parse_fasta_lines


def _get_exon_id(gene_sets):
    exon_ids = []
    prev_gene = None
    n = 0
    for gid in gene_sets:
        if prev_gene is None or prev_gene == gid:
            n += 1 
        else:
            n = 0
        exon_id = '{}.{}'.format(gid, n)
        prev_gene = gid
        exon_ids.append(exon_id)
    return(exon_ids)


def get_orthologs_df(n_genes=None, exons_per_gene_mean=None, n_species=4,
                     species_names=None):
    if n_genes is not None and exons_per_gene_mean is not None:
        genes = np.arange(n_genes)
        n_exons = np.random.poisson(exons_per_gene_mean, size=n_genes)
        gene_ids = np.hstack([[gene_id] * n_exon
                              for gene_id, n_exon in zip(genes, n_exons)])
        data = {'gene_set_id': gene_ids}
    else:
        data = {'gene_set_id': [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2]}
        
    if species_names is None:
        species_names = ['sp{}'.format(i) for i in range(n_species)]
        
    data['exon_set_id'] = _get_exon_id(data['gene_set_id'])
    data = pd.DataFrame(data).set_index('exon_set_id')
    
    for sp in species_names:
        strand = np.random.choice(['+', '-'])
        exon_sizes = np.random.poisson(120, size=data.shape[0])
        intron_sizes = np.random.poisson(1000, size=data.shape[0])
        starts = [np.sum(intron_sizes[:i]) + np.sum(exon_sizes[:i-1])
                  for i in range(data.shape[0])]
        ends = starts + exon_sizes
        chrom = 1
        exons = ['{}-{}:{}-{}{}'.format(sp, chrom, start, end, strand)
                    for start, end in zip(starts, ends)]
        if strand == '-':
            exons = exons[::-1]
        data[sp] = exons
    return(data) 


class OrthologsTests(unittest.TestCase):
    def test_genome_set(self):
        orthologs_fpath = join(TEST_DATA_DIR, 'orthologs.csv')
        genome_data_fpath = join(TEST_DATA_DIR, 'genome.data.csv')
        orthologs = pd.read_csv(orthologs_fpath, index_col=0)
        genome_data = pd.read_csv(genome_data_fpath, index_col=0)
        genome_data = genome_data.to_dict(orient='index')
        genome_set = GenomeSet(orthologs)
        genome_set.build(genome_data, fastafiles_dir=TEST_DATA_DIR,
                         annotation_dbs_dir=TEST_DATA_DIR)
        
        assert(np.all(genome_set.species == ['bonobo', 'chimp']))
        gene_set = list(genome_set.gene_orthologs_iter())[0]
        gene = gene_set['bonobo']
        assert(gene_set.id == 'ENSG00000134121_CHL1')
        seq = gene.tx_seq
        n_alt = seq.count('{') + seq.count('}')
        n_introns = seq.count('|')
        assert(n_alt == 0)
        assert(len(seq) == 7578)
        assert(n_introns == 26)
        
        # Use whole gene option
        genome_set = GenomeSet(orthologs, whole_gene=True)
        genome_set.build(genome_data, fastafiles_dir=TEST_DATA_DIR,
                         annotation_dbs_dir=TEST_DATA_DIR)
        gene_set = list(genome_set.gene_orthologs_iter())[0]
        gene = gene_set['bonobo']
        assert(gene_set.id == 'ENSG00000134121_CHL1')
        seq = gene_set['bonobo'].tx_seq
        n_alt = seq.count('{') + seq.count('}')
        n_introns = seq.count('|')
        assert(n_alt == 2)
        assert(len(seq) == 7686)
        assert(n_introns == 27)
    
    def test_genome_set_fasta_bin(self):
        orthologs_fpath = join(TEST_DATA_DIR, 'orthologs.csv')
        genome_data_fpath = join(TEST_DATA_DIR, 'genome.data.csv')
        bin_fpath = join(BIN_DIR, 'extract_orthologous_transcripts_seqs.py')
        
        exp_ids = ['chimp ENSPTRG00000014558 3:271559-271639+^3:346676-346861+^3:352942-353048+^3:355151-355339+^3:367769-367892+^3:368888-369059+^3:369961-370009+^3:371568-371689+^3:376306-376491+^3:381607-381739+^3:387234-387375+^3:388648-388760+^3:390151-390318+^3:392882-393048+^3:398601-398726+^3:402932-403034+^3:403227-403425+^3:404568-404639+^3:409990-410213+^3:411416-411532+^3:411667-411872+^3:412387-412510+^3:415411-415591+^3:418946-419108+^3:419738-419870+^3:422375-422448+^3:426244-430143+',
                   'bonobo ENSPPAG00000043754 3:310216-310296+^3:385204-385389+^3:391476-391582+^3:393680-393868+^3:406304-406427+^3:407422-407593+^3:408494-408542+^3:410086-410207+^3:414828-415013+^3:420116-420248+^3:425756-425897+^3:427167-427279+^3:428691-428858+^3:431426-431592+^3:438144-438269+^3:442480-442582+^3:442775-442973+^3:444114-444185+^3:449530-449753+^3:450979-451095+^3:451230-451435+^3:451950-452073+^3:454975-455155+^3:458509-458671+^3:459301-459433+^3:461929-462002+^3:465798-469715+']
        
        with TemporaryDirectory() as out_dir:
            cmd = ['python', bin_fpath, '-d', genome_data_fpath,
                   '-O', orthologs_fpath, '-db', TEST_DATA_DIR,
                   '-f', TEST_DATA_DIR, '-o', out_dir]
            check_call(cmd)
            
            fpath = join(out_dir, 'ENSG00000134121_CHL1.fasta')
            with open(fpath) as fhand:
                for seqid, _ in parse_fasta_lines(fhand):
                    assert(seqid in exp_ids)
        
        with TemporaryDirectory() as out_dir:
            cmd = ['python', bin_fpath, '-d', genome_data_fpath,
                   '-O', orthologs_fpath, '-db', TEST_DATA_DIR,
                   '-f', TEST_DATA_DIR, '-o', out_dir, '--only_cds']
            check_call(cmd)
            
        with TemporaryDirectory() as out_dir:
            cmd = ['python', bin_fpath, '-d', genome_data_fpath,
                   '-O', orthologs_fpath, '-db', TEST_DATA_DIR,
                   '-f', TEST_DATA_DIR, '-o', out_dir, '--whole_gene']
            check_call(cmd)
            fpath = join(out_dir, 'ENSG00000134121_CHL1.fasta')
            with open(fpath) as fhand:
                for seqid, seq in parse_fasta_lines(fhand):
                    assert('$' in seqid)
                    assert('{' in seq)
            
    def test_gene_set(self):
        fpath = join(TEST_DATA_DIR, 'ENSG00000134121_CHL1.fasta')
        gene_ids = {'bonobo': 'ENSPPAG00000043754',
                    'chimp': 'ENSPTRG00000014558'}
        with open(fpath) as fhand:
            gene_set = GeneSet()
            gene_set.read_fasta(fhand)
            assert(gene_set.species == ['bonobo', 'chimp'])
            assert(gene_set['bonobo'].id == gene_ids['bonobo'])
            for species, gene in gene_set:
                assert(gene.id == gene_ids[species])
    
    def test_align_gene_set(self):
        fpath = join(TEST_DATA_DIR, 'ENSG00000134121_CHL1.fasta')
        with open(fpath) as fhand:
            gene_set = GeneSet()
            gene_set.read_fasta(fhand)
            assert(len(gene_set['bonobo'].tx_seq) != len(gene_set['chimp'].tx_seq))
            gene_set.align()
            assert(gene_set['bonobo'].tx_seq != gene_set['chimp'].tx_seq)
            assert(len(gene_set['bonobo'].tx_seq) == len(gene_set['chimp'].tx_seq))
    
    def test_align_gene_set_bin(self):
        gene_set_ids_fpath = join(TEST_DATA_DIR, 'gene_set_ids.txt')
        bin_fpath = join(BIN_DIR, 'align_orthologous_transcripts.py')
        with TemporaryDirectory() as out_dir:
            cmd = ['python', bin_fpath, '-f', TEST_DATA_DIR,
                   '-g', gene_set_ids_fpath, '-o', out_dir]
            check_call(cmd)
    
    def test_calc_msa_pw_scores_gene_set(self):
        fpath = join(TEST_DATA_DIR, 'ENSG00000134121_CHL1.aln.fasta')
        with open(fpath) as fhand:
            gene_set = GeneSet()
            gene_set.read_fasta(fhand)
            assert(gene_set.exon_pos_matrix.shape == (7580, 2))
            scores = gene_set.calc_exon_alignment_scores()
            assert(scores[('bonobo-3:310216-310296+', 'chimp-3:271559-271639+')] == 80)
            assert(scores[('bonobo-3:385204-385389+', 'chimp-3:346676-346861+')] == 186)
            
            score_exons = ExonCompScorer()
            scores = gene_set.calc_exon_pairs_scores(score_exons)
            assert(scores[('bonobo-3:310216-310296+', 'chimp-3:271559-271639+')] == 1.0)
            assert(np.allclose(scores[('bonobo-3:465798-469715+', 'chimp-3:426244-430143+')], 0.99107))
            
    def test_calc_exon_orthologs_gene_set(self):
        fpath = join(TEST_DATA_DIR, 'ENSG00000134121_CHL1.aln.fasta')
        with open(fpath) as fhand:
            gene_set = GeneSet()
            gene_set.read_fasta(fhand)
            gene_set.calc_exon_alignment_scores()
            gene_set.get_exon_orthologs(min_size=2)
            assert(len(gene_set.exon_orthologs) == 27)
            with NamedTemporaryFile('w') as fhand:
                gene_set.write_exon_orthologs_header(fhand)
                gene_set.write_exon_orthologs(fhand)
    
    def test_calc_exon_orthologs_gene_set_bin(self):
        bin_fpath = join(BIN_DIR, 'get_orthologous_exons_from_MSA.py')
        species_fpath = join(TEST_DATA_DIR, 'species.aln.txt')
        gene_set_ids_fpath = join(TEST_DATA_DIR, 'gene_set_ids.aln.txt')
         
        with NamedTemporaryFile('w') as out_fhand:
            out_fpath = out_fhand.name
            cmd = ['python', bin_fpath, '-f', TEST_DATA_DIR,
                   '-g', gene_set_ids_fpath, '-s', species_fpath,
                   '-o', out_fpath, '-m', '2']
            check_call(cmd)
        
        # Test with real data
        species_fpath = join(TEST_DATA_DIR, 'species.aln2.txt')
        with NamedTemporaryFile('w') as out_fhand:
            out_fpath = out_fhand.name
            cmd = ['python', bin_fpath, '-f', join(TEST_DATA_DIR, 'aln'),
                   '-o', out_fpath, '-m', '2', '-s', species_fpath]
            check_call(cmd)
    
    def test_exon_orthologs_full_bin(self):
        orthologs_fpath = join(TEST_DATA_DIR, 'orthologs.csv')
        genome_data_fpath = join(TEST_DATA_DIR, 'genome.data.csv')
        species_fpath = join(TEST_DATA_DIR, 'species.aln.txt')
        bin_fpath = join(BIN_DIR, 'get_orthologous_exons_full.py')
        
        with NamedTemporaryFile('w') as out_fhand:
            out_fpath = out_fhand.name
            cmd = ['python', bin_fpath, '-d', genome_data_fpath,
                   '-O', orthologs_fpath, '-db', TEST_DATA_DIR,
                   '-f', TEST_DATA_DIR, '--msa', '-s', species_fpath,
                   '-o', out_fpath, '-m', '2']
            check_call(cmd)
        
        with NamedTemporaryFile('w') as out_fhand:
            out_fpath = out_fhand.name
            cmd = ['python', bin_fpath, '-d', genome_data_fpath,
                   '-O', orthologs_fpath, '-db', TEST_DATA_DIR,
                   '-f', TEST_DATA_DIR, '-s', species_fpath,
                   '-o', out_fpath, '-m', '2']
            check_call(cmd)
        
        with NamedTemporaryFile('w') as out_fhand:
            out_fpath = out_fhand.name
            cmd = ['python', bin_fpath, '-d', genome_data_fpath,
                   '-O', orthologs_fpath, '-db', TEST_DATA_DIR,
                   '-f', TEST_DATA_DIR, '-s', species_fpath, '--msa',
                   '-o', out_fpath, '-m', '2', '--only_cds', '--whole_gene']
            check_call(cmd)
    
    def test_nuc_alignment(self):
        seq1 = 'AATCGCCGTGC'
        seq2 = 'AATAAAGCTGTGC'

        score = nucleotide_alignment(seq1, seq2)
        assert(score == 26)

    def test_translate_alignment(self):
        seq1 = 'AATCGCCGTGCAATCGCCGTGC'
        seq2 = 'AACGCAATCGCCGTGC'

        score = translate_alignment(seq1, seq2)
        assert(score == 14)

    def test_scorer(self):
        seq1 = 'AGTCGCCGTCCGT'
        seq2 = 'AGTCGATGCCGTGCGT'

        score_exons = ExonCompScorer(score_function='nc', intronic_bases=2)
        score = score_exons(seq1, seq2)
        assert(score == 19)

        score_exons = ExonCompScorer(score_function='nc', intronic_bases=2,
                                     phase_w=20)
        score = score_exons(seq1, seq2)
        assert(score == 39)

        score_exons = ExonCompScorer(score_function='nc', intronic_bases=2,
                                     splice_sites_w=2)
        score = score_exons(seq1, seq2)
        assert(score == 59)

    def test_best_reciprocal_hists_matrix(self):
        pw_scores = {('e1.1', 'e2.1'): 10,
                     ('e1.1', 'e3.1'): 12,
                     ('e1.1', 'e2.2'): 1,
                     ('e1.1', 'e3.2'): 2,
                     ('e1.1', 'e2.3'): 0,
                     ('e1.1', 'e3.3'): 0,
                     ('e1.1', 'e2.4'): 2,
                     ('e1.1', 'e3.4'): 1,

                     ('e1.2', 'e2.1'): 0,
                     ('e1.2', 'e3.1'): 1,
                     ('e1.2', 'e2.2'): 7,
                     ('e1.2', 'e3.2'): 8,
                     ('e1.2', 'e2.3'): 0,
                     ('e1.2', 'e3.3'): 0,
                     ('e1.2', 'e2.4'): 2,
                     ('e1.2', 'e3.4'): 1,

                     ('e1.3', 'e2.1'): 2,
                     ('e1.3', 'e3.1'): 1,
                     ('e1.3', 'e2.2'): 2,
                     ('e1.3', 'e3.2'): 3,
                     ('e1.3', 'e2.3'): 10,
                     ('e1.3', 'e3.3'): 9,
                     ('e1.3', 'e2.4'): 0,
                     ('e1.3', 'e3.4'): 3,

                     ('e2.1', 'e3.1'): 12,
                     ('e2.1', 'e3.2'): 2,
                     ('e2.1', 'e3.3'): 0,
                     ('e2.1', 'e3.4'): 1,

                     ('e2.2', 'e3.1'): 1,
                     ('e2.2', 'e3.2'): 8,
                     ('e2.2', 'e3.3'): 0,
                     ('e2.2', 'e3.4'): 1,

                     ('e2.3', 'e3.1'): 1,
                     ('e2.3', 'e3.2'): 3,
                     ('e2.3', 'e3.3'): 9,
                     ('e2.3', 'e3.4'): 3,
                     }
        pw2 = {}
        for (e1, e2), value in pw_scores.items():
            pw2[(e2, e1)] = value
        pw_scores.update(pw2)

        gene_set = GeneSet()
        gene_set._exon_sets = {'sp1': ['e1.1', 'e1.2', 'e1.3'],
                               'sp2': ['e2.1', 'e2.2', 'e2.3'],
                               'sp3': ['e3.1', 'e3.2', 'e3.3']}
        m = gene_set.get_best_reciprocal_hits_matrix(pw_scores)
        expected = [[0, 0, 0, 1, 0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 1, 0, 0, 1, 0],
                    [0, 0, 0, 0, 0, 1, 0, 0, 1],
                    [1, 0, 0, 0, 0, 0, 1, 0, 0],
                    [0, 1, 0, 0, 0, 0, 0, 1, 0],
                    [0, 0, 1, 0, 0, 0, 0, 0, 1],
                    [1, 0, 0, 1, 0, 0, 0, 0, 0],
                    [0, 1, 0, 0, 1, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 1, 0, 0, 0]]
        assert(np.allclose(np.array(m.todense()), expected))

    def test_ortho_MCL_from_matrix(self):
        gene_set = GeneSet()
        gene_set._exon_sets = {'sp1': ['e1.1', 'e1.2', 'e1.3'],
                               'sp2': ['e2.1', 'e2.2', 'e2.3'],
                               'sp3': ['e3.1', 'e3.2', 'e3.3']}
        m = [[0, 0, 0, 1, 0, 0, 1, 0, 0],
             [0, 0, 0, 0, 1, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 1, 0, 0, 1],
             [1, 0, 0, 0, 0, 0, 1, 0, 0],
             [0, 1, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 1, 0, 0, 0, 0, 0, 1],
             [1, 0, 0, 1, 0, 0, 0, 0, 0],
             [0, 1, 0, 0, 1, 0, 0, 0, 0],
             [0, 0, 1, 0, 0, 1, 0, 0, 0]]
        expected = set([('e1.1', 'e2.1', 'e3.1'),
                        ('e1.2', 'e2.2', 'e3.2'),
                        ('e1.3', 'e2.3', 'e3.3')])
        df = pd.DataFrame(m, columns=gene_set.exon_ids, index=gene_set.exon_ids)
        graph = nx.from_pandas_adjacency(df)
        m = nx.to_scipy_sparse_matrix(graph, gene_set.exon_ids)

        # Without clique
        sets = gene_set.markov_clustering(m, min_size=2, complete_subgraph=False)
        sets = set([tuple(sorted(x)) for x in sets])
        assert(sets == expected)

        # With clique
        sets = gene_set.markov_clustering(m, min_size=2, complete_subgraph=True)
        sets = set([tuple(sorted(x)) for x in sets])
        assert(sets == expected)
        
        # With empty matrix
        m = None
        sets = gene_set.markov_clustering(m, min_size=2, complete_subgraph=True)
        assert(sets == [])
        

    def test_ortho_MCL(self):
        lines = ['>sp1 g1 1:1-10+^1:20-30+^1:50-60+^1:80-100+\n',
                 'AATCGCCGCGC|ATCGTGA|GGCTCCACC|CCCGAGAC\n',
                 '>sp2 g2 1:1-10+^1:20-30+^1:50-60+\n',
                 'CGCCGCGC|ATCGCGA|CCCGAGAC',
                 '>sp3 g3 1:1-10+^1:20-30+^1:50-60+^1:80-100+\n',
                 'AATAAACGCGC|ATCGTCA|GGCTCCACC|CCCGAGAC',
                 '>sp4 g4 1:1-10+^1:20-30+^1:50-60+\n',
                 'AATCACCACGC|GGCTCCACC|CTCGTGAC']
        gene_set = GeneSet(gene_set_id='geneX')
        gene_set.read_fasta(lines)
        score_exons = ExonCompScorer()
        gene_set.calc_exon_pairs_scores(score_exons)
        gene_set.get_exon_orthologs(min_size=2)
        exon_sets = gene_set.exon_orthologs

        # Unable to capture all relationships
        expected_sets = [['1:1-10+', '1:1-10+', '1:1-10+'],
                         ['1:20-30+', '1:20-30+', '1:20-30+'],
                         ['1:50-60+', '1:50-60+', '1:20-30+'],
                         ['1:50-60+', '1:80-100+', '1:80-100+']]
        for exon_set in exon_sets.values():
            exon_ids = [exon.id for exon in exon_set.values()]
            assert(exon_ids in expected_sets)

    def test_align_transcripts(self):
        # Transcripts with small changes
        transcripts = ['GAAAGTGAAA|AAAAACGATG|TACCGATGAC|CGGGATGCCC',
                       'AGTGAAA|AAAGACGATG|TACCAAT|CCGGATGCTC',
                       'GCAACTGAAA|AAAAACG|TACCGAAGAC|AGGGATGCCC',
                       'AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC']
        msa = run_msa(transcripts)
        expected = ['GAAAGTGAAA|AAAAACGATG|TACCGATGAC|CGGGATGCCC',
                    '---AGTGAAA|AAAGACGATG|TACCAAT---|CCGGATGCTC',
                    'GCAACTGAAA|AAAAAC---G|TACCGAAGAC|AGGGATGCCC',
                    '---AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC--']
        assert(msa == expected)

        # With missing a exon
        transcripts = ['GAAAGTGAAA|AAAAACGATG|TACCGATGAC|CGGGATGCCC',
                       'AGTGAAA|AAAGACGATG|CCGGATGCTC',
                       'GCAACTGAAA|AAAAACG|TACCGAAGAC|AGGGATGCCC',
                       'AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC']
        msa = run_msa(transcripts)
        expected = ['GAAAGTGAAA|AAAAACGATG|TACCGATGAC|CGGGATGCCC',
                    '---AGTGAAA|AAAGACGATG|--CC---------GGATGCTC',
                    'GCAACTGAAA|AAAAAC---G|TACCGAAGAC|AGGGATGCCC',
                    '---AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC--']
        assert(msa == expected)

        # With Ns in the sequence
        transcripts = ['GAAAGTGAAA|ANNNACGATG|TACCGATGAC|CGGGATGCCC',
                       'AGTGAAA|AAAGACGATG|CCGGATGCTC',
                       'GCAACTGAAA|AAAAACG|TACCGAAGAC|AGGGATGCCC',
                       'AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC']
        msa = run_msa(transcripts)
        expected = ['GAAAGTGAAA|ANNNACGATG|TACCGATGAC|CGGGATGCCC',
                    '---AGTGAAA|AAAGACGATG|--CC---------GGATGCTC',
                    'GCAACTGAAA|AAAAAC---G|TACCGAAGAC|AGGGATGCCC',
                    '---AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC--']
        assert(msa == expected)

        # Use muscle
        transcripts = ['GAAAGTGAAA|ANNNACGATG|TACCGATGAC|CGGGATGCCC',
                       'AGTGAAA|AAAGACGATG|CCGGATGCTC',
                       'GCAACTGAAA|AAAAACG|TACCGAAGAC|AGGGATGCCC',
                       'AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC']
        msa = run_msa(transcripts, method='muscle')
        expected = ['GAAAGTGAAA|ANNNACGATG|TACCGATGAC|CGGGATGCCC',
                    '---AGTGAAA|AAAGACGATG|--CC---------GGATGCTC',
                    'GCAACTGAAA|AAAAAC---G|TACCGAAGAC|AGGGATGCCC',
                    '---AGTGAAT|AAAAACGATG|TACCGATGAC|CGGGATGC--']
        assert(msa == expected)
        
    def test_align_splice_gene(self):
        # With alternative splice sites
        transcripts = ['GAAAGTGAAA|AAA{AACGATG|TACCGAT}GAC|CGGGATGCCC',
                       'AGTGAAA|AAA{GACGATG|TACCAAT|CCGGATGCTC',
                       'GCAACTGAAA|AAA{AACG|TACCGAA}GAC|AGGGATGCCC',
                       'AGTGAAT|AAA{AACGATG|TACCGAT}GAC|CGGGATGC']
        msa = run_msa(transcripts)
        expected = ['GAAAGTGAAA|AAA{AACGATG|TACCGAT}GAC|CGGGATGCCC',
                    '---AGTGAAA|AAA{GACGATG|TACCAAT----|CCGGATGCTC',
                    'GCAACTGAAA|AAA{AAC---G|TACCGAA}GAC|AGGGATGCCC',
                    '---AGTGAAT|AAA{AACGATG|TACCGAT}GAC|CGGGATGC--']
        assert(msa == expected)

    def test_basic_stats(self):
        np.random.seed(0)
        df = get_orthologs_df(n_species=4)
        orthologs = OrthologousExons()
        orthologs.load_exons(df)

        sizes = orthologs.calc_exon_sets_sizes()
        assert(np.all(sizes == 4))

        exons_per_gene = orthologs.calc_n_exons_per_genes()
        assert(np.all(exons_per_gene == [5, 6]))
        
        orthologs.n_exons_ref = {1: 6, 2: 7}
        m = orthologs.calc_n_exons_table()
        assert(np.all(m['p_ref'] == [5/6., 6/7.]))
        
        exons_per_sp = orthologs.calc_n_exons_per_species()
        for sp in orthologs.species:
            assert(np.all(exons_per_sp[sp] == [5, 6]))
    
    def test_character_matrix(self):
        df = get_orthologs_df(n_species=4)
        orthologs = OrthologousExons()
        orthologs.load_exons(df)
        orthologs.filter_n_exons_per_set(min_exons=2)
        m = orthologs.calc_character_matrix()
          
        # Plain character full of exon presences
        for seq in m.sequences():
            assert(seq.symbols_as_string() == '11111111111')
        
        # Add missing exons
        np.random.seed(0)
        df[np.random.uniform(size=df.shape) < 0.1]  = np.nan
        orthologs = OrthologousExons()
        orthologs.load_exons(df)
        orthologs.filter_n_exons_per_set(min_exons=2)
        assert(orthologs.n_exons == 10)
        expected = ['1111110111', '1111111111', '1111111111', '1111011111']
        m = orthologs.calc_character_matrix()
        for seq, exp in zip(m.sequences(), expected):
            assert(seq.symbols_as_string() == exp)
             
        # A whole missing gene across a species
        df = get_orthologs_df(n_species=4)
        df.iloc[:5, 1]  = np.nan
        orthologs = OrthologousExons()
        orthologs.load_exons(df)
        orthologs.filter_n_exons_per_set(min_exons=2)
        assert(orthologs.n_exons == 11)
        expected = ['?????111111', '11111111111', '11111111111', '11111111111']
        m = orthologs.calc_character_matrix()
        for seq, exp in zip(m.sequences(), expected):
            assert(seq.symbols_as_string() == exp)   
         
        # Missing a fragment of a gene in a given species
        df.iloc[5:7, 2]  = np.nan
        orthologs = OrthologousExons()
        orthologs.load_exons(df)
        orthologs.filter_n_exons_per_set(min_exons=2)
        assert(orthologs.n_exons == 11)
        
        expected = ['?????111111', '11111??1111', '11111111111', '11111111111']
        m = orthologs.calc_character_matrix()
        for seq, exp in zip(m.sequences(), expected):
            assert(seq.symbols_as_string() == exp)
     
    def test_exon_phylogeny(self):
        # Create tree
        tree_str = '(((homo_sapiens:76.0,mus_musculus:76.0)Inner5:13.0,'
        tree_str += 'bos_taurus:89.0)Inner4:84.0,monodelphis_domestica:173.0)'
        tree_str += 'Inner2:427.0;'
        tree = dendropy.Tree.get_from_string(tree_str, schema='newick',
                                             preserve_underscores=True)
        expected_tree = dendropy2ete(tree)
        orthologs = OrthologousExons()
        orthologs.simulate(tree, n_genes=500, exonization=0.001,
                           stablishment=0.001, pseudo_loss=0.0002,
                           loss=0.0001, exons_p=0.95, exons_per_gene_mean=5,
                           seed=0)
        orthologs.filter_variable_sets()
        
        # Neighbor joining
        inferred_tree = dendropy2ete(orthologs.calc_phylogeny_exons(method='nj'))
        inferred_tree.set_outgroup('monodelphis_domestica')
        assert(inferred_tree.robinson_foulds(expected_tree)[0] == 0)

        # RAXml
        inferred_tree = dendropy2ete(orthologs.calc_phylogeny_exons(method='raxml'))
        inferred_tree.set_outgroup('monodelphis_domestica')
        assert(inferred_tree.robinson_foulds(expected_tree)[0] == 0)

        # With some missing genes
        orthologs = OrthologousExons()
        orthologs.simulate(tree, n_genes=500, exonization=0.001,
                           stablishment=0.001, pseudo_loss=0.0002,
                           loss=0.0001, exons_p=0.95, exons_per_gene_mean=5,
                           seed=0, missing_gene_p=0.02)
        orthologs.filter_variable_sets()
        
        inferred_tree = dendropy2ete(orthologs.calc_phylogeny_exons(method='nj'))
        inferred_tree.set_outgroup('monodelphis_domestica')
        assert(inferred_tree.robinson_foulds(expected_tree)[0] == 0)

        inferred_tree = dendropy2ete(orthologs.calc_phylogeny_exons(method='raxml'))
        inferred_tree.set_outgroup('monodelphis_domestica')
        assert(inferred_tree.robinson_foulds(expected_tree)[0] == 0)
    
    def test_sort_exons(self):
        g = MetaExonGraph()
        exons = ['sp1-1:20-30+', 'sp1-1:1-10+', 'sp1-1:70-90+']
        s = ['sp1-1:1-10+', 'sp1-1:20-30+', 'sp1-1:70-90+']
        assert(g.sort_exons(exons) == s)
        
        exons = ['sp1-1:20-30-', 'sp1-1:1-10-', 'sp1-1:70-90-']
        s = ['sp1-1:70-90-', 'sp1-1:20-30-', 'sp1-1:1-10-']
        assert(g.sort_exons(exons) == s)
        
        exons = ['sp1-1:20-30-', 'sp1-1:1-10+', 'sp1-1:70-90-']
        try:
            g.sort_exons(exons)
            self.fail()
        except ValueError:
            pass
    
    def test_build_exon_graph(self):
        np.random.seed(0)
        exons_df = get_orthologs_df()
        exons_df = exons_df.loc[exons_df['gene_set_id'] == 1, :]
        g = MetaExonGraph()
        g.load_exons(exons_df)
        
        expected = {'1.1': {'1.3': 4}, '1.2': {'1.1': 4},
                    '1.3': {'1.4': 4}, '1.4': {'1.5': 4}}
        
        for node, children in g.nodes_children():
            for node2, c in children.items():
                assert(c == expected[node][node2])
    
    def test_counts_main_path(self):
        np.random.seed(2)
        exons_df = get_orthologs_df()
        exons_df = exons_df.loc[exons_df['gene_set_id'] == 1, :]
        g = MetaExonGraph()
        g.load_exons(exons_df)
        k, n = g.calc_counts_on_main_path()
        assert(n == 16)
        assert(k == 14)
        
    def test_graph_edge_entropy(self):
        np.random.seed(2)
        exons_df = get_orthologs_df()
        exons_df = exons_df.loc[exons_df['gene_set_id'] == 1, :]
        g = MetaExonGraph()
        g.load_exons(exons_df)
        entropy = g.calc_graph_edge_entropy(estimator='empirical')
        assert(np.allclose(entropy, 0.4056390622295664))
        
    def test_gene_contiguity_rel_H(self):
        np.random.seed(2)
        exons_df = get_orthologs_df()
        exons_df = exons_df.loc[exons_df['gene_set_id'] == 1, :]
        g = MetaExonGraph()
        g.load_exons(exons_df)
        entropy = g.calc_gene_contiguity_relative_H()
        assert(np.allclose(entropy, 0.24798590209455812))
    
    def test_calc_sets_sizes(self):
        exons_fpath = join(TEST_DATA_DIR, 'exon_orthologs.csv')
        exons_df = pd.read_csv(exons_fpath, index_col=0)
        orthologs = OrthologousExons()
        orthologs.load_exons(exons_df)
        sets_sizes = orthologs.calc_sets_size_table()
        assert(np.all(sets_sizes['exons'] == 4))
        assert(np.all(sets_sizes['p'] == 1))
    
    def test_contiguity_metrics(self):
        exons_fpath = join(TEST_DATA_DIR, 'exon_orthologs.csv')
        exons_df = pd.read_csv(exons_fpath, index_col=0)
        orthologs = OrthologousExons()
        orthologs.load_exons(exons_df)
        metrics = orthologs.calc_contiguity_metrics()
        assert(np.all(metrics['main_path_counts'] == [14, 19]))
        assert(np.all(metrics['total_counts'] == [16, 20]))
        assert(np.allclose(metrics['relative_entropy'], [0.247986, 0.181238]))
        assert(np.allclose(metrics['edge_entropy'], [0.468139, 0.156046]))
    
    def test_calc_metrics(self):
        exons_fpath = join(TEST_DATA_DIR, 'exon_orthologs.csv')
        exons_df = pd.read_csv(exons_fpath, index_col=0)
        orthologs = OrthologousExons()
        orthologs.load_exons(exons_df)
        metrics = orthologs.calc_metrics()
        
        assert(metrics['Median gene set size'] == 4)
        assert(metrics['Median exon set size'] == 4)
        assert(metrics['Median set size ratio'] == 1)
        assert(metrics['Proportion of edges on main path'] == 33/36.)
        
        with NamedTemporaryFile('w') as fhand:
            orthologs.write_metrics(fhand.name)
            orthologs.plot_metrics('{}.png'.format(fhand.name))
    
    def test_binary(self):
        exons_fpath = join(TEST_DATA_DIR, 'exon_orthologs.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.nh')
        bin_fpath = join(BIN_DIR, 'evaluate_orthologs.py')

        with NamedTemporaryFile('w') as fhand:        
            cmd = ['python', bin_fpath, '-e', exons_fpath, '-s', tree_fpath, 
                   '-m', 'nj', '-o', fhand.name]
            check_call(cmd)
    
        exons_fpath = join(TEST_DATA_DIR, 'exon_orthologs.medium.csv')
        tree_fpath = join(TEST_DATA_DIR, 'tree.medium.nh')
        
        with NamedTemporaryFile('w') as fhand:
            cmd = ['python', bin_fpath, '-e', exons_fpath, '-s', tree_fpath, 
                   '-m', 'nj', '-o', fhand.name]
            check_call(cmd)


if __name__ == '__main__':
    import sys
    sys.argv = ['', 'OrthologsTests']
    unittest.main()
