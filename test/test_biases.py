#!/usr/bin/env python
from os.path import join
from subprocess import check_call
import unittest

import numpy as np
import pandas as pd

from AS_quant.settings import TEST_DATA_DIR, BIN_DIR
from AS_quant.bias import (calc_sj_n_positions, calc_exon_n_positions,
                           calc_sj_n_fragments, calc_exon_n_fragments,
                           calc_eff_inc_len, calc_eff_skp_len,
                           calc_eff_len_table, get_meta_fragments,
                           get_transcript_fragment_seq, calc_gc, get_sj_overlap,
                           calc_fragments_gc_new, get_fragment_lengths)


class BiasTests(unittest.TestCase):
    def test_meta_fragment(self):
        upstream = [[0] * 10]
        exon = [1] * 5
        downstream = [[0] * 10]
        
        max_frag_len = 6
        overhang = 1
        
        inc_real = [0] * 5 + [1] * 5 + [0] * 5
        skp_real = [0] * 10
        inc, skp, _, _ = get_meta_fragments(upstream, exon, downstream,
                                                  max_frag_len, overhang,
                                                  def_seq=[])
        assert(np.all(inc == inc_real))
        assert(np.all(skp == skp_real))
    
    def test_sj_overlap_vector(self):
        meta_fragment_len = 15
        
        # Read length = 2
        solution = np.array([1, 0, 0, 0, 1,
                             1, 0, 0, 0, 1,
                             0, 0, 0, 0, 0])
        overlaps = get_sj_overlap(meta_fragment_len, [5, 10],
                                  fragment_len=6, read_len=2, overhang=1)
        assert(np.all(solution == overlaps))
         
        # Read length = 3
        solution = np.array([1, 1, 0, 1, 1,
                             1, 1, 0, 1, 1,
                             0, 0, 0, 0, 0])
        overlaps = get_sj_overlap(meta_fragment_len, [5, 10],
                                  fragment_len=6, read_len=3, overhang=1)
        assert(np.all(solution == overlaps))
        
        # Short first exon: can not generate some fragments
        meta_fragment_len = 30
        solution = np.array([0] + [1] * 4 + [0] * 3 + [1] * 2 + [0] * 20)
        overlaps = get_sj_overlap(meta_fragment_len, [5, 10],
                                  fragment_len=10, read_len=3, overhang=1)
        assert(np.all(solution == overlaps))
        
        # Short last exon: can not generate some fragments
        meta_fragment_len = 30
        solution = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 1, 1, 0, 0, 0, 1, 1, 1, 1,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,]
        solution = np.array(solution)
        overlaps = get_sj_overlap(meta_fragment_len, [20, 25],
                                  fragment_len=10, read_len=3, overhang=1)
        assert(np.all(solution == overlaps))
        
    def test_get_fragment_seq(self):
        exons = [[0] * 5, [1] * 5, [2] * 5]
        
        result = get_transcript_fragment_seq(exons, length=12, seq=[])
        assert(np.all(result == np.array([0] * 5 + [1] * 5 + [2] * 2)))
         
        result = get_transcript_fragment_seq(exons, length=12, go_upstream=True,
                                             seq=[])
        assert(np.all(result == np.array([0] * 2 + [1] * 5 + [2] * 5)))
        
        result = get_transcript_fragment_seq(exons, length=32, seq=[])
        assert(np.all(result == np.array([0] * 5 + [1] * 5 + [2] * 5)))
        
        result = get_transcript_fragment_seq(exons, length=32, go_upstream=True,
                                             seq=[])
        assert(np.all(result == np.array([0] * 5 + [1] * 5 + [2] * 5)))
    
    def test_calc_fragments_gc(self):
        seq = ['ACCGGCATGCTGCGGCC']
        sj_overlap = np.full(len(seq), fill_value=True)
        fragment_len = 10
        
        real_gc = [calc_gc(seq[i:i+fragment_len])
                   for i in range(len(seq) - fragment_len)]
        gc = calc_fragments_gc_new(seq, sj_overlap, fragment_len)
        assert(np.allclose(gc, real_gc))
    
    def test_n_mappable_positions_SJ(self):
        L = 100
        R = 8
        assert(calc_sj_n_positions(L, R, 1000, 2000) == 84)
        assert(calc_sj_n_positions(L, R, 50, 2000) == 42)
        
        d_start = np.array([1000, 50])
        d_end = np.array([2000, 2000])
        sj_n = calc_sj_n_positions(L, R, d_start, d_end)
        assert(np.allclose(sj_n, [84, 42]))
    
    def test_n_mappable_positions_exon(self):
        L = 100
        R = 8
        assert(calc_exon_n_positions(L, R, 1000, 2000, 200) == 168)
        assert(calc_exon_n_positions(L, R, 1000, 2000, 50) == 126)
        assert(calc_exon_n_positions(L, R, 50, 2000, 50) == 84)
        
        d_start = np.array([1000, 1000, 50])
        d_end = np.array([2000, 2000, 2000])
        exon_length = np.array([200, 50, 50])
        exon_n = calc_exon_n_positions(L, R, d_start, d_end, exon_length)
        assert(np.allclose(exon_n, [168, 126, 84]))
    
    def test_n_fragments_sj(self):
        L = 100
        R = 8
        assert(calc_sj_n_fragments(L, R, 1050, 2000, 150) == 168)
        assert(calc_sj_n_fragments(L, R, 100, 2000, 150) == 126)
        assert(calc_sj_n_fragments(L, R, 100, 100, 150) == 84)
        assert(calc_sj_n_fragments(L, R, 100, 150, 150) == 126)
        
        d_start = np.array([1050, 100, 100, 100])
        d_end = np.array([2000, 2000, 100, 150])
        fragment_size = 150
        sj_n = calc_sj_n_fragments(L, R, d_start, d_end, fragment_size)
        assert(np.allclose(sj_n, [168, 126, 84, 126]))
    
    def test_n_fragments_exon(self):
        L = 100
        R = 8
        assert(calc_exon_n_fragments(L, R, 1000, 2000, 200, 300) == 336)
        assert(calc_exon_n_fragments(L, R, 100, 2000, 150, 50) == 210)
        assert(calc_exon_n_fragments(L, R, 100, 100, 150, 50) == 210)
        assert(calc_exon_n_fragments(L, R, 50, 100, 150, 50) == 126)
        assert(calc_exon_n_fragments(L, R, 100, 50, 150, 50) == 126)
        
        d_start = np.array([1000, 100, 100, 50, 100])
        d_end = np.array([2000, 2000, 100, 100, 50])
        fragment_size = np.array([200, 150, 150, 150, 150])
        exon_length = np.array([300, 50, 50, 50, 50])
        exons_n = calc_exon_n_fragments(L, R, d_start, d_end, fragment_size,
                                     exon_length)
        assert(np.allclose(exons_n, [336, 210, 210, 126, 126]))
    
    def test_calc_eff_inc_len(self):
        frag_len, p = get_fragment_lengths(150, 25, 100)
        L = 100
        R = 8
        
        # Long transcript and exon: 2 times more inc than skp SJ reads
        inc_eff_len = calc_eff_inc_len(L, R, d_start=1000, exon_length=500,
                                       d_end=1000, fragment_size=frag_len,
                                       fragment_size_p=p)
        skp_eff_len = calc_eff_skp_len(L, R, d_start=1000, d_end=1000,
                                       fragment_size=frag_len,
                                       fragment_size_p=p)
        assert(np.allclose(inc_eff_len / skp_eff_len, 2))
        
        # Short transcript
        inc_eff_len = calc_eff_inc_len(L, R, d_start=100, exon_length=50,
                                       d_end=100, fragment_size=frag_len,
                                       fragment_size_p=p)
        assert(np.allclose(inc_eff_len, 195.11112223402517))
        
        skp_eff_len = calc_eff_skp_len(L, R, d_start=100, d_end=100,
                                       fragment_size=frag_len,
                                       fragment_size_p=p)
        assert(np.allclose(skp_eff_len, 84.58914773501458))
        
        # Short exon in long transcript without taking into account exon length
        inc_eff_len = calc_eff_inc_len(L, R, d_start=1000, exon_length=50,
                                       d_end=1000, fragment_size=frag_len,
                                       fragment_size_p=p, no_exon_length=True)
        skp_eff_len = calc_eff_skp_len(L, R, d_start=1000, d_end=1000,
                                       fragment_size=frag_len,
                                       fragment_size_p=p)
        assert(np.allclose(inc_eff_len / skp_eff_len, 2))
        
        # Vectorized version
        d_start = np.array([1000, 100])
        d_end = np.array([1000, 100])
        exon_length = np.array([500, 50])
        
        inc_eff_len = calc_eff_inc_len(L, R, d_start=d_start,
                                       exon_length=exon_length,
                                       d_end=d_end, fragment_size=frag_len,
                                       fragment_size_p=p)
        skp_eff_len = calc_eff_skp_len(L, R, d_start=d_start, d_end=d_end,
                                       fragment_size=frag_len,
                                       fragment_size_p=p)
        assert(np.allclose((inc_eff_len / skp_eff_len)[0], 2))
        assert(np.allclose(inc_eff_len[1], 195.11112223402517))
        assert(np.allclose(skp_eff_len[1], 84.58914773501458))
    
    def test_eff_len_table(self):
        frag_len, p = get_fragment_lengths(150, 25, 100)
        fpath = join(TEST_DATA_DIR, 'exon_distances.csv')
        exon_dist = pd.read_csv(fpath)
        df = calc_eff_len_table(exon_dist, read_len=76, overhang=8,
                                fragment_size=frag_len, fragment_size_p=p)
        assert(df.shape[0] == 298)
    
    def test_calc_frag_size_bias_bin(self):
        exons_fpath = join(TEST_DATA_DIR, 'exon_distances.csv')
        bin_fpath = join(BIN_DIR, 'calc_frag_size_bias.py')
        out_fpath = join(TEST_DATA_DIR,'exon_eff_len.csv')
        cmd = ['python', bin_fpath, exons_fpath, '-r', '76', '-O', '8', 
               '-o', out_fpath, '-f', '150', '-fs', '25']
        check_call(cmd)
        
        fragments_fpath = join(TEST_DATA_DIR, 'fragment_freq.txt')
        cmd = ['python', bin_fpath, exons_fpath, '-r', '76', '-O', '8', 
               '-o', out_fpath, '-F', fragments_fpath]
        check_call(cmd)


if __name__ == '__main__':
    import sys;sys.argv = ['', 'BiasTests']
    unittest.main()
