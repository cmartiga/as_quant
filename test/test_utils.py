#!/usr/bin/env python
import unittest
from tempfile import NamedTemporaryFile

import numpy as np
import pandas as pd

from AS_quant.utils import parse_fasta_lines, get_ncbi_taxonomy_tree
from AS_quant.bin_utils import read_log_bias


class UtilsTests(unittest.TestCase):
    def test_nuc_alignment(self):
        lines = ['>seq1', 'AATCGCCGTGC', 'AGGCCC', '>seq2', '', 
                 'AGGGACACACCCG', 'AATGCC']
        parsed_fasta = [('seq1', 'AATCGCCGTGCAGGCCC'),
                        ('seq2', 'AGGGACACACCCGAATGCC')]
        for parsed, real in zip(parse_fasta_lines(lines), parsed_fasta):
            assert(parsed == real)
    
    def test_ncbi_taxonomy(self):
        species = ['mus_musculus', 'mus_spretus', 'homo_sapiens',
                   'pan_paniscus', 'papio_anubis']
        tree = get_ncbi_taxonomy_tree(species)
        real = '(((homo_sapiens:1,pan_paniscus:1)1:1,papio_anubis:1)1:1,'
        real += '(mus_musculus:1,mus_spretus:1)1:1);'
        assert(tree.write() == real)
    
    def test_read_log_bias(self):
        exon_ids = np.array(['e1', 'e2', 'e3', 'e4'])
        sample_ids = np.array(['s1', 's2'])
        log_bias = pd.DataFrame({'log_bias': [0, 0, 0, 0]},
                                index=exon_ids)
        
        with NamedTemporaryFile('w') as fhand:
            fpath = fhand.name
            log_bias.to_csv(fpath)
            
            log_bias = read_log_bias(fpath, exon_ids, sample_ids)
            assert(log_bias.shape == (4, 2))
        
        log_bias = read_log_bias(None, exon_ids, sample_ids)
        assert(log_bias.shape == (4, 2))
        assert(np.allclose(log_bias, np.log(2)))


if __name__ == '__main__':
    import sys;sys.argv = ['', 'UtilsTests.test_read_log_bias']
    unittest.main()

