import unittest
from os.path import join

import numpy as np

from AS_quant.models.basic import get_backend_model, _BayesianModel
from AS_quant.settings import STAN_CODE_DIR


class ModelTests(unittest.TestCase):
    def test_model(self):
        fpath = join(STAN_CODE_DIR, 'psi.stan')
        k = 10
        x = np.random.normal(0, 1, size=k)
        p = np.exp(x) / (1+np.exp(x))
        data = {'K': k, 
                'log_map_ratio': np.random.normal(0.69, 0.1, size=k),
                'inclusion': np.random.binomial(30, p),
                'total': [30] * k}
        m = get_backend_model()(fpath, params=['psi'])
        m.fit(data)
        df = m.get_posterior_df()
        for colname in df.columns:
            assert(colname.startswith('psi'))
    
    def test_bayesian_model(self):
        k = 10
        x = np.random.normal(0, 1, size=k)
        p = np.exp(x) / (1+np.exp(x))
        data = {'K': k, 
                'log_map_ratio': np.random.normal(0.69, 0.1, size=k),
                'inclusion': np.random.binomial(30, p),
                'total': [30] * k}
        m = _BayesianModel()
        m.recompile = False
        m.model_label = 'psi'
        m.by_element = False
        m.get_stan_data = lambda : data
        m.fit()


if __name__ == '__main__':
    import sys
    sys.argv = ['', 'ModelTests']
    unittest.main()
