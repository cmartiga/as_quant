#!/usr/bin/env python
import unittest
import numpy as np
import pandas as pd
from AS_simulation.psi import (exon_psi_to_transcripts_psi,
                               transcript_psi_to_exon_psi, simulate_exon_psi)
from AS_quant.obs_models import ObservationsAS
from AS_simulation.utils import logit


class SimulationsTests(unittest.TestCase):
    def test_simulate_psi(self):
        np.random.seed(0)
        psi = simulate_exon_psi(0, 1, 0.2, 20)
        assert(psi.shape[0] == 20)
        assert(np.sum(psi != 1).sum() == 3)
    
    def test_simulate_AS_data(self):
        X = pd.DataFrame({'s1': logit(simulate_exon_psi(0, 1, 1, 20))})
        m = ObservationsAS()
        m.simulate(X, mean_coverage=20)
        assert(m.inclusion.shape == (1, 20))
        assert(m.total.shape == (1, 20))
        
    def test_exon_psi_to_transcript_psi(self):
        # Simple case with few exon skipping events 
        exon_psi = np.array([1, 0.6, 1, 1, 0.5, 1])
        transcripts, psis = exon_psi_to_transcripts_psi(exon_psi, max_skipped=3)
         
        expected_psis = [0.3, 0.2, 0.3, 0.2]
        assert(np.allclose(psis, expected_psis))
         
        derived_exon_psi = transcript_psi_to_exon_psi(transcripts, psis)
        assert(np.allclose(exon_psi, derived_exon_psi))
        
        # More complex case with more skipping events
        exon_psi = np.array([1, 0.6, 0.9, 0.9, 0.9, 0.5, 1])
        transcripts, psis = exon_psi_to_transcripts_psi(exon_psi, max_skipped=6)
        derived_exon_psi = transcript_psi_to_exon_psi(transcripts, psis)
        assert(psis.sum() == 1)
        assert(np.allclose(exon_psi - derived_exon_psi, 0, atol=1e-3))
    

if __name__ == '__main__':
    import sys;sys.argv = ['', 'SimulationsTests']
    unittest.main()
